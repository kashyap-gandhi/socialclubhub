<?php
/**
 * Template Name: Import Page
 */


get_header(); ?>


<?php
global $wpdb;



/************************ YOUR DATABASE CONNECTION END HERE  ****************************/



include 'Classes/PHPExcel/IOFactory.php';

// This is the file path to be uploaded.
$sheet_name='SCH_user_format13';
$inputFileName = 'C:/xampp/htdocs/socialclubhub/wp-content/themes/socialclubhub/'.$sheet_name.'.xlsx'; 

$msg=$sheet_name."====<br/><br/>";

try {
	$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
} catch(Exception $e) {
	die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
}


$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

//echo "<pre>"; print_r($allDataInSheet); die;
$arrayCount = count($allDataInSheet);  // Here get total count of row in that Excel sheet


for($i=1;$i<=$arrayCount;$i++){


$user_email = strtolower(trim($allDataInSheet[$i]["A"]));
$user_name = trim($allDataInSheet[$i]["B"]);

$first_name = trim($allDataInSheet[$i]["C"]);
$last_name = trim($allDataInSheet[$i]["D"]);
$middle_name = trim($allDataInSheet[$i]["E"]);
$phone_number = trim($allDataInSheet[$i]["F"]);

$birth_date = trim($allDataInSheet[$i]["G"]);

$description = trim($allDataInSheet[$i]["H"]);
$gender = trim($allDataInSheet[$i]["I"]);

$country = strtolower(trim($allDataInSheet[$i]["J"]));
$zipcode = trim($allDataInSheet[$i]["K"]);
$city = strtolower(trim($allDataInSheet[$i]["L"]));
$state = strtolower(trim($allDataInSheet[$i]["M"]));
$address = trim($allDataInSheet[$i]["N"]);

$msg.=$i."==";

if($user_email!=''){
    if(email_exists($user_email)) {
            $msg.= 'Record already exist='.$user_email.'<br/>';
    } else {
            
        $msg.= '<br/>';
        $user_pass='sch@1234';
        $new_user_id = wp_insert_user(array(
					'user_login'		=> $user_email,
					'user_pass'	 		=> $user_pass,
					'user_email'		=> $user_email,
					'first_name'		=> $first_name,
					'last_name'			=> $last_name,
					'user_registered'	=> date('Y-m-d H:i:s'),
					'role'				=> 'customer',
                           
				)
			);
			if($new_user_id) {
			
                            
                            mysql_query("update ".$wpdb->prefix."users set user_status=1 where ID=".$new_user_id);
                            
                            
                            
                                $user_description = $description;
                                update_user_meta($new_user_id, 'description', esc_textarea($user_description));



                                $user_phone_number = $phone_number;
                                update_user_meta($new_user_id, 'user_phone_number', esc_attr($user_phone_number));


                                $user_gender = (int) $gender;
                                update_user_meta($new_user_id, 'user_gender', esc_attr($user_gender));

                                $user_birth_year = (int) date('Y',strtotime($birth_date));
                                update_user_meta($new_user_id, 'user_birth_year', esc_attr($user_birth_year));
            
            
            
                        
                                $user_address = $address;
                                update_user_meta($new_user_id, 'user_address', esc_attr($user_address));

                                $user_zip_code = $zipcode;
                                update_user_meta($new_user_id, 'user_zip_code', esc_attr($user_zip_code));
                                
                                
                                
                                
            $get_countries = "select * from " . $wpdb->prefix . "countries where LOWER(country) like '" . $country . "%' order by country asc limit 1";
            $res_countries = $wpdb->get_row($get_countries);


            if(!empty($res_countries)){

                $user_country_id = (int) $res_countries->countryid;
                update_user_meta($new_user_id, 'user_country_id', esc_attr($user_country_id));

            }
                
            
             $get_state="select * from " . $wpdb->prefix . "states as st where LOWER(st.region) like '".$state."%' order by st.region asc limit 1";
             $res_state = $wpdb->get_row($get_state);

                if(!empty($res_state)){
             
                                
                                $user_state_id = (int) $res_state->regionid;
                                update_user_meta($new_user_id, 'user_state_id', esc_attr($user_state_id));
                                
                }              
                                

                                
                $get_city = "select *  from " . $wpdb->prefix . "cities as ct where LOWER(ct.City) like '" . $city . "%'  order by  ct.City asc limit 1";
                $res_city = $wpdb->get_row($get_city);

                if(!empty($res_city)){

                    $user_city_id = (int) $res_city->CityId;
                    update_user_meta($new_user_id, 'user_city_id', esc_attr($user_city_id));

                    $user_lat = $res_city->Latitude;
                    update_user_meta($new_user_id, 'user_lat', esc_attr($user_lat));

                    $user_long = $res_city->Longitude;
                    update_user_meta($new_user_id, 'user_long', esc_attr($user_long));
                }
    
       }    

        
    }
} else {
     $msg.= 'No email.'.$user_email.'<br/>';
}
}

 
echo $msg;


?>

<?php get_footer(); ?>

