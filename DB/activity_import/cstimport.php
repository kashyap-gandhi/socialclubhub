<?php
/**
 * Template Name: Import Page
 */


get_header(); ?>


<?php
global $wpdb;



/************************ YOUR DATABASE CONNECTION END HERE  ****************************/



require_once 'Classes/PHPExcel/IOFactory.php';

// This is the file path to be uploaded.
$sheet_name='SCH_club_activity';
$inputFileName = 'C:/xampp/htdocs/socialclubhub/wp-content/themes/socialclubhub/'.$sheet_name.'.xlsx'; 

$msg=$sheet_name."====<br/><br/>";

try {
	$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
} catch(Exception $e) {
	die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
}


$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

//echo "<pre>"; print_r($allDataInSheet); die;
$arrayCount = count($allDataInSheet);  // Here get total count of row in that Excel sheet



for($i=1;$i<=$arrayCount;$i++){


$club_id = (int) trim($allDataInSheet[$i]["A"]);

$activities=trim($allDataInSheet[$i]["B"]); 

$exp_act=explode(';',$activities);



$msg.=$i."==";

if($club_id>0){
    
    $post_id=$club_id;
    
    $main_term_id=0;
    $other_term_id='';
    
                      if(!empty($exp_act)){      
                          
                          $acnt=1;
                          
                          foreach ($exp_act as $key=>$act_name){       
                              
                              $act_name=htmlentities(strtolower(trim($act_name)));
                                
                                $get_term = "select * from " . $wpdb->prefix . "terms where LOWER(name) like '" . $act_name . "%' order by name asc limit 1";
                                $res_term = $wpdb->get_row($get_term);
                    
                                if(!empty($res_term)){

                                    $term_id = (int) $res_term->term_id;

                                    if($acnt==1){
                                       $main_term_id=$term_id;
                                    } else {
                                        $other_term_id.=$term_id.',';
                                    }

                                } else {
                                    $msg.='====SQL===='.$get_term.'=====SQL====<br/>';
                                   $msg.= 'No activity found== '.$post_id.'===='.$act_name.'<br/>'; 
                                }
                                $acnt++;
                                
                                
                                if($other_term_id!=''){
                                    //$other_term_id=substr($other_term_id, 0, -1);
                                }
                                
                                
                                
                          }
                          
                        //  $msg.= 'success== '.$post_id.'==='.$main_term_id.'===='.$other_term_id.'<br/>'; 
                          
                           update_post_meta($post_id, 'club_main_activity', (int) $main_term_id);
                           update_post_meta($post_id, 'club_other_activities', $other_term_id );
                
            
                      } else {
                         $msg.= 'No activity== '.$post_id.'<br/>'; 
                      }

        
    
   
    } else {
         $msg.= 'No user email== '.$user_email.'<br/>';
    }
}

 $msg.="<br/><br/>";
echo $msg;


?>

<?php get_footer(); ?>

