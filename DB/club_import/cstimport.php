<?php
/**
 * Template Name: Import Page
 */


get_header(); ?>


<?php
global $wpdb;



/************************ YOUR DATABASE CONNECTION END HERE  ****************************/



require_once 'Classes/PHPExcel/IOFactory.php';

// This is the file path to be uploaded.
$sheet_name='SCH_club_format7';
$inputFileName = 'C:/xampp/htdocs/socialclubhub/wp-content/themes/socialclubhub/'.$sheet_name.'.xlsx'; 

$msg=$sheet_name."====<br/><br/>";

try {
	$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
} catch(Exception $e) {
	die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
}


$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

//echo "<pre>"; print_r($allDataInSheet); die;
$arrayCount = count($allDataInSheet);  // Here get total count of row in that Excel sheet


$post_status='publish';
    $post_type='clubs';

for($i=1;$i<=$arrayCount;$i++){


$user_email = strtolower(trim($allDataInSheet[$i]["A"]));


$country = strtolower(trim($allDataInSheet[$i]["B"]));
$zipcode = trim($allDataInSheet[$i]["C"]);
$city = strtolower(trim($allDataInSheet[$i]["D"]));
$state = strtolower(trim($allDataInSheet[$i]["E"]));
$address = trim($allDataInSheet[$i]["F"]);

$club_name = trim($allDataInSheet[$i]["G"]);
$phone_number = trim($allDataInSheet[$i]["H"]);

$club_email = strtolower(trim($allDataInSheet[$i]["I"]));
$club_phone = trim($allDataInSheet[$i]["J"]);
$club_website = trim($allDataInSheet[$i]["K"]);

$club_description = trim($allDataInSheet[$i]["L"]);




$msg.=$i."==";

if($user_email!=''){
    
    $get_user=mysql_query("select * from ".$wpdb->prefix."users where LOWER(user_email)='".$user_email."'");
    
    if(mysql_num_rows($get_user)>0){
        
        $get_user_detail=mysql_fetch_array($get_user);
        
        $user_id=$get_user_detail['ID'];
        
        $user_first_name = esc_attr(get_the_author_meta('first_name', $user_id));
        $user_last_name = esc_attr(get_the_author_meta('last_name', $user_id));
        $user_phone_number = esc_attr(get_the_author_meta('user_phone_number', $user_id));
        
        
        $club_name = esc_attr($club_name);
        $club_description = esc_textarea(($club_description));
        
        $slug=sanitize_title_with_dashes($club_name);
            
        $msg.= '<br/>';
        
            $new_post = array(
                'post_title' => $club_name,
                'post_name' => $slug,
                'post_content' => $club_description,
                'post_status' => $post_status,
                'post_author' => $user_id,
                'post_type' => $post_type,

            );
            $post_id = wp_insert_post($new_post);
            
            
			if($post_id) {
			
                            
                            $club_first_name = $user_first_name;
                            update_post_meta($post_id, 'club_first_name', esc_attr($club_first_name));

                            $club_last_name = $user_last_name;
                            update_post_meta($post_id, 'club_last_name', esc_attr($club_last_name));
    
                            $club_email = $club_email;
                            update_post_meta($post_id, 'club_email', esc_attr($club_email));

                            $club_phone_number = $user_phone_number;
                            update_post_meta($post_id, 'club_phone_number', esc_attr($club_phone_number));
    
                            
                             $club_website = $club_website;
                            update_post_meta($post_id, 'club_website', esc_url_raw($club_website));

                            $club_user_id = (int) $user_id;
                            update_post_meta($post_id, 'club_user_id', esc_attr($club_user_id));
                            
                            
                               
                                
                            $club_address = $address;
                            update_post_meta($post_id, 'club_adderss', esc_attr($club_address));

                            $club_zip_code = $zipcode; 
                            update_post_meta($post_id, 'club_zip_code', esc_attr($club_zip_code));
    
    
                                
                                
            $get_countries = "select * from " . $wpdb->prefix . "countries where LOWER(country) like '" . $country . "%' order by country asc limit 1";
            $res_countries = $wpdb->get_row($get_countries);


            if(!empty($res_countries)){

                              
                $club_country_id = (int) $res_countries->countryid;
                update_post_meta($post_id, 'club_country_id', esc_attr($club_country_id));

            }
                
            
             $get_state="select * from " . $wpdb->prefix . "states as st where LOWER(st.region) like '".$state."%' order by st.region asc limit 1";
             $res_state = $wpdb->get_row($get_state);

                if(!empty($res_state)){
             
                                $club_state_id = (int) $res_state->regionid;
                                update_post_meta($post_id, 'club_state_id', esc_attr($club_state_id));
                                
                }              
                                

                                
                $get_city = "select *  from " . $wpdb->prefix . "cities as ct where LOWER(ct.City) like '" . $city . "%'  order by  ct.City asc limit 1";
                $res_city = $wpdb->get_row($get_city);

                if(!empty($res_city)){

                    
                    $club_city_id = (int) $res_city->CityId;
                    update_post_meta($post_id, 'club_city_id', esc_attr($club_city_id));

                    $club_lat = $res_city->Latitude;
                    update_post_meta($post_id, 'club_lat', esc_attr($club_lat));

                    $club_long = $res_city->Longitude;
                    update_post_meta($post_id, 'club_long', esc_attr($club_long));

                    
                }
    
       }    

        
    
    } else {
        $msg.= 'No user exists== '.$user_email.'<br/>';
    }
} else {
     $msg.= 'No user email== '.$user_email.'<br/>';
}
}

 
echo $msg;


?>

<?php get_footer(); ?>

