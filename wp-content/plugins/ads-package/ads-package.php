<?php
/*
  Plugin Name: Ads Package
  Plugin URI:
  Description: This plugin used for add edit delete and listing ads package module at admin side.
  Version: 1.0
  Author: iWS
  Author URI:
 */



add_action('init', 'create_ads_package');

function create_ads_package() {

    ////====set slug for direct open==/
    /////='supports' => array('title','editor','excerpt','trackbacks','custom-fields','comments','revisions','thumbnail','author','page-attributes',)
    //=taxonomies 'post_tag'

    register_post_type('ads_package', array('label' => 'Ads Packages', 'description' => 'New Listing', 'public' => true, 'show_ui' => true, 'show_in_menu' => true, 'capability_type' => 'page', 'hierarchical' => true, 'rewrite' => array('slug' => 'ads-package', 'with_front' => true), 'query_var' => true, 'exclude_from_search' => false, 'menu_position' => 18, 'supports' => array('title', 'editor'), 'taxonomies' => array(), 'labels' => array(
            'name' => 'Ads Packages',
            'singular_name' => 'Ads Package',
            'menu_name' => 'Manage Ads Packages',
            'add_new' => 'Add Ads Package',
            'add_new_item' => 'Add New Ads Package',
            'edit' => 'Edit',
            'edit_item' => 'Edit Ads Package',
            'new_item' => 'Ads Package New',
            'view' => 'View Ads Package',
            'view_item' => 'View Ads Package',
            'search_items' => 'Search Ads Packages',
            'not_found' => 'No Ads Packages Found',
            'not_found_in_trash' => 'No Ads Packages found in Trash',
            'parent' => 'Parent Ads Package',
        ),));
}

///=============add custom column==========


add_filter('manage_edit-ads_package_columns', 'ads_package_edit_columns');

function ads_package_edit_columns($columns) {

    $columns = array(
        'cb' => '<input type="checkbox" />',
        'id' => __('ID'),
        'title' => __('Title'),
        'ads_days' => __('Days'),
        'ads_price' => __('Price'),
        'post_status' => __('Status'),
        'date' => __('date')
    );

    return $columns;
}

add_action('manage_ads_package_posts_custom_column', 'my_manage_ads_package_columns', 10, 2);

function my_manage_ads_package_columns($column_name, $post_id) {
    global $wpdb;
    global $post;

    switch ($column_name) {

        case 'id':
            echo $post_id;
            break;

        case 'post_status':
            echo $post->post_status;
            break;

        case 'ads_days':
            $ads_days = get_post_meta($post->ID, 'ads_days', TRUE);
            echo $ads_days;

            break;
        case 'ads_price':
            $ads_price = get_post_meta($post->ID, 'ads_price', TRUE);
            echo $ads_price;
            break;




        default:
            break;
    } // end switch
}

add_filter('manage_edit-ads_package_sortable_columns', 'my_ads_package_sortable_columns');

function my_ads_package_sortable_columns($columns) {

    //$columns['id'] = 'id';

    $columns['post_status'] = 'post_status';
    $columns['ads_days'] = 'ads_days';
    $columns['ads_price'] = 'ads_price';


    return $columns;
}

/**
 * Add stylesheet to the page
 */
add_action('admin_enqueue_scripts', 'ads_package_stylesheet_to_admin');

function ads_package_stylesheet_to_admin() {
    wp_enqueue_style('ads_package_css', plugins_url('/css/ads-package.css', __FILE__));
}

add_action('admin_init', 'meta_ads_package_extra_fields_type_var');

function meta_ads_package_extra_fields_type_var() {



    // please add other post type

    $post_types = array('ads_package');



    foreach ($post_types as $post_type) {



        add_meta_box('ads_package_parameter_info', 'Package Information', 'ads_package_parameter_info_setup', $post_type, 'normal', 'high');
    }

    // save metabox

    add_action('save_post', 'ads_package_extra_field_save');
}

function ads_package_parameter_info_setup() {

    global $wpdb, $post;




    $ads_days = get_post_meta($post->ID, 'ads_days', TRUE);
    $ads_price = get_post_meta($post->ID, 'ads_price', TRUE);
    ?>






    <div class="left_label">
        <label for="" class="">Ads Days</label>
    </div>
    <div class="right_label">
        <input type="text" id="ads_days" value="<?php echo $ads_days; ?>" size="2" name="ads_days">

    </div>
    <div class="clear"></div>


    <div class="left_label">
        <label for="" class="">Ads Price($)</label>
    </div>
    <div class="right_label">
        <input type="text" id="ads_price" value="<?php echo $ads_price; ?>" size="5" name="ads_price">

    </div>
    <div class="clear"></div>





    <?php
    echo '<input type="hidden" name="iws_adminadspackage_nonce" value="' . wp_create_nonce('iws-adminadspackage-nonce') . '" />';
}

// Display any errors
add_action('admin_notices', 'ads_package_admin_notice_handler');

function ads_package_admin_notice_handler() {

    global $post_type, $pagenow;

    $html = '';

    if (isset($_GET['message'])) {

        if ((int) $_GET['message'] == (int) 4) {
            $html = '<div class="error">
                        <p><strong>Validation errors</strong></p>
                        <p>- Please add price or days to package.</p></div>';
        }
    }




    echo $html;
}

function ads_package_extra_field_save($post_id) {

    global $post, $wpdb;

    // don't do on autosave or when new posts are first created
    if (( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) || (isset($post) && !empty($post) && $post->post_status == 'auto-draft'))
        return $post_id;



    // authentication checks
    // make sure data came from our meta box





    if ($_POST && isset($_POST['iws_adminadspackage_nonce']) && wp_verify_nonce($_POST['iws_adminadspackage_nonce'], 'iws-adminadspackage-nonce')) {







        // check user permissions

        if ($_POST['post_type'] == 'page') {

            if (!current_user_can('edit_page', $post_id))
                return $post_id;
        } else {

            if (!current_user_can('edit_post', $post_id))
                return $post_id;
        }






        //===info all



        $ads_days = (int) sanitize_text_field($_POST['ads_days']);
        update_post_meta($post_id, 'ads_days', esc_attr($ads_days));

        $ads_price = (double) sanitize_text_field($_POST['ads_price']);
        update_post_meta($post_id, 'ads_price', esc_attr($ads_price));






        // just checking it's not empty - you could do other tests...
        if ((int) $ads_days == (int) 0 || (double) $ads_price == 0) {
            $meta_missing = true;
        }

        // on attempting to publish - check for completion and intervene if necessary
        if (( isset($_POST['publish']) || isset($_POST['save']) ) && $_POST['post_status'] == 'publish') {
            //  don't allow publishing while any of these are incomplete
            if ($meta_missing == true) {
                global $wpdb;
                $wpdb->update($wpdb->posts, array('post_status' => 'pending'), array('ID' => $post_id));
                // filter the query URL to change the published message
                add_filter('redirect_post_location', create_function('$location', 'return add_query_arg("message", "4", $location);'));
            }
        }
    }

    return $post_id;
}

///http://code.tutsplus.com/articles/quick-tip-hierarchical-custom-post-types-not-working-for-you--wp-26508
//http://ahabman.com/blog/2013/02/wordpress-custom-post-type-dynamic-sub-pages/
//http://www.placementedge.com/blog/create-post-sub-pages/

function setup_user_package_admin_menus() {
    add_menu_page('My Custom Page', 'My Custom Page', 'manage_options', 'my-top-level-slug', 'admin_user_package_list');

    add_submenu_page('my-top-level-slug', 'My Custom Page', 'My Custom Page', 'manage_options', 'my-top-level-slug');

    add_submenu_page('my-top-level-slug', 'My Custom Submenu Page', 'My Custom Submenu Page', 'manage_options', 'my-secondary-slug');
}

//add_action("admin_menu", "setup_user_package_admin_menus");

function admin_user_package_list() {
    echo "sfsdfD";
    die;
}




add_action('wp_ajax_check_ads_availability', 'iws_check_ads_availability');  //===for logged in user
add_action('wp_ajax_nopriv_check_ads_availability', 'iws_check_ads_availability');  //==for not logged in user


function iws_check_ads_availability(){
        
    global $wpdb;
    $aid = intval($_POST['activity']);
    
    $msg='<p style="color:red;">Currently no place find for your ads.</p>';
   
    if((int) $aid >0 ) {
        
        $cur_date=date('Y-m-d H:i:s');
        
        $max_ads=4;
        
        $check_sql="select * from ".$wpdb->prefix."user_ads as usad where usad.activity_id=".$aid." and ( ('".$cur_date."' between usad.ads_start_date and usad.ads_end_date) or  (usad.ads_start_date>'".$cur_date."') ) ";
        
        $get_user_packages = mysql_query($check_sql) or die("Sql error : " . mysql_error());
        
        if(mysql_num_rows($get_user_packages)>=$max_ads){
            
            $get_last_available_sql="select * from ".$wpdb->prefix."user_ads as usad where usad.ads_end_date>'".$cur_date."' order by usad.ads_end_date asc limit 1";
            
            $get_last_package_available=mysql_query($get_last_available_sql);
            
            if(mysql_num_rows($get_last_package_available)>0){
                
                $rows=mysql_fetch_array($get_last_package_available);
                
                
                
                $msg='<p style="color:red;">Next available date for ads is '.date('d M, Y H:i:s',strtotime($rows['ads_end_date']." + 20 minutes")).'. First come first serve.</p>';
                
            } else {
                $msg="success";
            }
            
            
        } else {
            $msg="success";
        }
        
        
        
    }
    
    
   
    echo $msg;
    die();
}


function do_buy_ads_package() {

    
    
    // only show the registration form to non-logged-in members
    if (is_user_logged_in()) {

    } else {
        $redirect_link = wp_login_url();
        wp_redirect($redirect_link);
        exit;
    }
    
    

    ob_start();

    global $wpdb, $post;


    $sql = '';

    $sql.='SELECT ps.*, CAST(mt1.meta_value AS DECIMAL(10,2)) as ads_price ,CAST(mt2.meta_value AS UNSIGNED) as ads_days ';



    $sql.=' FROM ' . $wpdb->prefix . 'posts as ps ';


    $sql.=' INNER JOIN ' . $wpdb->prefix . 'postmeta AS mt1 ON ( ps.ID = mt1.post_id ) ';


    $sql.=' INNER JOIN ' . $wpdb->prefix . 'postmeta AS mt2 ON ( ps.ID = mt2.post_id ) ';


    $sql.=" WHERE 1=1 AND ps.post_type = 'ads_package' AND (ps.post_status = 'publish') ";


    $sql.=" AND ( ";

    $sql.="( mt1.meta_key = 'ads_price' AND CAST(mt1.meta_value AS DECIMAL(10,2)) > 0 ) ";

    $sql.=" AND ";

    $sql.=" ( mt2.meta_key = 'ads_days' AND CAST(mt2.meta_value AS UNSIGNED) > 0 ) ";
    $sql.=" ) ";


    $sql.=' GROUP BY ps.ID ORDER BY ads_days ASC, ads_price ASC ';



    //echo $sql;
    // die;


    $get_ads_packages = mysql_query($sql) or die("Sql error : " . mysql_error());

    $res_activities = get_all_activities();


    $ads_main_activity = '';
    $ads_package_id = '';
    if (isset($_POST['iws_buyads_nonce'])) {

        if (isset($_POST['ads_main_activity'])) {
            $ads_main_activity = $_POST['ads_main_activity'];
        }
        if (isset($_POST['ads_package_id'])) {
            $ads_package_id = $_POST['ads_package_id'];
        }
    }
    ?>
    <div class="col-sm-12 signUpBlock">




    <?php
// show any error messages after form submission
    echo iws_show_error_messages();
    ?>

        <form id="iws_buyads_form" class="form-horizontal" action="" method="POST">


            <div class="form-group">
                <label for="" class="col-sm-4 control-label">Activity</label>
                <div class="col-sm-8">

<?php $first_activity=0;

$acnt=1;?>
                    <select name="ads_main_activity" id="ads_main_activity" class="form-control">
                        <option value="0">---Select Main Activity---</option>                    
        <?php
        if (!empty($res_activities)) {
            foreach ($res_activities as $row_acti) {
                if($acnt==1){  $first_activity=$row_acti->term_id; }
                
                
                
                ?>
               
                                <option value="<?php echo $row_acti->term_id; ?>" data-slug="<?php echo $row_acti->slug; ?>" <?php if ($row_acti->term_id == $ads_main_activity) { ?> selected <?php } ?>><?php echo ucwords($row_acti->name); ?></option>
        <?php $acnt++; }
    } ?>

                    </select>
                </div>
            </div>
            
            <div class="form-group">
                <div class="col-sm-4"></div><div class="col-sm-8" id="activity_info"></div>
            </div>

            <div class="form-group">
                <label for="" class="col-sm-4 control-label">Package</label>
                <div class="col-sm-8">

<?php $ncnt=1;
$first_package_name='';
$first_package_days=''; 
$first_package_price=''; 
$first_package_id=''; 
?>
                    <select name="ads_package_id" id="ads_package_id" class="form-control">
                        <option value="0">---Select Ads Package---</option>                    
                        <?php
                        if (mysql_num_rows($get_ads_packages) > 0) {
                            while ($row = mysql_fetch_array($get_ads_packages)) {



                                $package_id = $row['ID'];
                                $package_name = $row['post_title'];
                                $package_days = $row['ads_days'];
                                $package_price = $row['ads_price'];
                                
                                
                                if($ncnt==1){
                                    $first_package_days=$package_days;
                                    $first_package_name=$package_name;
                                    $first_package_price=$package_price;
                                    $first_package_id=$package_id;
                                }
                                
                                ?>
                                <option value="<?php echo $package_id; ?>" data-adsdays="<?php echo $package_days; ?>" data-adsprice="<?php echo $package_price; ?>" <?php if ($package_id == $ads_package_id) { ?> selected <?php } ?>><?php echo ucwords($package_name . " | " . $package_days . " Days | $" . $package_price); ?></option>
        <?php $ncnt++; }
    } ?>

                    </select>
                </div>
            </div>





            <div class="form-group">

                <div class="col-sm-offset-4 col-sm-8">

                    <div class="checkbox">

                        <label>
                            <input type="checkbox" value="1" name="iws_agree_on_terms" id="iws_agree_on_terms" /> I have read and agreed to the <a href="<?php echo esc_url(wp_termscond_url()); ?>" target="_blank">Terms & Conditions</a>.
                        </label>

                    </div>

                </div>

            </div>
            
            
            <div id="cart1" style="display: none;">
                <?php echo do_shortcode('[wp_cart_button name="'.$first_package_name.'" price="'.$first_package_price.'" item_number="'.$first_package_id.'"]'); ?></div>
            
            
            
            <div class="form-group" id="cart2" style="display: none;">

                <div class="col-sm-offset-4 col-sm-8">
                    <input type="hidden" name="iws_buyads_nonce" value="<?php echo wp_create_nonce('iws-buyads-nonce'); ?>"/>

                    <button id="iws_buyads_submit" type="button" class="btn btn-default sign-btn">Add To Cart</button>
                </div>

            </div>





        </form>

    </div>
    
    <script>


     var ajaxurl='<?php echo admin_url('admin-ajax.php'); ?>';
     
     jQuery(document).ready(function($){
	
        
        
            jQuery('#ads_main_activity').change(function(e) {
                
                var activity=jQuery(this).val();
                
                if(activity==0 || activity==null || activity=='' || activity==undefined){
                    activity='<?php echo $first_activity; ?>';
                }
                
		var data = {
			'action': 'check_ads_availability',
			'activity': activity
		};
		
	
		jQuery.post(ajaxurl, data, function(response) {
			
                        if(response=='success'){
                            jQuery("#cart2").show();
                            jQuery("#activity_info").html('');
                            
                            
                            
                        } else {
                            jQuery("#cart2").hide();
                            jQuery("#activity_info").html(response);
                            
                        }
                        
                        
		});
        });
       
        jQuery("#iws_buyads_submit").on("click",function(e){
            
            
                var ads_main_activity=$('#ads_main_activity option:selected').val();
                var ads_activity_slug=$('#ads_main_activity option:selected').attr('data-slug');
            
                var ads_package=$('#ads_package_id option:selected').val();
                var ads_days=$('#ads_package_id option:selected').attr('data-adsdays');
                var ads_price=$('#ads_package_id option:selected').attr('data-adsprice');
                var ads_title=$('#ads_package_id option:selected').text();
                
                
                if(ads_main_activity==0 || ads_main_activity=='' || ads_main_activity==undefined || ads_main_activity==null){
                    alert('Please select ads activity.');
                    return false;
                } else {
                        var vdata = {
                            'action': 'check_ads_availability',
                            'activity': ads_main_activity
                        };  
		
	
                        var response=jQuery.ajax({url:ajaxurl,type:'POST',dataType :'HTML', data:vdata, async:false,cache:false}).responseText;
                        
                        
                        if(response=='success'){
                                    jQuery("#cart2").show();
                                    jQuery("#activity_info").html('');
                                    
                                } else {
                                    jQuery("#cart2").hide();
                                    jQuery("#activity_info").html(response);
                                    
                                    return false;
                                }
                }
                
                
                
                if(ads_package==0 || ads_package=='' || ads_package==undefined || ads_package==null){
                    alert('Please select ads package.');
                    return false;
                }
                
                
                 if($("#iws_agree_on_terms").is(':checked')){
                     
                 } else {
                     alert("Please agree on terms and conditions.");
                     return false;
                 }
                     
                
                var product_tmp=jQuery(".wp_cart_button_wrapper input[type=hidden][name=product_tmp]").val();
                            
                //var new_ads_title=ads_title+" ("+ads_main_activity+")";
                //var new_ads_title=product_tmp+" | "+ads_main_activity+"-"+ads_activity_slug+" | "+ads_days+" days | $"+ads_price;
                
                var new_ads_title=ads_title+" | "+ads_main_activity+"-"+ads_activity_slug;
                
                var newproduct=jQuery('.wp_cart_button_wrapper input[type=hidden][name=product]').val(new_ads_title);

                
                
                $('.wp_cart_button_wrapper input[type=hidden][name=item_number]').val(ads_package);
                $('.wp_cart_button_wrapper input[type=hidden][name=price]').val(ads_price);
                //$('.wp_cart_button_wrapper input[type=hidden][name=product]').val(ads_title);
                
                
                
                $('.wp_cart_button_wrapper input[type=submit]').trigger("click");
            
        });
        
        
    });
</script>
    
    
    <?php
    //$cart_btn=
    //$lhtml.=$cart_btn;
    return ob_get_clean();
}

add_shortcode('buy_ads_package', 'do_buy_ads_package');
?>