<?php
ob_start();

global $wpdb, $post;

$edit_add_text='Add';


$obj_id=0;
$merchant_name='';
$merchant_description='';


$merchant_address='';
$merchant_zip_code='';
$merchant_city_id='';
$merchant_state_id='';
$merchant_country_id='';

$merchant_lat='';
$merchant_long='';

$merchant_main_activity='';


$merchant_fax_number = '';
$merchant_phone_number = '';
$merchant_website='';
$merchant_user_id='';





if(isset($_GET['id']) && (int) $_GET['id']>0){
                
    $obj_id = (int) $_GET['id'];
    $edit_add_text='Edit';
    
    
    $post_data=get_post($obj_id,true);
    
    
    
    $merchant_name=$post_data['post_title'];
    
    $merchant_description=$post_data['post_content'];
    
    
    
    
    
    $merchant_address = get_post_meta($obj_id, 'merchant_adderss', TRUE);
    
    $merchant_zip_code = get_post_meta($obj_id, 'merchant_zip_code', TRUE);
    
    $merchant_city_id = get_post_meta($obj_id, 'merchant_city_id', TRUE);
    $merchant_state_id = get_post_meta($obj_id, 'merchant_state_id', TRUE);
    $merchant_country_id = get_post_meta($obj_id, 'merchant_country_id', TRUE);
    $merchant_lat = get_post_meta($obj_id, 'merchant_lat', TRUE);
    $merchant_long = get_post_meta($obj_id, 'merchant_long', TRUE);


    
    $merchant_main_activity = get_post_meta($obj_id, 'merchant_main_activity', TRUE);
    
    
    $merchant_fax_number = get_post_meta($obj_id, 'merchant_fax_number', TRUE);
    $merchant_phone_number = get_post_meta($obj_id, 'merchant_phone_number', TRUE);
    
    $merchant_website = get_post_meta($obj_id, 'merchant_website', TRUE);

    $merchant_user_id = get_post_meta($obj_id, 'merchant_user_id', TRUE);

   
    

}

    

    if (isset($_POST['merchant_country_id']) && (int) $_POST['merchant_country_id'] > 0) {
        $merchant_country_id = (int) $_POST['merchant_country_id'];
    }
    if (isset($_POST['merchant_state_id']) && (int) $_POST['merchant_state_id'] > 0) {
        $merchant_state_id = (int) $_POST['merchant_state_id'];
    }
    if (isset($_POST['merchant_city_id']) && (int) $_POST['merchant_city_id'] > 0) {
        $merchant_city_id = (int) $_POST['merchant_city_id'];
    }

    

    $res_countries = get_all_country();


    $res_states = array();
    if ($merchant_country_id > 0) {
        
        $res_states = get_states_from_country_by_id($merchant_country_id);
    }

    $res_cities = array();
    if ($merchant_state_id > 0) {
       
        $res_cities = get_cities_from_state_by_id($merchant_state_id);
    }
    
    
    $res_activities = get_all_activities();    
    if (isset($_POST['merchant_main_activity']) && (int) $_POST['merchant_main_activity'] > 0) {
        $merchant_main_activity = (int) $_POST['merchant_main_activity'];
    }
    
    
    


?>	



    <div class="col-sm-12 signUpBlock">
    
    
    <h3><?php echo $edit_add_text;?> Merchant Link</h3>

    <?php
// show any error messages after form submission
    echo iws_show_error_messages();
    ?>
    <form id="iws_registration_form" class="form-horizontal" action="" method="POST">



        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Merchant Name'); ?>:</label>
            <div class="col-sm-8">
                <input name="merchant_name" id="merchant_name" value="<?php
    if (isset($_POST['merchant_name'])) {
        echo $_POST['merchant_name'];
    } else {
        echo $merchant_name;
    }
    ?>" class="form-control" type="text"/>
            </div>
        </div>
        
        
        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('About the Merchant'); ?><br/><small>Please tell us about your merchant so we can showcase your merchant to our viewers.</small></label>
            <div class="col-sm-8">
                <textarea name="merchant_description" id="merchant_description" class="form-control" rows="5"><?php
                       if (isset($_POST['merchant_description'])) {
                           echo $_POST['merchant_description'];
                       } else { echo $merchant_description; }
    ?></textarea>
            </div>
        </div>



        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Phone Number'); ?>:</label>
            <div class="col-sm-8">
                <input name="merchant_phone_number" id="merchant_phone_number" value="<?php
                       if (isset($_POST['merchant_phone_number'])) {
                           echo $_POST['merchant_phone_number'];
                       } else { echo $merchant_phone_number; } 
    ?>" class="form-control" type="text"/>
            </div>
        </div>
        
        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Fax Number'); ?>:</label>
            <div class="col-sm-8">
                <input name="merchant_fax_number" id="merchant_fax_number" value="<?php
                       if (isset($_POST['merchant_fax_number'])) {
                           echo $_POST['merchant_fax_number'];
                       } else { echo $merchant_fax_number; } 
    ?>" class="form-control" type="text"/>
            </div>
        </div>
        
        

        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Merchant website address'); ?>:</label>
            <div class="col-sm-8">
                <input name="merchant_website" id="merchant_website" value="<?php
                       if (isset($_POST['merchant_website'])) {
                           echo $_POST['merchant_website'];
                       } else { echo $merchant_website; } 
    ?>" class="form-control" type="text"/>
            </div>
        </div>





        <h3>Activities</h3>

        <div class="form-group">
            <label for="" class="col-sm-4 control-label">Merchant Activity</label>
     <div class="col-sm-8">
   

        <select name="merchant_main_activity" id="merchant_main_activity" class="form-control">
            <option value="0">---Select Activity---</option>                    
    <?php
    if (!empty($res_activities)) {
        foreach ($res_activities as $row_acti) {
            ?>
                    <option value="<?php echo $row_acti->term_id; ?>" <?php if ($row_acti->term_id == $merchant_main_activity) { ?> selected <?php } ?>><?php echo ucwords($row_acti->name); ?></option>
        <?php }
    } ?>

        </select>
   </div>
 </div>



        
        
        <h3>Merchant Location</h3>
        
        
        
        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e("Address"); ?></label>
            <div class="col-sm-8">

                <input type="text" name="merchant_adderss" id="merchant_adderss" value="<?php if (isset($_POST['merchant_adderss'])) {
        echo $_POST['merchant_adderss'];
    } else {
        echo $merchant_address;
    } ?>" class="form-control" />
            </div>
        </div>







        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e("Country"); ?></label>
            <div class="col-sm-8">

                <select name="merchant_country_id" id="merchant_country_id" class="form-control" >
                    <option value="0">---Select Country---</option>   
                    <?php if (!empty($res_countries)) {
                        foreach ($res_countries as $row_country) { ?>

                            <option value="<?php echo $row_country->countryid; ?>" <?php if ($row_country->countryid == $merchant_country_id) { ?> selected <?php } ?>><?php echo ucfirst($row_country->country); ?></option>

        <?php }
    } ?>

                </select> 
            </div>
        </div>





        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e("State"); ?></label>
            <div class="col-sm-8">

                <select name="merchant_state_id" id="merchant_state_id" class="form-control" >
                    <option value="0">---Select State---</option>

    <?php
    if (!empty($res_states)) {
        foreach ($res_states as $row_states) {
            ?>

                            <option value="<?php echo $row_states->regionid; ?>" <?php if ($row_states->regionid == $merchant_state_id) { ?> selected <?php } ?>><?php echo ucfirst($row_states->region); ?></option>


                            <?php
                        }
                    }
                    ?>

                </select> 
            </div>
        </div>







        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e("City"); ?></label>
            <div class="col-sm-8">

                <select name="merchant_city_id" id="merchant_city_id" class="form-control" >
                    <option value="0">---Select City---</option>

    <?php
    if (!empty($res_cities)) {
        foreach ($res_cities as $row_cities) {
            ?>

                            <option value="<?php echo $row_cities->CityId; ?>" data-lat="<?php echo $row_cities->Latitude; ?>" data-long="<?php echo $row_cities->Longitude; ?>" <?php if ($row_cities->CityId == $merchant_city_id) { ?> selected <?php } ?>><?php echo ucfirst($row_cities->City); ?></option>


        <?php }
    } ?>


                </select> 


                <input type="hidden" name="merchant_lat" id="merchant_lat" value="<?php if (isset($_POST['merchant_lat'])) {
        echo $_POST['merchant_lat'];
    } else {
        echo $merchant_lat;
    } ?>" />
                <input type="hidden" name="merchant_long" id="merchant_long" value="<?php if (isset($_POST['merchant_long'])) {
        echo $_POST['merchant_long'];
    } else {
        echo $merchant_long;
    } ?>" />

            </div>
        </div>









        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e("Zip Code"); ?></label>
            <div class="col-sm-8">

                <input type="text" name="merchant_zip_code" id="merchant_zip_code" value="<?php if (isset($_POST['merchant_zip_code'])) {
        echo $_POST['merchant_zip_code'];
    } else {
        echo $merchant_zip_code;
    } ?>" class="form-control" />
            </div>
        </div>

        
        
        
        

        <h3>Upload picture of merchant's logo</h3>                       

        <?php
        $allowed_ext = "jpg,gif,png";
        $post_id = $obj_id;
        $attr['name'] = '_thumbnail_id';
        $attr['max_size'] = "10000";
        $attr['count'] = 1;

        $type = 'post';

        $uploaded_items = $post_id ? get_post_meta($post_id, $attr['name'], true) : array();



        
        $has_featured_image = false;
        $has_images = false;
        $has_avatar = false;
        

        if ($post_id) {

                   
            if (isset($_POST['iws_files']['_thumbnail_id'])) {
                 $thumb_id = $_POST['iws_files']['_thumbnail_id'][0]; 
                if ($thumb_id) {
                    $has_featured_image = true;
                    $featured_image = attach_html($thumb_id); 
                    echo '<input type="hidden" name="iws_files[_thumbnail_id][]" value="'.$thumb_id.'">';
                }
            } else {

                    // it's a featured image then
                    $thumb_id = get_post_thumbnail_id($post_id);

                    if ($thumb_id) {
                        $has_featured_image = true;
                        $featured_image = attach_html($thumb_id); 
                    }
            }
        } else {
            if (isset($_POST['iws_files']['_thumbnail_id'])) {
                echo $thumb_id = $_POST['iws_files']['_thumbnail_id'][0]; die;
                if ($thumb_id) {
                    $has_featured_image = true;
                    $featured_image = attach_html($thumb_id); 
                    echo '<input type="hidden" name="iws_files[_thumbnail_id][]" value="'.$thumb_id.'">';
                }
            }
        }
        ?>


        <div class="iws-fields">
            <div id="iws-<?php echo $attr['name']; ?>-upload-container">
                <div class="iws-attachment-upload-filelist" data-type="file" >
                    <a id="iws-<?php echo $attr['name']; ?>-pickfiles" class="btn btn-danger btn-small button file-selector <?php echo ' iws_' . $attr['name']; ?>" href="#">Upload Image</a>

                    <ul class="iws-attachment-list thumbnails" >
    <?php
    if ($has_featured_image) {
        echo $featured_image;
    }

    if ($has_avatar) {
        $avatar = get_user_meta($post_id, 'user_avatar', true);
        if ($avatar) {
            
            
            echo '<li class="iws-image-wrap thumbnail"><div class="attachment-name">'.$featured_image.'</div><div class="caption"><a href="javascript:void(0)" data-confirm="Are you sure to delete your image?" class="btn btn-danger btn-small iws-button button iws-delete-file">Delete</a></div></li>';
            
            
        }
    }

    
    ?>
                    </ul>
                </div>
            </div><!-- .container -->



        </div> <!-- .iws-fields -->

        
        <style>
            .thumbnail {
                border: none !important;
            }
        </style>
            
        
        
        <script type="text/javascript">
            jQuery(function($) {
                new IWS_Uploader('iws-<?php echo $attr['name']; ?>-pickfiles', 'iws-<?php echo $attr['name']; ?>-upload-container', <?php echo $attr['count']; ?>, '<?php echo $attr['name']; ?>', '<?php echo $allowed_ext; ?>', <?php echo $attr['max_size'] ?>);
                    
                    
                    
                jQuery(".iws-delete-file").on("click",function(e){
                       
                       
                    e.preventDefault();

                    if ( confirm( $(this).data('confirm') ) ) {
                        $.post('<?php echo admin_url('admin-ajax.php'); ?>',{action: 'iws_file_del'}, function() {
                            window.location.reload();
                        });
                    }
                       
                });
            });
        </script>





        <div class="form-group">

          <div class="col-sm-12 text-right">
                <input type="hidden" name="iws_merchantlinks_editadd_nonce" value="<?php echo wp_create_nonce('iws-merchantlinks-editadd-nonce'); ?>"/>

                <button id="iws_merchantlinks_editadd_submit" type="submit" class="btn btn-default sign-btn">Submit</button>
            </div>

        </div>
        
        
        <script>
            var ajaxurl='<?php echo admin_url('admin-ajax.php'); ?>';
            jQuery(document).ready(function(){
				
	jQuery('#merchant_country_id').change(function(e) {
		var data = {
			'action': 'get_states_of_country',
			'country': jQuery(this).val()
		};
		
		var state_ops = '<option value="">---Select State---</option>';
		var city_ops = '<option value="">---Select City---</option>';
		
		jQuery("#merchant_state_id").html(state_ops);
		jQuery("#merchant_city_id").html(city_ops);

		// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
		jQuery.post(ajaxurl, data, function(response) {
			if(jQuery("#merchant_state_id").length>0)
			{
				jQuery("#merchant_state_id").html(response);
			}
		});
    });
	
	jQuery('#merchant_state_id').change(function(e) {
		var data = {
			'action': 'get_cities_of_state',
			'state': jQuery(this).val()
			
		};
		
		
		var city_ops = '<option value="">---Select City---</option>';
		
		jQuery("#merchant_city_id").html(city_ops);

		// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
		jQuery.post(ajaxurl, data, function(response) {
			if(jQuery("#merchant_city_id").length>0)
			{
				jQuery("#merchant_city_id").html(response);
			}
		});
    });
	
	jQuery('#merchant_city_id').change(function(e) {
		var vlat=jQuery("#merchant_city_id option:selected").attr('data-lat');
		var vlong=jQuery("#merchant_city_id option:selected").attr('data-long');
		
		jQuery("#merchant_lat").val(vlat);
		jQuery("#merchant_long").val(vlong);
		
	});
	
});
        </script>


        
    </form>
</div>
<?php
echo ob_get_clean();
?>