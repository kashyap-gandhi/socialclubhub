<?php


add_action('wp_ajax_check_email', 'check_email');  //===for logged in user
function check_email(){
    $email = trim($_POST['email']);
  $email_exists = email_exists($email);
  if ( $email_exists ) {
    echo "That E-mail is registered to user number " . $exists;
  } else {
    echo "That E-mail doesn't belong to any registered users on this site";
  }
  die;
}


add_action('wp_ajax_check_username', 'check_username');  //===for logged in user
function check_username(){
     $username = trim($_POST['username']);
       if ( username_exists( $username ) ) {
           echo "Username In Use!";
       } else {
           echo "Username Not In Use!";
       }
       
       die;
}


add_action('show_user_profile', 'extra_user_profile_fields');
add_action('edit_user_profile', 'extra_user_profile_fields');

function extra_user_profile_fields($user) {
    
    
     $user_phone_number = esc_attr(get_the_author_meta('user_phone_number', $user->ID));
     $user_address = esc_attr(get_the_author_meta('user_address', $user->ID));
     $user_country_id=esc_attr(get_the_author_meta('user_country_id', $user->ID));
     $user_state_id=esc_attr(get_the_author_meta('user_state_id', $user->ID));
     $user_city_id=esc_attr(get_the_author_meta('user_city_id', $user->ID));
     $user_zip_code=esc_attr(get_the_author_meta('user_zip_code', $user->ID));
     
     $user_lat=esc_attr(get_the_author_meta('user_lat', $user->ID));
     $user_long=esc_attr(get_the_author_meta('user_long', $user->ID));
     
     $user_gender = esc_attr(get_the_author_meta('user_gender', $user->ID));

    if (isset($_POST['user_gender']) && (int) $_POST['user_gender'] > 0) {
        $user_gender = (int) $_POST['user_gender'];
    }
    
    $user_birth_year = esc_attr(get_the_author_meta('user_birth_year', $user->ID));

    if (isset($_POST['user_birth_year']) && (int) $_POST['user_birth_year'] > 0) {
        $user_birth_year = (int) $_POST['user_birth_year'];
    }
    

   
    

    $res_countries = get_all_country();

    $res_states = array();
    if ($user_country_id > 0) {
        
        $res_states = get_states_from_country_by_id($user_country_id);
    }

    $res_cities = array();
    if ($user_state_id > 0) {
        
        $res_cities = get_cities_from_state_by_id($user_state_id);
    }
    
    
    
    $res_activities = get_all_activities();
    
    
     $user_like_activities = esc_attr(get_the_author_meta('user_like_activities', $user->ID));

    if ($user_like_activities != '') {
        $user_like_activities = explode(',', $user_like_activities);
    }

    
    
    
    ?>
    <h3><?php _e("Extra profile information", "blank"); ?></h3>

    
   
    
   
    
    <table class="form-table">
   
        
        <tr>
             <th><label for="user_actitivities"><?php _e("User Like Activities"); ?></label></th>
            <td>
                  <?php
    if (!empty($res_activities)) {
        foreach ($res_activities as $row_acti) {
            ?>
                <div style="min-width:200px;width:25%;float:left;"><label><input type="checkbox" name="user_like_activities[]" value="<?php echo $row_acti->term_id; ?>" <?php if (!empty($user_like_activities)) {
                if (in_array($row_acti->term_id, $user_like_activities)) { ?> checked <?php }
            } ?> /><?php echo ucwords($row_acti->name); ?></label></div>
                <?php }
            } ?>

            </td>
        </tr>
        
        
        <tr>
            <th><label for="address"><?php _e("Phone Number"); ?></label></th>
            <td>
                <input type="text" name="user_phone_number" id="user_phone_number" value="<?php echo $user_phone_number; ?>" class="regular-text" /><br />
                <span class="description"><?php _e("Please enter your phone number."); ?></span>
            </td>
        </tr>
        
        
        
        <tr>
            <th><label for="address"><?php _e("Gender"); ?></label></th>
            <td>

                <select name="user_gender" id="user_gender" class="form-control" >
                    <option value="0">---Select Gender---</option>   
                 
                     

                            <option value="1" <?php if ($user_gender==1) { ?> selected <?php } ?>>Male</option>
                            <option value="2" <?php if ($user_gender==2) { ?> selected <?php } ?>>Female</option>

        
                </select> 
           </td>
        </tr>
        
        <tr>
            <th><label for="address"><?php _e("Birth year"); ?></label></th>
            <td>

                <select name="user_birth_year" id="user_birth_year" class="form-control" >
                    <option value="0">---Select Year---</option>   
                 
                     
                    <?php for($yr=1950;$yr<=date('Y');$yr++){ ?>
                            <option value="<?php echo $yr; ?>" <?php if ($user_birth_year==$yr) { ?> selected <?php } ?>><?php echo $yr; ?></option>
                            <?php } ?>

        
                </select> 
            </td>
        </tr>
        




        
        <tr>
            <th><label for="address"><?php _e("Address"); ?></label></th>
            <td>
                <input type="text" name="user_address" id="user_address" value="<?php echo $user_address; ?>" class="regular-text" /><br />
                <span class="description"><?php _e("Please enter your address."); ?></span>
            </td>
        </tr>
        
        
        
           <tr>
            <th><label for="city"><?php _e("Country"); ?></label></th>
            <td>
                <select name="user_country_id" id="user_country_id" class="regular-text" >
            <option value="0">---Select Country---</option>   
    <?php if (!empty($res_countries)) {
        foreach ($res_countries as $row_country) { ?>

                    <option value="<?php echo $row_country->countryid; ?>" <?php if ($row_country->countryid == $user_country_id) { ?> selected <?php } ?>><?php echo ucfirst($row_country->country); ?></option>

        <?php }
    } ?>

        </select> <br />
                <span class="description"><?php _e("Please select your country."); ?></span>
            </td>
        </tr>
        
        
         <tr>
            <th><label for="state"><?php _e("State"); ?></label></th>
            <td>
                 <select name="user_state_id" id="user_state_id" class="regular-text" >
            <option value="0">---Select State---</option>

    <?php
    if (!empty($res_states)) {
        foreach ($res_states as $row_states) {
            ?>

                    <option value="<?php echo $row_states->regionid; ?>" <?php if ($row_states->regionid == $user_state_id) { ?> selected <?php } ?>><?php echo ucfirst($row_states->region); ?></option>


                <?php
                }
            }
            ?>

        </select> <br />
                <span class="description"><?php _e("Please select your state."); ?></span>
            </td>
        </tr>
        
        
        
         <tr>
            <th><label for="city"><?php _e("City"); ?></label></th>
            <td>
                 <select name="user_city_id" id="user_city_id" class="regular-text" >
               <option value="0">---Select City---</option>

            <?php
            if (!empty($res_cities)) {
                foreach ($res_cities as $row_cities) {
                    ?>

                    <option value="<?php echo $row_cities->CityId; ?>" data-lat="<?php echo $row_cities->Latitude; ?>" data-long="<?php echo $row_cities->Longitude; ?>" <?php if ($row_cities->CityId == $user_city_id) { ?> selected <?php } ?>><?php echo ucfirst($row_cities->City); ?></option>


                <?php }
            } ?>


        </select> <br />
                <span class="description"><?php _e("Please select your city."); ?></span>
                
                
                
    <input type="hidden" name="user_lat" id="user_lat" value="<?php echo $user_lat; ?>" />
    <input type="hidden" name="user_long" id="user_long" value="<?php echo $user_long; ?>" />
                
            </td>
        </tr>
        
        
        
        
        
        <tr>
            <th><label for="zipcode"><?php _e("Zip Code"); ?></label></th>
            <td>
                <input type="text" name="user_zip_code" id="user_zip_code" value="<?php echo $user_zip_code; ?>" class="regular-text" /><br />
                <span class="description"><?php _e("Please enter your zip code."); ?></span>
            </td>
        </tr>
    </table>
<?php
}

add_action('personal_options_update', 'save_extra_user_profile_fields');
add_action('edit_user_profile_update', 'save_extra_user_profile_fields');

function save_extra_user_profile_fields($user_id) {

    if (!current_user_can('edit_user', $user_id)) {
        return false;
    }


    update_user_meta($user_id, 'user_phone_number', $_POST['user_phone_number']);
    update_user_meta($user_id, 'user_address', $_POST['user_address']);
    update_user_meta($user_id, 'user_country_id', (int) $_POST['user_country_id']);
    update_user_meta($user_id, 'user_state_id', (int) $_POST['user_state_id']);
    update_user_meta($user_id, 'user_city_id', (int) $_POST['user_city_id']);
    update_user_meta($user_id, 'user_zip_code', $_POST['user_zip_code']);
    update_user_meta($user_id, 'user_lat', $_POST['user_lat']);
    update_user_meta($user_id, 'user_long', $_POST['user_long']);
    
    $user_gender = (int) sanitize_text_field($_POST['user_gender']);
            update_user_meta($user_id, 'user_gender', esc_attr($user_gender));
            
            $user_birth_year = (int) sanitize_text_field($_POST['user_birth_year']);
            update_user_meta($user_id, 'user_birth_year', esc_attr($user_birth_year));
            
    
    
    $user_like_activity = '';

    $get_other_activity = $_POST['user_like_activities'];

    if (!empty($get_other_activity)) {
        $get_other_activity = array_unique(array_map('intval', $get_other_activity));
        $user_like_activity = implode(',', $get_other_activity);

        if ($user_like_activity == ',') {
            $user_like_activity = '';
        }
    }

    update_user_meta($user_id, 'user_like_activities', esc_attr($user_like_activity));


    
    
    
}
/**
 * Add script to the page
 */
add_action('admin_enqueue_scripts', 'user_profile_script_to_admin');

function user_profile_script_to_admin($page) {
    if (is_admin()) {
        if ('user-new.php' == $page || 'user-edit.php' == $page) {
            wp_register_script('admin_user_profile_edit_js', plugins_url('/js/admin_user_profile.js', __FILE__), array('jquery'));
            
            // Enqueue Scripts that are needed on all the pages
            wp_enqueue_script('jquery');
            wp_enqueue_script('admin_user_profile_edit_js');
        } else {
            return;
        }
    }
}
?>