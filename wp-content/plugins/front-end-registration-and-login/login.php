<?php

// user login form
function iws_login_form() {


    $output = '';

    if (!is_user_logged_in()) {

        global $iws_load_css;

        // set this to true so the CSS is loaded
        $iws_load_css = true;

        $output = iws_login_form_fields();
    } else {
        // could show some logged in user info here
        // $output = 'user info here';
        //wp_redirect(get_permalink(199)); exit;


        global $current_user;

        $after_login_redirect = iws_login_redirect($current_user);
        // echo "<script>window.location.href='" . home_url() . "'</script>";
        wp_redirect($after_login_redirect);
        //echo "dfdfdfd"; die;
        //wp_redirect( home_url() );

        exit;
    }
    return $output;
}

add_shortcode('login_form', 'iws_login_form');

// login form fields
function iws_login_form_fields() {

    ob_start();
    ?>
   

    <?php
    // show any error messages after form submission
   echo iws_show_error_messages();
    echo iws_show_general_messages();
    ?>

    <form id="iws_login_form"  class="form-horizontal" action="<?php echo esc_url(wp_login_url()); ?>" method="post">
        <div class="form-group">
            <label for="" class="col-sm-4 control-label">User Name</label>
            <div class="col-sm-8">
<input name="iws_user_login" id="iws_user_login" value="<?php if (isset($_POST['iws_user_login'])) {
echo $_POST['iws_user_login'];
} ?>" class="form-control" type="text" placeholder="Username"/>
           </div>
        </div>
        
            <div class="form-group">
            <label for="" class="col-sm-4 control-label">Password</label>
            <div class="col-sm-8">
                <input name="iws_user_pass" id="iws_user_pass" class="form-control" type="password"/>
                </div>
            </div>
        
            <div class="form-group">

                                    <div class="col-sm-offset-4 col-sm-8">

                                      <div class="checkbox">

                                        <label>

                <input value="1"  name="iws_remember_me" id="iws_remember_me" class="required" type="checkbox"/> Remember me
            </label>

                                      </div>

                                    </div>

                                  </div>
        
           <div class="form-group">

                                    <div class="col-sm-offset-4 col-sm-8 text-center">
                                        <button type="submit" id="iws_login_submit" class="btn btn-default sign-btn">Sign in</button><br>
                <input type="hidden" name="iws_login_nonce" value="<?php echo wp_create_nonce('iws-login-nonce'); ?>"/>
                
                
                

                
            </div>

                                  </div>

        <div class="col-sm-offset-3 col-sm-12 text-center">
            <p id="nav">

<!--                <a href="<?php //echo esc_url(wp_registration_url()); ?>">Register</a> |-->

                <a href="<?php echo esc_url(wp_lostpassword_url()); ?>" title="<?php esc_attr_e('Password Lost and Found') ?>"><?php echo 'Lost your password?'; ?></a> |
                <a href="<?php echo esc_url(wp_resend_verification_url()); ?>" title="<?php esc_attr_e('Resend Confirmation Link'); ?>"><?php echo 'Resend Confirmation Link'; ?></a>


            </p>
        </div>
            

        
    </form>
    <?php
    return ob_get_clean();
}

// logs a member in after submitting a form
function iws_login_member() {

    if (isset($_POST['iws_user_login']) && isset($_POST['iws_login_nonce']) && wp_verify_nonce($_POST['iws_login_nonce'], 'iws-login-nonce')) {



        if ($_POST['iws_user_login'] == '') {

            iws_errors()->add('empty_login', __('Please enter a username'));
        }


        if (!isset($_POST['iws_user_pass']) || $_POST['iws_user_pass'] == '') {
            // if no password was entered
            iws_errors()->add('empty_password', __('Please enter a password'));
        }

        // this returns the user ID and other info from the user name
        $user = get_user_by('login', $_POST['iws_user_login']);

        if ($user) {
            // check the user's login with their password
            if (!wp_check_password($_POST['iws_user_pass'], $user->user_pass, $user->ID)) {
                // if the password is incorrect for the specified user
                iws_errors()->add('empty_password', __('Incorrect password'));
            }


            if (is_array($user->roles) && in_array('administrator', $user->roles)) {
                
            } else {
                $activation_code_is_use = (int) get_user_meta($user->ID, 'activation-code-is-use', true);

                if ($activation_code_is_use == 0) {
                    iws_errors()->add('verify_email', __('Please verify your email address to logged in.'));
                }
            }
        } else {
            // if the user name doesn't exist
            iws_errors()->add('empty_username', __('Invalid username'));
        }





        // retrieve all error messages
        $errors = iws_errors()->get_error_messages();

        // only log the user in if there are no errors
        if (empty($errors)) {



            $creds = array();
            $creds['user_login'] = $_POST['iws_user_login'];
            $creds['user_password'] = $_POST['iws_user_pass'];
            $creds['remember'] = true;
            $user = wp_signon($creds, false);


            if (!is_wp_error($user)) {
                if (!empty($user)) {
                    
                } else {
                    iws_errors()->add('signon_msg', 'Cannot find user account');
                }
            } else {
                $error_string = $user->get_error_message();

                iws_errors()->add('signon_msg', $error_string);
            }



            $errors = iws_errors()->get_error_messages();



            if (empty($errors)) {

                //echo "come for login".date("H:i:s")."<br/>"; die;

                wp_set_auth_cookie($user->ID, true, true);
                wp_set_current_user($user->ID, $_POST['iws_user_login']);
                do_action('wp_login', $_POST['iws_user_login']);

                $after_login_redirect = iws_login_redirect($user);
                //echo "<script>window.location.href='" . $after_login_redirect . "'</script>";
                wp_redirect($after_login_redirect);
                exit;
            }
        }
    }
}

add_action('init', 'iws_login_member');

///////////========================redirect user after login



function iws_login_redirect($user, $redirect_to='', $request='') {

    $final_url = site_url();

    if (is_user_logged_in()) {

        global $current_user;

        $user = $current_user;


        /* echo "<pre>";
          print_r($user); die; */


        if (is_array($user->roles) && in_array('administrator', $user->roles)) {

            $final_url = admin_url();
        } else {
            $final_url = wp_myprofile_url();
            
             
            
        }
    }

    return $final_url;
}

add_filter('login_redirect', 'iws_login_redirect', 10, 3);

//===action hooks==



function go_home() {
    $redirect_link = wp_login_url() . "?msg=logout";
    wp_redirect($redirect_link);
    exit();
}

add_action('wp_logout', 'go_home');

//===end logout
?>