<?php
/*
  Plugin Name: Front End Registration and Login
  Plugin URI:
  Description: Provides simple front end registration and login forms
  Version: 1.0
  Author: iWS
  Author URI:
 */




global $wp_rewrite;
$wp_rewrite = new WP_Rewrite();


// Add rewrite rule and flush on plugin activation
register_activation_hook(__FILE__, 'iWS_activate');

function iWS_activate() {
   // iws_rewrite();
    flush_rewrite_rules();
}

// Flush on plugin deactivation
register_deactivation_hook(__FILE__, 'iWS_deactivate');

function iWS_deactivate() {
    flush_rewrite_rules();
}

// Create new rewrite rule
//add_action( 'init', 'iws_rewrite' );
function iws_rewrite() {
    add_rewrite_rule( 'login/?$', 'wp-login.php', 'top' );
    add_rewrite_rule( 'registration/?$', 'wp-login.php?action=register', 'top' );
    add_rewrite_rule( 'forgot-password/?$', 'wp-login.php?action=lostpassword', 'top' );
}


function app_output_buffer() {
	ob_start();
} // soi_output_buffer
add_action('init', 'app_output_buffer');


//http://wordpress.stackexchange.com/questions/78257/changing-wp-login-url-without-htaccess
//http://www.inkthemes.com/how-to-redirecting-wordpress-default-login-into-a-custom-login-page/
//http://stackoverflow.com/questions/1976781/redirecting-wordpresss-login-register-page-to-a-custom-login-registration-page
/* start  */



//echo "<pre>"; print_r($socialclubhub['register_page_id']); die;
add_filter('register_url', 'custom_register_url');

function custom_register_url($register_url) {

    $socialclubhub = get_option("socialclubhub_theme_config");
    $register_page_id=0;
    if(isset($socialclubhub['register_page_id']) && (int) $socialclubhub['register_page_id']!= 0) { $register_page_id= (int) $socialclubhub['register_page_id']; }
    
    //echo $register_page_id; die;
    
    $register_url = get_permalink($register_page_id);
    return $register_url;
}




add_filter('login_url', 'custom_login_url');
//$force_reauth, $redirect
function custom_login_url() {
    
    
    
    $socialclubhub = get_option("socialclubhub_theme_config");
    $login_page_id=0;
    if(isset($socialclubhub['login_page_id']) && (int) $socialclubhub['login_page_id']!= 0) { $login_page_id= (int) $socialclubhub['login_page_id']; }
    
    $login_url = get_permalink($login_page_id);
    return $login_url;


    /*
      if ( !empty($redirect) )
      $login_url = add_query_arg( 'redirect_to', urlencode( $redirect ), $login_url );
      if ( $force_reauth )
      $login_url = add_query_arg( 'reauth', '1', $login_url ) ;
      return $login_url ;
     */
}

add_filter('lostpassword_url', 'custom_lostpassword_url');

function custom_lostpassword_url($lostpassword_url) {
    
     $socialclubhub = get_option("socialclubhub_theme_config");
    $lostpassword_page_id=0;
    if(isset($socialclubhub['forgotpassword_page_id']) && (int) $socialclubhub['forgotpassword_page_id']!= 0) { $lostpassword_page_id= (int) $socialclubhub['forgotpassword_page_id']; }
   
    
    $lostpassword_url = get_permalink($lostpassword_page_id);
    return $lostpassword_url;
}

function wp_resend_verification_url(){
    
       $socialclubhub = get_option("socialclubhub_theme_config");
    $resendverification_page_id=0;
    if(isset($socialclubhub['resend_verification_page_id']) && (int) $socialclubhub['resend_verification_page_id']!= 0) { $resendverification_page_id= (int) $socialclubhub['resend_verification_page_id']; }
   
    
    
    $resendverification_url = get_permalink($resendverification_page_id);
    return $resendverification_url;
}

function wp_myprofile_url(){
    
    
      $socialclubhub = get_option("socialclubhub_theme_config");
    $myprofile_page_id=0;
    if(isset($socialclubhub['myprofile_page_id']) && (int) $socialclubhub['myprofile_page_id']!= 0) { $myprofile_page_id= (int) $socialclubhub['myprofile_page_id']; }
   
    
    
    $myprofile_url = get_permalink($myprofile_page_id);
    return $myprofile_url;
}


function wp_termscond_url(){
    
    
      $socialclubhub = get_option("socialclubhub_theme_config");
      
    $terms_page_id=0;
    if(isset($socialclubhub['terms_page_id']) && (int) $socialclubhub['terms_page_id']!= 0) { $terms_page_id= (int) $socialclubhub['terms_page_id']; }
   
    
    
    $terms_page_url = get_permalink($terms_page_id);
    return $terms_page_url;
}

/* end */

function encrypt_decrypt($action, $string) {
    $output = false;

    $encrypt_method = "AES-256-CBC";
    $secret_key = 'iWebSol@123';
    $secret_iv = 'iWS';

    // hash
    $key = hash('sha256', $secret_key);

    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);

    if ($action == 'encrypt') {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
    } else if ($action == 'decrypt') {
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }

    return $output;
}

function wp_change_default_email_change_from_email2($email_old) {

    $wcdeOptions = get_option('wp_change_default_email_options');
    if (isset($wcdeOptions['from']) && is_email($wcdeOptions['from'])) {
        return $wcdeOptions['from'];
    } else {
        return $email_old;
    }
}

function wp_change_default_email_change_from_name2($fromname) {

    $wcdeOptions = get_option('wp_change_default_email_options');

    if (!empty($wcdeOptions['fromname'])) {
        return $wcdeOptions['fromname'];
    } else {
        return $fromname;
    }
}


/*check user login */

function check_user_logged_in() {
    if (is_user_logged_in()) {
        return true;
    }
    return false;
}


//====
// used for tracking error messages
function iws_errors() {
    static $wp_error; // Will hold global variable safely
    return isset($wp_error) ? $wp_error : ($wp_error = new WP_Error(null, null, null));
}

// displays error messages from form submissions
function iws_show_error_messages() {
    
    $msg_html='';
    
    if ($codes = iws_errors()->get_error_codes()) {
        $msg_html.= '<div class="iws_error vc_message_box vc_message_box-standard vc_message_box-rounded vc_color-danger">
	<div class="vc_message_box-icon"><i class="fa fa-info-circle"></i></div>';
        // Loop error codes and display errors
        foreach ($codes as $code) {
            $message = iws_errors()->get_error_message($code);
            $msg_html.= '<p><strong>' . __('Error') . '</strong>: ' . $message . '</p>';
        }
        $msg_html.= '</div>';
        
        //info,  warning,        danger,        success
        
        
        
    }
    
    return $msg_html;
}

function iws_show_general_messages(){
    
    $register_success_msg="You have successfully register. Please verify your email address for login.";
    $verify_success_msg ="Your email address is verified successfully. Please take login to your account.";
    $verify_not_match_msg = "Verification code does not match. Please try once again or click to resend verification email.";
    $verify_already_msg = "You have already verify your email address. Please take login to your account.";
    $verify_wrong_msg=$verify_not_match_msg;
    
    $logout_success_msg="You have successfully logged out.";
    
    $passwordrequest_success_msg="New password has been sent to your email address.";
    
    $resendverification_success_msg="New verification email sent to your email address.";
    
    $profileupdated_success_msg="Your profile has been updated successfully.";
    
    $passwordupdated_success_msg="Your password has been updated successfully.";
    
    
    $obj_notowner_msg="You have not right to access this record.";
    $obj_notfound_msg="Record does not exists.";
    
    $clubdelete_success_msg="Club has been deleted successfully.";
    $clubdelete_fail_msg="Unable to delete club. Please try once again.";
    $clubadd_success_msg="New club has been added successfully. Please wait for administrator approval.";
    $clubedit_success_msg="Club data has been updated successfully.";
    
    
    $dealsdiscountdelete_success_msg="Deal has been deleted successfully.";
    $dealsdiscountdelete_fail_msg="Unable to delete deal. Please try once again.";
    $dealsdiscountadd_success_msg="New deal has been added successfully. Please wait for administrator approval.";
    $dealsdiscountedit_success_msg="Deal data has been updated successfully.";
    
    $merchantlinksdelete_success_msg="Merchant has been deleted successfully.";
    $merchantlinksdelete_fail_msg="Unable to delete merchant. Please try once again.";
    $merchantlinksadd_success_msg="New merchant has been added successfully. Please wait for administrator approval.";
    $merchantlinksedit_success_msg="Merchant data has been updated successfully.";
    
    $useradsupdate_success_msg='Ad data has been updated successfully.';
    
    
    $msg_html='';
    
    if(isset($_GET['msg']) && trim($_GET['msg'])!=''){
        
        $msg=trim($_GET['msg']);
        
        //===user ads msg
        
        if($msg=='useradsupdatesuccess'){
             $msg_html.= '<div class="iws_success vc_message_box vc_message_box-standard vc_message_box-rounded vc_color-success">
	<div class="vc_message_box-icon"><i class="fa fa-info-circle"></i></div>';
                $msg_html.= '<p><strong>' . __('Success') . '</strong>: ' .$useradsupdate_success_msg . '</p>';
            $msg_html.= '</div>';
        }
        
        //===merchant links msg
        
        if($msg=='merchantlinksaddsuccess') {
             $msg_html.= '<div class="iws_success vc_message_box vc_message_box-standard vc_message_box-rounded vc_color-success">
	<div class="vc_message_box-icon"><i class="fa fa-info-circle"></i></div>';
                $msg_html.= '<p><strong>' . __('Success') . '</strong>: ' .$merchantlinksadd_success_msg . '</p>';
            $msg_html.= '</div>';
        }
        if($msg=='merchantlinksupdatesuccess') {
             $msg_html.= '<div class="iws_success vc_message_box vc_message_box-standard vc_message_box-rounded vc_color-success">
	<div class="vc_message_box-icon"><i class="fa fa-info-circle"></i></div>';
                $msg_html.= '<p><strong>' . __('Success') . '</strong>: ' .$merchantlinksedit_success_msg . '</p>';
            $msg_html.= '</div>';
        }
        if($msg=='merchantlinksdeletesuccess') {
             $msg_html.= '<div class="iws_success vc_message_box vc_message_box-standard vc_message_box-rounded vc_color-success">
	<div class="vc_message_box-icon"><i class="fa fa-info-circle"></i></div>';
                $msg_html.= '<p><strong>' . __('Success') . '</strong>: ' .$merchantlinksdelete_success_msg . '</p>';
            $msg_html.= '</div>';
        }
        if($msg=='merchantlinksdeletefail') {
            $msg_html.= '<div class="iws_error vc_message_box vc_message_box-standard vc_message_box-rounded vc_color-danger">
	<div class="vc_message_box-icon"><i class="fa fa-info-circle"></i></div>';
                $msg_html.= '<p><strong>' . __('Error') . '</strong>: ' .$merchantlinksdelete_fail_msg . '</p>';
            $msg_html.= '</div>';
        }
        
        
        //===deals disocunts msg
        
        if($msg=='dealsdiscountaddsuccess') {
             $msg_html.= '<div class="iws_success vc_message_box vc_message_box-standard vc_message_box-rounded vc_color-success">
	<div class="vc_message_box-icon"><i class="fa fa-info-circle"></i></div>';
                $msg_html.= '<p><strong>' . __('Success') . '</strong>: ' .$dealsdiscountadd_success_msg . '</p>';
            $msg_html.= '</div>';
        }
        if($msg=='dealsdiscountupdatesuccess') {
             $msg_html.= '<div class="iws_success vc_message_box vc_message_box-standard vc_message_box-rounded vc_color-success">
	<div class="vc_message_box-icon"><i class="fa fa-info-circle"></i></div>';
                $msg_html.= '<p><strong>' . __('Success') . '</strong>: ' .$dealsdiscountedit_success_msg . '</p>';
            $msg_html.= '</div>';
        }
        if($msg=='dealsdiscountdeletesuccess') {
             $msg_html.= '<div class="iws_success vc_message_box vc_message_box-standard vc_message_box-rounded vc_color-success">
	<div class="vc_message_box-icon"><i class="fa fa-info-circle"></i></div>';
                $msg_html.= '<p><strong>' . __('Success') . '</strong>: ' .$dealsdiscountdelete_success_msg . '</p>';
            $msg_html.= '</div>';
        }
        if($msg=='dealsdiscountdeletefail') {
            $msg_html.= '<div class="iws_error vc_message_box vc_message_box-standard vc_message_box-rounded vc_color-danger">
	<div class="vc_message_box-icon"><i class="fa fa-info-circle"></i></div>';
                $msg_html.= '<p><strong>' . __('Error') . '</strong>: ' .$dealsdiscountdelete_fail_msg . '</p>';
            $msg_html.= '</div>';
        }
        
        ////===club msg===
        
        if($msg=='clubaddsuccess') {
             $msg_html.= '<div class="iws_success vc_message_box vc_message_box-standard vc_message_box-rounded vc_color-success">
	<div class="vc_message_box-icon"><i class="fa fa-info-circle"></i></div>';
                $msg_html.= '<p><strong>' . __('Success') . '</strong>: ' .$clubadd_success_msg . '</p>';
            $msg_html.= '</div>';
        }
        if($msg=='clubupdatesuccess') {
             $msg_html.= '<div class="iws_success vc_message_box vc_message_box-standard vc_message_box-rounded vc_color-success">
	<div class="vc_message_box-icon"><i class="fa fa-info-circle"></i></div>';
                $msg_html.= '<p><strong>' . __('Success') . '</strong>: ' .$clubedit_success_msg . '</p>';
            $msg_html.= '</div>';
        }
        if($msg=='clubdeletesuccess') {
             $msg_html.= '<div class="iws_success vc_message_box vc_message_box-standard vc_message_box-rounded vc_color-success">
	<div class="vc_message_box-icon"><i class="fa fa-info-circle"></i></div>';
                $msg_html.= '<p><strong>' . __('Success') . '</strong>: ' .$clubdelete_success_msg . '</p>';
            $msg_html.= '</div>';
        }
        if($msg=='clubdeletefail') {
            $msg_html.= '<div class="iws_error vc_message_box vc_message_box-standard vc_message_box-rounded vc_color-danger">
	<div class="vc_message_box-icon"><i class="fa fa-info-circle"></i></div>';
                $msg_html.= '<p><strong>' . __('Error') . '</strong>: ' .$clubdelete_fail_msg . '</p>';
            $msg_html.= '</div>';
        }
        
        
        //==general message 
        
        
        if($msg=='notowner') {
            $msg_html.= '<div class="iws_error vc_message_box vc_message_box-standard vc_message_box-rounded vc_color-danger">
	<div class="vc_message_box-icon"><i class="fa fa-info-circle"></i></div>';
                $msg_html.= '<p><strong>' . __('Error') . '</strong>: ' .$obj_notowner_msg . '</p>';
            $msg_html.= '</div>';
        }
        
        if($msg=='notfound') {
            $msg_html.= '<div class="iws_error vc_message_box vc_message_box-standard vc_message_box-rounded vc_color-danger">
	<div class="vc_message_box-icon"><i class="fa fa-info-circle"></i></div>';
                $msg_html.= '<p><strong>' . __('Error') . '</strong>: ' .$obj_notfound_msg . '</p>';
            $msg_html.= '</div>';
        }
        
        if($msg=='passwordupdated') {
             $msg_html.= '<div class="iws_success vc_message_box vc_message_box-standard vc_message_box-rounded vc_color-success">
	<div class="vc_message_box-icon"><i class="fa fa-info-circle"></i></div>';
                $msg_html.= '<p><strong>' . __('Success') . '</strong>: ' .$passwordupdated_success_msg . '</p>';
            $msg_html.= '</div>';
        }
        
        if($msg=='profileupdated') {
             $msg_html.= '<div class="iws_success vc_message_box vc_message_box-standard vc_message_box-rounded vc_color-success">
	<div class="vc_message_box-icon"><i class="fa fa-info-circle"></i></div>';
                $msg_html.= '<p><strong>' . __('Success') . '</strong>: ' .$profileupdated_success_msg . '</p>';
            $msg_html.= '</div>';
        }
        
        if($msg=='resendverification') {
            $msg_html.= '<div class="iws_success vc_message_box vc_message_box-standard vc_message_box-rounded vc_color-success">
	<div class="vc_message_box-icon"><i class="fa fa-info-circle"></i></div>';
                $msg_html.= '<p><strong>' . __('Success') . '</strong>: ' .$resendverification_success_msg . '</p>';
            $msg_html.= '</div>';
        }
        
        if($msg=='passwordrequest') {
             $msg_html.= '<div class="iws_success vc_message_box vc_message_box-standard vc_message_box-rounded vc_color-success">
	<div class="vc_message_box-icon"><i class="fa fa-info-circle"></i></div>';
                $msg_html.= '<p><strong>' . __('Success') . '</strong>: ' .$passwordrequest_success_msg . '</p>';
            $msg_html.= '</div>';
        }
        
        if($msg=='logout') {
             $msg_html.= '<div class="iws_success vc_message_box vc_message_box-standard vc_message_box-rounded vc_color-success">
	<div class="vc_message_box-icon"><i class="fa fa-info-circle"></i></div>';
                $msg_html.= '<p><strong>' . __('Success') . '</strong>: ' .$logout_success_msg . '</p>';
            $msg_html.= '</div>';
        }
        
        if($msg=='verifylink') {
             $msg_html.= '<div class="iws_success vc_message_box vc_message_box-standard vc_message_box-rounded vc_color-success">
	<div class="vc_message_box-icon"><i class="fa fa-info-circle"></i></div>';
                $msg_html.= '<p><strong>' . __('Success') . '</strong>: ' .$register_success_msg . '</p>';
            $msg_html.= '</div>';
        }
        
        if($msg=='verifysuccess') {
            $msg_html.= '<div class="iws_success vc_message_box vc_message_box-standard vc_message_box-rounded vc_color-success">
	<div class="vc_message_box-icon"><i class="fa fa-info-circle"></i></div>';
                $msg_html.= '<p><strong>' . __('Success') . '</strong>: ' .$verify_success_msg . '</p>';
            $msg_html.= '</div>';
        }
        
        if($msg=='verifynotmatch') {
            $msg_html.= '<div class="iws_error vc_message_box vc_message_box-standard vc_message_box-rounded vc_color-danger">
	<div class="vc_message_box-icon"><i class="fa fa-info-circle"></i></div>';
                $msg_html.= '<p><strong>' . __('Error') . '</strong>: ' .$verify_not_match_msg . '</p>';
            $msg_html.= '</div>';
        }
        
        if($msg=='verifyalready') {
            $msg_html.= '<div class="iws_error vc_message_box vc_message_box-standard vc_message_box-rounded vc_color-warning">
	<div class="vc_message_box-icon"><i class="fa fa-info-circle"></i></div>';
                $msg_html.= '<p><strong>' . __('Error') . '</strong>: ' .$verify_already_msg . '</p>';
            $msg_html.= '</div>';
        }
        
        if($msg=='verifywrong') {
            $msg_html.= '<div class="iws_error vc_message_box vc_message_box-standard vc_message_box-rounded vc_color-danger">
	<div class="vc_message_box-icon"><i class="fa fa-info-circle"></i></div>';
                $msg_html.= '<p><strong>' . __('Error') . '</strong>: ' .$verify_wrong_msg . '</p>';
            $msg_html.= '</div>';
        }
        
    }
    
    return $msg_html;
}


require_once 'iwsupload.php';
require_once 'verification.php';
require_once 'login.php';
require_once 'register.php';
require_once 'forgotpassword.php';
require_once 'resendverification.php';
require_once 'myprofile.php';
require_once 'club-signup.php';
require_once 'admin_user_profile.php';
require_once 'changepassword.php';
//require_once 'user_image.php';


//////////////=========remove admin bar from front

function remove_admin_bar() {
    if (!current_user_can('administrator') && !is_admin()) {
        add_action('admin_print_scripts-profile.php', 'iws_hide_admin_bar_settings');
        add_filter('show_admin_bar', '__return_false');
    }
}

function iws_hide_admin_bar_settings() {
    ?>
    <style type="text/css">
        .show-admin-bar {
            display: none !important;
        }
        html { margin-top: 0px !important; }
        * html body { margin-top: 0px !important; }
    </style>
    <?php
}

add_action('init', 'remove_admin_bar',1);
//////////===========block user from accessing the admin side

// add_action('admin_init', 'no_mo_dashboard');

  function no_mo_dashboard() {
  if (!current_user_can('manage_options') && $_SERVER['DOING_AJAX'] != '/wp-admin/admin-ajax.php') {
  wp_redirect(home_url());
  exit;
  }
  } 

add_action('init', 'blockusers_init');

function blockusers_init() {
    if (is_admin() && !current_user_can('administrator') && !(defined('DOING_AJAX') && DOING_AJAX)) {
        
        //echo "block users";
        wp_redirect(home_url());
        exit;
    }
}


// register our form css
function iws_register_css() {
    wp_register_style('iws-form-css', plugin_dir_url(__FILE__) . '/css/forms.css');
}

add_action('init', 'iws_register_css');

// load our form css
function iws_print_css() {
    global $iws_load_css;

    // this variable is set to TRUE if the short code is used on a page/post
    if (!$iws_load_css)
        return; // this means that neither short code is present, so we get out of here

    wp_print_styles('iws-form-css');
}

add_action('wp_footer', 'iws_print_css');
?>
