<?php

// user registration login form
function iws_registration_form() {
 
	// only show the registration form to non-logged-in members
	if(!is_user_logged_in()) {
 
		global $iws_load_css;
 
		// set this to true so the CSS is loaded
		$iws_load_css = true;
 
		// check to make sure user registration is enabled
		$registration_enabled = get_option('users_can_register');
 
		// only show the registration form if allowed
		if($registration_enabled) {
			$output = iws_registration_form_fields();
		} else {
			$output = __('User registration is not enabled');
		}
		return $output;
	} else {
             //echo "<script>window.location.href='".get_permalink(199)."'</script>";
             wp_redirect(wp_myprofile_url());
             
                 die;
        }
}
add_shortcode('register_form', 'iws_registration_form');


// registration form fields
function iws_registration_form_fields() {
 
	ob_start(); 
        
        $user_country_id=0;
     if(isset($_POST['user_country_id']) && (int) $_POST['user_country_id']>0){
         $user_country_id=(int) $_POST['user_country_id'];
     }
     $user_state_id=0;
     if(isset($_POST['user_state_id']) && (int) $_POST['user_state_id']>0){
         $user_state_id=(int) $_POST['user_state_id'];
     }
     $user_city_id=0;
     if(isset($_POST['user_city_id']) && (int) $_POST['user_city_id']>0){
         $user_city_id=(int) $_POST['user_city_id'];
     }
     
     
     
     
       
    $res_countries = get_all_country();

    $res_states = array();
    if ($user_country_id > 0) {
        
        $res_states = get_states_from_country_by_id($user_country_id);
    }

    $res_cities = array();
    if ($user_state_id > 0) {
        
        $res_cities = get_cities_from_state_by_id($user_state_id);
    }
    
    ?>	
		
 
		<?php 
		// show any error messages after form submission
		echo iws_show_error_messages(); ?>
 
		<form id="iws_registration_form" class="form-horizontal" action="<?php echo esc_url(wp_registration_url()); ?>" method="POST">
		
                    
                    
                    <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('First Name'); ?></label>
            <div class="col-sm-8">
                <input name="iws_user_first" id="iws_user_first" type="text" value="<?php if(isset($_POST['iws_user_first'])) {echo $_POST['iws_user_first']; } ?>" class="form-control" />
				</div>
                </div>
				<div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Last Name'); ?></label>
            <div class="col-sm-8">
                <input name="iws_user_last" id="iws_user_last" type="text" value="<?php if(isset($_POST['iws_user_last'])) {echo $_POST['iws_user_last']; } ?>" class="form-control" />
				</div>
                </div>
                    
                    
                    
                    
                     <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e("Country"); ?></label>
            <div class="col-sm-8">
                <select name="user_country_id" id="user_country_id" class="form-control" >
            <option value="0">---Select Country---</option>   
    <?php if (!empty($res_countries)) {
        foreach ($res_countries as $row_country) { ?>

                    <option value="<?php echo $row_country->countryid; ?>" <?php if ($row_country->countryid == $user_country_id) { ?> selected <?php } ?>><?php echo ucfirst($row_country->country); ?></option>

        <?php }
    } ?>

        </select> 
        </div>
                </div>
                    
        
        
         
            <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e("State"); ?></label>
            <div class="col-sm-8">
                 <select name="user_state_id" id="user_state_id" class="form-control" >
            <option value="0">---Select State---</option>

    <?php
    if (!empty($res_states)) {
        foreach ($res_states as $row_states) {
            ?>

                    <option value="<?php echo $row_states->regionid; ?>" <?php if ($row_states->regionid == $user_state_id) { ?> selected <?php } ?>><?php echo ucfirst($row_states->region); ?></option>


                <?php
                }
            }
            ?>

        </select> 
            </div>
                </div>
                    
        
        
        
        
         
            <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e("City"); ?></label>
            <div class="col-sm-8">
                 <select name="user_city_id" id="user_city_id" class="form-control" >
               <option value="0">---Select City---</option>

            <?php
            if (!empty($res_cities)) {
                foreach ($res_cities as $row_cities) {
                    ?>

                    <option value="<?php echo $row_cities->CityId; ?>" data-lat="<?php echo $row_cities->Latitude; ?>" data-long="<?php echo $row_cities->Longitude; ?>" <?php if ($row_cities->CityId == $user_city_id) { ?> selected <?php } ?>><?php echo ucfirst($row_cities->City); ?></option>


                <?php }
            } ?>


        </select> 
                
                
    <input type="hidden" name="user_lat" id="user_lat" value="<?php if(isset($_POST['user_lat'])) { echo $_POST['user_lat']; } ?>" />
    <input type="hidden" name="user_long" id="user_long" value="<?php if(isset($_POST['user_long'])) { echo $_POST['user_long']; } ?>" />
            
        </div>
                </div>
                    
        
        
        
        
        
            <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e("Zip Code"); ?></label>
            <div class="col-sm-8">
                <input type="text" name="user_zip_code" id="user_zip_code" value="<?php  if(isset($_POST['user_zip_code'])) { echo $_POST['user_zip_code']; } ?>" class="form-control" />
        </div>
                </div>
                    
	
                    
                   
				<div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Email Address'); ?></label>
            <div class="col-sm-8">
					<input name="iws_user_email" id="iws_user_email" value="<?php if(isset($_POST['iws_user_email'])) {echo $_POST['iws_user_email']; } ?>" class="form-control" type="email"/>
				</div>
                </div>
                    
                     <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('User Name'); ?></label>
             <div class="col-sm-8">
					<input name="iws_user_login" id="iws_user_login" value="<?php if(isset($_POST['iws_user_login'])) {echo $_POST['iws_user_login']; } ?>" class="form-control" type="text"/>
                    </div>
                </div>
                    
				
				<div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Password'); ?></label>
            <div class="col-sm-8">
					<input name="iws_user_pass" id="password" class="form-control" type="password"/>
				</div>
                </div>
				<div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Repeat Password'); ?></label>
            <div class="col-sm-8">
					<input name="iws_user_pass_confirm" id="password_again" class="form-control" type="password"/>
				</div>
                </div>
                                
            <div class="form-group">

                                    <div class="col-sm-offset-4 col-sm-8">

                                      <div class="checkbox">

                                        <label>
                                            <input type="checkbox" value="1" name="iws_agree_on_terms" id="iws_agree_on_terms" /> I have read and agreed to the <a href="<?php echo esc_url(wp_termscond_url()); ?>" target="_blank">Terms & Conditions</a>.
                                </label>

                                      </div>

                                    </div>

                                  </div>
        
           <div class="form-group">

                                   <div class="col-sm-offset-4 col-sm-8">
					<input type="hidden" name="iws_register_nonce" value="<?php echo wp_create_nonce('iws-register-nonce'); ?>"/>
					
				
                                        <button id="iws_register_submit" type="submit" class="btn btn-default sign-btn">Register Your Account</button>
                                
                                        <p id="nav">
            <a href="<?php echo esc_url( wp_login_url() ); ?>"><?php _e( 'Log in' ); ?></a> |
            <a href="<?php echo esc_url( wp_lostpassword_url() ); ?>" title="<?php esc_attr_e( 'Password Lost and Found' ) ?>"><?php _e( 'Lost your password?' ); ?></a> |
            <a href="<?php echo esc_url(wp_resend_verification_url()); ?>" title="<?php esc_attr_e('Resend Confirmation Link'); ?>"><?php _e('Resend Confirmation Link'); ?></a>
        </p>

        </div>

                                  </div>

			
		</form>
	<?php
	return ob_get_clean();
}



// register a new user
function iws_add_new_member() {
  	if (isset( $_POST["iws_user_login"] ) && isset($_POST['iws_register_nonce']) && wp_verify_nonce($_POST['iws_register_nonce'], 'iws-register-nonce')) {
		$user_login		= $_POST["iws_user_login"];	
		$user_email		= $_POST["iws_user_email"];
		$user_first 	= $_POST["iws_user_first"];
		$user_last	 	= $_POST["iws_user_last"];
		$user_pass		= $_POST["iws_user_pass"];
		$pass_confirm 	= $_POST["iws_user_pass_confirm"];
 
		
                
                if ( 4 > strlen( $user_login ) ) {
                    iws_errors()->add( 'username_length',__('Username too short. At least 4 characters is required' ));
                }
 
		if(username_exists($user_login)) {
			// Username already registered
			iws_errors()->add('username_unavailable', __('Username already taken'));
		}
		if(!validate_username($user_login)) {
			// invalid username
			iws_errors()->add('username_invalid', __('Invalid username'));
		}
		if($user_login == '') {
			// empty username
			iws_errors()->add('username_empty', __('Please enter a username'));
		}
		if(!is_email($user_email)) {
			//invalid email
			iws_errors()->add('email_invalid', __('Invalid email'));
		}
		if(email_exists($user_email)) {
			//Email address already registered
			iws_errors()->add('email_used', __('Email already registered'));
		}
                
                if ( 7 > strlen( $user_pass ) ) {
                      iws_errors()->add( 'password_length', 'Password length at leaset 8 character required' );
                }
                
		if($user_pass == '') {
			// passwords do not match
			iws_errors()->add('password_empty', __('Please enter a password'));
		}
		if($user_pass != $pass_confirm) {
			// passwords do not match
			iws_errors()->add('password_mismatch', __('Passwords do not match'));
		}
                
                if(isset($_POST['iws_agree_on_terms']) && (int) $_POST['iws_agree_on_terms']==(int) 1){
                    
                } else {
                    	iws_errors()->add('agree_on_terms', __('Please select agree on terms and conditions.'));
                }
                
 
		$errors = iws_errors()->get_error_messages();
 
		// only create the user in if there are no errors
		if(empty($errors)) {
                        
                        
                        
			$new_user_id = wp_insert_user(array(
					'user_login'		=> $user_login,
					'user_pass'	 		=> $user_pass,
					'user_email'		=> $user_email,
					'first_name'		=> $user_first,
					'last_name'			=> $user_last,
					'user_registered'	=> date('Y-m-d H:i:s'),
					'role'				=> 'customer',
                           
				)
			);
			if($new_user_id) {
			
                            
                            
                            
                            
                            
    $user_country_id = (int) sanitize_text_field($_POST['user_country_id']);
    update_user_meta($new_user_id, 'user_country_id', esc_attr($user_country_id));
    
    $user_state_id = (int) sanitize_text_field($_POST['user_state_id']);
    update_user_meta($new_user_id, 'user_state_id', esc_attr($user_state_id));
    
    $user_city_id = (int) sanitize_text_field($_POST['user_city_id']);
    update_user_meta($new_user_id, 'user_city_id', esc_attr($user_city_id));
    
    
    $user_zip_code = sanitize_text_field($_POST['user_zip_code']);
    update_user_meta($new_user_id, 'user_zip_code', esc_attr($user_zip_code));
    
    $user_lat = sanitize_text_field($_POST['user_lat']);
    update_user_meta($new_user_id, 'user_lat', esc_attr($user_lat));
    
    $user_long = sanitize_text_field($_POST['user_long']);
    update_user_meta($new_user_id, 'user_long', esc_attr($user_long));
    
                            


                                // send an email to the admin alerting them of the registration
				//wp_new_user_notification($new_user_id);
                                
                
                            
                            
                            
                                
                                // generate activation code
                        $activation_code = generate_random_code( 12 );
                        
                        update_user_meta($new_user_id, 'activation-code', $activation_code);
                        update_user_meta($new_user_id, 'activation-code-is-use', 0);
                                
                        
                        $url_activation_code=encrypt_decrypt('encrypt', $new_user_id.','.$activation_code);
                        
                        

      $message='';                          
                                
//$message .= sprintf(__('Username: %s', 'user-activation-email'), $user_login) . "\r\n"; 
  //   	$message .= sprintf(__('Password: %s', 'user-activation-email'), $user_pass) . "\r\n\n";
     	///////$message .= sprintf(__('Activation Code: %s', 'user-activation-email'), $activation_code) . "\r\n\n"; 
    // 	$message .= wp_login_url().'?vky='.urlencode( $url_activation_code )."\r\n"; 

		$message = apply_filters('iws_user_message', $message, $user_login, $user_email, $user_pass, $activation_code);
		
		//$subject = apply_filters('iws_user_message_subject', __('Your username and password', 'user-activation-email'), $user_login, $user_email, $user_pass, $activation_code);
                  
                $subject="www.socialclubhub.com - Registration";
                
                
                
               $message.= 'Hello '.$user_first.' '.$user_last.',<br/><br/>';

$message.= 'Welcome to the Social Club Hub your destination for all of your club needs!<br/><br/>';

$message.= 'This is confirmation that your account in SocialClubHub.com has just been created.<br/><br/>';

$message.= 'This e-mail contains important information regarding your new account.  Please save it for future reference.<br/><br/>';

$message.= 'Here are your account login details:<br/>';
$message.= 'Login: <b>'. $user_login.'</b><br/>';
$message.= 'Password: <b>'.$user_pass.'</b><br/><br/>';

$message.= 'Please click or copy-paste below link to verify your account.<br/><br/>';
$message.=wp_login_url().'?vky='.urlencode( $url_activation_code );


$message.= '<br/><br/>You can now manage all of your events, conduct live chat with our merchants, and browse the different clubs available at Social Club Hub. You can modify your profile information after you have logged in to your account.<br/><br/>';

$message.= 'To access your account, simply click <a href="'.wp_login_url().'">here</a> to log in.<br/><br/>';

$message.= 'If you have problems accessing the site or with your account, please feel free to contact us through info@socialclubhub.com<br/><br/>';

$message.= 'We wish you a great experience getting out to club events, having fun and meeting new people!<br/><br/>';

$message.= 'Best regards,<br/><br/>';

$message.= '<b>Social Club Hub Team</b><br/>';
$message.= '<b>SocialClubHub.com</b><br/><br/>';

$message.= '-------------------------------------------------------------------------------------------------------------------------------------<br/>';
$message.= '*** Please DO NOT REPLY to this e-mail; it is not monitored for replies ***<br/>';
$message.= '-------------------------------------------------------------------------------------------------------------------------------------';
                
                
               
                
                add_filter('wp_mail_from', 'wp_change_default_email_change_from_email2');
                add_filter('wp_mail_from_name', 'wp_change_default_email_change_from_name2');

                
		
                $headers = array('Content-Type: text/html; charset=UTF-8');
                
		wp_mail($user_email, $subject, $message,$headers);	                                
                                
                                
                                
                                
                                
                                
                                
                                
				// remove below comment for log the new user in  
				//wp_setcookie($user_login, $user_pass, true);
				//wp_set_current_user($new_user_id, $user_login);	
				//do_action('wp_login', $user_login);
                                
                                
				// send the newly created user to the home page after logging them in
				//wp_redirect(home_url()); exit;
                
                $login_url=wp_login_url().'?msg=verifylink';
                                wp_redirect($login_url); exit;
                                
			}
 
		}
 
	}
}
add_action('init', 'iws_add_new_member');



/**
 * Add script to the page
 */
add_action('wp_enqueue_scripts', 'user_register_script_to_front');

function user_register_script_to_front($page) {
    
      
        wp_register_script('front_user_profile_edit_js',  plugins_url('/js/front_user_profile.js', __FILE__), array('jquery'));
        
        wp_localize_script( 'front_user_profile_edit_js', 'ProfileAJ', array(            
            'nonce'      => wp_create_nonce( 'iws_nonce' ),
            'ajaxurl'    => admin_url( 'admin-ajax.php' ),            
        ));





// Enqueue Scripts that are needed on all the pages
            wp_enqueue_script('jquery');
            wp_enqueue_script('front_user_profile_edit_js');
        
 
}

?>