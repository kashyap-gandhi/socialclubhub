<?php

// user registration login form
function iws_changepassword_form() {
 
	if(is_user_logged_in()) {
 
		
		$output = iws_changepassword_form_fields();
		
		return $output;
	} else {
             $redirect_link = wp_login_url();
                wp_redirect($redirect_link);
                exit;
        }
}
add_shortcode('changepassword_form', 'iws_changepassword_form');


// registration form fields
function iws_changepassword_form_fields() {
 
	ob_start(); ?>	
		
 
		<?php 
		// show any error messages after form submission
		echo iws_show_error_messages();
               echo iws_show_general_messages();
                ?>
 
		<form id="iws_registration_form" class="form-horizontal" action="" method="POST">
			
        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Current Password'); ?></label>
            <div class="col-sm-8">
                <input name="iws_user_current_pass" id="iws_user_current_pass" type="password" class="form-control" />
				 </div>
        </div>

				
        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('New Password'); ?></label>
            <div class="col-sm-8">
					<input name="iws_user_pass" id="iws_user_pass" class="form-control" type="password"/>
				 </div>
        </div>

				
        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('New Password Again'); ?></label>
            <div class="col-sm-8">
					<input name="iws_user_pass_confirm" id="iws_user_pass_confirm" class="form-control" type="password"/>
				 </div>
        </div>

                                
				
        <div class="form-group">

            <div class="col-sm-offset-4 col-sm-8">
					<input type="hidden" name="iws_changepassword_nonce" value="<?php echo wp_create_nonce('iws-changepassword-nonce'); ?>"/>
					
				 <button id="iws_changepassword_submit" type="submit" class="btn btn-default sign-btn">Update</button>
            </div>

        </div>
		</form>
	<?php
	return ob_get_clean();
}



// register a new user
function iws_changepassword_member() {
  	if (isset( $_POST["iws_user_current_pass"] ) && isset($_POST['iws_changepassword_nonce']) && wp_verify_nonce($_POST['iws_changepassword_nonce'], 'iws-changepassword-nonce')) {
		
                
            global $current_user;
              
              $user=$current_user;
              
              $user_id=$user->ID;
              
              
                $user_current_pass = trim($_POST["iws_user_current_pass"]);
		$user_pass = trim($_POST["iws_user_pass"]);
		$pass_confirm = trim($_POST["iws_user_pass_confirm"]);
 
		
                
                
                if($user_current_pass == '') {
			// passwords do not match
			iws_errors()->add('current_password_empty', __('Please enter a current password'));
		} else {
                    if ( 7 > strlen( $user_current_pass ) ) {
                          iws_errors()->add( 'current_password_length', 'Current password length at leaset 8 character required' );
                    } else {

                        if (!wp_check_password($user_current_pass, $user->user_pass, $user_id)) {
                            // if the password is incorrect for the specified user
                            iws_errors()->add('current_password_does_not_match', __('Current password is does not match with existing password.'));
                        }
                    }
                }
                
                
		if($user_pass == '') {
			// passwords do not match
			iws_errors()->add('password_empty', __('Please enter a new password'));
		} else {
                    if ( 7 > strlen( $user_pass ) ) {
                          iws_errors()->add( 'password_length', 'New Password length at leaset 8 character required' );
                    } else {
                
                        if($user_pass != $pass_confirm) {
                                // passwords do not match
                                iws_errors()->add('password_mismatch', __('Passwords do not match'));
                        }
                    }
                }
                
                
 
		$errors = iws_errors()->get_error_messages();
 
		// only create the user in if there are no errors
		if(empty($errors)) {
                        
                        
                        //wp_set_password($user_pass,$user_id);
                          
                        wp_update_user(array('ID' => $user_id, 'user_pass' => $user_pass));



//wp_signon(array('user_login' => $user->user_login, 'user_password' => $user_pass));
                                
                                
                                
                
                                $redirect_url=current_page_url().'?msg=passwordupdated';
                                wp_redirect($redirect_url); exit;
                                
			
 
		}
 
	}
}
add_action('init', 'iws_changepassword_member');



?>