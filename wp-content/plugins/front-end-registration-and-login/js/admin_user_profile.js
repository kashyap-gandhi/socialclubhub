jQuery(document).ready(function(){
				
	jQuery('#user_country_id').change(function(e) {
		var data = {
			'action': 'get_states_of_country',
			'country': jQuery(this).val()
		};
		
		var state_ops = '<option value="">---Select State---</option>';
		var city_ops = '<option value="">---Select City---</option>';
		
		jQuery("#user_state_id").html(state_ops);
		jQuery("#user_city_id").html(city_ops);

		// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
		jQuery.post(ajaxurl, data, function(response) {
			if(jQuery("#user_state_id").length>0)
			{
				jQuery("#user_state_id").html(response);
			}
		});
    });
	
	jQuery('#user_state_id').change(function(e) {
		var data = {
			'action': 'get_cities_of_state',
			'state': jQuery(this).val()
			
		};
		
		
		var city_ops = '<option value="">---Select City---</option>';
		
		jQuery("#user_city_id").html(city_ops);

		// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
		jQuery.post(ajaxurl, data, function(response) {
			if(jQuery("#user_city_id").length>0)
			{
				jQuery("#user_city_id").html(response);
			}
		});
    });
	
	jQuery('#user_city_id').change(function(e) {
		var vlat=jQuery("#user_city_id option:selected").attr('data-lat');
		var vlong=jQuery("#user_city_id option:selected").attr('data-long');
		
		jQuery("#user_lat").val(vlat);
		jQuery("#user_long").val(vlong);
		
	});
	
});