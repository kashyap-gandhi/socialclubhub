<?php
 
 
// Directory for uploaded images 
$uploaddir = ABSPATH . 'wp-content/uploads/userpic';  

// Allowed mimes    
$allowed_ext = "jpg, gif, png";  

// Default is 50kb 
$max_size = get_option('iws_size');  
$max_size='10000kb';

// height in pixels, default is 175px 
$max_height = get_option('iws_height');  
$max_height = '1000px';

// width in pixels, default is 450px 
$max_width = get_option('iws_width');  
$max_width="1000px";



if (isset( $_POST["iws_user_login"] ) && isset($_POST['iws_myprofile_nonce']) && wp_verify_nonce($_POST['iws_myprofile_nonce'], 'iws-myprofile-nonce')) {
// Check mime types are allowed  
$extension = pathinfo($_FILES['iwsuploader']['name']);  


$extension = $extension['extension'];  
$allowed_paths = explode(", ", $allowed_ext);  
for($i = 0; $i < count($allowed_paths); $i++) {  
if ($allowed_paths[$i] == "$extension") {  
    $ok = "1";  
}  
}  




// Check File Size  
if ($ok == "1") {  
if($_FILES['iwsuploader']['size'] > $max_size)  
{  
    print "Image size is too big!";  
    exit;  
}  

// Check Height & Width  
if ($max_width && $max_height) {  
    list($width, $height, $type, $w) = getimagesize($_FILES['iwsuploader']['tmp_name']);  
    if($width > $max_width || $height > $max_height)  
    {  
        print "Image is too big! max allowable width is&nbsp;" . get_option(iws_width) ."px and max allowable height is&nbsp;" . get_option(iws_width) ."px";  
        exit;  
    }  
}  
global $user_id;
get_currentuserinfo();
$image_name=$current_user->id.'.'.$extension;
//the new name will be containing the full path where will be stored (images folder)

// Rename file and move to folder
$newname="$uploaddir./".$image_name;  
if(is_uploaded_file($_FILES['iwsuploader']['tmp_name']))  
{ 
    move_uploaded_file($_FILES['iwsuploader']['tmp_name'], $newname);  
}  
print "Your image has been uploaded successfully!";  
}

}

// Create shortcode for adding to edit user page
add_shortcode("wp-author-logo", "iwsuploader_input");

function iwsuploader_input() {
$iwsuploader_output = iwsuploader_showform();
return $iwsuploader_output;
}

function iwsuploader_showform() {
$iwsuploader_output = '<p><label for="wpauploader">Upload Pic:</label><br />
<input type="file" name="iwsuploader" id="iwsuploader" />
<br />
<small style="color:#ff0000; font-size:0.7em;">Allowed image types are .jpg, .gif, .png.<br />
Max image width = ' . get_option('iws_width') . 'px, Max image height = ' . get_option('iws_height') . 'px </small>';
return $iwsuploader_output;
}

add_action('admin_menu', 'iws_menu');

function iws_menu() {
add_options_page('WP Author Logo', 'WP Author Logo', 'manage_options', 'iws_wp-author-logo', 'wpal');
}

function wpal() {
if (!current_user_can('manage_options'))  {
    wp_die( __('You do not have sufficient permissions to access this page.') ); 
}
?>   
<div class="wrap">
<div class="leftwrap">
    <?php    echo "<h2>" . __( 'Wordpress Author Logo Plugin', 'iws_lang' ) . "</h2>"; ?>

    <?php  
        if($_POST['iws_author_logo_success'] == 'Y') {  
            //Form data sent  
            $iws_width = $_POST['iws_width'];  
            update_option('iws_width', $iws_width);  

            $iws_height = $_POST['iws_height'];  
            update_option('iws_height', $iws_height);

            $iws_size = $_POST['iws_size'];  
            update_option('iws_size', $iws_size);    

            $iws_logos = $_POST['iws_logos'];  
            update_option('iws_logos', $iws_logos); 
        ?>  
        <div class="updated"><p><strong><?php _e('WP Author Logo Plugin Options Saved.' ); ?></strong></p></div>  
        <?php  
        } else {  
            //Normal page display  
            $iws_width = get_option('iws_width');  
            $iws_height = get_option('iws_height');
            $iws_size = get_option('iws_size');  
            $iws_logos = get_option('iws_logos');    
        }  
    ?>    

    <form name="iws_settingsform" method="post" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>">  
        <input type="hidden" name="iws_author_logo_success" value="Y">  
        <?php    echo "<h4>" . __( 'Wordpress Author Logo Settings', 'iws_lang' ) . "</h4>"; ?>  
        <p><label for="iws_width"><?php _e("Maximum Width: " ); ?></label><br /><input type="text" name="iws_width" value="<?php echo $iws_width; ?>" size="20"><?php _e("px"); ?></p>  
        <p><label for="iws_height"><?php _e("Maximum Height: " ); ?></label><br /><input type="text" name="iws_height" value="<?php echo $iws_height; ?>" size="20"><?php _e("px" ); ?></p>
        <p><label for="iws_size"><?php _e("Maximum Size: " ); ?></label><br /><input type="text" name="iws_size" value="<?php echo $iws_size; ?>" size="20"><?php _e("Bytes: hint 50000 bytes = 50Kbs" ); ?></p>  
        <p><label for="iws_logos"><?php _e("Logo Images Folder: " ); ?></label><br /><input type="text" name="iws_logos" value="<?php echo $iws_logos; ?>" size="20"><?php _e(" ex: /iws_logo_images/" ); ?></p>  

        <p class="submit">  
            <input type="submit" name="Submit" value="<?php _e('Update Options', 'iws_lang' ) ?>" />  
        </p>  
    </form>
</div><!-- / leftwrap -->
</div><!-- / wrap -->
<?php }   

       