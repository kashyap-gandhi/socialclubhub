<?php

// user login form
function iws_forgotpassword_form() {


    $output = '';

    if (!is_user_logged_in()) {

        global $iws_load_css;

        // set this to true so the CSS is loaded
        $iws_load_css = true;

        $output = iws_forgotpassword_form_fields();
    } else {
        // could show some logged in user info here
        // $output = 'user info here';
        //wp_redirect(get_permalink(199)); exit;


        global $current_user;

        $after_login_redirect = iws_login_redirect($current_user);
        // echo "<script>window.location.href='" . home_url() . "'</script>";
        wp_redirect($after_login_redirect);
        //echo "dfdfdfd"; die;
        //wp_redirect( home_url() );

        exit;
    }
    return $output;
}

add_shortcode('forgotpassword_form', 'iws_forgotpassword_form');

// login form fields
function iws_forgotpassword_form_fields() {

    ob_start();
    ?>
    

    <?php
    // show any error messages after form submission
    echo iws_show_error_messages();
    echo iws_show_general_messages();
    ?>

    <form id="iws_fogotpassword_form"  class="form-horizontal" action="<?php echo esc_url(wp_lostpassword_url()); ?>" method="post">
        <div class="form-group">
            <label for="" class="col-sm-4 control-label">User Name</label>
             <div class="col-sm-8">
                <input name="iws_user_login" id="iws_user_login" value="<?php if (isset($_POST['iws_user_login'])) {
        echo $_POST['iws_user_login'];
    } ?>" class="form-control" type="text"/>
             </div>
        </div>
          <div class="form-group">

                                    <div class="col-sm-offset-4 col-sm-8 text-center">
                <input type="hidden" name="iws_forgotpassword_nonce" value="<?php echo wp_create_nonce('iws-forgotpassword-nonce'); ?>"/>
                 <button type="submit" id="iws_forgotpassword_submit" class="btn btn-default sign-btn">Send</button><br>
                
           


            <p id="nav">

<!--                <a href="<?php //echo esc_url(wp_registration_url()); ?>">Register</a> |-->

                <a href="<?php echo esc_url(wp_login_url()); ?>"><?php echo 'Login'; ?></a> |
                <a href="<?php echo esc_url(wp_resend_verification_url()); ?>" title="<?php esc_attr_e('Resend Confirmation Link'); ?>"><?php echo 'Resend Confirmation Link'; ?></a>


            </p>

  </div>

                                  </div>


            

        
    </form>
    <?php
    return ob_get_clean();
}

// logs a member in after submitting a form
function iws_forgotpassword_member() {

    if (isset($_POST['iws_user_login']) && isset($_POST['iws_forgotpassword_nonce']) && wp_verify_nonce($_POST['iws_forgotpassword_nonce'], 'iws-forgotpassword-nonce')) {



        if ($_POST['iws_user_login'] == '') {

            iws_errors()->add('empty_login', __('Please enter a username'));
        } else {


            // this returns the user ID and other info from the user name
            $user = get_user_by('login', $_POST['iws_user_login']);

            if (!$user) {

                // if the user name doesn't exist
                iws_errors()->add('notfound_username', __('Cannot found any record with this username.'));
            }

        }   



        // retrieve all error messages
        $errors = iws_errors()->get_error_messages();

        // only log the user in if there are no errors
        if (empty($errors)) {


            
                //===
                $random_password = generate_random_code(12);

                wp_set_password($random_password,$user->ID);
                
                $user_login=$user->user_login;
                
                $user_email=$user->user_email;
                
                $user_pass=$random_password;

                $message = "Hello,<br/>Your request for new password has been received successfully. Please try with new password.";

                $message .= sprintf(__('Username: %s', 'user-activation-email'), $user_login) . "\r\n";
                
                $message .= sprintf(__('Password: %s', 'user-activation-email'), $user_pass) . "\r\n\n";
                
                
                $message.="<br/>Thank You.";

                $message = apply_filters('iws_user_message', $message, $user_login, $user_email, $user_pass);

                $subject = apply_filters('iws_user_message_subject', __('New password request', 'user-activation-email'), $user_login, $user_email, $user_pass);





                add_filter('wp_mail_from', 'wp_change_default_email_change_from_email2');
                add_filter('wp_mail_from_name', 'wp_change_default_email_change_from_name2');





                wp_mail($user_email, $subject, $message);




                //==



                $redirect_link = wp_login_url()."?msg=passwordrequest";
                wp_redirect($redirect_link);
                exit;
            
        }
    }
}

add_action('init', 'iws_forgotpassword_member');
?>