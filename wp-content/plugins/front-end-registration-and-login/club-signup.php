<?php

// user registration login form
function iws_clubsignup_form() {

    // only show the registration form to non-logged-in members
    if (!is_user_logged_in()) {

        global $iws_load_css;

        // set this to true so the CSS is loaded
        $iws_load_css = true;

        // check to make sure user registration is enabled
        $registration_enabled = get_option('users_can_register');

        // only show the registration form if allowed
        if ($registration_enabled) {
            $output = iws_clubsignup_form_fields();
        } else {
            $output = __('User registration is not enabled');
        }
        return $output;
    } else {
        //echo "<script>window.location.href='".get_permalink(199)."'</script>";
        wp_redirect(wp_myprofile_url());

        die;
    }
}

add_shortcode('club_register_form', 'iws_clubsignup_form');

// registration form fields
function iws_clubsignup_form_fields() {

    ob_start();
    ?>	
    

    <?php
    // show any error messages after form submission
    echo iws_show_error_messages();
    ?>

    <form id="iws_registration_form" class="form-horizontal" action="" method="POST">
        

             
                    <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Club Name'); ?>:</label>
            <div class="col-sm-8">
                <input name="club_name" id="club_name" value="<?php if (isset($_POST['club_name'])) {
        echo $_POST['club_name'];
    } ?>" class="form-control" type="text"/>
           </div>
                </div>

            
    
     
                    <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Club website address'); ?>:</label>
            <div class="col-sm-8">
                <input name="club_website" id="club_website" value="<?php if (isset($_POST['club_website'])) {
        echo $_POST['club_website'];
    } ?>" class="form-control" type="text"/>
            </div>
                </div>




 
                    <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Contact Person First Name'); ?></label>
            <div class="col-sm-8">
                <input name="iws_user_first" id="iws_user_first" type="text" value="<?php if (isset($_POST['iws_user_first'])) {
        echo $_POST['iws_user_first'];
        } ?>" class="form-control" />
           </div>
                </div>
            
    
     
                    <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Contact Person Last Name'); ?></label>
            <div class="col-sm-8">
                <input name="iws_user_last" id="iws_user_last" type="text" value="<?php if (isset($_POST['iws_user_last'])) {
        echo $_POST['iws_user_last'];
        } ?>" class="form-control" />
          </div>
                </div>



            
                    <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('User Name'); ?></label>
            <div class="col-sm-8">
                <input name="iws_user_login" id="iws_user_login" value="<?php if (isset($_POST['iws_user_login'])) {
        echo $_POST['iws_user_login'];
    } ?>" class="form-control" type="text"/>
          </div>
                </div>
            
        
        
         
     
                    <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Email'); ?></label>
            <div class="col-sm-8">
                <input name="iws_user_email" id="iws_user_email" value="<?php if (isset($_POST['iws_user_email'])) {
        echo $_POST['iws_user_email'];
    } ?>" class="form-control" type="email"/>
           </div>
                </div>
        
        
        
 
                    <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Password'); ?></label>
            <div class="col-sm-8">
                <input name="iws_user_pass" id="password" class="form-control" type="password"/>
           </div>
                </div>
            
            
             
                    <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Repeat Password'); ?></label>
            <div class="col-sm-8">
                <input name="iws_user_pass_confirm" id="password_again" class="form-control" type="password"/>
            </div>
                </div>



<div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Club Phone Number'); ?></label>
            <div class="col-sm-8">
                <input name="club_phone_number" id="club_phone_number" value="<?php if (isset($_POST['club_phone_number'])) {
        echo $_POST['club_phone_number'];
    } ?>" class="form-control" type="text"/>
           </div>
                </div>

    

 
                   

     
                    <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Number of club members'); ?></label>
            <div class="col-sm-8">
                <input name="club_no_of_members" id="club_no_of_members" value="<?php if (isset($_POST['club_no_of_members'])) {
        echo $_POST['club_no_of_members'];
    } ?>" class="form-control" type="text"/>
           </div>
                </div>


             
                    <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Where do you host most of your club events?'); ?></label>
            <div class="col-sm-8">
                <input name="club_event_host" id="club_event_host" value="<?php if (isset($_POST['club_event_host'])) {
        echo $_POST['club_event_host'];
    } ?>" class="form-control" type="text"/>
           </div>
                </div>


             
                    <div class="form-group">
                        <label for="" class="col-sm-4 control-label"><?php _e('About the Club'); ?><br/><small>Please tell us about your club so we can showcase your club to our viewers.</small></label>
            <div class="col-sm-8">
                <textarea name="club_description" id="club_description" class="form-control" rows="5"><?php if (isset($_POST['club_description'])) {
        echo $_POST['club_description'];
    } ?></textarea>
            </div>
                </div>





            
        
        
            <div class="form-group">

                                    <div class="col-sm-offset-4 col-sm-8">

                                      <div class="checkbox">

                                        <label>
                                            <input type="checkbox" value="1" name="iws_agree_on_terms" id="iws_agree_on_terms" /> I have read and agreed to the <a href="<?php echo esc_url(wp_termscond_url()); ?>" target="_blank">Terms & Conditions</a>.
            </label>

                                      </div>

                                    </div>

                                  </div>
               <div class="form-group">

                                   <div class="col-sm-offset-4 col-sm-8">
                <input type="hidden" name="iws_clubsignup_nonce" value="<?php echo wp_create_nonce('iws-clubsignup-nonce'); ?>"/>
                
                <button id="iws_club_register_submit" type="submit" class="btn btn-default sign-btn">Register Your Club</button>
            </div>

                                  </div>




       
    </form>
    <?php
    return ob_get_clean();
}

// register a new user
function iws_add_new_club_and_member() {
    if (isset($_POST["iws_user_login"]) && isset($_POST['iws_clubsignup_nonce']) && wp_verify_nonce($_POST['iws_clubsignup_nonce'], 'iws-clubsignup-nonce')) {
        $user_login = trim($_POST["iws_user_login"]);
        $user_email = trim($_POST["iws_user_email"]);
        $user_first = trim($_POST["iws_user_first"]);
        $user_last = trim($_POST["iws_user_last"]);
        $user_pass = trim($_POST["iws_user_pass"]);
        $pass_confirm = trim($_POST["iws_user_pass_confirm"]);
        
        
        $club_name = trim($_POST["club_name"]);
        $club_description = trim($_POST["club_description"]);
        
        $club_phone_number = trim($_POST["club_phone_number"]);
        $club_no_of_members = (int) $_POST["club_no_of_members"];
        $club_event_host = trim($_POST["club_event_host"]);
        $club_website = trim($_POST["club_website"]);
        
       
        
        if($club_name==''){
            iws_errors()->add('club_name_empty', __('Please enter a club name'));
        }
        if (4 > strlen($club_name)) {
            iws_errors()->add('club_name_length', __('Club name too short. At least 4 characters is required'));
        }
        
        if($club_website==''){
            iws_errors()->add('club_website_empty', __('Please enter a club website'));
        }
        
        if(!valid_url($club_website)){
            iws_errors()->add('club_website_valid', __('Please enter a valid club website'));
        }
        
        
        
        if($club_phone_number==''){
            iws_errors()->add('club_phone_number_empty', __('Please enter a club phone number'));
        }
        
        if (9 > strlen($club_phone_number)) {
            iws_errors()->add('club_phone_number_min_length', __('Club phone number is too short. At least 9 digit is required'));
        }
        
        if (strlen($club_phone_number)>15) {
            iws_errors()->add('club_phone_number_max_length', __('Club phone number is too long. Maximum 15 digit is allowed'));
        }
        
        if(!contact_check($club_phone_number)){
            iws_errors()->add('club_phone_number_valid', __('Please enter a valid club phone number'));
        }
        
        
        if (strlen($club_event_host)>70) {
            iws_errors()->add('club_event_host_max_length', __('Club event host place is too long. Maximum 70 characters is allowed'));
        }
        
        
        
        if (strlen($user_first)>20) {
            iws_errors()->add('user_first_max_length', __('Club contact person first name is too long. Maximum 20 characters is allowed'));
        }
        
        if (strlen($user_last)>20) {
            iws_errors()->add('user_last_max_length', __('Club contact person last name is too long. Maximum 20 characters is allowed'));
        }
        
        
        
        
        
        
        
        
        
        if ($user_login == '') {
            // empty username
            iws_errors()->add('username_empty', __('Please enter a username'));
        }
        
        if (4 > strlen($user_login)) {
            iws_errors()->add('username_length', __('Username too short. At least 4 characters is required'));
        }

        if (username_exists($user_login)) {
            // Username already registered
            iws_errors()->add('username_unavailable', __('Username already taken'));
        }
        if (!validate_username($user_login)) {
            // invalid username
            iws_errors()->add('username_invalid', __('Invalid username'));
        }
        
        if (!is_email($user_email)) {
            //invalid email
            iws_errors()->add('email_invalid', __('Invalid email'));
        }
        if (email_exists($user_email)) {
            //Email address already registered
            iws_errors()->add('email_used', __('Email already registered'));
        }

        
        if (7 > strlen($user_pass)) {
            iws_errors()->add('password_length', 'Password length at leaset 8 character required');
        }

        if ($user_pass == '') {
            // passwords do not match
            iws_errors()->add('password_empty', __('Please enter a password'));
        }
        if ($user_pass != $pass_confirm) {
            // passwords do not match
            iws_errors()->add('password_mismatch', __('Passwords do not match'));
        }

        if (isset($_POST['iws_agree_on_terms']) && (int) $_POST['iws_agree_on_terms'] == (int) 1) {
            
        } else {
            iws_errors()->add('agree_on_terms', __('Please select agree on terms and conditions.'));
        }

        $errors = iws_errors()->get_error_messages();

        // only create the user in if there are no errors
        if (empty($errors)) {



            
            
            $new_user_id = wp_insert_user(array(
                'user_login' => $user_login,
                'user_pass' => $user_pass,
                'user_email' => $user_email,
                'first_name' => $user_first,
                'last_name' => $user_last,
                'user_registered' => date('Y-m-d H:i:s'),
                'role' => 'customer',
                    )
            );
            
            
            
            if ($new_user_id) {
               
                
                
            $club_name = esc_attr(sanitize_text_field($club_name));
            $club_description = esc_textarea(sanitize_text_field($club_description));
                    
            
    ////https://codex.wordpress.org/Function_Reference/wp_insert_post            
           
            
            $post_status='draft';
            $post_type='clubs';
            
            $slug=sanitize_title_with_dashes($club_name);
            
$new_post = array(
    'post_title' => $club_name,
    'post_name' => $slug,
    'post_content' => $club_description,
    'post_status' => $post_status,
    'post_date' => date('Y-m-d H:i:s'),
    'post_author' => $new_user_id,
    'post_type' => $post_type,
    
);
$post_id = wp_insert_post($new_post);
                
            if($post_id>0){

            
                
                /*$post_parent=0;
                
                
             $slug= wp_unique_post_slug( $slug, $post_id, $post_status, $post_type, $post_parent );  
            
            $my_post=array();
            
                $my_post['ID']=$post_id;
                $my_post['post_name']=$slug;
           
            
             wp_update_post( $my_post,true  );*/
            
           
                
                $club_first_name = sanitize_text_field($user_first);
                    update_post_meta($post_id, 'club_first_name', esc_attr($club_first_name));

                    $club_last_name = sanitize_text_field($user_last);
                    update_post_meta($post_id, 'club_last_name', esc_attr($club_last_name));

                    $club_email = sanitize_email($user_email);
                    update_post_meta($post_id, 'club_email', esc_attr($club_email));

                    $club_phone_number = sanitize_text_field($club_phone_number);
                    update_post_meta($post_id, 'club_phone_number', esc_attr($club_phone_number));

                    $club_no_of_members = (int) sanitize_text_field($club_no_of_members);
                    update_post_meta($post_id, 'club_no_of_members', esc_attr($club_no_of_members));

                    $club_event_host = sanitize_text_field($club_event_host);
                    update_post_meta($post_id, 'club_event_host', esc_attr($club_event_host));

                    $club_website = sanitize_text_field($club_website);
                    update_post_meta($post_id, 'club_website', esc_url_raw($club_website));

                    $club_user_id = (int) sanitize_text_field($new_user_id);
                    update_post_meta($post_id, 'club_user_id', esc_attr($club_user_id));

            }    
                
                
                


                // send an email to the admin alerting them of the registration
                //wp_new_user_notification($new_user_id);
                // generate activation code
                $activation_code = generate_random_code(12);

                update_user_meta($new_user_id, 'activation-code', $activation_code);
                update_user_meta($new_user_id, 'activation-code-is-use', 0);


                $url_activation_code = encrypt_decrypt('encrypt', $new_user_id . ',' . $activation_code);





                $message = sprintf(__('Username: %s', 'user-activation-email'), $user_login) . "\r\n";
                $message .= sprintf(__('Password: %s', 'user-activation-email'), $user_pass) . "\r\n\n";
                //$message .= sprintf(__('Activation Code: %s', 'user-activation-email'), $activation_code) . "\r\n\n"; 
                $message .= wp_login_url() . '?vky=' . urlencode($url_activation_code) . "\r\n";

                $message = apply_filters('iws_user_message', $message, $user_login, $user_email, $user_pass, $activation_code);

                $subject = apply_filters('iws_user_message_subject', __('Your username and password', 'user-activation-email'), $user_login, $user_email, $user_pass, $activation_code);





                add_filter('wp_mail_from', 'wp_change_default_email_change_from_email2');
                add_filter('wp_mail_from_name', 'wp_change_default_email_change_from_name2');





                wp_mail($user_email, $subject, $message);


                
                
                
                
                





                // remove below comment for log the new user in  
                //wp_setcookie($user_login, $user_pass, true);
                //wp_set_current_user($new_user_id, $user_login);	
                //do_action('wp_login', $user_login);
                // send the newly created user to the home page after logging them in
                //wp_redirect(home_url()); exit;

                $login_url = wp_login_url() . '?msg=verifylink';
                wp_redirect($login_url);
                exit;
            }
        }
    }
}

add_action('init', 'iws_add_new_club_and_member');
?>