<?php

// user registration login form
function iws_myprofile_form() {

    // only show the registration form to non-logged-in members
    if (is_user_logged_in()) {


        $output = iws_myprofile_form_fields();

        return $output;
    } else {
        $redirect_link = wp_login_url();
        wp_redirect($redirect_link);
        exit;
    }
}

add_shortcode('myprofile_form', 'iws_myprofile_form');

// registration form fields
function iws_myprofile_form_fields() {

    ob_start();

    global $current_user;

    $user = $current_user;


    $user_description = esc_attr(get_the_author_meta('description', $user->ID));
    $user_first_name = esc_attr(get_the_author_meta('first_name', $user->ID));
    $user_last_name = esc_attr(get_the_author_meta('last_name', $user->ID));

    $user_phone_number = esc_attr(get_the_author_meta('user_phone_number', $user->ID));
    $user_address = esc_attr(get_the_author_meta('user_address', $user->ID));
    $user_country_id = esc_attr(get_the_author_meta('user_country_id', $user->ID));
    $user_state_id = esc_attr(get_the_author_meta('user_state_id', $user->ID));
    $user_city_id = esc_attr(get_the_author_meta('user_city_id', $user->ID));
    $user_zip_code = esc_attr(get_the_author_meta('user_zip_code', $user->ID));

    $user_lat = esc_attr(get_the_author_meta('user_lat', $user->ID));
    $user_long = esc_attr(get_the_author_meta('user_long', $user->ID));
    
    
    $user_gender = esc_attr(get_the_author_meta('user_gender', $user->ID));

    if (isset($_POST['user_gender']) && (int) $_POST['user_gender'] > 0) {
        $user_gender = (int) $_POST['user_gender'];
    }
    
    $user_birth_year = esc_attr(get_the_author_meta('user_birth_year', $user->ID));

    if (isset($_POST['user_birth_year']) && (int) $_POST['user_birth_year'] > 0) {
        $user_birth_year = (int) $_POST['user_birth_year'];
    }
    

    

    if (isset($_POST['user_country_id']) && (int) $_POST['user_country_id'] > 0) {
        $user_country_id = (int) $_POST['user_country_id'];
    }
    if (isset($_POST['user_state_id']) && (int) $_POST['user_state_id'] > 0) {
        $user_state_id = (int) $_POST['user_state_id'];
    }
    if (isset($_POST['user_city_id']) && (int) $_POST['user_city_id'] > 0) {
        $user_city_id = (int) $_POST['user_city_id'];
    }





    $res_countries = get_all_country();

    $res_states = array();
    if ($user_country_id > 0) {

        $res_states = get_states_from_country_by_id($user_country_id);
    }

    $res_cities = array();
    if ($user_state_id > 0) {

        $res_cities = get_cities_from_state_by_id($user_state_id);
    }



    $res_activities = get_all_activities();


    $user_like_activities = esc_attr(get_the_author_meta('user_like_activities', $user->ID));

    if ($user_like_activities != '') {
        $user_like_activities = explode(',', $user_like_activities);
    }

    if (isset($_POST['user_like_activities']) && is_array($_POST['user_like_activities'])) {
        $user_like_activities = $_POST['user_like_activities'];
    }
    ?>	


    <?php
    // show any error messages after form submission
    echo iws_show_error_messages();
    echo iws_show_general_messages();
    ?>

    <form id="iws_registration_form" class="form-horizontal signUpBlock" action="<?php echo esc_url(wp_myprofile_url()); ?>" method="POST" enctype="multipart/form-data">


        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('First Name'); ?></label>
            <div class="col-sm-8">
                <input name="iws_user_first" id="iws_user_first" type="text" value="<?php if (isset($_POST['iws_user_first'])) {
        echo $_POST['iws_user_first'];
    } elseif (isset($user_first_name)) {
        echo $user_first_name;
    } ?>" class="form-control" />
            </div>
        </div>


        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Last Name'); ?></label>
            <div class="col-sm-8">
                <input name="iws_user_last" id="iws_user_last" type="text" value="<?php if (isset($_POST['iws_user_last'])) {
        echo $_POST['iws_user_last'];
    } elseif (isset($user_last_name)) {
        echo $user_last_name;
    } ?>" class="form-control" />
            </div>
        </div>


        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('User Name'); ?></label>
            <div class="col-sm-8">
                <input name="iws_user_login" id="iws_user_login" value="<?php if (isset($_POST['iws_user_login'])) {
        echo $_POST['iws_user_login'];
    } elseif (isset($user->user_login)) {
        echo $user->user_login;
    } ?>" class="form-control" type="text"/>
            </div>
        </div>



        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Email'); ?></label>
            <div class="col-sm-8">
                <input name="iws_user_email" id="iws_user_email" value="<?php if (isset($_POST['iws_user_email'])) {
        echo $_POST['iws_user_email'];
    } elseif (isset($user->user_email)) {
        echo $user->user_email;
    } ?>" class="form-control" type="email"/>
            </div>
        </div>





        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e("Phone Number"); ?></label>
            <div class="col-sm-8">

                <input type="text" name="user_phone_number" id="user_phone_number" value="<?php if (isset($_POST['user_phone_number'])) {
        echo $_POST['user_phone_number'];
    } elseif (isset($user_phone_number)) {
        echo $user_phone_number;
    } ?>" class="form-control" />
            </div>
        </div>
        
        
        
        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e("Gender"); ?></label>
            <div class="col-sm-8">

                <select name="user_gender" id="user_gender" class="form-control" >
                    <option value="0">---Select Gender---</option>   
                 
                     

                            <option value="1" <?php if ($user_gender==1) { ?> selected <?php } ?>>Male</option>
                            <option value="2" <?php if ($user_gender==2) { ?> selected <?php } ?>>Female</option>

        
                </select> 
            </div>
        </div>

        
        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e("Birth year"); ?></label>
            <div class="col-sm-8">

                <select name="user_birth_year" id="user_birth_year" class="form-control" >
                    <option value="0">---Select Year---</option>   
                 
                     
                    <?php for($yr=1950;$yr<=date('Y');$yr++){ ?>
                            <option value="<?php echo $yr; ?>" <?php if ($user_birth_year==$yr) { ?> selected <?php } ?>><?php echo $yr; ?></option>
                            <?php } ?>

        
                </select> 
            </div>
        </div>








        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('About you'); ?></label>
            <div class="col-sm-8">
                <textarea name="iws_user_bio" id="iws_user_bio" class="form-control" rows="5"><?php if (isset($_POST['iws_user_bio'])) {
        echo $_POST['iws_user_bio'];
    } elseif (isset($user_description)) {
        echo $user_description;
    } ?></textarea>
            </div>
        </div>



        <h3>Your favourite activities</h3>            


        <div class="form-group">
            <label for="" class="col-sm-4 control-label"></label>
            <div class="col-sm-8" style="padding:0;">

    <?php
    if (!empty($res_activities)) {
        foreach ($res_activities as $row_acti) {
            ?>
                        <div class="checkbox col-sm-4">

                            <label>

                                <input type="checkbox" name="user_like_activities[]" value="<?php echo $row_acti->term_id; ?>" <?php if (!empty($user_like_activities)) {
                if (in_array($row_acti->term_id, $user_like_activities)) { ?> checked <?php }
            } ?> /><?php echo ucwords($row_acti->name); ?></label>

                        </div>
        <?php }
    } ?>

            </div>
        </div>





        <h3>Your Location</h3>

        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e("Address"); ?></label>
            <div class="col-sm-8">

                <input type="text" name="user_address" id="user_address" value="<?php if (isset($_POST['user_address'])) {
        echo $_POST['user_address'];
    } elseif (isset($user_address)) {
        echo $user_address;
    } ?>" class="form-control" />
            </div>
        </div>







        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e("Country"); ?></label>
            <div class="col-sm-8">

                <select name="user_country_id" id="user_country_id" class="form-control" >
                    <option value="0">---Select Country---</option>   
                    <?php if (!empty($res_countries)) {
                        foreach ($res_countries as $row_country) { ?>

                            <option value="<?php echo $row_country->countryid; ?>" <?php if ($row_country->countryid == $user_country_id) { ?> selected <?php } ?>><?php echo ucfirst($row_country->country); ?></option>

        <?php }
    } ?>

                </select> 
            </div>
        </div>





        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e("State"); ?></label>
            <div class="col-sm-8">

                <select name="user_state_id" id="user_state_id" class="form-control" >
                    <option value="0">---Select State---</option>

    <?php
    if (!empty($res_states)) {
        foreach ($res_states as $row_states) {
            ?>

                            <option value="<?php echo $row_states->regionid; ?>" <?php if ($row_states->regionid == $user_state_id) { ?> selected <?php } ?>><?php echo ucfirst($row_states->region); ?></option>


                            <?php
                        }
                    }
                    ?>

                </select> 
            </div>
        </div>







        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e("City"); ?></label>
            <div class="col-sm-8">

                <select name="user_city_id" id="user_city_id" class="form-control" >
                    <option value="0">---Select City---</option>

    <?php
    if (!empty($res_cities)) {
        foreach ($res_cities as $row_cities) {
            ?>

                            <option value="<?php echo $row_cities->CityId; ?>" data-lat="<?php echo $row_cities->Latitude; ?>" data-long="<?php echo $row_cities->Longitude; ?>" <?php if ($row_cities->CityId == $user_city_id) { ?> selected <?php } ?>><?php echo ucfirst($row_cities->City); ?></option>


        <?php }
    } ?>


                </select> 


                <input type="hidden" name="user_lat" id="user_lat" value="<?php if (isset($_POST['user_lat'])) {
        echo $_POST['user_lat'];
    } elseif (isset($user_lat)) {
        echo $user_lat;
    } ?>" />
                <input type="hidden" name="user_long" id="user_long" value="<?php if (isset($_POST['user_long'])) {
        echo $_POST['user_long'];
    } elseif (isset($user_long)) {
        echo $user_long;
    } ?>" />

            </div>
        </div>









        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e("Zip Code"); ?></label>
            <div class="col-sm-8">

                <input type="text" name="user_zip_code" id="user_zip_code" value="<?php if (isset($_POST['user_zip_code'])) {
        echo $_POST['user_zip_code'];
    } elseif (isset($user_zip_code)) {
        echo $user_zip_code;
    } ?>" class="form-control" />
            </div>
        </div>





        <h3>Upload your picture</h3>                       

        <?php
        $allowed_ext = "jpg,gif,png";
        $post_id = $user->ID;
        $attr['name'] = 'avatar';
        $attr['max_size'] = "10000";
        $attr['count'] = 1;

        $type = 'avatar';

        $uploaded_items = $post_id ? get_post_meta($post_id, $attr['name'], true) : array();



        $has_featured_image = false;
        $has_images = false;
        $has_avatar = false;

        if ($post_id) {

            $is_meta = get_post_meta($post_id, $attr['name'], true);

            if ($is_meta != '') {
                $images = $is_meta;
                $has_images = true;
            } else {

                if ($type == 'post') {
                    // it's a featured image then
                    $thumb_id = get_post_thumbnail_id($post_id);

                    if ($thumb_id) {
                        $has_featured_image = true;
                        $featured_image = attach_html($thumb_id);
                    }
                } else {
                    // it must be a user avatar
                    $has_avatar = true;
                    $featured_image = get_avatar($post_id);
                }
            }
        }
        ?>


        <div class="iws-fields">
            <div id="iws-<?php echo $attr['name']; ?>-upload-container">
                <div class="iws-attachment-upload-filelist" data-type="file" >
                    <a id="iws-<?php echo $attr['name']; ?>-pickfiles" class="btn btn-danger btn-small button file-selector <?php echo ' iws_' . $attr['name']; ?>" href="#">Upload Image</a>

                    <ul class="iws-attachment-list thumbnails" >
    <?php
    if ($has_featured_image) {
        echo $featured_image;
    }

    if ($has_avatar) {
        $avatar = get_user_meta($post_id, 'user_avatar', true);
        if ($avatar) {
            
            
            echo '<li class="iws-image-wrap thumbnail"><div class="attachment-name">'.$featured_image.'</div><div class="caption"><a href="javascript:void(0)" data-confirm="Are you sure to delete your image?" class="btn btn-danger btn-small iws-button button iws-delete-avatar">Delete</a></div></li>';
            
            
        }
    }

    if ($has_images) {
        foreach ($images as $attach_id) {
            echo attach_html($attach_id, $attr['name']);
        }
    }
    ?>
                    </ul>
                </div>
            </div><!-- .container -->



        </div> <!-- .iws-fields -->

        
        <style>
            .thumbnail {
                border: none !important;
            }
        </style>
            
        
        
        <script type="text/javascript">
            jQuery(function($) {
                new IWS_Uploader('iws-<?php echo $attr['name']; ?>-pickfiles', 'iws-<?php echo $attr['name']; ?>-upload-container', <?php echo $attr['count']; ?>, '<?php echo $attr['name']; ?>', '<?php echo $allowed_ext; ?>', <?php echo $attr['max_size'] ?>);
                    
                    
                    
                jQuery(".iws-delete-avatar").on("click",function(e){
                       
                       
                    e.preventDefault();

                    if ( confirm( $(this).data('confirm') ) ) {
                        $.post('<?php echo admin_url('admin-ajax.php'); ?>',{action: 'iws_delete_avatar'}, function() {
                            window.location.reload();
                        });
                    }
                       
                });
            });
        </script>




        <div class="form-group">

            <div class="col-sm-offset-4 col-sm-8">
                <input type="hidden" name="iws_myprofile_nonce" value="<?php echo wp_create_nonce('iws-myprofile-nonce'); ?>"/>

                <button id="iws_edit_user_profile_submit" type="submit" class="btn btn-default sign-btn">Update Your Account</button>
            </div>

        </div>






    </form>
    <?php
    return ob_get_clean();
}

// register a new user
function iws_edit_member() {
    if (isset($_POST["iws_user_login"]) && isset($_POST['iws_myprofile_nonce']) && wp_verify_nonce($_POST['iws_myprofile_nonce'], 'iws-myprofile-nonce')) {

        //http://www.hughlashbrooke.com/2014/03/wordpress-upload-user-submitted-files-frontend/

        global $current_user;

        $user = $current_user;

        $user_id = $user->ID;


        $user_login = $_POST["iws_user_login"];
        $user_email = $_POST["iws_user_email"];
        $user_first = $_POST["iws_user_first"];
        $user_last = $_POST["iws_user_last"];

        $user_phone_number = $_POST["user_phone_number"];
        $user_zip_code = $_POST['user_zip_code'];



        if (4 > strlen($user_login)) {
            iws_errors()->add('username_length', __('Username too short. At least 4 characters is required'));
        }



        if ($exists_user = username_exists($user_login)) {

            if ((int) $user_id != (int) $exists_user) {

                // Username already registered
                iws_errors()->add('username_unavailable', __('Username already taken'));
            }
        }
        if (!validate_username($user_login)) {
            // invalid username
            iws_errors()->add('username_invalid', __('Invalid username'));
        }
        if ($user_login == '') {
            // empty username
            iws_errors()->add('username_empty', __('Please enter a username'));
        }
        if (!is_email($user_email)) {
            //invalid email
            iws_errors()->add('email_invalid', __('Invalid email'));
        }
        if ($exists_email = email_exists($user_email)) {

            if ((int) $user_id != (int) $exists_email) {
                //Email address already registered
                iws_errors()->add('email_used', __('Email already registered'));
            }
        }



        if ($user_phone_number == '') {
            iws_errors()->add('user_phone_number_empty', __('Please enter a user phone number'));
        }

        if (9 > strlen($user_phone_number)) {
            iws_errors()->add('user_phone_number_min_length', __('User phone number is too short. At least 9 digit is required'));
        }

        if (strlen($user_phone_number) > 15) {
            iws_errors()->add('user_phone_number_max_length', __('User phone number is too long. Maximum 15 digit is allowed'));
        }

        if (!contact_check($user_phone_number)) {
            iws_errors()->add('user_phone_number_valid', __('Please enter a valid user phone number'));
        }



        if (strlen($user_first) > 20) {
            iws_errors()->add('user_first_max_length', __('Club contact person first name is too long. Maximum 20 characters is allowed'));
        }

        if (strlen($user_last) > 20) {
            iws_errors()->add('user_last_max_length', __('Club contact person last name is too long. Maximum 20 characters is allowed'));
        }

        if (3 > strlen($user_zip_code)) {
            iws_errors()->add('user_zip_code_min_length', __('User zipcode is too short. At least 4 digit is required'));
        }

        if (strlen($user_zip_code) > 12) {
            iws_errors()->add('user_zip_code_number_max_length', __('User zipcode is too long. Maximum 15 digit is allowed'));
        }


        $errors = iws_errors()->get_error_messages();

        // only create the user in if there are no errors
        if (empty($errors)) {

            //https://codex.wordpress.org/Function_Reference/wp_update_user

            $update_user = wp_update_user(array(
                'ID' => $user_id,
                'user_login' => $user_login,
                'user_email' => $user_email,
                    )
            );

            // set featured image if there's any
            if (isset($_POST['iws_files']['avatar'])) {
                $attachment_id = $_POST['iws_files']['avatar'][0];

                iws_update_avatar($user_id, $attachment_id);
            }

            $user_first_name = sanitize_text_field($_POST['iws_user_first']);
            update_user_meta($user_id, 'first_name', esc_attr($user_first_name));

            $user_last_name = sanitize_text_field($_POST['iws_user_last']);
            update_user_meta($user_id, 'last_name', esc_attr($user_last_name));

            $user_description = sanitize_text_field($_POST['iws_user_bio']);
            update_user_meta($user_id, 'description', esc_textarea($user_description));



            $user_phone_number = sanitize_text_field($_POST['user_phone_number']);
            update_user_meta($user_id, 'user_phone_number', esc_attr($user_phone_number));
            
            
            $user_gender = (int) sanitize_text_field($_POST['user_gender']);
            update_user_meta($user_id, 'user_gender', esc_attr($user_gender));
            
            $user_birth_year = (int) sanitize_text_field($_POST['user_birth_year']);
            update_user_meta($user_id, 'user_birth_year', esc_attr($user_birth_year));
            
            

            $user_address = sanitize_text_field($_POST['user_address']);
            update_user_meta($user_id, 'user_address', esc_attr($user_address));

            $user_country_id = (int) sanitize_text_field($_POST['user_country_id']);
            update_user_meta($user_id, 'user_country_id', esc_attr($user_country_id));

            $user_state_id = (int) sanitize_text_field($_POST['user_state_id']);
            update_user_meta($user_id, 'user_state_id', esc_attr($user_state_id));

            $user_city_id = (int) sanitize_text_field($_POST['user_city_id']);
            update_user_meta($user_id, 'user_city_id', esc_attr($user_city_id));


            $user_zip_code = sanitize_text_field($_POST['user_zip_code']);
            update_user_meta($user_id, 'user_zip_code', esc_attr($user_zip_code));

            $user_lat = sanitize_text_field($_POST['user_lat']);
            update_user_meta($user_id, 'user_lat', esc_attr($user_lat));

            $user_long = sanitize_text_field($_POST['user_long']);
            update_user_meta($user_id, 'user_long', esc_attr($user_long));




            $user_like_activity = '';

            $get_other_activity = $_POST['user_like_activities'];

            if (!empty($get_other_activity)) {
                $get_other_activity = array_unique(array_map('intval', $get_other_activity));
                $user_like_activity = implode(',', $get_other_activity);

                if ($user_like_activity == ',') {
                    $user_like_activity = '';
                }
            }

            update_user_meta($user_id, 'user_like_activities', esc_attr($user_like_activity));







            $redirect_url = wp_myprofile_url() . '?msg=profileupdated';
            wp_redirect($redirect_url);
            exit;
        }
    }
}

add_action('init', 'iws_edit_member');


/**
 * Add script to the page
 */
add_action('wp_enqueue_scripts', 'user_profile_script_to_front');

function user_profile_script_to_front($page) {


    wp_register_script('front_user_profile_edit_js', plugins_url('/js/front_user_profile.js', __FILE__), array('jquery'));

    wp_enqueue_script('plupload-handlers');

    wp_enqueue_script('iws-upload', plugins_url('/js/iwsupload.js', __FILE__), array('jquery', 'plupload-handlers'));



    wp_localize_script('front_user_profile_edit_js', 'ProfileAJ', array(
        'confirmMsg' => __('Are you sure?', 'iws'),
        'nonce' => wp_create_nonce('iws_nonce'),
        'ajaxurl' => admin_url('admin-ajax.php'),
        'plupload' => array(
            'url' => admin_url('admin-ajax.php') . '?nonce=' . wp_create_nonce('iws_featured_img'),
            'flash_swf_url' => includes_url('js/plupload/plupload.flash.swf'),
            'filters' => array(array('title' => __('Allowed Files'), 'extensions' => '*')),
            'multipart' => true,
            'urlstream_upload' => true,
        )
    ));





// Enqueue Scripts that are needed on all the pages
    wp_enqueue_script('jquery');
    wp_enqueue_script('front_user_profile_edit_js');
}
?>