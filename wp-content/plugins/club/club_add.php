<?php
ob_start();

global $wpdb, $post;

$edit_add_text='Add';


$obj_id=0;
$club_name='';
$club_description='';
$club_first_name='';
$club_last_name='';
$club_email='';
$club_phone_number='';
$club_no_of_members='';
$club_event_host='';
$club_website='';

$club_address='';
$club_zip_code='';
$club_state_id='';
$club_city_id='';
$club_country_id='';

$club_lat='';
$club_long='';

$club_main_activity='';
$club_other_activity = array();




if(isset($_GET['id']) && (int) $_GET['id']>0){
                
    $obj_id = (int) $_GET['id'];
    $edit_add_text='Edit';
    
    
    $post_data=get_post($obj_id,true);
    
    
    
    $club_name=$post_data['post_title'];
    
    $club_description=$post_data['post_content'];
    
    
    $club_first_name = get_post_meta($obj_id, 'club_first_name', TRUE);
    $club_last_name = get_post_meta($obj_id, 'club_last_name', TRUE);

    $club_email = get_post_meta($obj_id, 'club_email', TRUE);
    $club_phone_number = get_post_meta($obj_id, 'club_phone_number', TRUE);

    $club_no_of_members = get_post_meta($obj_id, 'club_no_of_members', TRUE);
    $club_event_host = get_post_meta($obj_id, 'club_event_host', TRUE);
    $club_website = get_post_meta($obj_id, 'club_website', TRUE);

    $club_user_id = get_post_meta($obj_id, 'club_user_id', TRUE);

    $club_address = get_post_meta($obj_id, 'club_adderss', TRUE);
    
    $club_zip_code = get_post_meta($obj_id, 'club_zip_code', TRUE);
    
    $club_city_id = get_post_meta($obj_id, 'club_city_id', TRUE);
    $club_state_id = get_post_meta($obj_id, 'club_state_id', TRUE);
    $club_country_id = get_post_meta($obj_id, 'club_country_id', TRUE);
    $club_lat = get_post_meta($obj_id, 'club_lat', TRUE);
    $club_long = get_post_meta($obj_id, 'club_long', TRUE);

    
    $club_main_activity = get_post_meta($obj_id, 'club_main_activity', TRUE);
    

    $get_other_activity = get_post_meta($obj_id, 'club_other_activities', TRUE);

    if ($get_other_activity != '') {
        $club_other_activity = explode(',', $get_other_activity);
    }


    

}

    

    if (isset($_POST['club_country_id']) && (int) $_POST['club_country_id'] > 0) {
        $club_country_id = (int) $_POST['club_country_id'];
    }
    if (isset($_POST['club_state_id']) && (int) $_POST['club_state_id'] > 0) {
        $club_state_id = (int) $_POST['club_state_id'];
    }
    if (isset($_POST['club_city_id']) && (int) $_POST['club_city_id'] > 0) {
        $club_city_id = (int) $_POST['club_city_id'];
    }

    

    $res_countries = get_all_country();


    $res_states = array();
    if ($club_country_id > 0) {
        
        $res_states = get_states_from_country_by_id($club_country_id);
    }

    $res_cities = array();
    if ($club_state_id > 0) {
        
        $res_cities = get_cities_from_state_by_id($club_state_id);
    }
    
    $res_activities = get_all_activities();
    if (isset($_POST['club_other_activities']) && is_array($_POST['club_other_activities'])) {
        $club_other_activity = $_POST['club_other_activities'];
    }
    
    
    if (isset($_POST['club_main_activity']) && (int) $_POST['club_main_activity'] > 0) {
        $club_main_activity = (int) $_POST['club_main_activity'];
    }

    


?>	



    <div class="col-sm-12 signUpBlock">
    
    
    <h3><?php echo $edit_add_text;?> Club</h3>

    <?php
// show any error messages after form submission
    echo iws_show_error_messages();
    ?>
    <form id="iws_registration_form" class="form-horizontal" action="" method="POST">



        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Club Name'); ?>:</label>
            <div class="col-sm-8">
                <input name="club_name" id="club_name" value="<?php
    if (isset($_POST['club_name'])) {
        echo $_POST['club_name'];
    } else {
        echo $club_name;
    }
    ?>" class="form-control" type="text"/>
            </div>
        </div>




        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Club website address'); ?>:</label>
            <div class="col-sm-8">
                <input name="club_website" id="club_website" value="<?php
                       if (isset($_POST['club_website'])) {
                           echo $_POST['club_website'];
                       } else { echo $club_website; } 
    ?>" class="form-control" type="text"/>
            </div>
        </div>





        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Contact Person First Name'); ?></label>
            <div class="col-sm-8">
                <input name="club_first_name" id="club_first_name" type="text" value="<?php
                       if (isset($_POST['club_first_name'])) {
                           echo $_POST['club_first_name'];
                       } else { echo $club_first_name; }
    ?>" class="form-control" />
            </div>
        </div>



        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Contact Person Last Name'); ?></label>
            <div class="col-sm-8">
                <input name="club_last_name" id="club_last_name" type="text" value="<?php
                       if (isset($_POST['club_last_name'])) {
                           echo $_POST['club_last_name'];
                       } else { echo $club_last_name; } 
    ?>" class="form-control" />
            </div>
        </div>





        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Email Address'); ?></label>
            <div class="col-sm-8">
                <input name="club_email" id="club_email" value="<?php
                       if (isset($_POST['club_email'])) {
                           echo $_POST['club_email'];
                       } else { echo $club_email; }
    ?>" class="form-control" type="email"/>
            </div>
        </div>





        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Club Phone Number'); ?></label>
            <div class="col-sm-8">
                <input name="club_phone_number" id="club_phone_number" value="<?php
                       if (isset($_POST['club_phone_number'])) {
                           echo $_POST['club_phone_number'];
                       } else { echo $club_phone_number; } 
    ?>" class="form-control" type="text"/>
            </div>
        </div>







        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Number of club members'); ?></label>
            <div class="col-sm-8">
                <input name="club_no_of_members" id="club_no_of_members" value="<?php
                       if (isset($_POST['club_no_of_members'])) {
                           echo $_POST['club_no_of_members'];
                       } else { echo $club_no_of_members; }
    ?>" class="form-control" type="text"/>
            </div>
        </div>



        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Where do you host most of your club events?'); ?></label>
            <div class="col-sm-8">
                <input name="club_event_host" id="club_event_host" value="<?php
                       if (isset($_POST['club_event_host'])) {
                           echo $_POST['club_event_host'];
                       } else { echo $club_event_host; }
    ?>" class="form-control" type="text"/>
            </div>
        </div>



        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('About the Club'); ?><br/><small>Please tell us about your club so we can showcase your club to our viewers.</small></label>
            <div class="col-sm-8">
                <textarea name="club_description" id="club_description" class="form-control" rows="5"><?php
                       if (isset($_POST['club_description'])) {
                           echo $_POST['club_description'];
                       } else { echo $club_description; }
    ?></textarea>
            </div>
        </div>




        <h3>Activities</h3>

        <div class="form-group">
            <label for="" class="col-sm-4 control-label">Main Club Activity</label>
     <div class="col-sm-8">
   

        <select name="club_main_activity" id="club_main_activity" class="form-control">
            <option value="0">---Select Main Activity---</option>                    
    <?php
    if (!empty($res_activities)) {
        foreach ($res_activities as $row_acti) {
            ?>
                    <option value="<?php echo $row_acti->term_id; ?>" <?php if ($row_acti->term_id == $club_main_activity) { ?> selected <?php } ?>><?php echo ucwords($row_acti->name); ?></option>
        <?php }
    } ?>

        </select>
   </div>
 </div>



        <div class="form-group">
            <label for="" class="col-sm-4 control-label">Other Activities</label>
    <div class="col-sm-8" style="padding:0;">
    <?php
    if (!empty($res_activities)) {
        foreach ($res_activities as $row_acti) {
            ?>
                <div class="checkbox col-sm-4">
                                            <label><input type="checkbox" name="club_other_activities[]" value="<?php echo $row_acti->term_id; ?>" <?php if (!empty($club_other_activity)) {
                if (in_array($row_acti->term_id, $club_other_activity)) { ?> checked <?php }
            } ?> /> <?php echo ucwords($row_acti->name); ?></label></div>
                <?php }
            } ?>


    </div></div>


        
        <h3>Club Location</h3>
        
        
        
        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e("Address"); ?></label>
            <div class="col-sm-8">

                <input type="text" name="club_adderss" id="club_adderss" value="<?php if (isset($_POST['club_adderss'])) {
        echo $_POST['club_adderss'];
    } else {
        echo $club_address;
    } ?>" class="form-control" />
            </div>
        </div>







        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e("Country"); ?></label>
            <div class="col-sm-8">

                <select name="club_country_id" id="club_country_id" class="form-control" >
                    <option value="0">---Select Country---</option>   
                    <?php if (!empty($res_countries)) {
                        foreach ($res_countries as $row_country) { ?>

                            <option value="<?php echo $row_country->countryid; ?>" <?php if ($row_country->countryid == $club_country_id) { ?> selected <?php } ?>><?php echo ucfirst($row_country->country); ?></option>

        <?php }
    } ?>

                </select> 
            </div>
        </div>





        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e("State"); ?></label>
            <div class="col-sm-8">

                <select name="club_state_id" id="club_state_id" class="form-control" >
                    <option value="0">---Select State---</option>

    <?php
    if (!empty($res_states)) {
        foreach ($res_states as $row_states) {
            ?>

                            <option value="<?php echo $row_states->regionid; ?>" <?php if ($row_states->regionid == $club_state_id) { ?> selected <?php } ?>><?php echo ucfirst($row_states->region); ?></option>


                            <?php
                        }
                    }
                    ?>

                </select> 
            </div>
        </div>







        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e("City"); ?></label>
            <div class="col-sm-8">

                <select name="club_city_id" id="club_city_id" class="form-control" >
                    <option value="0">---Select City---</option>

    <?php
    if (!empty($res_cities)) {
        foreach ($res_cities as $row_cities) {
            ?>

                            <option value="<?php echo $row_cities->CityId; ?>" data-lat="<?php echo $row_cities->Latitude; ?>" data-long="<?php echo $row_cities->Longitude; ?>" <?php if ($row_cities->CityId == $club_city_id) { ?> selected <?php } ?>><?php echo ucfirst($row_cities->City); ?></option>


        <?php }
    } ?>


                </select> 


                <input type="hidden" name="club_lat" id="club_lat" value="<?php if (isset($_POST['club_lat'])) {
        echo $_POST['club_lat'];
    } else {
        echo $club_lat;
    } ?>" />
                <input type="hidden" name="club_long" id="club_long" value="<?php if (isset($_POST['club_long'])) {
        echo $_POST['club_long'];
    } else {
        echo $club_long;
    } ?>" />

            </div>
        </div>









        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e("Zip Code"); ?></label>
            <div class="col-sm-8">

                <input type="text" name="club_zip_code" id="club_zip_code" value="<?php if (isset($_POST['club_zip_code'])) {
        echo $_POST['club_zip_code'];
    } else {
        echo $club_zip_code;
    } ?>" class="form-control" />
            </div>
        </div>

        
        
        
        

        <h3>Upload picture of club's logo</h3>                       

        <?php
        $allowed_ext = "jpg,gif,png";
        $post_id = $obj_id;
        $attr['name'] = '_thumbnail_id';
        $attr['max_size'] = "10000";
        $attr['count'] = 1;

        $type = 'post';

        $uploaded_items = $post_id ? get_post_meta($post_id, $attr['name'], true) : array();



        
        $has_featured_image = false;
        $has_images = false;
        $has_avatar = false;
        

        if ($post_id) {

                   
            if (isset($_POST['iws_files']['_thumbnail_id'])) {
                 $thumb_id = $_POST['iws_files']['_thumbnail_id'][0]; 
                if ($thumb_id) {
                    $has_featured_image = true;
                    $featured_image = attach_html($thumb_id); 
                    echo '<input type="hidden" name="iws_files[_thumbnail_id][]" value="'.$thumb_id.'">';
                }
            } else {

                    // it's a featured image then
                    $thumb_id = get_post_thumbnail_id($post_id);

                    if ($thumb_id) {
                        $has_featured_image = true;
                        $featured_image = attach_html($thumb_id); 
                    }
            }
        } else {
            if (isset($_POST['iws_files']['_thumbnail_id'])) {
			$thumb_id = $_POST['iws_files']['_thumbnail_id'][0]; 
                if ($thumb_id) {
                    $has_featured_image = true;
                    $featured_image = attach_html($thumb_id); 
                    echo '<input type="hidden" name="iws_files[_thumbnail_id][]" value="'.$thumb_id.'">';
                }
            }
        }
        ?>


        <div class="iws-fields">
            <div id="iws-<?php echo $attr['name']; ?>-upload-container">
                <div class="iws-attachment-upload-filelist" data-type="file" >
                    <a id="iws-<?php echo $attr['name']; ?>-pickfiles" class="btn btn-danger btn-small button file-selector <?php echo ' iws_' . $attr['name']; ?>" href="#">Upload Image</a>

                    <ul class="iws-attachment-list thumbnails" >
    <?php
    if ($has_featured_image) {
        echo $featured_image;
    }

    if ($has_avatar) {
        $avatar = get_user_meta($post_id, 'user_avatar', true);
        if ($avatar) {
            
            
            echo '<li class="iws-image-wrap thumbnail"><div class="attachment-name">'.$featured_image.'</div><div class="caption"><a href="javascript:void(0)" data-confirm="Are you sure to delete your image?" class="btn btn-danger btn-small iws-button button iws-delete-file">Delete</a></div></li>';
            
            
        }
    }

    
    ?>
                    </ul>
                </div>
            </div><!-- .container -->



        </div> <!-- .iws-fields -->

        
        <style>
            .thumbnail {
                border: none !important;
            }
        </style>
            
        
        
        <script type="text/javascript">
            jQuery(function($) {
                new IWS_Uploader('iws-<?php echo $attr['name']; ?>-pickfiles', 'iws-<?php echo $attr['name']; ?>-upload-container', <?php echo $attr['count']; ?>, '<?php echo $attr['name']; ?>', '<?php echo $allowed_ext; ?>', <?php echo $attr['max_size'] ?>);
                    
                    
                    
                jQuery(".iws-delete-file").on("click",function(e){
                       
                       
                    e.preventDefault();

                    if ( confirm( $(this).data('confirm') ) ) {
                        $.post('<?php echo admin_url('admin-ajax.php'); ?>',{action: 'iws_file_del'}, function() {
                            window.location.reload();
                        });
                    }
                       
                });
            });
        </script>





        <div class="form-group">

          <div class="col-sm-12 text-right">
                <input type="hidden" name="iws_club_editadd_nonce" value="<?php echo wp_create_nonce('iws-club-editadd-nonce'); ?>"/>

                <button id="iws_club_editadd_submit" type="submit" class="btn btn-default sign-btn">Submit</button>
            </div>

        </div>
        
        
        <script>
            var ajaxurl='<?php echo admin_url('admin-ajax.php'); ?>';
            jQuery(document).ready(function(){
				
	jQuery('#club_country_id').change(function(e) {
		var data = {
			'action': 'get_states_of_country',
			'country': jQuery(this).val()
		};
		
		var state_ops = '<option value="">---Select State---</option>';
		var city_ops = '<option value="">---Select City---</option>';
		
		jQuery("#club_state_id").html(state_ops);
		jQuery("#club_city_id").html(city_ops);

		// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
		jQuery.post(ajaxurl, data, function(response) {
			if(jQuery("#club_state_id").length>0)
			{
				jQuery("#club_state_id").html(response);
			}
		});
    });
	
	jQuery('#club_state_id').change(function(e) {
		var data = {
			'action': 'get_cities_of_state',
			'state': jQuery(this).val()
			
		};
		
		
		var city_ops = '<option value="">---Select City---</option>';
		
		jQuery("#club_city_id").html(city_ops);

		// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
		jQuery.post(ajaxurl, data, function(response) {
			if(jQuery("#club_city_id").length>0)
			{
				jQuery("#club_city_id").html(response);
			}
		});
    });
	
	jQuery('#club_city_id').change(function(e) {
		var vlat=jQuery("#club_city_id option:selected").attr('data-lat');
		var vlong=jQuery("#club_city_id option:selected").attr('data-long');
		
		jQuery("#club_lat").val(vlat);
		jQuery("#club_long").val(vlong);
		
	});
	
});
        </script>


        
    </form>
</div>
<?php
echo ob_get_clean();
?>