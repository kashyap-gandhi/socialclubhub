<?php

function get_similar_clubs($post_id) {

    global $wpdb, $post;
    
    $result=array();
    
    $city_id=get_post_meta($post_id,'club_city_id',true);
    $state_id=get_post_meta($post_id,'club_state_id',true);
    
    if($city_id>0 || $state_id>0){
    
    $get_club_sql = "select tm.*, tt.description from " . $wpdb->prefix . "term_taxonomy tt, " . $wpdb->prefix . "terms tm where tt.term_id=tm.term_id and tt.taxonomy='club-activities' and tt.parent=0  order by tm.name asc";
    
    
    
    $sql='SELECT ps.* ';
    
            
    $sql.=' FROM '.$wpdb->prefix.'posts as ps ';
    
     if ($city_id>0) {
        $sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt1 ON ( ps.ID = mt1.post_id ) ';
    
     }
    if ($state_id>0) {
        $sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt2 ON ( ps.ID = mt2.post_id ) ';
    }
    
    $sql.=" WHERE 1=1 AND ps.post_type = 'clubs' AND (ps.post_status = 'publish') ";
    
    
    $sql.=' AND ps.ID !='.$post_id;
    
    if ($city_id>0 || $state_id>0 ) {
    
        $sql.=" AND ( ";
    
    }
    
    
    
    
    if ($city_id>0) {
        $sql.="( mt1.meta_key = 'club_city_id' AND CAST(mt1.meta_value AS CHAR) = '".trim($city_id)."' ) ";
    }
    
     if ( $city_id>0 && $state_id>0 ) {
            $sql.=" AND ";
     }
     
    if ($state_id>0) {
        $sql.=" ( mt2.meta_key = 'club_state_id' AND CAST(mt2.meta_value AS CHAR) = '".trim($state_id)."' ) ";
        
    }
    
    
     if ($city_id>0 || $state_id>0 ) {
   
            $sql.=" ) ";
     }
     
    $sql.=' GROUP BY ps.ID ORDER BY RAND() limit 3 ';
    
    
    
    

    $result = $wpdb->get_results($sql);
    
  //echo "<pre>";  print_r($result); die;

    } 
    
    return $result;
}

?>