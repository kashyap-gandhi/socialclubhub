jQuery(document).ready(function(){


		/*jQuery('#post').submit(function() {

			var form_data = jQuery('#post').serializeArray();
			form_data = jQuery.param(form_data);
			var data = {
				action: 'my_pre_submit_validation',
				//security: '<?php echo wp_create_nonce( 'pre_publish_validation' ); ?>',
				form_data: {email:jQuery("#club_email").val(),user_name:jQuery("#user_name").val(),club_user_id:jQuery("#club_user_id option:selected").val()}
			};
			jQuery.post(ajaxurl, data, function(response){
					
					var res= jQuery.parseJSON(response);
					
				if(res.status == 'success'){
					jQuery('#ajax-loading').hide();
					jQuery('#publish').removeClass('button-primary-disabled');
					
					return true;
				} else {
					alert("Please correct the following errors.");
					
					jQuery("#user_name_error").html(res.label_msg.user_name);
					jQuery("#club_email_error").html(res.label_msg.email);
					
					jQuery('#ajax-loading').hide();
					jQuery('#publish').removeClass('button-primary-disabled');
					return false;
				}
			});
			return false;
		});*/
		
				
				
	jQuery('#club_country_id').change(function(e) {
		var data = {
			'action': 'get_states_of_country',
			'country': jQuery(this).val()
		};
		
		var state_ops = '<option value="">---Select State---</option>';
		var city_ops = '<option value="">---Select City---</option>';
		
		jQuery("#club_state_id").html(state_ops);
		jQuery("#club_city_id").html(city_ops);

		// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
		jQuery.post(ajaxurl, data, function(response) {
			if(jQuery("#club_state_id").length>0)
			{
				jQuery("#club_state_id").html(response);
			}
		});
    });
	
	jQuery('#club_state_id').change(function(e) {
		var data = {
			'action': 'get_cities_of_state',
			'state': jQuery(this).val()
			
		};
		
		
		var city_ops = '<option value="">---Select City---</option>';
		
		jQuery("#club_city_id").html(city_ops);

		// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
		jQuery.post(ajaxurl, data, function(response) {
			if(jQuery("#club_city_id").length>0)
			{
				jQuery("#club_city_id").html(response);
			}
		});
    });
	
	jQuery('#club_city_id').change(function(e) {
		var vlat=jQuery("#club_city_id option:selected").attr('data-lat');
		var vlong=jQuery("#club_city_id option:selected").attr('data-long');
		
		jQuery("#club_lat").val(vlat);
		jQuery("#club_long").val(vlong);
		
	});
	
});