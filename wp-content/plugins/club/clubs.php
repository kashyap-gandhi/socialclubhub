<?php
/*
  Plugin Name: Clubs
  Plugin URI:
  Description: This plugin used for add edit delete and listing clubs module at admin side.
  Version: 1.0
  Author: iWS
  Author URI:
 */


add_action('init', 'create_club');

function create_club() {

    ////====set slug for direct open==/
    /////='supports' => array('title','editor','excerpt','trackbacks','custom-fields','comments','revisions','thumbnail','author','page-attributes',)
    //=taxonomies 'post_tag'

    register_post_type('clubs', array('label' => 'Clubs', 'description' => 'Club Listing', 'public' => true,  'show_ui' => true, 'show_in_menu' => true, 'capability_type' => 'page', 'hierarchical' => true, 'rewrite' => array('slug' => 'club', 'with_front' => true), 'query_var' => true, 'exclude_from_search' => false, 'menu_position' => 18, 'supports' => array('title', 'editor', 'thumbnail'), 'taxonomies' => array(), 'labels' => array(
            'name' => 'Clubs',
            'singular_name' => 'Club',
            'menu_name' => 'Manage Clubs',
            'add_new' => 'Add New',
            'add_new_item' => 'Add New Club',
            'edit' => 'Edit',
            'edit_item' => 'Edit Club',
            'new_item' => 'New Club',
            'view' => 'View Club',
            'view_item' => 'View Club',
            'search_items' => 'Search Clubs',
            'not_found' => 'No Clubs Found',
            'not_found_in_trash' => 'No Clubs found in Trash',
            'parent' => 'Parent Club',
        ),));


    // Add new taxonomy, make it hierarchical (like categories)
    $labels = array(
        'name' => _x('Activities', 'taxonomy general name'),
        'singular_name' => _x('Activity', 'taxonomy singular name'),
        'search_items' => __('Search Activities'),
        'all_items' => __('All Activities'),
        'parent_item' => __('Parent Activity'),
        'parent_item_colon' => __('Parent Activity:'),
        'edit_item' => __('Edit Activity'),
        'update_item' => __('Update Activity'),
        'add_new_item' => __('Add New Activity'),
        'new_item_name' => __('New Activity Name'),
        'menu_name' => __('Activity'),
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'activities'),
    );

    register_taxonomy('club-activities', array('clubs'), $args);
}




///=============add custom column==========


add_filter('manage_edit-clubs_columns', 'clubs_edit_columns');

function clubs_edit_columns($columns) {

    $columns = array(
        'cb' => '<input type="checkbox" />',
        'id' => __('ID'),
        'title' => __('Title'),
        //'club_status' => __('Type'),
        'post_status' => __('Status'),
        'date' => __('date')
    );

    return $columns;
}

add_action('manage_clubs_posts_custom_column', 'my_manage_clubs_columns', 10, 2);

function my_manage_clubs_columns($column_name, $post_id) {
    global $wpdb;
    global $post;

    switch ($column_name) {

        case 'id':
            echo $post_id;
            break;

        case 'post_status':
            echo $post->post_status;

            break;

        case 'club_status':

            $club_status = get_post_meta($post_id, 'club_status', true);

            if ((int) $club_status == (int) 1) {
                echo "Approve";
            } else {
                echo "Unapprove";
            }


            break;




        default:
            break;
    } // end switch
}

add_filter('manage_edit-clubs_sortable_columns', 'my_clubs_sortable_columns');

function my_clubs_sortable_columns($columns) {

    //$columns['id'] = 'id';
    $columns['club_status'] = 'club_status';
    $columns['post_status'] = 'post_status';


    return $columns;
}

/////////////===========add club active part================



require_once 'club_function.php';


//===inline edit 
//http://wpdreamer.com/2012/03/manage-wordpress-posts-using-bulk-edit-and-quick-edit/
//http://wordpress.stackexchange.com/questions/578/adding-a-taxonomy-filter-to-admin-list-for-a-custom-post-type
///http://www.smashingmagazine.com/2013/12/05/modifying-admin-post-lists-in-wordpress/

add_action('restrict_manage_posts', 'clubs_restrict_manage_posts');

function clubs_restrict_manage_posts() {
    global $typenow, $wpdb;

    /* if ($typenow == 'clubs') {
      $args = array(
      'show_option_all' => "Show All Categories",
      'taxonomy' => 'club-activities',
      'name' => 'club-activities',
      'selected' => $_GET['club-activities']
      );
      wp_dropdown_categories($args);
      } */




    // an array of all the taxonomyies you want to display. Use the taxonomy name or slug
    $taxonomies = array('club-activities');

    // must set this to the post type you want the filter(s) displayed on
    if ($typenow == 'clubs') {


        //===add category drop down
        foreach ($taxonomies as $tax_slug) {
            $tax_obj = get_taxonomy($tax_slug);
            $tax_name = $tax_obj->labels->name;
            //$terms = get_terms($tax_slug);
            
            
            $terms=get_all_activities();
            
            //echo "<pre>";    print_r($terms); die;

            if (count($terms) > 0) {
                echo "<select name='club_main_activity' id='club_main_activity' class='postform'>";
                echo "<option value='0'>Show All $tax_name</option>";
                foreach ($terms as $term) {
                    echo '<option value="' . $term->term_id.'"';
                    
                    if(isset($_GET['club_main_activity']) && (int) $_GET['club_main_activity'] == (int) $term->term_id){
                    echo ' selected="selected"';
                            
                    }
                    echo '>' . $term->name . '</option>';
                    
                    
                    //$term->count
                }
                echo "</select>";
            }
        }



        //===custom fields
        echo '';



        $use_country = $wpdb->get_results("SELECT ct.country, pm.meta_value FROM " . $wpdb->prefix . "postmeta pm, " . $wpdb->prefix . "posts ps, " . $wpdb->prefix . "countries ct WHERE pm.post_id=ps.ID  and pm.meta_value=ct.countryid and pm.meta_key = 'club_country_id' AND ps.post_type = 'clubs' AND ps.post_status != 'trash'  group by pm.meta_value ");


        echo "<select name='club_country_id' id='club_country_id' class='postform'>";
        echo "<option value='0'>Show All Country</option>";

        if (!empty($use_country)) {
            foreach ($use_country as $row_country) {
                echo '<option value=' . $row_country->meta_value;
                if (isset($_GET['club_country_id']) && $_GET['club_country_id'] == $row_country->meta_value) {
                    echo ' selected="selected"';
                }
                echo '>' . $row_country->country . '</option>';
            }
        }

        echo "</select>";

        echo '';


        echo '<input type="hidden" name="post_type" class="post_type_page" value="clubs">';


        ///===end code
    }
}

//add_action('request', 'clubs_request');
function clubs_request($request) {
    if (is_admin() && $GLOBALS['PHP_SELF'] == '/wp-admin/edit.php' && isset($request['post_type']) && $request['post_type'] == 'clubs') {
        $request['term'] = get_term($request['club-activities'], 'club-activities')->name;
    }
    return $request;
}

//http://codex.wordpress.org/Class_Reference/WP_Query#Custom_Field_Parameters
add_filter('parse_query', 'club_table_filter');

function club_table_filter($query) {
    if (is_admin() AND $query->query['post_type'] == 'clubs') {
        $qv = &$query->query_vars;
        $qv['meta_query'] = array();

        if (!empty($_GET['club_country_id']) && !empty($_GET['club_main_activity'])) {

            $qv['meta_query']['relation'] = 'AND';
        }

        if (!empty($_GET['club_country_id'])) {

            $club_country_id = (int) $_GET['club_country_id'];

            $qv['meta_query'][] = array(
                'key' => 'club_country_id',
                'value' => $club_country_id,
                'compare' => '=',
                'type' => 'CHAR'
            );
        }

        if (!empty($_GET['club_main_activity'])) {

            $club_main_activities = (int) $_GET['club_main_activity'];

            $qv['meta_query'][] = array(
                'key' => 'club_main_activity',
                'value' => $club_main_activities,
                'compare' => '=',
                'type' => 'NUMERIC'
            );
        }

       
       
//echo "<pre>"; print_r($query); die;

        /* if( !empty( $_GET['orderby'] ) AND $_GET['orderby'] == 'event_date' ) {
          $qv['orderby'] = 'meta_value';
          $qv['meta_key'] = '_bs_meta_event_date';
          $qv['order'] = strtoupper( $_GET['order'] );
          } */
    }
}




//https://pippinsplugins.com/adding-custom-meta-fields-to-taxonomies/
// Add term page
function club_activities_taxonomy_add_new_meta_field() {
	// this will add the custom meta field to the add new term page
	?>
	<div class="form-field">
		<label for="term_meta[show_to_left]"></label>
		<input type="checkbox" name="term_meta[show_to_left]" id="term_meta[show_to_left]" value="1"><?php _e( 'Show to Left Panel' ); ?>
		<p class="description"><?php _e( 'Please check for display in left panel of front side.' ); ?></p>
	</div>
<?php
}
add_action( 'club-activities_add_form_fields', 'club_activities_taxonomy_add_new_meta_field', 10, 2 );




// Edit term page
function club_activities_taxonomy_edit_meta_field($term) {
 
	// put the term ID into a variable
	$t_id = $term->term_id;
 
	// retrieve the existing value(s) for this meta field. This returns an array
	$term_meta = get_option( "taxonomy_".$t_id ); ?>
	<tr class="form-field">
	<th scope="row" valign="top"><label for="term_meta[show_to_left]"><?php _e( 'Show to Left Panel' ); ?></label></th>
		<td>
			<input <?php if(isset($term_meta['show_to_left']) && (int) $term_meta['show_to_left'] ==1) { echo "checked"; } ?> type="checkbox" name="term_meta[show_to_left]" id="term_meta[show_to_left]" value="1">
			<p class="description"><?php _e( 'Please check for display in left panel of front side.' ); ?></p>
		</td>
	</tr>
<?php
}
add_action( 'club-activities_edit_form_fields', 'club_activities_taxonomy_edit_meta_field', 10, 2 );






// Save extra taxonomy fields callback function.
function save_club_activities_taxonomy_custom_meta( $term_id ) {

        $t_id = $term_id;
        $term_meta = get_option( "taxonomy_".$t_id );

	if ( isset( $_POST['term_meta'] ) ) {
		
		$cat_keys = array_keys( $_POST['term_meta'] );
                
                
                
		foreach ( $cat_keys as $key ) {
			if ( isset ( $_POST['term_meta'][$key] ) ) {
				$term_meta[$key] = $_POST['term_meta'][$key];
			}                      
                        
		}
	
	}
        
        
        if ( isset ( $_POST['term_meta']['show_to_left'] ) && (int) $_POST['term_meta']['show_to_left']==(int) 1 ) {
                $term_meta['show_to_left'] = $_POST['term_meta']['show_to_left'];
        } else {
            $term_meta['show_to_left'] = 0;
        }
        
        	// Save the option array.
                //print_r($term_meta); die;
		update_option( "taxonomy_".$t_id, $term_meta );
}  
add_action( 'edited_club-activities', 'save_club_activities_taxonomy_custom_meta', 10, 2 );  
add_action( 'create_club-activities', 'save_club_activities_taxonomy_custom_meta', 10, 2 );





//https://www.skyverge.com/blog/add-custom-bulk-action/
add_action('admin_footer-edit.php', 'clubs_bulk_admin_footer');

function clubs_bulk_admin_footer() {

    global $post_type;

    if ($post_type == 'clubs') {
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function() {
                jQuery('<option>').val('export').text('<?php _e('Export') ?>').appendTo("select[name='action']");
                jQuery('<option>').val('export').text('<?php _e('Export') ?>').appendTo("select[name='action2']");
            });
        </script>
        <?php
    }
}

add_action('load-edit.php', 'clubs_bulk_action');

function clubs_bulk_action() {
    global $typenow;
    $post_type = $typenow;

    if ($post_type == 'clubs') {

        // get the action
        $wp_list_table = _get_list_table('WP_Posts_List_Table');  // depending on your resource type this could be WP_Users_List_Table, WP_Comments_List_Table, etc
        $action = $wp_list_table->current_action();

        $allowed_actions = array("export");
        if (!in_array($action, $allowed_actions))
            return;

        // security check
        //check_admin_referer('bulk-posts');
        // make sure ids are submitted.  depending on the resource type, this may be 'media' or 'ids'
        /* if(isset($_REQUEST['post'])) {
          $post_ids = array_map('intval', $_REQUEST['post']);
          }

          if(empty($post_ids)) return; */

        // this is based on wp-admin/edit.php
        $sendback = remove_query_arg(array('exported', 'doexported', 'untrashed', 'deleted', 'ids'), wp_get_referer());
        if (!$sendback)
            $sendback = admin_url("edit.php?post_type=$post_type");

        $pagenum = $wp_list_table->get_pagenum();
        $sendback = add_query_arg('paged', $pagenum, $sendback);

        switch ($action) {
            case 'export':

                // if we set up user permissions/capabilities, the code might look like:
                //if ( !current_user_can($post_type_object->cap->export_post, $post_id) )
                //	wp_die( __('You are not allowed to export this post.') );

                $exported = 0;
                /* foreach( $post_ids as $post_id ) {

                  if ( !$this->perform_export($post_id) )
                  wp_die( __('Error exporting post.') );

                  $exported++;
                  } */

                //perform_export();
                //$sendback = add_query_arg( array('exported' => $exported, 'ids' => join(',', $post_ids) ), $sendback );

                $sendback = add_query_arg(array('doexported' => $exported), $sendback);

                break;

            default: return;
        }

        $sendback = remove_query_arg(array('action', 'action2', 'tags_input', 'post_author', 'comment_status', 'ping_status', '_status', 'post', 'bulk_edit', 'post_view'), $sendback);

        wp_redirect($sendback);
        exit();
    }
}

if (isset($_REQUEST['doexported']) && (int) $_REQUEST['doexported'] == 0) {

    global $post_type, $pagenow, $typenow;
    $post_type = $typenow;

    if (trim($post_type) == '') {

        if (isset($_REQUEST['post_type'])) {
            $post_type = $_REQUEST['post_type'];
        }
    }


    if ($pagenow == 'edit.php' && $post_type == 'clubs') {
        
        
 //      require_once( ABSPATH . "wp-includes/pluggable.php" );

        club_perform_export();
    }
}


function get_club_meta_all($post_id){
    global $wpdb;

    $data   =   array();

    $wpdb->query(" SELECT `meta_key`, `meta_value`  FROM ".$wpdb->prefix."postmeta  WHERE meta_key!='_edit_last' and meta_key!='_edit_lock' and `post_id` = ".$post_id."  ");

    foreach($wpdb->last_result as $k => $v){
        $data[$v->meta_key] =   $v->meta_value;
    };

    return $data;
}

//==http://stackoverflow.com/questions/16722818/wordpress-admin-widget-that-exports-data
//http://wordpress.stackexchange.com/questions/10505/export-wordpress-table-to-excel
function club_perform_export() {

    global $wpdb,$post;
    // do whatever work needs to be done
    
        
   /*SELECT   sch_posts.* FROM sch_posts  INNER JOIN sch_postmeta ON ( sch_posts.ID = sch_postmeta.post_id )  INNER JOIN sch_postmeta AS mt1 ON ( sch_posts.ID = mt1.post_id )  INNER JOIN sch_postmeta AS mt2 ON ( sch_posts.ID = mt2.post_id )  INNER JOIN sch_postmeta AS mt3 ON ( sch_posts.ID = mt3.post_id ) WHERE 1=1  AND sch_posts.post_type = 'clubs' AND (sch_posts.post_status = 'publish' OR sch_posts.post_status = 'future' OR sch_posts.post_status = 'draft' OR sch_posts.post_status = 'pending' OR sch_posts.post_author = 1 AND sch_posts.post_status = 'private')  AND ( 
  ( sch_postmeta.meta_key = 'club_country_id' AND CAST(sch_postmeta.meta_value AS CHAR) = '113' ) 
  AND 
  ( mt1.meta_key = 'club_main_activity' AND CAST(mt1.meta_value AS CHAR) = '2' ) 
  AND 
  ( mt2.meta_key = 'club_state_id' AND CAST(mt2.meta_value AS CHAR) = '2175' ) 
  AND 
  ( mt3.meta_key = 'club_city_id' AND CAST(mt3.meta_value AS CHAR) = '15771' )
) GROUP BY sch_posts.ID ORDER BY sch_posts.ID ASC */
    
    
    $sql='';
    
    $sql.='SELECT ps.* ';
    
            
    $sql.=' FROM '.$wpdb->prefix.'posts as ps ';
    
     if (isset($_GET['club_country_id']) && !empty($_GET['club_country_id'])) {
        $sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt1 ON ( ps.ID = mt1.post_id ) ';
    
     }
    if (isset($_GET['club_main_activity']) && !empty($_GET['club_main_activity'])) {
        $sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt2 ON ( ps.ID = mt2.post_id ) ';
    }
    
    $sql.=" WHERE 1=1 AND ps.post_type = 'clubs' AND (ps.post_status = 'publish' OR ps.post_status = 'future' OR ps.post_status = 'draft' OR ps.post_status = 'pending' OR ps.post_status = 'private') ";
    
    
    if ( (isset($_GET['club_main_activity']) && !empty($_GET['club_main_activity'])) ||  (isset($_GET['club_country_id']) && !empty($_GET['club_country_id']) ) ) {
    
        $sql.=" AND ( ";
    
    }
    
    
    
    
    if (isset($_GET['club_country_id']) && !empty($_GET['club_country_id'])) {
        $sql.="( mt1.meta_key = 'club_country_id' AND CAST(mt1.meta_value AS CHAR) = '".trim($_GET['club_country_id'])."' ) ";
    }
    
     if ( (isset($_GET['club_main_activity']) && !empty($_GET['club_main_activity'])) &&  isset($_GET['club_country_id']) && !empty($_GET['club_country_id']) ) {
            $sql.=" AND ";
     }
     
    if (isset($_GET['club_main_activity']) && !empty($_GET['club_main_activity'])) {
        $sql.=" ( mt2.meta_key = 'club_main_activity' AND CAST(mt2.meta_value AS CHAR) = '".$_GET['club_main_activity']."' ) ";
        
    }
    
    
     if ( (isset($_GET['club_main_activity']) && !empty($_GET['club_main_activity'])) ||  (isset($_GET['club_country_id']) && !empty($_GET['club_country_id'])) ) {
   
            $sql.=" ) ";
     }
     
    $sql.=' GROUP BY ps.ID ORDER BY ps.ID ASC ';
    
    
    
    //echo $sql;
   // die;
 

    
    $file = 'export'.rand();
    
    $export = mysql_query($sql) or die("Sql error : " . mysql_error());
    
    //echo "<pre>";
    
   // print_r(mysql_fetch_row($export));
    
    //die;
    
    $fields = mysql_num_fields($export);
    $header='';$data='';
    for ($i = 0; $i < $fields; $i++) {
        $header .= mysql_field_name($export, $i) . "\t";
    }
    
   
    
    $cnt=0;
    while ($row = mysql_fetch_row($export)) {
        
        //echo "<pre>";
        //print_r($row);
        //die;
        
         $get_all_meta=get_club_meta_all($row[0]);
        
        // print_r($get_all_meta);
         //die;
         
         if($cnt==0) {
             if(!empty($get_all_meta)) {
                foreach($get_all_meta as $mkey=>$mval) {
                 $header .= $mkey . "\t"; 
                }
             }
         }
         
         $cnt++;
        
         
        $line = '';
        foreach ($row as $value) {
            if ((!isset($value) ) || ( $value == "" )) {
                $value = "\t";
            } else {
                $value = str_replace('"', '""', $value);
                $value = '"' . $value . '"' . "\t";
            }
            $line .= $value;
        }
        
         if(!empty($get_all_meta)) {
                foreach($get_all_meta as $mkey=>$mval) {
                    if ((!isset($mval) ) || ( $mval == "" )) {
                        $mval = "\t";
                    } else {
                        $mval = str_replace('"', '""', $mval);
                        $mval = '"' . $mval . '"' . "\t";
                    }
                    $line .= $mval;
                }
         }
        $data .= trim($line) . "\n";
        
        
        
    }
    $data = str_replace("\r", "", $data);
    if ($data == "") {
        $data = "\n(0) Records Found!\n";
    }

    $filename = $file . "_" . date("M-d-Y");

    header("Content-type: application/octet-stream");
    header("Content-disposition: filename=" . $filename . ".xlsx");
    header("Pragma: no-cache");
    header("Expires: 0");
    print "$header\n$data";
    die;
}



//==change title or metabox on the fly
add_action('add_meta_boxes', 'clubs_change_meta_box_titles', 999);

function clubs_change_meta_box_titles() {
    global $wp_meta_boxes; // array of defined meta boxes
    // cycle through the array, change the titles you want

    remove_meta_box('club-activitiesdiv', 'clubs', 'side');
    //$wp_meta_boxes['clubs']['side']['core']['club-activitiesdiv']['title']= 'Club Other Activities';
}




/**
 * Add stylesheet to the page
 */
add_action('admin_enqueue_scripts', 'club_stylesheet_to_admin');

function club_stylesheet_to_admin() {
    wp_enqueue_style('club_css', plugins_url('/css/club.css', __FILE__));
}

/**
 * Add script to the page
 */
add_action('admin_enqueue_scripts', 'club_script_to_admin');

function club_script_to_admin($page) {
    if (is_admin()) {
        if ('post-new.php' == $page || 'post.php' == $page) {
            wp_register_script('club_js', plugins_url('/js/club_script.js', __FILE__), array('jquery'));
            // Enqueue Scripts that are needed on all the pages
            wp_enqueue_script('jquery');
            wp_enqueue_script('club_js');
        } else {
            return;
        }
    }
}




//add_action('wp_ajax_my_pre_submit_validation', 'pre_submit_validation');
function pre_submit_validation(){
    //simple Security check
   // check_ajax_referer( 'pre_publish_validation', 'security' );

    //do your validation here
    //all of the form fields are in $_POST['form_data'] array
    //and return true to submit: echo 'true'; die();
    //or your error message: echo 'bal bla bla'; die();
    
    $status='fail';
    $msg=''; 
    
    
    
    $lable_msg=array(
        'email'=>'',
        'user_name'=>''
    );
    
    
    if($_POST['form_data']){
        
        
        
        if((int) $_POST['form_data']['club_user_id']==0){
          
          $email = trim($_POST['form_data']['email']);         
          
          $email_exists = get_user_by('email', $email);
          
          if ($email_exists){
              $lable_msg['email']="This E-mail is already registerd. Try with new one.";
          } 
          
          
          
          
          $user_name = trim($_POST['form_data']['user_name']);         
          
          $username_exists = get_user_by('login', $user_name);
          
          if ($username_exists){
              $lable_msg['user_name']="This Username is already registerd. Try with new one.";
          } 
          
          
          if($email_exists && $username_exists){
              $status='fail';
          } else {
              $status='success';
          }
          
          
          
          
          
        } else {
             $status='success';
        }
        
	
        
        
    }
    
    
    
    echo json_encode(array('status'=>$status,'msg'=>$msg,'label_msg'=>$lable_msg));
    
    die();
}

add_action('admin_init', 'meta_club_extra_fields_type_var');

function meta_club_extra_fields_type_var() {



    // please add other post type

    $post_types = array('clubs');



    foreach ($post_types as $post_type) {


        //add_meta_box('club_status_type', 'Club Status', 'club_status_type_setup', $post_type, 'side', 'high');
        add_meta_box('club_contact_info', 'Club Information', 'club_contact_info_setup', $post_type, 'normal', 'high');
        add_meta_box('club_location_info', 'Club Location', 'club_address_info_setup', $post_type, 'normal', 'high');
        add_meta_box('club_activity_info', 'Club Activity', 'club_activity_info_setup', $post_type, 'normal', 'high');
    }

    // save metabox
    
    add_action('save_post', 'club_extra_field_save');
    
    //add_action('save_post', 'club_extra_field_save',10,2);
    //add_action('save_post', 'club_completion_validator', 20, 2);
}

function club_activity_info_setup() {

    global $wpdb, $post;


    $club_main_activity = get_post_meta($post->ID, 'club_main_activity', TRUE);
    $club_other_activity = array();

    $get_other_activity = get_post_meta($post->ID, 'club_other_activities', TRUE);

    if ($get_other_activity != '') {
        $club_other_activity = explode(',', $get_other_activity);
    }


    

    $res_activities = get_all_activities();
    ?>


    <div class="left_label">
        <label for="club_adderss" class="">Club Main Activity</label>
    </div>
    <div class="right_label">

        <select name="club_main_activity" id="club_main_activity">
            <option value="0">---Select Main Activity---</option>                    
    <?php
    if (!empty($res_activities)) {
        foreach ($res_activities as $row_acti) {
            ?>
                    <option value="<?php echo $row_acti->term_id; ?>" <?php if ($row_acti->term_id == $club_main_activity) { ?> selected <?php } ?>><?php echo ucwords($row_acti->name); ?></option>
        <?php }
    } ?>

        </select>
    </div>
    <div class="clear"></div>



    <div class="left_label">
        <label for="club_adderss" class="">Club Other Activities</label>
    </div>
    <div class="right_label">

    <?php
    if (!empty($res_activities)) {
        foreach ($res_activities as $row_acti) {
            ?>
                <div class="col-33"><input type="checkbox" name="club_other_activities[]" value="<?php echo $row_acti->term_id; ?>" <?php if (!empty($club_other_activity)) {
                if (in_array($row_acti->term_id, $club_other_activity)) { ?> checked <?php }
            } ?> /> <label><?php echo ucwords($row_acti->name); ?></label></div>
                <?php }
            } ?>


    </div>
    <div class="clear"></div>  


    <?php
    echo '<input type="hidden" name="iws_adminclub_nonce" value="' . wp_create_nonce('iws-adminclub-nonce') . '" />';
}

function club_address_info_setup() {
    global $wpdb, $post;

    $club_address = get_post_meta($post->ID, 'club_adderss', TRUE);
    
    $club_zip_code = get_post_meta($post->ID, 'club_zip_code', TRUE);
    
    $club_city_id = get_post_meta($post->ID, 'club_city_id', TRUE);
    $club_state_id = get_post_meta($post->ID, 'club_state_id', TRUE);
    $club_country_id = get_post_meta($post->ID, 'club_country_id', TRUE);
    $club_lat = get_post_meta($post->ID, 'club_lat', TRUE);
    $club_long = get_post_meta($post->ID, 'club_long', TRUE);



   
    $res_countries = get_all_country();


    $res_states = array();
    if ($club_country_id > 0) {
        
        $res_states = get_states_from_country_by_id($club_country_id);
    }

    $res_cities = array();
    if ($club_state_id > 0) {
        
        $res_cities = get_cities_from_state_by_id($club_state_id);
    }
    ?>

    <div class="left_label">
        <label for="club_adderss" class="">Address</label>
    </div>
    <div class="right_label">
        <input type="text" id="club_adderss" value="<?php echo $club_address; ?>" size="50" name="club_adderss">
    </div>
    <div class="clear"></div>



    <div class="left_label">
        <label for="club_country_id" class="">Country</label>
    </div>
    <div class="right_label">

        <select name="club_country_id" id="club_country_id">
            <option value="0">---Select Country---</option>   
    <?php if (!empty($res_countries)) {
        foreach ($res_countries as $row_country) { ?>

                    <option value="<?php echo $row_country->countryid; ?>" <?php if ($row_country->countryid == $club_country_id) { ?> selected <?php } ?>><?php echo ucfirst($row_country->country); ?></option>

        <?php }
    } ?>

        </select>

    </div>
    <div class="clear"></div>


    <div class="left_label">
        <label for="club_state_id" class="">State</label>
    </div>
    <div class="right_label">

        <select name="club_state_id" id="club_state_id">
            <option value="0">---Select State---</option>

    <?php
    if (!empty($res_states)) {
        foreach ($res_states as $row_states) {
            ?>

                    <option value="<?php echo $row_states->regionid; ?>" <?php if ($row_states->regionid == $club_state_id) { ?> selected <?php } ?>><?php echo ucfirst($row_states->region); ?></option>


                <?php
                }
            }
            ?>

        </select>

    </div>
    <div class="clear"></div>


    <div class="left_label">
        <label for="club_city_id" class="">City</label>
    </div>
    <div class="right_label">

        <select name="club_city_id" id="club_city_id">
            <option value="0">---Select City---</option>

            <?php
            if (!empty($res_cities)) {
                foreach ($res_cities as $row_cities) {
                    ?>

                    <option value="<?php echo $row_cities->CityId; ?>" data-lat="<?php echo $row_cities->Latitude; ?>" data-long="<?php echo $row_cities->Longitude; ?>" <?php if ($row_cities->CityId == $club_city_id) { ?> selected <?php } ?>><?php echo ucfirst($row_cities->City); ?></option>


                <?php }
            } ?>

        </select>

    </div>
    <div class="clear"></div>
    
    
    
    <div class="left_label">
        <label for="club_adderss" class="">Zipcode</label>
    </div>
    <div class="right_label">
        <input type="text" id="club_zip_code" value="<?php echo $club_zip_code; ?>" size="16" maxlength="12" name="club_zip_code">
    </div>
    <div class="clear"></div>


    <input type="hidden" name="club_lat" id="club_lat" value="<?php echo $club_lat; ?>" />
    <input type="hidden" name="club_long" id="club_long" value="<?php echo $club_long; ?>" />

    <?php
    
    echo '<input type="hidden" name="iws_adminclub_nonce" value="' . wp_create_nonce('iws-adminclub-nonce') . '" />';
}



function club_contact_info_setup() {

    global $wpdb, $post;

    $club_first_name = get_post_meta($post->ID, 'club_first_name', TRUE);
    $club_last_name = get_post_meta($post->ID, 'club_last_name', TRUE);

    $club_email = get_post_meta($post->ID, 'club_email', TRUE);
    $club_phone_number = get_post_meta($post->ID, 'club_phone_number', TRUE);

    $club_no_of_members = get_post_meta($post->ID, 'club_no_of_members', TRUE);
    $club_event_host = get_post_meta($post->ID, 'club_event_host', TRUE);
    $club_website = get_post_meta($post->ID, 'club_website', TRUE);

    $club_user_id = get_post_meta($post->ID, 'club_user_id', TRUE);

    
    
    


    //$get_customer = new WP_User_Query(array('role' => array('administrator','customer'), 'orderby' => 'display_name', 'order' => 'ASC'));

    

    $roles = array('customer', 'administrator');

$get_customer=get_user_by_role($roles);
    
    ?>




    <div id="select_user">

        <div class="left_label">
            <label for="club_user_id" class="">Select User</label>
        </div>
        <div class="right_label">
            <select name="club_user_id" id="club_user_id">
                <option value="0">---Select User---</option>
    <?php
    if (!empty($get_customer->results)) {
        foreach ($get_customer->results as $customer) {
            ?>
                        <option value="<?php echo $customer->ID; ?>" <?php if ($customer->ID == $club_user_id) { ?> selected <?php } ?>><?php echo $customer->display_name . " || " . $customer->user_email; ?></option>
        <?php }
    } ?>
            </select> <small>(Club owner user)</small>

        </div>
        <div class="clear"></div>


    </div>

    <div id="signup_user">

        <div class="left_label">
            <label for="club_first_name" class="">First Name</label>
        </div>
        <div class="right_label">
            <input type="text" id="club_first_name" value="<?php echo $club_first_name; ?>" size="30" name="club_first_name">
        </div>
        <div class="clear"></div>

        <div class="left_label">
            <label for="club_last_name" class="">Last Name</label>
        </div>
        <div class="right_label">
            <input type="text" id="club_last_name" value="<?php echo $club_last_name; ?>" size="30" name="club_last_name">
        </div>
        <div class="clear"></div>


        <div class="left_label">
            <label for="club_email" class="">Email</label>
        </div>
        <div class="right_label">
            <input type="text" id="club_email" value="<?php echo $club_email; ?>" size="30" name="club_email" />
            <span class="club_email_error" id="club_email_error" style="color:red;"></span>
        </div>
        <div class="clear"></div>

        
        
        <?php /*
        <div class="left_label">

            <label for="user_name" class="">Username</label>
        </div>
        <div class="right_label">
            <input type="text" id="user_name" value="<?php echo $user_name; ?>" size="30" name="user_name">
            <span class="user_name_error" id="user_name_error" style="color:red;"></span>
        </div>
        <div class="clear"></div>
        */ ?>


        <div class="left_label">
            <label for="club_phone_numer" class="">Phone Number</label>
        </div>
        <div class="right_label">
            <input type="text" id="club_phone_number" value="<?php echo $club_phone_number; ?>" size="30" name="club_phone_number">
        </div>
        <div class="clear"></div>







<?php /*
        <div class="left_label">
            <label for="password" class="">Password</label>
        </div>
        <div class="right_label">
            <input type="password" id="password" value="<?php echo $password; ?>" size="30" name="password">
        </div>
        <div class="clear"></div>

        <div class="left_label">
            <label for="confirm_password" class="">Confirm Password</label>
        </div>
        <div class="right_label">
            <input type="password" id="confirm_password" value="<?php echo $confirm_password; ?>" size="30" name="confirm_password">
        </div>
        <div class="clear"></div>
 
 */ ?>
    </div>  


    <div class="left_label">
        <label for="club_website" class="">Website</label>
    </div>
    <div class="right_label">
        <input type="text" id="club_website" value="<?php echo $club_website; ?>" size="30" name="club_website">
    </div>
    <div class="clear"></div>


    <div class="left_label">
        <label for="club_no_of_members" class="">Number Of Members</label>
    </div>
    <div class="right_label">
        <input type="text" id="club_no_of_members" value="<?php echo $club_no_of_members; ?>" size="30" name="club_no_of_members">
    </div>
    <div class="clear"></div>


    <div class="left_label">
        <label for="club_event_host" class="">Where do you host most of your club events?</label>
    </div>
    <div class="right_label">
        <input type="text" id="club_event_host" value="<?php echo $club_event_host; ?>" size="30" name="club_event_host">
    </div>
    <div class="clear"></div>



    <?php
    echo '<input type="hidden" name="iws_adminclub_nonce" value="' . wp_create_nonce('iws-adminclub-nonce') . '" />';
}

function club_status_type_setup() {

    global $post;


    $club_status = get_post_meta($post->ID, 'club_status', TRUE);
    ?>



    <select name="club_status">


        <option <?php
    if ((int) $club_status == (int) 0 || $club_status == '') {
        echo ' selected="selected"';
    }
    ?>>Un-approve</option>
        <option <?php
    if ((int) $club_status == (int) 1) {
        echo ' selected="selected"';
    }
    ?>>Approve</option>



    </select>



    <?php
    // create a custom nonce for submit verification later

    echo '<input type="hidden" name="iws_adminclub_nonce" value="' . wp_create_nonce('iws-adminclub-nonce') . '" />';
}

//===validation

/*
  add_action('pre_post_update', 'validate_meta', 99, 2);
  function validate_meta($post_id){

  if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return $post_id;

  if (!isset($_POST['post_type']) )  return $post_id;

  if ( !current_user_can( 'edit_posts', $post_id ) ) return $post_id;

  $errors = array();

  if(trim($_POST['user_name']) == ''){
  $errors[] = new WP_Error('cpt_validation_titke',  'Bitte einen Titel eintragen', 'error');
  }

  if (!empty($errors)) {
  add_user_meta(get_current_user_id(), 'club_admin_notices', $errors, true);
  $url = admin_url( 'post.php?post=' . $post_id ) . '&action=edit';
  //exit;
  wp_redirect( $url );
  exit;
  }
  return $_POST;
  } */

// Display any errors
add_action('admin_notices', 'club_admin_notice_handler');

function club_admin_notice_handler() {

    global $post_type, $pagenow;

    $html='';
    
    if (isset($_GET['message'])) {

        if ((int) $_GET['message'] == (int) 4) {
            $html = '<div class="error">
                        <p><strong>Validation errors</strong></p>
                        <p>- Please select club user.</p></div>';
        }
    }



    if ($pagenow == 'edit.php' && $post_type == 'clubs' && isset($_REQUEST['doexported']) && (int) $_REQUEST['doexported']) {
        $message = _('Club cxported.');
        $html = "<div class='updated'><p>".$message."</p></div>";
    }

    echo $html;

    /*
      $user_id = get_current_user_id();
      $admin_notices = get_user_meta($user_id, 'club_admin_notices', true);

      if(!empty($admin_notices)){
      $html = '';

      if(is_wp_error($admin_notices[0])){

      delete_user_meta($user_id, 'club_admin_notices');

      foreach($admin_notices AS $notice){

      $msgs = $notice->get_error_messages();

      if(!empty($msgs)){
      $msg_type = $notice->get_error_data();
      if(!empty($notice_type)){
      $html .= '<div class="'.$msg_type.'">';
      } else {
      $html .= '<div class="error">';
      $html .= '<p><strong>Validation errors</strong></p>';
      }

      foreach($msgs as $msg){
      $html .= '<p>- '.$msg.'</p>';
      }
      $html .= '</div>';
      }
      }
      }

      echo $html;
      } */
}

function club_completion_validator($post_id, $post) {


    // don't do on autosave or when new posts are first created
    if (( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) || $post->post_status == 'auto-draft')
        return $post_id;
    if ($post->post_type != 'clubs')
        return $post_id;





    // init completion marker (add more as needed)
    $meta_missing = false;

    // retrieve meta to be validated
    $club_user_id = $_POST['club_user_id'];

    // just checking it's not empty - you could do other tests...
    if (empty($club_user_id) || (int) $club_user_id == (int) 0) {
        $meta_missing = true;
    }

    // on attempting to publish - check for completion and intervene if necessary
    if (( isset($_POST['publish']) || isset($_POST['save']) ) && $_POST['post_status'] == 'publish') {
        //  don't allow publishing while any of these are incomplete
        if ($meta_missing == true) {
            global $wpdb;
            $wpdb->update($wpdb->posts, array('post_status' => 'pending'), array('ID' => $post_id));
            // filter the query URL to change the published message
            add_filter('redirect_post_location', create_function('$location', 'return add_query_arg("message", "4", $location);'));
        }
    }
}

function club_extra_field_save($post_id) {

    global $post, $wpdb;
    
        // don't do on autosave or when new posts are first created
    if (( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) || (isset($post) && !empty($post) && $post->post_status == 'auto-draft')) return $post_id;

    
    
    // authentication checks
    // make sure data came from our meta box
    
         

  
if($_POST && isset($_POST['iws_adminclub_nonce']) && wp_verify_nonce($_POST['iws_adminclub_nonce'],'iws-adminclub-nonce')){     






    // check user permissions

    if ($_POST['post_type'] == 'page') {

        if (!current_user_can('edit_page', $post_id))
            return $post_id;
    } else {

        if (!current_user_can('edit_post', $post_id))
            return $post_id;
    }
    
    
    
    
    //====check user part===
    
    

    //===db validation  http://www.paulund.co.uk/data-validation-with-wordpress
   
    
  
    
    
    
    
    //===info all
    

    $club_first_name = sanitize_text_field($_POST['club_first_name']);
    update_post_meta($post_id, 'club_first_name', esc_attr($club_first_name));

    $club_last_name = sanitize_text_field($_POST['club_last_name']);
    update_post_meta($post_id, 'club_last_name', esc_attr($club_last_name));

    $club_email = sanitize_email($_POST['club_email']);
    update_post_meta($post_id, 'club_email', esc_attr($club_email));

    $club_phone_number = sanitize_text_field($_POST['club_phone_number']);
    update_post_meta($post_id, 'club_phone_number', esc_attr($club_phone_number));

    $club_no_of_members = (int) sanitize_text_field($_POST['club_no_of_members']);
    update_post_meta($post_id, 'club_no_of_members', esc_attr($club_no_of_members));

    $club_event_host = sanitize_text_field($_POST['club_event_host']);
    update_post_meta($post_id, 'club_event_host', esc_attr($club_event_host));

    $club_website = sanitize_text_field($_POST['club_website']);
    update_post_meta($post_id, 'club_website', esc_url_raw($club_website));

    $club_user_id = (int) sanitize_text_field($_POST['club_user_id']);
    update_post_meta($post_id, 'club_user_id', esc_attr($club_user_id));

    
    
    
    
    
    $wpdb->update($wpdb->posts, array('post_author' => $club_user_id), array('ID' => $post_id));





    //==activity


    $club_other_activity = '';

    if(isset($_POST['club_other_activities'])){
    $get_other_activity = $_POST['club_other_activities'];

    if (!empty($get_other_activity)) {
        $get_other_activity = array_unique(array_map('intval', $get_other_activity));
        $club_other_activity = implode(',', $get_other_activity);

        if ($club_other_activity == ',') {
            $club_other_activity = '';
        }
    }
    }
    update_post_meta($post_id, 'club_other_activities', esc_attr($club_other_activity));


    $club_main_activity = (int) sanitize_text_field($_POST['club_main_activity']);
    update_post_meta($post_id, 'club_main_activity', esc_attr($club_main_activity));



    //===status

    /*$club_status = (int) sanitize_text_field($_POST['club_status']);
    update_post_meta($post_id, 'club_status', esc_attr($club_status));*/


    ///===address

    $club_address = sanitize_text_field($_POST['club_adderss']);
    update_post_meta($post_id, 'club_adderss', esc_attr($club_address));
    
    $club_zip_code = sanitize_text_field($_POST['club_zip_code']); 
    update_post_meta($post_id, 'club_zip_code', esc_attr($club_zip_code));

    $club_country_id = (int) sanitize_text_field($_POST['club_country_id']);
    update_post_meta($post_id, 'club_country_id', esc_attr($club_country_id));

    $club_state_id = (int) sanitize_text_field($_POST['club_state_id']);
    update_post_meta($post_id, 'club_state_id', esc_attr($club_state_id));

    $club_city_id = (int) sanitize_text_field($_POST['club_city_id']);
    update_post_meta($post_id, 'club_city_id', esc_attr($club_city_id));

    $club_lat = sanitize_text_field($_POST['club_lat']);
    update_post_meta($post_id, 'club_lat', esc_attr($club_lat));

    $club_long = sanitize_text_field($_POST['club_long']);
    update_post_meta($post_id, 'club_long', esc_attr($club_long));



    // just checking it's not empty - you could do other tests...
    if (empty($club_user_id) || (int) $club_user_id == (int) 0) {
        $meta_missing = true;
    }

    
     
        
        
    // on attempting to publish - check for completion and intervene if necessary
    if (( isset($_POST['publish']) || isset($_POST['save']) ) && $_POST['post_status'] == 'publish') {
        //  don't allow publishing while any of these are incomplete
        if ($meta_missing == true) {
            
            $wpdb->update($wpdb->posts, array('post_status' => 'pending'), array('ID' => $post_id));
            // filter the query URL to change the published message
            add_filter('redirect_post_location', create_function('$location', 'return add_query_arg("message", "4", $location);'));
        }
    }

    }

    return $post_id;
}



function get_latest_clubs(){
    
    global $wpdb,$post;
       
    
    $sql='';
    
    $sql.='SELECT ps.* ';
    
            
    $sql.=' FROM '.$wpdb->prefix.'posts as ps ';
    
    /*if (isset($_GET['club_country_id']) && !empty($_GET['club_country_id'])) {
        $sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt1 ON ( ps.ID = mt1.post_id ) ';
    
     }
    if (isset($_GET['club_main_activity']) && !empty($_GET['club_main_activity'])) {
        $sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt2 ON ( ps.ID = mt2.post_id ) ';
    }*/
    
    $sql.=" WHERE 1=1 AND ps.post_type = 'clubs' AND (ps.post_status = 'publish' ) ";
    
    
    /*if ( (isset($_GET['club_main_activity']) && !empty($_GET['club_main_activity'])) ||  (isset($_GET['club_country_id']) && !empty($_GET['club_country_id']) ) ) {
    
        $sql.=" AND ( ";
    
    }
    
    
    
    
    if (isset($_GET['club_country_id']) && !empty($_GET['club_country_id'])) {
        $sql.="( mt1.meta_key = 'club_country_id' AND CAST(mt1.meta_value AS CHAR) = '".trim($_GET['club_country_id'])."' ) ";
    }
    
     if ( (isset($_GET['club_main_activity']) && !empty($_GET['club_main_activity'])) &&  isset($_GET['club_country_id']) && !empty($_GET['club_country_id']) ) {
            $sql.=" AND ";
     }
     
    if (isset($_GET['club_main_activity']) && !empty($_GET['club_main_activity'])) {
        $sql.=" ( mt2.meta_key = 'club_main_activity' AND CAST(mt2.meta_value AS CHAR) = '".$_GET['club_main_activity']."' ) ";
        
    }
    
    
     if ( (isset($_GET['club_main_activity']) && !empty($_GET['club_main_activity'])) ||  (isset($_GET['club_country_id']) && !empty($_GET['club_country_id']) )) {
   
            $sql.=" ) ";
     }*/
     
    $sql.=' GROUP BY ps.ID ORDER BY ps.ID DESC ';
    
    
    
    //echo $sql;
   // die;
 

    $get_club = mysql_query($sql) or die("Sql error : " . mysql_error());
    
    $lhtml='';
    
    if(mysql_num_rows($get_club)>0){
        while ($row = mysql_fetch_array($get_club)) {
            
            $thumb_src= esc_url( get_template_directory_uri() ).'/images/default_image.jpg';
            $attach_id = get_post_thumbnail_id( $row['ID'] );
            if($attach_id>0){
                $image_url = wp_get_attachment_image_src( $attach_id, 'sch-thumb' );
                if(isset($image_url[0]) && $image_url[0]!=''){
                    $thumb_src=$image_url[0];
                }
            }
            
            $club_name=$row['post_title'];
            
            $club_link=get_permalink($row['ID']);
            
            $lhtml.='<div class="ca-item"><div class="ca-item-main">';
            $lhtml.='<a href="'.$club_link.'">';
            $lhtml.='<img src="'.$thumb_src.'" class="img-responsive aligncenter" alt="'.$club_name.'" title="'.$club_name.'"/>';
            $lhtml.='</a>';
            $lhtml.='</div></div>';
        
        }
    }
    
    return $lhtml;
}
add_shortcode('club_latest','get_latest_clubs');



function check_club_owner($id){
    
     global $wpdb,$post,$current_user;

    $user = $current_user;
    
    
   
    
    
    $sql='';
    
    $sql.='SELECT ps.* ';
    
            
    $sql.=' FROM '.$wpdb->prefix.'posts as ps ';
    
    
    $sql.=" WHERE 1=1 AND ps.post_type = 'clubs' AND (ps.post_status = 'draft' ||  ps.post_status = 'future' || ps.post_status = 'publish' || ps.post_status = 'pending') ";
    
    $sql.=" and ps.ID=".$id;
    
    $sql.=" and ps.post_author=".$user->ID;
     
    $sql.=' GROUP BY ps.ID ORDER BY ps.ID DESC ';
    
    if(mysql_num_rows(mysql_query($sql))>0){
        return true;
    }
    return false;
    
        
}


function get_user_clubs(){
    global $wpdb,$post;
    
    
    // only show the registration form to non-logged-in members
    if (is_user_logged_in()) {

    } else {
        $redirect_link = wp_login_url();
        wp_redirect($redirect_link);
        exit;
    }
    
    
    
    
    if(isset($_GET['act']) && trim($_GET['act'])!=''){
        $act_type=trim($_GET['act']);
        switch ($act_type){
            
            case 'add':
                require_once 'club_add.php';
                break;
            case 'edit':
                
                if(isset($_GET['id']) && (int) $_GET['id']>0){
                
                    $club_id = (int) $_GET['id'];
                    $chk_owner=check_club_owner($club_id);
                    
                    if($chk_owner){
                        require_once 'club_add.php';
                    } else {
                        wp_redirect(original_page_url().'?msg=notowner');
                    }
                } else {
                    wp_redirect(original_page_url().'?msg=notfound');
                }
                
                break;
            default :
                break;
        }
    } else {
       require_once 'club_list.php';
    }
    
    
    
    
}

add_shortcode('my_clubs','get_user_clubs');





function iws_front_addedit_club($post_id) {

    global $wpdb,$post,$current_user;

    $user = $current_user;
    
    
            
  
if($_POST && isset($_POST['iws_club_editadd_nonce']) && wp_verify_nonce($_POST['iws_club_editadd_nonce'],'iws-club-editadd-nonce')){     

    
        
    $act='add';
    $post_id=0;
    $msg='clubaddsuccess';
    
    
    $post_status='pending';
    $post_type='clubs';
            
    
    
    
    if(isset($_GET['act']) && trim($_GET['act'])=='edit'){
        if(isset($_GET['id']) && (int) $_GET['id']>0){
                
                    $club_id = (int) $_GET['id'];
                    $chk_owner=check_club_owner($club_id);
                    
                    if($chk_owner){
                        
                        $post_data=get_post($club_id,true);
                        
                        $post_status=$post_data['post_status'];
                        
                        $post_id=$club_id;                       
                        $act='edit';
                        $msg='clubupdatesuccess';
                        
                    } else {
                        wp_redirect(original_page_url().'?msg=notowner');
                    }
                } else {
                    wp_redirect(original_page_url().'?msg=notfound');
                }
        }   
    
    
           
    
    
    
        $club_email = trim($_POST["club_email"]);
        $club_first_name = trim($_POST["club_first_name"]);
        $club_last_name = trim($_POST["club_last_name"]);
        
        
        
        $club_name = trim($_POST["club_name"]);
        $club_description = trim($_POST["club_description"]);
        
        $club_phone_number = trim($_POST["club_phone_number"]);
        $club_no_of_members = (int) $_POST["club_no_of_members"];
        $club_event_host = trim($_POST["club_event_host"]);
        $club_website = trim($_POST["club_website"]);
        
        $club_main_activity = (int) $_POST["club_main_activity"];
        
       
        
        if($club_name==''){
            iws_errors()->add('club_name_empty', __('Please enter a club name'));
        }
        if (4 > strlen($club_name)) {
            iws_errors()->add('club_name_length', __('Club name too short. At least 4 characters is required'));
        }
        
        if($club_website==''){
            iws_errors()->add('club_website_empty', __('Please enter a club website'));
        }
        
        if(!valid_url($club_website)){
            iws_errors()->add('club_website_valid', __('Please enter a valid club website'));
        }
        
        
        
        if($club_phone_number==''){
            iws_errors()->add('club_phone_number_empty', __('Please enter a club phone number'));
        }
        
        if (9 > strlen($club_phone_number)) {
            iws_errors()->add('club_phone_number_min_length', __('Club phone number is too short. At least 9 digit is required'));
        }
        
        if (strlen($club_phone_number)>15) {
            iws_errors()->add('club_phone_number_max_length', __('Club phone number is too long. Maximum 15 digit is allowed'));
        }
        
        if(!contact_check($club_phone_number)){
            iws_errors()->add('club_phone_number_valid', __('Please enter a valid club phone number'));
        }
        
        
        if (strlen($club_event_host)>70) {
            iws_errors()->add('club_event_host_max_length', __('Club event host place is too long. Maximum 70 characters is allowed'));
        }
        
        
        
        if (strlen($club_first_name)>20) {
            iws_errors()->add('user_first_max_length', __('Club contact person first name is too long. Maximum 20 characters is allowed'));
        }
        
        if (strlen($club_last_name)>20) {
            iws_errors()->add('user_last_max_length', __('Club contact person last name is too long. Maximum 20 characters is allowed'));
        }
        
        
        
        
        
        
        
        
        
        if (!is_email($club_email)) {
            //invalid email
            iws_errors()->add('email_invalid', __('Invalid email'));
        }
        
        if ($club_main_activity=='' || $club_main_activity==0) {
            //invalid email
            iws_errors()->add('select_main_activity', __('Please select club main activity.'));
        }
        
        
        
        if(isset($_POST["club_adderss"]) && trim($_POST["club_adderss"])==''){
            iws_errors()->add('enter_address', __('Please enter club address.'));
        } else {
            
            if (strlen(trim($_POST["club_adderss"]))>50) {
                iws_errors()->add('address_max_limit', __('Please enter club address maximum 50 characters.'));
            }
            
            if (strlen(trim($_POST["club_adderss"]))<5) {
                iws_errors()->add('address_max_limit', __('Please enter club address minimum 5 characters.'));
            }
        }
        
        
        if (isset($_POST["club_country_id"]) && ( (int) $_POST["club_country_id"]==0 || (int) $_POST["club_country_id"]=='')) {
            //invalid email
            iws_errors()->add('select_country', __('Please select club country.'));
        }
        
        if (isset($_POST["club_state_id"]) && ( (int) $_POST["club_state_id"]==0 || (int) $_POST["club_state_id"]=='')) {
            //invalid email
            iws_errors()->add('select_state', __('Please select club state.'));
        }
        
        if (isset($_POST["club_city_id"]) && ( (int) $_POST["club_city_id"]==0 || (int) $_POST["club_city_id"]=='')) {
            //invalid email
            iws_errors()->add('select_city', __('Please select club city.'));
        }
        
        
        if(isset($_POST["club_zip_code"]) && trim($_POST["club_zip_code"])==''){
            iws_errors()->add('enter_zipcode', __('Please enter club postal code.'));
        } else {
            
            if (strlen(trim($_POST["club_zip_code"]))>20) {
                iws_errors()->add('zipcode_max_limit', __('Please enter club postal code maximum 20 characters.'));
            }
            if (strlen(trim($_POST["club_zip_code"]))<3) {
                iws_errors()->add('zipcode_min_limit', __('Please enter club postal code minimum 3 characters.'));
            }
        }
        
       

        $errors = iws_errors()->get_error_messages();

        // only create the user in if there are no errors
        if (empty($errors)) {


            
            
            
            

        
        $club_name = esc_attr(sanitize_text_field($club_name));
        $club_description = esc_textarea(($club_description));

        $slug=sanitize_title_with_dashes($club_name);
        
        
        
        if($act=='edit' && $post_id>0){
            
            
            $new_post = array(
                'ID'=>$post_id,
                'post_title' => $club_name,
                'post_content' => $club_description
            );
            wp_update_post($new_post);
            
            
        } else {
            
            
            $new_post = array(
                'post_title' => $club_name,
                'post_name' => $slug,
                'post_content' => $club_description,
                'post_status' => $post_status,
                'post_author' => $user->ID,
                'post_type' => $post_type,

            );
            $post_id = wp_insert_post($new_post);
            
        }
    
    
    
        //==thumbnail
        
        // set featured image if there's any
            if (isset($_POST['iws_files']['_thumbnail_id'])) {
                $attachment_id = $_POST['iws_files']['_thumbnail_id'][0];

                iws_update_thumbnail($post_id, $attachment_id);
            }
    
    //===info all
    

    $club_first_name = sanitize_text_field($_POST['club_first_name']);
    update_post_meta($post_id, 'club_first_name', esc_attr($club_first_name));

    $club_last_name = sanitize_text_field($_POST['club_last_name']);
    update_post_meta($post_id, 'club_last_name', esc_attr($club_last_name));

    $club_email = sanitize_email($_POST['club_email']);
    update_post_meta($post_id, 'club_email', esc_attr($club_email));

    $club_phone_number = sanitize_text_field($_POST['club_phone_number']);
    update_post_meta($post_id, 'club_phone_number', esc_attr($club_phone_number));

    $club_no_of_members = (int) sanitize_text_field($_POST['club_no_of_members']);
    update_post_meta($post_id, 'club_no_of_members', esc_attr($club_no_of_members));

    $club_event_host = sanitize_text_field($_POST['club_event_host']);
    update_post_meta($post_id, 'club_event_host', esc_attr($club_event_host));

    $club_website = sanitize_text_field($_POST['club_website']);
    update_post_meta($post_id, 'club_website', esc_url_raw($club_website));

    $club_user_id = (int) $user->ID;
    update_post_meta($post_id, 'club_user_id', esc_attr($club_user_id));

    
    //==activity


    $club_other_activity = '';

    if(isset($_POST['club_other_activities'])){
    $get_other_activity = $_POST['club_other_activities'];

    if (!empty($get_other_activity)) {
        $get_other_activity = array_unique(array_map('intval', $get_other_activity));
        $club_other_activity = implode(',', $get_other_activity);

        if ($club_other_activity == ',') {
            $club_other_activity = '';
        }
    }
    }
    update_post_meta($post_id, 'club_other_activities', esc_attr($club_other_activity));


    $club_main_activity = (int) sanitize_text_field($_POST['club_main_activity']);
    update_post_meta($post_id, 'club_main_activity', esc_attr($club_main_activity));



    //===status

    /*$club_status = (int) sanitize_text_field($_POST['club_status']);
    update_post_meta($post_id, 'club_status', esc_attr($club_status));*/


    ///===address

    $club_address = sanitize_text_field($_POST['club_adderss']);
    update_post_meta($post_id, 'club_adderss', esc_attr($club_address));
    
    $club_zip_code = sanitize_text_field($_POST['club_zip_code']); 
    update_post_meta($post_id, 'club_zip_code', esc_attr($club_zip_code));

    $club_country_id = (int) sanitize_text_field($_POST['club_country_id']);
    update_post_meta($post_id, 'club_country_id', esc_attr($club_country_id));

    $club_state_id = (int) sanitize_text_field($_POST['club_state_id']);
    update_post_meta($post_id, 'club_state_id', esc_attr($club_state_id));

    $club_city_id = (int) sanitize_text_field($_POST['club_city_id']);
    update_post_meta($post_id, 'club_city_id', esc_attr($club_city_id));

    $club_lat = sanitize_text_field($_POST['club_lat']);
    update_post_meta($post_id, 'club_lat', esc_attr($club_lat));

    $club_long = sanitize_text_field($_POST['club_long']);
    update_post_meta($post_id, 'club_long', esc_attr($club_long));


    

        wp_redirect(original_page_url().'?msg='.$msg);
    
         die; 
        
        }  

    }

    
}
add_action('init', 'iws_front_addedit_club');



add_action('wp_ajax_iws_club_delete', 'delete_club');

function delete_club() {
    
    
    
    if(isset($_POST['id']) && (int) $_POST['id']>0){
                
        $club_id = (int) $_POST['id'];
        $chk_owner=check_club_owner($club_id);

        if($chk_owner){
            
            //===put delete code with image delte from folder
            
            if(wp_trash_post($club_id)){
                $msg='success'; //clubdeletefail
            } else {
              $msg='clubdeletefail'; //clubdeletefail
            }
            
            
        } else {
            $msg='notowner';
        }
    } else {
        $msg='notfound';
    }
    
    echo $msg; die;
    
    //check_ajax_referer('iws_nonce', 'nonce');

    
}

function get_list_all_activities(){
    $result=get_all_activities();
    
    $lhtml='';
    
    $get_taxonomy_images=get_option("taxonomy_image_plugin");
    
    
    if(!empty($result)){
        
        foreach($result as $row){
            
            
            $act_link=get_term_link($row->slug,'club-activities');
            
            
           
            $thumb_src= esc_url( get_template_directory_uri() ).'/images/default_image.jpg';
            if(!empty($get_taxonomy_images)){
                
                if(array_key_exists($row->term_id, $get_taxonomy_images)){
                    
                    $attach_image_id=$get_taxonomy_images[$row->term_id]; 
                    if($attach_image_id>0){
                        $image_data=wp_get_attachment_image_src($attach_image_id, 'thumbnail');
                        if(isset($image_data[0]) && $image_data[0]!=''){
                            $thumb_src=$image_data[0];
                        }
                        
                    }
                    
                }
            }
            
          ?>
                            
                            <div class="activityWrap allactivity">

                        <div class="activityBlock">

                            <div class="imgBlock">

            
            
             <a href="<?php echo $act_link; ?>"><img src="<?php echo $thumb_src; ?>" class="img-responsive" /></a></div>

                            <p><strong><?php echo $row->name; ?></strong></p>

            

                        </div><!-- activityBlock -->

                    </div><!-- activityWrap -->
            
            <?php
            
            
            
            
            
        }
        
    }
    
    
    return $lhtml;
    
}


add_shortcode('activities_all','get_list_all_activities');



?>