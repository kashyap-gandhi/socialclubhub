<?php
/*
  Plugin Name: Deals & Discount
  Plugin URI:
  Description: This plugin used for add edit delete and listing deals and discount module at admin side.
  Version: 1.0
  Author: iWS
  Author URI:
 */


add_action('init', 'create_deals_discount');

function create_deals_discount() {

    ////====set slug for direct open==/
    /////='supports' => array('title','editor','excerpt','trackbacks','custom-fields','comments','revisions','thumbnail','author','page-attributes',)
    //=taxonomies 'post_tag'

    register_post_type('deals_discount', array('label' => 'Deals', 'description' => 'Deal Listing', 'public' => true, 'show_ui' => true, 'show_in_menu' => true, 'capability_type' => 'page', 'hierarchical' => true, 'rewrite' => array('slug' => 'deals', 'with_front' => true), 'query_var' => true, 'exclude_from_search' => false, 'menu_position' => 18, 'supports' => array('title', 'editor', 'thumbnail'), 'taxonomies' => array(), 'labels' => array(
            'name' => 'Deals',
            'singular_name' => 'Deal',
            'menu_name' => 'Manage Deals',
            'add_new' => 'Add New',
            'add_new_item' => 'Add New Deal',
            'edit' => 'Edit',
            'edit_item' => 'Edit Deal',
            'new_item' => 'New Deal',
            'view' => 'View Deal',
            'view_item' => 'View Deal',
            'search_items' => 'Search Deals',
            'not_found' => 'No Deals Found',
            'not_found_in_trash' => 'No Deals found in Trash',
            'parent' => 'Parent Deal',
        ),));


}

//===inline edit 
//http://wpdreamer.com/2012/03/manage-wordpress-posts-using-bulk-edit-and-quick-edit/
//http://wordpress.stackexchange.com/questions/578/adding-a-taxonomy-filter-to-admin-list-for-a-custom-post-type
///http://www.smashingmagazine.com/2013/12/05/modifying-admin-post-lists-in-wordpress/

add_action('restrict_manage_posts', 'deals_discount_restrict_manage_posts');

function deals_discount_restrict_manage_posts() {
    global $typenow, $wpdb;





    // an array of all the taxonomyies you want to display. Use the taxonomy name or slug
    $taxonomies = array('club-activities');

    // must set this to the post type you want the filter(s) displayed on
    if ($typenow == 'deals_discount') {


        //===add category drop down
        foreach ($taxonomies as $tax_slug) {
            $tax_obj = get_taxonomy($tax_slug);
            $tax_name = $tax_obj->labels->name;
            //$terms = get_terms($tax_slug);
            
            
            $terms=get_all_activities();

            if (count($terms) > 0) {
                echo "<select name='deal_main_activity' id='deal_main_activity' class='postform'>";
                echo "<option value=''>Show All $tax_name</option>";
                foreach ($terms as $term) {
                  
                    
                    echo '<option value="' . $term->term_id.'"';
                    
                    if(isset($_GET['deal_main_activity']) && (int) $_GET['deal_main_activity'] == (int) $term->term_id){
                    echo ' selected="selected"';
                            
                    }
                    echo '>' . $term->name . '</option>';
                    
                    
                    
                    
                    //$term->count
                }
                echo "</select>";
            }
        }



        //===custom fields
        echo '';



        $use_country = $wpdb->get_results("SELECT ct.country, pm.meta_value FROM " . $wpdb->prefix . "postmeta pm, " . $wpdb->prefix . "posts ps, " . $wpdb->prefix . "countries ct WHERE pm.post_id=ps.ID  and pm.meta_value=ct.countryid and pm.meta_key = 'deal_country_id' AND ps.post_type = 'deals_discount' AND ps.post_status != 'trash'  group by pm.meta_value ");


        echo "<select name='deal_country_id' id='deal_country_id' class='postform'>";
        echo "<option value=''>Show All Country</option>";

        if (!empty($use_country)) {
            foreach ($use_country as $row_country) {
                echo '<option value=' . $row_country->meta_value;
                if (isset($_GET['deal_country_id']) && $_GET['deal_country_id'] == $row_country->meta_value) {
                    echo ' selected="selected"';
                }
                echo '>' . $row_country->country . '</option>';
            }
        }

        echo "</select>";

        echo '';


        echo '<input type="hidden" name="post_type" class="post_type_page" value="deals_discount">';


        ///===end code
    }
}

//add_action('request', 'deals_discount_request');
function deals_discount_request($request) {
    if (is_admin() && $GLOBALS['PHP_SELF'] == '/wp-admin/edit.php' && isset($request['post_type']) && $request['post_type'] == 'deals_discount') {
        $request['term'] = get_term($request['club-activities'], 'club-activities')->name;
    }
    return $request;
}

//http://codex.wordpress.org/Class_Reference/WP_Query#Custom_Field_Parameters
add_filter('parse_query', 'deals_discount_table_filter');

function deals_discount_table_filter($query) {
    if (is_admin() AND $query->query['post_type'] == 'deals_discount') {
        $qv = &$query->query_vars;
        $qv['meta_query'] = array();

        if (!empty($_GET['deal_country_id']) && !empty($_GET['deal_main_activity'])) {

            $qv['meta_query']['relation'] = 'AND';
        }

        if (!empty($_GET['deal_country_id'])) {

            $deal_country_id = (int) $_GET['deal_country_id'];

            $qv['meta_query'][] = array(
                'key' => 'deal_country_id',
                'value' => $deal_country_id,
                'compare' => '=',
                'type' => 'CHAR'
            );
        }

        if (!empty($_GET['deal_main_activity'])) {

            $deal_main_activities = (int) $_GET['deal_main_activity'];

            $qv['meta_query'][] = array(
                'key' => 'deal_main_activity',
                'value' => $deal_main_activities,
                'compare' => '=',
                'type' => 'CHAR'
            );
        }

       
       


        /* if( !empty( $_GET['orderby'] ) AND $_GET['orderby'] == 'event_date' ) {
          $qv['orderby'] = 'meta_value';
          $qv['meta_key'] = '_bs_meta_event_date';
          $qv['order'] = strtoupper( $_GET['order'] );
          } */
    }
}

//https://www.skyverge.com/blog/add-custom-bulk-action/
add_action('admin_footer-edit.php', 'deals_discount_bulk_admin_footer');

function deals_discount_bulk_admin_footer() {

    global $post_type;

    if ($post_type == 'deals_discount') {
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function() {
                jQuery('<option>').val('export').text('<?php _e('Export') ?>').appendTo("select[name='action']");
                jQuery('<option>').val('export').text('<?php _e('Export') ?>').appendTo("select[name='action2']");
            });
        </script>
        <?php
    }
}

add_action('load-edit.php', 'deals_discount_bulk_action');

function deals_discount_bulk_action() {
    global $typenow;
    $post_type = $typenow;

    if ($post_type == 'deals_discount') {

        // get the action
        $wp_list_table = _get_list_table('WP_Posts_List_Table');  // depending on your resource type this could be WP_Users_List_Table, WP_Comments_List_Table, etc
        $action = $wp_list_table->current_action();

        $allowed_actions = array("export");
        if (!in_array($action, $allowed_actions))
            return;

        // security check
        //check_admin_referer('bulk-posts');
        // make sure ids are submitted.  depending on the resource type, this may be 'media' or 'ids'
        /* if(isset($_REQUEST['post'])) {
          $post_ids = array_map('intval', $_REQUEST['post']);
          }

          if(empty($post_ids)) return; */

        // this is based on wp-admin/edit.php
        $sendback = remove_query_arg(array('exported', 'doexported', 'untrashed', 'deleted', 'ids'), wp_get_referer());
        if (!$sendback)
            $sendback = admin_url("edit.php?post_type=$post_type");

        $pagenum = $wp_list_table->get_pagenum();
        $sendback = add_query_arg('paged', $pagenum, $sendback);

        switch ($action) {
            case 'export':

                // if we set up user permissions/capabilities, the code might look like:
                //if ( !current_user_can($post_type_object->cap->export_post, $post_id) )
                //	wp_die( __('You are not allowed to export this post.') );

                $exported = 0;
                /* foreach( $post_ids as $post_id ) {

                  if ( !$this->perform_export($post_id) )
                  wp_die( __('Error exporting post.') );

                  $exported++;
                  } */

                //perform_export();
                //$sendback = add_query_arg( array('exported' => $exported, 'ids' => join(',', $post_ids) ), $sendback );

                $sendback = add_query_arg(array('doexported' => $exported), $sendback);

                break;

            default: return;
        }

        $sendback = remove_query_arg(array('action', 'action2', 'tags_input', 'post_author', 'comment_status', 'ping_status', '_status', 'post', 'bulk_edit', 'post_view'), $sendback);

        wp_redirect($sendback);
        exit();
    }
}

if (isset($_REQUEST['doexported']) && (int) $_REQUEST['doexported'] == 0) {

    global $post_type, $pagenow, $typenow;
    $post_type = $typenow;

    if (trim($post_type) == '') {

        if (isset($_REQUEST['post_type'])) {
            $post_type = $_REQUEST['post_type'];
        }
    }


    if ($pagenow == 'edit.php' && $post_type == 'deals_discount') {
        
        
       //require_once( ABSPATH . "wp-includes/pluggable.php" );

        deals_discount_perform_export();
    }
}


function get_deals_discount_meta_all($post_id){
    global $wpdb;

    $data   =   array();

    $wpdb->query(" SELECT `meta_key`, `meta_value`  FROM ".$wpdb->prefix."postmeta  WHERE meta_key!='_edit_last' and meta_key!='_edit_lock' and `post_id` = ".$post_id."  ");

    foreach($wpdb->last_result as $k => $v){
        $data[$v->meta_key] =   $v->meta_value;
    };

    return $data;
}

//==http://stackoverflow.com/questions/16722818/wordpress-admin-widget-that-exports-data
//http://wordpress.stackexchange.com/questions/10505/export-wordpress-table-to-excel
function deals_discount_perform_export() {

    global $wpdb,$post;
    // do whatever work needs to be done
    
        
    
    
    $sql='';
    
    $sql.='SELECT ps.* ';
    
            
    $sql.=' FROM '.$wpdb->prefix.'posts as ps ';
    
     if (isset($_GET['deal_country_id']) && !empty($_GET['deal_country_id'])) {
        $sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt1 ON ( ps.ID = mt1.post_id ) ';
    
     }
    if (isset($_GET['deal_main_activity']) && !empty($_GET['deal_country_id'])) {
        $sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt2 ON ( ps.ID = mt2.post_id ) ';
    }
    
    $sql.=" WHERE 1=1 AND ps.post_type = 'deals_discount' AND (ps.post_status = 'publish' OR ps.post_status = 'future' OR ps.post_status = 'draft' OR ps.post_status = 'pending' OR ps.post_status = 'private') ";
    
    
    if ( (isset($_GET['deal_main_activity']) && !empty($_GET['deal_main_activity'])) ||  (isset($_GET['deal_country_id']) && !empty($_GET['deal_country_id'])) ) {
    
        $sql.=" AND ( ";
    
    }
    
    
    
    
    if (isset($_GET['deal_country_id']) && !empty($_GET['deal_country_id'])) {
        $sql.="( mt1.meta_key = 'deal_country_id' AND CAST(mt1.meta_value AS CHAR) = '".trim($_GET['deal_country_id'])."' ) ";
    }
    
     if ( (isset($_GET['deal_main_activity']) && !empty($_GET['deal_main_activity'])) &&  isset($_GET['deal_country_id']) && !empty($_GET['deal_country_id']) ) {
            $sql.=" AND ";
     }
     
    if (isset($_GET['deal_main_activity']) && !empty($_GET['deal_main_activity'])) {
        $sql.=" ( mt2.meta_key = 'deal_main_activity' AND CAST(mt2.meta_value AS CHAR) = '".$_GET['deal_main_activity']."' ) ";
        
    }
    
    
     if ( (isset($_GET['deal_main_activity']) && !empty($_GET['deal_main_activity'])) ||  (isset($_GET['deal_country_id']) && !empty($_GET['deal_country_id'])) ) {
   
            $sql.=" ) ";
     }
     
    $sql.=' GROUP BY ps.ID ORDER BY ps.ID ASC ';
    
    
    
    //echo $sql;
   // die;
 

    
    $file = 'export'.rand();
    
    $export = mysql_query($sql) or die("Sql error : " . mysql_error());
    
    //echo "<pre>";
    
   // print_r(mysql_fetch_row($export));
    
    //die;
    
    $fields = mysql_num_fields($export);
    $header='';$data='';
    for ($i = 0; $i < $fields; $i++) {
        $header .= mysql_field_name($export, $i) . "\t";
    }
    
   
    
    $cnt=0;
    while ($row = mysql_fetch_row($export)) {
        
        //echo "<pre>";
        //print_r($row);
        //die;
        
         $get_all_meta=get_deals_discount_meta_all($row[0]);
        
        // print_r($get_all_meta);
         //die;
         
         if($cnt==0) {
             if(!empty($get_all_meta)) {
                foreach($get_all_meta as $mkey=>$mval) {
                 $header .= $mkey . "\t"; 
                }
             }
         }
         
         $cnt++;
        
         
        $line = '';
        foreach ($row as $value) {
            if ((!isset($value) ) || ( $value == "" )) {
                $value = "\t";
            } else {
                $value = str_replace('"', '""', $value);
                $value = '"' . $value . '"' . "\t";
            }
            $line .= $value;
        }
        
         if(!empty($get_all_meta)) {
                foreach($get_all_meta as $mkey=>$mval) {
                    if ((!isset($mval) ) || ( $mval == "" )) {
                        $mval = "\t";
                    } else {
                        $mval = str_replace('"', '""', $mval);
                        $mval = '"' . $mval . '"' . "\t";
                    }
                    $line .= $mval;
                }
         }
        $data .= trim($line) . "\n";
        
        
        
    }
    $data = str_replace("\r", "", $data);
    if ($data == "") {
        $data = "\n(0) Records Found!\n";
    }

    $filename = $file . "_" . date("M-d-Y");

    header("Content-type: application/octet-stream");
    header("Content-disposition: filename=" . $filename . ".xls");
    header("Pragma: no-cache");
    header("Expires: 0");
    print "$header\n$data";
    die;
}



//==change title or metabox on the fly
//add_action('add_meta_boxes', 'deals_discount_change_meta_box_titles', 999);

function deals_discount_change_meta_box_titles() {
    global $wp_meta_boxes; // array of defined meta boxes
    // cycle through the array, change the titles you want

    //remove_meta_box('club-activitiesdiv', 'clubs', 'side');
    //$wp_meta_boxes['clubs']['side']['core']['club-activitiesdiv']['title']= 'Club Other Activities';
}

///=============add custom column==========


add_filter('manage_edit-deals_discount_columns', 'deals_discount_edit_columns');

function deals_discount_edit_columns($columns) {

    $columns = array(
        'cb' => '<input type="checkbox" />',
        'id' => __('ID'),
        'title' => __('Title'),
        //'club_status' => __('Type'),
        'post_status' => __('Status'),
        'date' => __('date')
    );

    return $columns;
}

add_action('manage_deals_discount_posts_custom_column', 'my_manage_deals_discount_columns', 10, 2);

function my_manage_deals_discount_columns($column_name, $post_id) {
    global $wpdb;
    global $post;

    switch ($column_name) {

        case 'id':
            echo $post_id;
            break;

        case 'post_status':
            echo $post->post_status;

            break;

       



        default:
            break;
    } // end switch
}

add_filter('manage_edit-deals_discount_sortable_columns', 'my_deals_discount_sortable_columns');

function my_deals_discount_sortable_columns($columns) {

    //$columns['id'] = 'id';
   
    $columns['post_status'] = 'post_status';


    return $columns;
}

/////////////===========add club active part================



/**
 * Add stylesheet to the page
 */
add_action('admin_enqueue_scripts', 'deals_discount_stylesheet_to_admin');

function deals_discount_stylesheet_to_admin() {
    wp_enqueue_style('deals_discount_css', plugins_url('/css/deals.css', __FILE__));
}

/**
 * Add script to the page
 */
add_action('admin_enqueue_scripts', 'deals_discount_script_to_admin');

function deals_discount_script_to_admin($page) {
    if (is_admin()) {
        if ('post-new.php' == $page || 'post.php' == $page) {
            wp_register_script('deals_discount_js', plugins_url('/js/deals_script.js', __FILE__), array('jquery'));
            // Enqueue Scripts that are needed on all the pages
            wp_enqueue_script('jquery');
            wp_enqueue_script('deals_discount_js');
        } else {
            return;
        }
    }
}



add_action('admin_init', 'meta_deals_discount_extra_fields_type_var');

function meta_deals_discount_extra_fields_type_var() {



    // please add other post type

    $post_types = array('deals_discount');



    foreach ($post_types as $post_type) {


       
        add_meta_box('deals_discount_contact_info', 'Deals Discount Information', 'deals_discount_contact_info_setup', $post_type, 'normal', 'high');
        add_meta_box('deals_discount_location_info', 'Deal Location', 'deals_discount_address_info_setup', $post_type, 'normal', 'high');
        add_meta_box('deals_discount_activity_info', 'Deal Activity', 'deals_discount_activity_info_setup', $post_type, 'normal', 'high');
    }

    // save metabox
    
    add_action('save_post', 'deals_discount_extra_field_save');
    
    
}

function deals_discount_activity_info_setup() {

    global $wpdb, $post;


    $deal_main_activity = get_post_meta($post->ID, 'deal_main_activity', TRUE);
    


    $res_activities = get_all_activities();
    ?>


    <div class="left_label">
        <label for="club_adderss" class="">Deal Activity</label>
    </div>
    <div class="right_label">

        <select name="deal_main_activity" id="deal_main_activity">
            <option value="0">---Select Activity---</option>                    
    <?php
    if (!empty($res_activities)) {
        foreach ($res_activities as $row_acti) {
            ?>
                    <option value="<?php echo $row_acti->term_id; ?>" <?php if ($row_acti->term_id == $deal_main_activity) { ?> selected <?php } ?>><?php echo ucwords($row_acti->name); ?></option>
        <?php }
    } ?>

        </select>
    </div>
    <div class="clear"></div>



    <?php
    echo '<input type="hidden" name="iws_admindealdiscount_nonce" value="' . wp_create_nonce('iws-admindealdiscount-nonce') . '" />';
}

function deals_discount_address_info_setup() {
    global $wpdb, $post;

    $deal_address = get_post_meta($post->ID, 'deal_adderss', TRUE);
    
    $deal_zip_code = get_post_meta($post->ID, 'deal_zip_code', TRUE);
    
    $deal_city_id = get_post_meta($post->ID, 'deal_city_id', TRUE);
    $deal_state_id = get_post_meta($post->ID, 'deal_state_id', TRUE);
    $deal_country_id = get_post_meta($post->ID, 'deal_country_id', TRUE);
    $deal_lat = get_post_meta($post->ID, 'deal_lat', TRUE);
    $deal_long = get_post_meta($post->ID, 'deal_long', TRUE);



    
    $res_countries = get_all_country();


    $res_states = array();
    if ($deal_country_id > 0) {
        
        $res_states = get_states_from_country_by_id($deal_country_id);
    }

    $res_cities = array();
    if ($deal_state_id > 0) {
       
        $res_cities = get_cities_from_state_by_id($deal_state_id);
    }
    ?>

    <div class="left_label">
        <label for="deal_adderss" class="">Address</label>
    </div>
    <div class="right_label">
        <input type="text" id="deal_adderss" value="<?php echo $deal_address; ?>" size="50" name="deal_adderss">
    </div>
    <div class="clear"></div>



    <div class="left_label">
        <label for="deal_country_id" class="">Country</label>
    </div>
    <div class="right_label">

        <select name="deal_country_id" id="deal_country_id">
            <option value="0">---Select Country---</option>   
    <?php if (!empty($res_countries)) {
        foreach ($res_countries as $row_country) { ?>

                    <option value="<?php echo $row_country->countryid; ?>" <?php if ($row_country->countryid == $deal_country_id) { ?> selected <?php } ?>><?php echo ucfirst($row_country->country); ?></option>

        <?php }
    } ?>

        </select>

    </div>
    <div class="clear"></div>


    <div class="left_label">
        <label for="deal_state_id" class="">State</label>
    </div>
    <div class="right_label">

        <select name="deal_state_id" id="deal_state_id">
            <option value="0">---Select State---</option>

    <?php
    if (!empty($res_states)) {
        foreach ($res_states as $row_states) {
            ?>

                    <option value="<?php echo $row_states->regionid; ?>" <?php if ($row_states->regionid == $deal_state_id) { ?> selected <?php } ?>><?php echo ucfirst($row_states->region); ?></option>


                <?php
                }
            }
            ?>

        </select>

    </div>
    <div class="clear"></div>


    <div class="left_label">
        <label for="deal_city_id" class="">City</label>
    </div>
    <div class="right_label">

        <select name="deal_city_id" id="deal_city_id">
            <option value="0">---Select City---</option>

            <?php
            if (!empty($res_cities)) {
                foreach ($res_cities as $row_cities) {
                    ?>

                    <option value="<?php echo $row_cities->CityId; ?>" data-lat="<?php echo $row_cities->Latitude; ?>" data-long="<?php echo $row_cities->Longitude; ?>" <?php if ($row_cities->CityId == $deal_city_id) { ?> selected <?php } ?>><?php echo ucfirst($row_cities->City); ?></option>


                <?php }
            } ?>

        </select>

    </div>
    <div class="clear"></div>
    
    
    
    <div class="left_label">
        <label for="deal_adderss" class="">Zipcode</label>
    </div>
    <div class="right_label">
        <input type="text" id="deal_zip_code" value="<?php echo $deal_zip_code; ?>" size="16" maxlength="12" name="deal_zip_code">
    </div>
    <div class="clear"></div>


    <input type="hidden" name="deal_lat" id="deal_lat" value="<?php echo $deal_lat; ?>" />
    <input type="hidden" name="deal_long" id="deal_long" value="<?php echo $deal_long; ?>" />

    <?php
    
    echo '<input type="hidden" name="iws_admindealdiscount_nonce" value="' . wp_create_nonce('iws-admindealdiscount-nonce') . '" />';
}



function deals_discount_contact_info_setup() {

    global $wpdb, $post;

    
    $deal_discount = get_post_meta($post->ID, 'deal_discount', TRUE);
    $deal_discount_type = get_post_meta($post->ID, 'deal_discount_type', TRUE);
    
    $deal_code = get_post_meta($post->ID, 'deal_code', TRUE);

    
    $deal_company_name = get_post_meta($post->ID, 'deal_company_name', TRUE);
    $deal_fax_number = get_post_meta($post->ID, 'deal_fax_number', TRUE);
    $deal_phone_number = get_post_meta($post->ID, 'deal_phone_number', TRUE);
    
    $deal_website = get_post_meta($post->ID, 'deal_website', TRUE);

    $deal_user_id = get_post_meta($post->ID, 'deal_user_id', TRUE);




    //$get_customer = new WP_User_Query(array('role' => array('administrator','customer'), 'orderby' => 'display_name', 'order' => 'ASC'));

    
    $roles = array('customer', 'administrator');

$get_customer=get_user_by_role($roles);

    
    ?>






        <div class="left_label">
            <label for="deal_user_id" class="">Select User</label>
        </div>
        <div class="right_label">
            <select name="deal_user_id" id="deal_user_id">
                <option value="0">---Select User---</option>
    <?php
    if (!empty($get_customer->results)) {
        foreach ($get_customer->results as $customer) {
            ?>
                        <option value="<?php echo $customer->ID; ?>" <?php if ($customer->ID == $deal_user_id) { ?> selected <?php } ?>><?php echo $customer->display_name . " || " . $customer->user_email; ?></option>
        <?php }
    } ?>
            </select> <small>(Deal owner user)</small>

        </div>
        <div class="clear"></div>


        
        
        <div class="left_label">
            <label for="deal_discount_type" class="">Discount Type</label>
        </div>
        <div class="right_label">
            <select name="deal_discount_type" id="deal_discount_type">
             
                <option value="fixed" <?php if ($deal_discount_type=='fixed' || $deal_discount_type=='') { ?> selected <?php } ?>>Fix($)</option>
                <option value="percentage" <?php if ($deal_discount_type=='percentage') { ?> selected <?php } ?>>Percentage(%)</option>
        
            </select> 

        </div>
        <div class="clear"></div>


        



        <div class="left_label">
            <label for="deal_discount" class="">Discount</label>
        </div>
        <div class="right_label">
            <input type="text" id="deal_discount" value="<?php echo $deal_discount; ?>" size="30" name="deal_discount">
        </div>
        <div class="clear"></div>
        
        
        


        <div class="left_label">
            <label for="deal_code" class="">Code</label>
        </div>
        <div class="right_label">
            <input type="text" id="deal_code" value="<?php echo $deal_code; ?>" size="30" name="deal_code">
        </div>
        <div class="clear"></div>


        
        
        <div class="left_label">
            <label for="deal_company_name" class="">Company Name</label>
        </div>
        <div class="right_label">
            <input type="text" id="deal_company_name" value="<?php echo $deal_company_name; ?>" size="30" name="deal_company_name">
        </div>
        <div class="clear"></div>
        
        <div class="left_label">
            <label for="deal_phone_number" class="">Phone Number</label>
        </div>
        <div class="right_label">
            <input type="text" id="deal_phone_number" value="<?php echo $deal_phone_number; ?>" size="30" name="deal_phone_number">
        </div>
        <div class="clear"></div>
        
        <div class="left_label">
            <label for="deal_fax_number" class="">Fax Number</label>
        </div>
        <div class="right_label">
            <input type="text" id="deal_fax_number" value="<?php echo $deal_fax_number; ?>" size="30" name="deal_fax_number">
        </div>
        <div class="clear"></div>
        
        
        
        
        

    <div class="left_label">
        <label for="deal_website" class="">Website</label>
    </div>
    <div class="right_label">
        <input type="text" id="deal_website" value="<?php echo $deal_website; ?>" size="30" name="deal_website">
    </div>
    <div class="clear"></div>



    <?php
    echo '<input type="hidden" name="iws_admindealdiscount_nonce" value="' . wp_create_nonce('iws-admindealdiscount-nonce') . '" />';
}

// Display any errors
add_action('admin_notices', 'deals_discount_admin_notice_handler');

function deals_discount_admin_notice_handler() {

    global $post_type, $pagenow;
    
    $html='';
    
    if (isset($_GET['message'])) {

        if ((int) $_GET['message'] == (int) 4) {
            $html = '<div class="error">
                        <p><strong>Validation errors</strong></p>
                        <p>- Please select deal owner user.</p></div>';
        }
    }



    if ($pagenow == 'edit.php' && $post_type == 'deals_discount' && isset($_REQUEST['doexported']) && (int) $_REQUEST['doexported']) {
        $message = _('Deals exported.');
        $html = "<div class='updated'><p>".$message."</p></div>";
    }

    echo $html;

}


function deals_discount_extra_field_save($post_id) {
 global $post,$wpdb;
    
        // don't do on autosave or when new posts are first created
    if (( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) || (isset($post) && !empty($post) && $post->post_status == 'auto-draft')) return $post_id;

    
    
    // authentication checks
    // make sure data came from our meta box
    
         

    
        

if($_POST && isset($_POST['iws_admindealdiscount_nonce']) && wp_verify_nonce($_POST['iws_admindealdiscount_nonce'],'iws-admindealdiscount-nonce')){




    // check user permissions

    if ($_POST['post_type'] == 'page') {

        if (!current_user_can('edit_page', $post_id))
            return $post_id;
    } else {

        if (!current_user_can('edit_post', $post_id))
            return $post_id;
    }
    
    
    
      
    
    
    //===info all


    $deal_discount_type = sanitize_text_field($_POST['deal_discount_type']);
    update_post_meta($post_id, 'deal_discount_type', esc_attr($deal_discount_type));

    $deal_discount = (float) sanitize_text_field($_POST['deal_discount']);
    update_post_meta($post_id, 'deal_discount', esc_attr($deal_discount));
    
    
    $deal_code = sanitize_text_field($_POST['deal_code']);
    update_post_meta($post_id, 'deal_code', esc_attr($deal_code));

    
    
    $deal_company_name = sanitize_text_field($_POST['deal_company_name']);
    update_post_meta($post_id, 'deal_company_name', esc_attr($deal_company_name));
    
    $deal_fax_number = sanitize_text_field($_POST['deal_fax_number']);
    update_post_meta($post_id, 'deal_fax_number', esc_attr($deal_fax_number));
    
    $deal_phone_number = sanitize_text_field($_POST['deal_phone_number']);
    update_post_meta($post_id, 'deal_phone_number', esc_attr($deal_phone_number));
    
    
    $deal_website = sanitize_text_field($_POST['deal_website']);
    update_post_meta($post_id, 'deal_website', esc_url_raw($deal_website));

    $deal_user_id = (int) sanitize_text_field($_POST['deal_user_id']);
    update_post_meta($post_id, 'deal_user_id', esc_attr($deal_user_id));

   
    $wpdb->update($wpdb->posts, array('post_author' => $deal_user_id), array('ID' => $post_id));


    //==activity


   

    $deal_main_activity = (int) sanitize_text_field($_POST['deal_main_activity']);
    update_post_meta($post_id, 'deal_main_activity', esc_attr($deal_main_activity));




    ///===address

    $deal_address = sanitize_text_field($_POST['deal_adderss']);
    update_post_meta($post_id, 'deal_adderss', esc_attr($deal_address));
    
    $deal_zip_code = sanitize_text_field($_POST['deal_zip_code']); 
    update_post_meta($post_id, 'deal_zip_code', esc_attr($deal_zip_code));

    $deal_country_id = (int) sanitize_text_field($_POST['deal_country_id']);
    update_post_meta($post_id, 'deal_country_id', esc_attr($deal_country_id));

    $deal_state_id = (int) sanitize_text_field($_POST['deal_state_id']);
    update_post_meta($post_id, 'deal_state_id', esc_attr($deal_state_id));

    $deal_city_id = (int) sanitize_text_field($_POST['deal_city_id']);
    update_post_meta($post_id, 'deal_city_id', esc_attr($deal_city_id));

    $deal_lat = sanitize_text_field($_POST['deal_lat']);
    update_post_meta($post_id, 'deal_lat', esc_attr($deal_lat));

    $deal_long = sanitize_text_field($_POST['deal_long']);
    update_post_meta($post_id, 'deal_long', esc_attr($deal_long));



    // just checking it's not empty - you could do other tests...
    if (empty($deal_user_id) || (int) $deal_user_id == (int) 0) {
        $meta_missing = true;
    }

    // on attempting to publish - check for completion and intervene if necessary
    if (( isset($_POST['publish']) || isset($_POST['save']) ) && $_POST['post_status'] == 'publish') {
        //  don't allow publishing while any of these are incomplete
        if ($meta_missing == true) {
            global $wpdb;
            $wpdb->update($wpdb->posts, array('post_status' => 'pending'), array('ID' => $post_id));
            // filter the query URL to change the published message
            add_filter('redirect_post_location', create_function('$location', 'return add_query_arg("message", "4", $location);'));
        }
    }

}

    return $post_id;
    
    
}




function check_dealsdiscount_owner($id){
    
     global $wpdb,$post,$current_user;

    $user = $current_user;
    
    
   
    
    
    $sql='';
    
    $sql.='SELECT ps.* ';
    
            
    $sql.=' FROM '.$wpdb->prefix.'posts as ps ';
    
    
    $sql.=" WHERE 1=1 AND ps.post_type = 'deals_discount' AND (ps.post_status = 'draft' ||  ps.post_status = 'future' || ps.post_status = 'publish' || ps.post_status = 'pending') ";
    
    $sql.=" and ps.ID=".$id;
    
    $sql.=" and ps.post_author=".$user->ID;
     
    $sql.=' GROUP BY ps.ID ORDER BY ps.ID DESC ';
    
    if(mysql_num_rows(mysql_query($sql))>0){
        return true;
    }
    return false;
    
        
}


function get_user_dealsdiscount(){
    global $wpdb,$post;
    
    
    // only show the registration form to non-logged-in members
    if (is_user_logged_in()) {

    } else {
        $redirect_link = wp_login_url();
        wp_redirect($redirect_link);
        exit;
    }
    
    
    
    
    if(isset($_GET['act']) && trim($_GET['act'])!=''){
        $act_type=trim($_GET['act']);
        switch ($act_type){
            
            case 'add':
                require_once 'deal_add.php';
                break;
            case 'edit':
                
                if(isset($_GET['id']) && (int) $_GET['id']>0){
                
                    $dealsdiscount_id = (int) $_GET['id'];
                    $chk_owner=check_dealsdiscount_owner($dealsdiscount_id);
                    
                    if($chk_owner){
                        require_once 'deal_add.php';
                    } else {
                        wp_redirect(original_page_url().'?msg=notowner');
                    }
                } else {
                    wp_redirect(original_page_url().'?msg=notfound');
                }
                
                break;
            default :
                break;
        }
    } else {
       require_once 'deal_list.php';
    }
    
    
    
    
}

add_shortcode('my_dealsdiscount','get_user_dealsdiscount');


add_action('wp_ajax_iws_dealsdiscount_delete', 'delete_dealsdiscounts');

function delete_dealsdiscounts() {
    
    
    
    if(isset($_POST['id']) && (int) $_POST['id']>0){
                
        $dealsdiscount_id = (int) $_POST['id'];
        $chk_owner=check_dealsdiscount_owner($dealsdiscount_id);

        if($chk_owner){
            
            //===put delete code with image delte from folder
            
            if(wp_trash_post($dealsdiscount_id)){
                $msg='success'; //clubdeletefail
            } else {
              $msg='dealsdiscountdeletefail'; //clubdeletefail
            }
            
            
        } else {
            $msg='notowner';
        }
    } else {
        $msg='notfound';
    }
    
    echo $msg; die;
    
    //check_ajax_referer('iws_nonce', 'nonce');

    
}




function iws_front_addedit_dealsdiscount($post_id) {

    global $wpdb,$post,$current_user;

    $user = $current_user;
    
    
            
  
if($_POST && isset($_POST['iws_dealsdiscount_editadd_nonce']) && wp_verify_nonce($_POST['iws_dealsdiscount_editadd_nonce'],'iws-dealsdiscount-editadd-nonce')){     

    
        
    $act='add';
    $post_id=0;
    $msg='dealsdiscountaddsuccess';
    
    
    $post_status='pending';
    $post_type='deals_discount';
            
    
    
    
    if(isset($_GET['act']) && trim($_GET['act'])=='edit'){
        if(isset($_GET['id']) && (int) $_GET['id']>0){
                
                    $dealsdiscount_id = (int) $_GET['id'];
                    $chk_owner=check_dealsdiscount_owner($dealsdiscount_id);
                    
                    if($chk_owner){
                        
                        $post_data=get_post($dealsdiscount_id,true);
                        
                        $post_status=$post_data['post_status'];
                        
                        $post_id=$dealsdiscount_id;                       
                        $act='edit';
                        $msg='dealsdiscountupdatesuccess';
                        
                    } else {
                        wp_redirect(original_page_url().'?msg=notowner');
                    }
                } else {
                    wp_redirect(original_page_url().'?msg=notfound');
                }
        }   
    
    
        
        
        $deal_name = trim($_POST["deal_name"]);
        $deal_description = trim($_POST["deal_description"]);
        
        $deal_website = trim($_POST["deal_website"]);
        
        $deal_main_activity = (int) $_POST["deal_main_activity"];
        
        
        
        $deal_company_name = trim($_POST['deal_company_name']);
    $deal_fax_number = trim($_POST['deal_fax_number']);
    $deal_phone_number = trim($_POST['deal_phone_number']);
        
       
        
        if($deal_name==''){
            iws_errors()->add('deal_name_empty', __('Please enter a deal name'));
        } else {
            if (4 > strlen($deal_name)) {
                iws_errors()->add('deal_name_length', __('Deal name too short. At least 4 characters is required'));
            }
        }
        
        if($deal_website==''){
            iws_errors()->add('deal_website_empty', __('Please enter a deal website'));
        } else {
        
            if(!valid_url($deal_website)){
                iws_errors()->add('deal_website_valid', __('Please enter a valid deal website'));
            }

        }
        
        
        if($deal_phone_number!=''){           
        
            if(!contact_check($deal_phone_number)){
                iws_errors()->add('deal_phone_number_valid', __('Please enter a valid phone number'));
            }

        }
        
        if($deal_fax_number!=''){           
        
            if(!contact_check($deal_fax_number)){
                iws_errors()->add('deal_fax_number_valid', __('Please enter a valid fax number'));
            }

        }
                    
                
        
        if ($deal_main_activity=='' || $deal_main_activity==0) {
            //invalid email
            iws_errors()->add('select_main_activity', __('Please select deal activity.'));
        }
        
        
        
        if(isset($_POST["deal_company_name"]) && trim($_POST["deal_company_name"])==''){
            iws_errors()->add('enter_company_name', __('Please enter company name.'));
        } else {
            
            if (strlen(trim($_POST["deal_company_name"]))>150) {
                iws_errors()->add('deal_company_name_max_limit', __('Please enter company name maximum 150 characters.'));
            }
            
            if (strlen(trim($_POST["deal_company_name"]))<3) {
                iws_errors()->add('deal_company_name_min_limit', __('Please enter company name minimum 3 characters.'));
            }
        }
        
        
        if(isset($_POST["deal_adderss"]) && trim($_POST["deal_adderss"])==''){
            iws_errors()->add('enter_address', __('Please enter deal address.'));
        } else {
            
            if (strlen(trim($_POST["deal_adderss"]))>50) {
                iws_errors()->add('address_max_limit', __('Please enter deal address maximum 50 characters.'));
            }
            
            if (strlen(trim($_POST["deal_adderss"]))<5) {
                iws_errors()->add('address_min_limit', __('Please enter deal address minimum 5 characters.'));
            }
        }
        
        
        if (isset($_POST["deal_discount_type"]) && trim($_POST["deal_discount_type"])=='') {
            //invalid email
            iws_errors()->add('select_deal_discount_type', __('Please select deal discount type.'));
        }
        
        if (isset($_POST["deal_discount"]) && ( (float) $_POST["deal_discount"] =='' || (float) $_POST["deal_discount"]==0)) {
            //invalid email
            iws_errors()->add('select_deal_discount', __('Please enter deal discount.'));
        }
        
        if (isset($_POST["deal_code"]) && trim($_POST["deal_code"])=='') {
            //invalid email
            iws_errors()->add('select_deal_code', __('Please enter deal code.'));
        } else {
            if (strlen(trim($_POST["deal_code"]))>20) {
                iws_errors()->add('dealcode_max_limit', __('Please enter deal code maximum 20 characters.'));
            }
            if (strlen(trim($_POST["deal_code"]))<3) {
                iws_errors()->add('dealcode_min_limit', __('Please enter deal code minimum 3 characters.'));
            }
        }
        
        
        if (isset($_POST["deal_country_id"]) && ( (int) $_POST["deal_country_id"]==0 || (int) $_POST["deal_country_id"]=='')) {
            //invalid email
            iws_errors()->add('select_country', __('Please select deal country.'));
        }
        
        if (isset($_POST["deal_state_id"]) && ( (int) $_POST["deal_state_id"]==0 || (int) $_POST["deal_state_id"]=='')) {
            //invalid email
            iws_errors()->add('select_state', __('Please select deal state.'));
        }
        
        if (isset($_POST["deal_city_id"]) && ( (int) $_POST["deal_city_id"]==0 || (int) $_POST["deal_city_id"]=='')) {
            //invalid email
            iws_errors()->add('select_city', __('Please select deal city.'));
        }
        
        
        if(isset($_POST["deal_zip_code"]) && trim($_POST["deal_zip_code"])==''){
            iws_errors()->add('enter_zipcode', __('Please enter deal postal code.'));
        } else {
            
            if (strlen(trim($_POST["deal_zip_code"]))>20) {
                iws_errors()->add('zipcode_max_limit', __('Please enter deal postal code maximum 20 characters.'));
            }
            if (strlen(trim($_POST["deal_zip_code"]))<3) {
                iws_errors()->add('zipcode_min_limit', __('Please enter deal postal code minimum 3 characters.'));
            }
        }
        
       

        $errors = iws_errors()->get_error_messages();

        // only create the user in if there are no errors
        if (empty($errors)) {


            
            
            
            

        
        $deal_name = esc_attr(sanitize_text_field($deal_name));
        $deal_description = esc_textarea(sanitize_text_field($deal_description));

        $slug=sanitize_title_with_dashes($deal_name);
        
        
        
        if($act=='edit' && $post_id>0){
            
            
            $new_post = array(
                'ID'=>$post_id,
                'post_title' => $deal_name,
                'post_content' => $deal_description
            );
            wp_update_post($new_post);
            
            
        } else {
            
            
            $new_post = array(
                'post_title' => $deal_name,
                'post_name' => $slug,
                'post_content' => $deal_description,
                'post_status' => $post_status,
                'post_author' => $user->ID,
                'post_type' => $post_type,

            );
            $post_id = wp_insert_post($new_post);
            
        }
    
    
    
        //==thumbnail
        
        // set featured image if there's any
            if (isset($_POST['iws_files']['_thumbnail_id'])) {
                $attachment_id = $_POST['iws_files']['_thumbnail_id'][0];

                iws_update_thumbnail($post_id, $attachment_id);
            }
    
    

    //===info all


    $deal_discount_type = sanitize_text_field($_POST['deal_discount_type']);
    update_post_meta($post_id, 'deal_discount_type', esc_attr($deal_discount_type));

    $deal_discount = (float) sanitize_text_field($_POST['deal_discount']);
    update_post_meta($post_id, 'deal_discount', esc_attr($deal_discount));
    
    
    $deal_code = sanitize_text_field($_POST['deal_code']);
    update_post_meta($post_id, 'deal_code', esc_attr($deal_code));
    
    
    $deal_company_name = sanitize_text_field($_POST['deal_company_name']);
    update_post_meta($post_id, 'deal_company_name', esc_attr($deal_company_name));
    
    $deal_fax_number = sanitize_text_field($_POST['deal_fax_number']);
    update_post_meta($post_id, 'deal_fax_number', esc_attr($deal_fax_number));
    
    $deal_phone_number = sanitize_text_field($_POST['deal_phone_number']);
    update_post_meta($post_id, 'deal_phone_number', esc_attr($deal_phone_number));

    $deal_website = sanitize_text_field($_POST['deal_website']);
    update_post_meta($post_id, 'deal_website', esc_url_raw($deal_website));

    $deal_user_id = (int) sanitize_text_field($_POST['deal_user_id']);
    update_post_meta($post_id, 'deal_user_id', esc_attr($deal_user_id));

   
   


    //==activity


   

    $deal_main_activity = (int) sanitize_text_field($_POST['deal_main_activity']);
    update_post_meta($post_id, 'deal_main_activity', esc_attr($deal_main_activity));




    ///===address

    $deal_address = sanitize_text_field($_POST['deal_adderss']);
    update_post_meta($post_id, 'deal_adderss', esc_attr($deal_address));
    
    $deal_zip_code = sanitize_text_field($_POST['deal_zip_code']); 
    update_post_meta($post_id, 'deal_zip_code', esc_attr($deal_zip_code));

    $deal_country_id = (int) sanitize_text_field($_POST['deal_country_id']);
    update_post_meta($post_id, 'deal_country_id', esc_attr($deal_country_id));

    $deal_state_id = (int) sanitize_text_field($_POST['deal_state_id']);
    update_post_meta($post_id, 'deal_state_id', esc_attr($deal_state_id));

    $deal_city_id = (int) sanitize_text_field($_POST['deal_city_id']);
    update_post_meta($post_id, 'deal_city_id', esc_attr($deal_city_id));

    $deal_lat = sanitize_text_field($_POST['deal_lat']);
    update_post_meta($post_id, 'deal_lat', esc_attr($deal_lat));

    $deal_long = sanitize_text_field($_POST['deal_long']);
    update_post_meta($post_id, 'deal_long', esc_attr($deal_long));

    

        wp_redirect(original_page_url().'?msg='.$msg);
    
         die; 
        
        }  

    }

    
}
add_action('init', 'iws_front_addedit_dealsdiscount');





?>