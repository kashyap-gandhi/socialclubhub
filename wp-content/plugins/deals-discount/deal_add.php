<?php
ob_start();

global $wpdb, $post;

$edit_add_text='Add';


$obj_id=0;
$deal_name='';
$deal_description='';


$deal_adderss='';
$deal_zip_code='';
$deal_city_id='';
$deal_state_id='';
$deal_country_id='';

$deal_lat='';
$deal_long='';

$deal_main_activity='';


$deal_discount='';
$deal_discount_type='';
$deal_code='';


$deal_company_name = '';    
$deal_fax_number = '';    
$deal_phone_number = '';


$deal_website='';
$deal_user_id='';




if(isset($_GET['id']) && (int) $_GET['id']>0){
                
    $obj_id = (int) $_GET['id'];
    $edit_add_text='Edit';
    
    
    $post_data=get_post($obj_id,true);
    
    
    
    $deal_name=$post_data['post_title'];
    
    $deal_description=$post_data['post_content'];
    
    
    
    
    
    
    $deal_adderss = get_post_meta($obj_id, 'deal_adderss', TRUE);
    
    $deal_zip_code = get_post_meta($obj_id, 'deal_zip_code', TRUE);
    
    $deal_city_id = get_post_meta($obj_id, 'deal_city_id', TRUE);
    $deal_state_id = get_post_meta($obj_id, 'deal_state_id', TRUE);
    $deal_country_id = get_post_meta($obj_id, 'deal_country_id', TRUE);
    $deal_lat = get_post_meta($obj_id, 'deal_lat', TRUE);
    $deal_long = get_post_meta($obj_id, 'deal_long', TRUE);

    
    $deal_main_activity = get_post_meta($obj_id, 'deal_main_activity', TRUE);
    
    
    $deal_discount = get_post_meta($obj_id, 'deal_discount', TRUE);
    $deal_discount_type = get_post_meta($obj_id, 'deal_discount_type', TRUE);
    
    $deal_code = get_post_meta($obj_id, 'deal_code', TRUE);
    
    
    $deal_company_name = get_post_meta($obj_id, 'deal_company_name', TRUE);
    $deal_fax_number = get_post_meta($obj_id, 'deal_fax_number', TRUE);
    $deal_phone_number = get_post_meta($obj_id, 'deal_phone_number', TRUE);

    $deal_website = get_post_meta($obj_id, 'deal_website', TRUE);

    $deal_user_id = get_post_meta($obj_id, 'deal_user_id', TRUE);


   
    

}

    

    if (isset($_POST['deal_country_id']) && (int) $_POST['deal_country_id'] > 0) {
        $deal_country_id = (int) $_POST['deal_country_id'];
    }
    if (isset($_POST['deal_state_id']) && (int) $_POST['deal_state_id'] > 0) {
        $deal_state_id = (int) $_POST['deal_state_id'];
    }
    if (isset($_POST['deal_city_id']) && (int) $_POST['deal_city_id'] > 0) {
        $deal_city_id = (int) $_POST['deal_city_id'];
    }

    

    $res_countries = get_all_country();


    $res_states = array();
    if ($deal_country_id > 0) {
        
        $res_states = get_states_from_country_by_id($deal_country_id);
    }

    $res_cities = array();
    if ($deal_state_id > 0) {
       
        $res_cities = get_cities_from_state_by_id($deal_state_id);
    }
    
    
    $res_activities = get_all_activities();    
    if (isset($_POST['deal_main_activity']) && (int) $_POST['deal_main_activity'] > 0) {
        $deal_main_activity = (int) $_POST['deal_main_activity'];
    }
    
    if (isset($_POST['deal_discount_type']) && (int) $_POST['deal_discount_type'] > 0) {
        $deal_discount_type = (int) $_POST['deal_discount_type'];
    }
    
    


?>	



    <div class="col-sm-12 signUpBlock">
    
    
    <h3><?php echo $edit_add_text;?> Deals & Discounts</h3>

    <?php
// show any error messages after form submission
    echo iws_show_error_messages();
    ?>
    <form id="iws_registration_form" class="form-horizontal" action="" method="POST">



        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Deals Name'); ?>:</label>
            <div class="col-sm-8">
                <input name="deal_name" id="deal_name" value="<?php
    if (isset($_POST['deal_name'])) {
        echo $_POST['deal_name'];
    } else {
        echo $deal_name;
    }
    ?>" class="form-control" type="text"/>
            </div>
        </div>
        
        
        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('About the Deal'); ?><br/><small>Please tell us about your deal so we can showcase your deal to our viewers.</small></label>
            <div class="col-sm-8">
                <textarea name="deal_description" id="deal_description" class="form-control" rows="5"><?php
                       if (isset($_POST['deal_description'])) {
                           echo $_POST['deal_description'];
                       } else { echo $deal_description; }
    ?></textarea>
            </div>
        </div>



        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Discount Type'); ?></label>
            <div class="col-sm-8">
                <select name="deal_discount_type" id="deal_discount_type" class="form-control">
             
                <option value="fixed" <?php if ($deal_discount_type=='fixed' || $deal_discount_type=='') { ?> selected <?php } ?>>Fix($)</option>
                <option value="percentage" <?php if ($deal_discount_type=='percentage') { ?> selected <?php } ?>>Percentage(%)</option>
        
            </select> 
            </div>
        </div>



        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Deal Discount'); ?></label>
            <div class="col-sm-8">
                <input name="deal_discount" id="deal_discount" type="text" value="<?php
                       if (isset($_POST['deal_discount'])) {
                           echo $_POST['deal_discount'];
                       } else { echo $deal_discount; } 
    ?>" class="form-control" />
            </div>
        </div>





        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Deal Code'); ?></label>
            <div class="col-sm-8">
                <input name="deal_code" id="deal_code" value="<?php
                       if (isset($_POST['deal_code'])) {
                           echo $_POST['deal_code'];
                       } else { echo $deal_code; }
    ?>" class="form-control" type="text"/>
            </div>
        </div>

        
        
        
        
        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Company Name'); ?>:</label>
            <div class="col-sm-8">
                <input name="deal_company_name" id="deal_company_name" value="<?php
                       if (isset($_POST['deal_company_name'])) {
                           echo $_POST['deal_company_name'];
                       } else { echo $deal_company_name; } 
    ?>" class="form-control" type="text"/>
            </div>
        </div>
        
        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Phone Number'); ?>:</label>
            <div class="col-sm-8">
                <input name="deal_phone_number" id="deal_phone_number" value="<?php
                       if (isset($_POST['deal_phone_number'])) {
                           echo $_POST['deal_phone_number'];
                       } else { echo $deal_phone_number; } 
    ?>" class="form-control" type="text"/>
            </div>
        </div>
        
        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Fax Number'); ?>:</label>
            <div class="col-sm-8">
                <input name="deal_fax_number" id="deal_fax_number" value="<?php
                       if (isset($_POST['deal_fax_number'])) {
                           echo $_POST['deal_fax_number'];
                       } else { echo $deal_fax_number; } 
    ?>" class="form-control" type="text"/>
            </div>
        </div>
        
        

        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Deal website address'); ?>:</label>
            <div class="col-sm-8">
                <input name="deal_website" id="deal_website" value="<?php
                       if (isset($_POST['deal_website'])) {
                           echo $_POST['deal_website'];
                       } else { echo $deal_website; } 
    ?>" class="form-control" type="text"/>
            </div>
        </div>





        <h3>Activities</h3>

        <div class="form-group">
            <label for="" class="col-sm-4 control-label">Deal Activity</label>
     <div class="col-sm-8">
   

        <select name="deal_main_activity" id="deal_main_activity" class="form-control">
            <option value="0">---Select Activity---</option>                    
    <?php
    if (!empty($res_activities)) {
        foreach ($res_activities as $row_acti) {
            ?>
                    <option value="<?php echo $row_acti->term_id; ?>" <?php if ($row_acti->term_id == $deal_main_activity) { ?> selected <?php } ?>><?php echo ucwords($row_acti->name); ?></option>
        <?php }
    } ?>

        </select>
   </div>
 </div>



        
        
        <h3>Deal Location</h3>
        
        
        
        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e("Address"); ?></label>
            <div class="col-sm-8">

                <input type="text" name="deal_adderss" id="deal_adderss" value="<?php if (isset($_POST['deal_adderss'])) {
        echo $_POST['deal_adderss'];
    } else {
        echo $deal_adderss;
    } ?>" class="form-control" />
            </div>
        </div>







        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e("Country"); ?></label>
            <div class="col-sm-8">

                <select name="deal_country_id" id="deal_country_id" class="form-control" >
                    <option value="0">---Select Country---</option>   
                    <?php if (!empty($res_countries)) {
                        foreach ($res_countries as $row_country) { ?>

                            <option value="<?php echo $row_country->countryid; ?>" <?php if ($row_country->countryid == $deal_country_id) { ?> selected <?php } ?>><?php echo ucfirst($row_country->country); ?></option>

        <?php }
    } ?>

                </select> 
            </div>
        </div>





        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e("State"); ?></label>
            <div class="col-sm-8">

                <select name="deal_state_id" id="deal_state_id" class="form-control" >
                    <option value="0">---Select State---</option>

    <?php
    if (!empty($res_states)) {
        foreach ($res_states as $row_states) {
            ?>

                            <option value="<?php echo $row_states->regionid; ?>" <?php if ($row_states->regionid == $deal_state_id) { ?> selected <?php } ?>><?php echo ucfirst($row_states->region); ?></option>


                            <?php
                        }
                    }
                    ?>

                </select> 
            </div>
        </div>







        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e("City"); ?></label>
            <div class="col-sm-8">

                <select name="deal_city_id" id="deal_city_id" class="form-control" >
                    <option value="0">---Select City---</option>

    <?php
    if (!empty($res_cities)) {
        foreach ($res_cities as $row_cities) {
            ?>

                            <option value="<?php echo $row_cities->CityId; ?>" data-lat="<?php echo $row_cities->Latitude; ?>" data-long="<?php echo $row_cities->Longitude; ?>" <?php if ($row_cities->CityId == $deal_city_id) { ?> selected <?php } ?>><?php echo ucfirst($row_cities->City); ?></option>


        <?php }
    } ?>


                </select> 


                <input type="hidden" name="deal_lat" id="deal_lat" value="<?php if (isset($_POST['deal_lat'])) {
        echo $_POST['deal_lat'];
    } else {
        echo $deal_lat;
    } ?>" />
                <input type="hidden" name="deal_long" id="deal_long" value="<?php if (isset($_POST['deal_long'])) {
        echo $_POST['deal_long'];
    } else {
        echo $deal_long;
    } ?>" />

            </div>
        </div>









        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e("Zip Code"); ?></label>
            <div class="col-sm-8">

                <input type="text" name="deal_zip_code" id="deal_zip_code" value="<?php if (isset($_POST['deal_zip_code'])) {
        echo $_POST['deal_zip_code'];
    } else {
        echo $deal_zip_code;
    } ?>" class="form-control" />
            </div>
        </div>

        
        
        
        

        <h3>Upload picture of deal's logo</h3>                       

        <?php
        $allowed_ext = "jpg,gif,png";
        $post_id = $obj_id;
        $attr['name'] = '_thumbnail_id';
        $attr['max_size'] = "10000";
        $attr['count'] = 1;

        $type = 'post';

        $uploaded_items = $post_id ? get_post_meta($post_id, $attr['name'], true) : array();



        
        $has_featured_image = false;
        $has_images = false;
        $has_avatar = false;
        

        if ($post_id) {

                   
            if (isset($_POST['iws_files']['_thumbnail_id'])) {
                 $thumb_id = $_POST['iws_files']['_thumbnail_id'][0]; 
                if ($thumb_id) {
                    $has_featured_image = true;
                    $featured_image = attach_html($thumb_id); 
                    echo '<input type="hidden" name="iws_files[_thumbnail_id][]" value="'.$thumb_id.'">';
                }
            } else {

                    // it's a featured image then
                    $thumb_id = get_post_thumbnail_id($post_id);

                    if ($thumb_id) {
                        $has_featured_image = true;
                        $featured_image = attach_html($thumb_id); 
                    }
            }
        } else {
            if (isset($_POST['iws_files']['_thumbnail_id'])) {
                echo $thumb_id = $_POST['iws_files']['_thumbnail_id'][0]; die;
                if ($thumb_id) {
                    $has_featured_image = true;
                    $featured_image = attach_html($thumb_id); 
                    echo '<input type="hidden" name="iws_files[_thumbnail_id][]" value="'.$thumb_id.'">';
                }
            }
        }
        ?>


        <div class="iws-fields">
            <div id="iws-<?php echo $attr['name']; ?>-upload-container">
                <div class="iws-attachment-upload-filelist" data-type="file" >
                    <a id="iws-<?php echo $attr['name']; ?>-pickfiles" class="btn btn-danger btn-small button file-selector <?php echo ' iws_' . $attr['name']; ?>" href="#">Upload Image</a>

                    <ul class="iws-attachment-list thumbnails" >
    <?php
    if ($has_featured_image) {
        echo $featured_image;
    }

    if ($has_avatar) {
        $avatar = get_user_meta($post_id, 'user_avatar', true);
        if ($avatar) {
            
            
            echo '<li class="iws-image-wrap thumbnail"><div class="attachment-name">'.$featured_image.'</div><div class="caption"><a href="javascript:void(0)" data-confirm="Are you sure to delete your image?" class="btn btn-danger btn-small iws-button button iws-delete-file">Delete</a></div></li>';
            
            
        }
    }

    
    ?>
                    </ul>
                </div>
            </div><!-- .container -->



        </div> <!-- .iws-fields -->

        
        <style>
            .thumbnail {
                border: none !important;
            }
        </style>
            
        
        
        <script type="text/javascript">
            jQuery(function($) {
                new IWS_Uploader('iws-<?php echo $attr['name']; ?>-pickfiles', 'iws-<?php echo $attr['name']; ?>-upload-container', <?php echo $attr['count']; ?>, '<?php echo $attr['name']; ?>', '<?php echo $allowed_ext; ?>', <?php echo $attr['max_size'] ?>);
                    
                    
                    
                jQuery(".iws-delete-file").on("click",function(e){
                       
                       
                    e.preventDefault();

                    if ( confirm( $(this).data('confirm') ) ) {
                        $.post('<?php echo admin_url('admin-ajax.php'); ?>',{action: 'iws_file_del'}, function() {
                            window.location.reload();
                        });
                    }
                       
                });
            });
        </script>





        <div class="form-group">

          <div class="col-sm-12 text-right">
                <input type="hidden" name="iws_dealsdiscount_editadd_nonce" value="<?php echo wp_create_nonce('iws-dealsdiscount-editadd-nonce'); ?>"/>

                <button id="iws_dealsdiscount_editadd_submit" type="submit" class="btn btn-default sign-btn">Submit</button>
            </div>

        </div>
        
        
        <script>
            var ajaxurl='<?php echo admin_url('admin-ajax.php'); ?>';
            jQuery(document).ready(function(){
				
	jQuery('#deal_country_id').change(function(e) {
		var data = {
			'action': 'get_states_of_country',
			'country': jQuery(this).val()
		};
		
		var state_ops = '<option value="">---Select State---</option>';
		var city_ops = '<option value="">---Select City---</option>';
		
		jQuery("#deal_state_id").html(state_ops);
		jQuery("#deal_city_id").html(city_ops);

		// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
		jQuery.post(ajaxurl, data, function(response) {
			if(jQuery("#deal_state_id").length>0)
			{
				jQuery("#deal_state_id").html(response);
			}
		});
    });
	
	jQuery('#deal_state_id').change(function(e) {
		var data = {
			'action': 'get_cities_of_state',
			'state': jQuery(this).val()
			
		};
		
		
		var city_ops = '<option value="">---Select City---</option>';
		
		jQuery("#deal_city_id").html(city_ops);

		// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
		jQuery.post(ajaxurl, data, function(response) {
			if(jQuery("#deal_city_id").length>0)
			{
				jQuery("#deal_city_id").html(response);
			}
		});
    });
	
	jQuery('#deal_city_id').change(function(e) {
		var vlat=jQuery("#deal_city_id option:selected").attr('data-lat');
		var vlong=jQuery("#deal_city_id option:selected").attr('data-long');
		
		jQuery("#deal_lat").val(vlat);
		jQuery("#deal_long").val(vlong);
		
	});
	
});
        </script>


        
    </form>
</div>
<?php
echo ob_get_clean();
?>