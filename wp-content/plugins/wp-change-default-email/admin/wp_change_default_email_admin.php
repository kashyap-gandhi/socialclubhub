<?php
/*
Filename: wp_change_default_email_admin.php
Description: Adds the admin pages for WP Change Default Email Plugin
License: GPLv2
Author: Vijay Sharma
Author URI: http://www.techeach.com/
*/

function wp_change_default_email_admin() {
	add_options_page('WP Change Default Email Options', 'WP Change Default Email','manage_options', 
			  __FILE__, 'wp_change_default_email_page');
}

function wp_change_default_email_page() {
	global $wcdeOptions;
	if ( isset($_POST['wp_change_default_email_update'])) {
	    $wcdeOptions = array();
	    $wcdeOptions["from"] = trim($_POST["wp_change_default_email_from"]);
	    $wcdeOptions["fromname"] = trim($_POST['wp_change_default_email_fromname']);		

	    update_option("wp_change_default_email_options",$wcdeOptions);
	    if(!is_email($wcdeOptions["from"])){
			echo '<div id="message" class="updated error fade"><p><strong>' . __("The field \"From\" must be a valid email address!") . '</strong></p></div>';
	    } else{
			echo '<div id="message" class="updated fade"><p><strong>' . __("Options saved.", 'wp-change-default-email') . '</strong></p></div>';
   	    }
	} else {
		$cst_opt=get_site_option( "wp_change_default_email_options");
		
		if(!empty($cst_opt)){
			
			if(isset($cst_opt['from'])) {  $wcdeOptions["from"] = trim($cst_opt["from"]);  } 
			if(isset($cst_opt['fromname'])) {  $wcdeOptions["fromname"] = trim($cst_opt["fromname"]);  } 
			
		}
		
	}
	
	

?>
<div class="wrap">
	
<h2><?php _e("WP Change Default Email", 'wp-change-default-email'); ?></h2>

<form action="" method="post" enctype="multipart/form-data" name="wp_change_default_email_form">
<table class="form-table">
	<tr valign="top">
		<th scope="row">
			<?php _e('From','wp-change-default-email'); ?>
		</th>
		<td>
			<label>
				<input type="text" name="wp_change_default_email_from" value="<?php echo $wcdeOptions["from"]; ?>" size="43" style="width:272px;height:24px;" />
			</label>
		</td>
	</tr>
	<tr valign="top">
		<th scope="row">
			<?php _e('From Name', 'wp-change-default-email'); ?>
		</th>
		<td>
			<label>
				<input type="text" name="wp_change_default_email_fromname" value="<?php echo $wcdeOptions["fromname"]; ?>" size="43" style="width:272px;height:24px;" />
			</label>
		</td>
	</tr>
</table>

<p class="submit">
<input type="hidden" name="wp_change_default_email_update" value="update" />
<input type="submit" class="button-primary" name="Submit" value="<?php _e('Save Changes', 'wp-change-default-email'); ?>" />
</p>

</form>



<br />

</div>
<?php 
}
add_action('admin_menu', 'wp_change_default_email_admin');
