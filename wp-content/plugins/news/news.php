<?php
/*
  Plugin Name: News
  Plugin URI:
  Description: This plugin used for add edit delete and listing news module at admin side.
  Version: 1.0
  Author: iWS
  Author URI:
 */



add_action('init', 'create_news');

function create_news() {

    ////====set slug for direct open==/
    /////='supports' => array('title','editor','excerpt','trackbacks','custom-fields','comments','revisions','thumbnail','author','page-attributes',)
    //=taxonomies 'post_tag'

    register_post_type('news', array('label' => 'News', 'description' => 'New Listing', 'public' => true,'has_archive' => true,  'show_ui' => true, 'show_in_menu' => true, 'capability_type' => 'page', 'hierarchical' => true, 'rewrite' => array('slug' => 'news', 'with_front' => true), 'query_var' => true, 'exclude_from_search' => false, 'menu_position' => 18, 'supports' => array('title', 'editor', 'thumbnail','page-attributes'), 'taxonomies' => array(), 'labels' => array(
            'name' => 'News',
            'singular_name' => 'New',
            'menu_name' => 'Manage News',
            'add_new' => 'Add New',
            'add_new_item' => 'Add New New',
            'edit' => 'Edit',
            'edit_item' => 'Edit New',
            'new_item' => 'New New',
            'view' => 'View New',
            'view_item' => 'View New',
            'search_items' => 'Search News',
            'not_found' => 'No News Found',
            'not_found_in_trash' => 'No News found in Trash',
            'parent' => 'Parent New',
        ),));
    
    
    // Add new taxonomy, make it hierarchical (like categories)
    $labels = array(
        'name' => _x('Categories', 'taxonomy general name'),
        'singular_name' => _x('Category', 'taxonomy singular name'),
        'search_items' => __('Search Categories'),
        'all_items' => __('All Categories'),
        'parent_item' => __('Parent Category'),
        'parent_item_colon' => __('Parent Category:'),
        'edit_item' => __('Edit Category'),
        'update_item' => __('Update Category'),
        'add_new_item' => __('Add New Category'),
        'new_item_name' => __('New Category Name'),
        'menu_name' => __('Category'),
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'news-cat'),
    );

    register_taxonomy('news-cat', array('news'), $args);


}

//===inline edit 
//http://wpdreamer.com/2012/03/manage-wordpress-posts-using-bulk-edit-and-quick-edit/
//http://wordpress.stackexchange.com/questions/578/adding-a-taxonomy-filter-to-admin-list-for-a-custom-post-type
///http://www.smashingmagazine.com/2013/12/05/modifying-admin-post-lists-in-wordpress/

add_action('restrict_manage_posts', 'news_restrict_manage_posts');

function news_restrict_manage_posts() {
    global $typenow, $wpdb;





    // an array of all the taxonomyies you want to display. Use the taxonomy name or slug
    $taxonomies = array('news-cat');

    // must set this to the post type you want the filter(s) displayed on
    if ($typenow == 'news') {


        //===add category drop down
        foreach ($taxonomies as $tax_slug) {
            $tax_obj = get_taxonomy($tax_slug);
            $tax_name = $tax_obj->labels->name;
            $terms = get_terms($tax_slug);

            if (count($terms) > 0) {
                echo "<select name='news-cat' id='news-cat' class='postform'>";
                echo "<option value=''>Show All $tax_name</option>";
                foreach ($terms as $term) {
                    
                     echo '<option value="' . $term->term_id.'"';
                    
                    if(isset($_GET['news-cat']) && (int) $_GET['news-cat'] == (int) $term->term_id){
                    echo ' selected="selected"';
                            
                    }
                    echo '>' . $term->name . '</option>';
                    
                    
                    //$term->count
                }
                echo "</select>";
            }
        }



        //===custom fields
        echo '';




        ///===end code
    }
}

//add_action('request', 'news_request');
function news_request($request) {
    if (is_admin() && $GLOBALS['PHP_SELF'] == '/wp-admin/edit.php' && isset($request['post_type']) && $request['post_type'] == 'news') {
        $request['term'] = get_term($request['news-cat'], 'news-cat')->name;
    }
    return $request;
}

//http://codex.wordpress.org/Class_Reference/WP_Query#Custom_Field_Parameters

add_filter('parse_query', 'news_table_filter');
function news_table_filter($query) {
    if (is_admin() AND $query->query['post_type'] == 'news') {
        $qv = &$query->query_vars;
        
        
        $qv['tax_query'] = array();
       
		
        
        
          if (!empty($_GET['news-cat']) && (int) $_GET['news-cat'] > 0) {
            
              $news_cat=(int) $_GET['news-cat'];
              
              
                    $term = get_term_by('id',$news_cat,'news-cat');
                    
                    $qv['news-cat'] = $term->slug;

                            
          }
          
//          echo "<pre>";
//          print_r($qv);
//          die;
//        
        /*$qv['meta_query'] = array();

        if (!empty($_GET['merchant_country_id'])) {

            $qv['meta_query']['relation'] = 'AND';
        }

        if (!empty($_GET['merchant_country_id'])) {

            $merchant_country_id = (int) $_GET['merchant_country_id'];

            $qv['meta_query'][] = array(
                'key' => 'merchant_country_id',
                'value' => $merchant_country_id,
                'compare' => '=',
                'type' => 'CHAR'
            );
        }

        if (!empty($_GET['merchant_main_activity'])) {

            $merchant_main_activities = (int) $_GET['merchant_main_activity'];

            $qv['meta_query'][] = array(
                'key' => 'merchant_main_activity',
                'value' => $merchant_main_activities,
                'compare' => '=',
                'type' => 'CHAR'
            );
        }

       */
       


        /* if( !empty( $_GET['orderby'] ) AND $_GET['orderby'] == 'event_date' ) {
          $qv['orderby'] = 'meta_value';
          $qv['meta_key'] = '_bs_meta_event_date';
          $qv['order'] = strtoupper( $_GET['order'] );
          } */
    }
}

//https://www.skyverge.com/blog/add-custom-bulk-action/
add_action('admin_footer-edit.php', 'news_bulk_admin_footer');

function news_bulk_admin_footer() {

    global $post_type;

    if ($post_type == 'news') {
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function() {
                jQuery('<option>').val('export').text('<?php _e('Export') ?>').appendTo("select[name='action']");
                jQuery('<option>').val('export').text('<?php _e('Export') ?>').appendTo("select[name='action2']");
            });
        </script>
        <?php
    }
}

add_action('load-edit.php', 'news_bulk_action');

function news_bulk_action() {
    global $typenow;
    $post_type = $typenow;

    if ($post_type == 'news') {

        // get the action
        $wp_list_table = _get_list_table('WP_Posts_List_Table');  // depending on your resource type this could be WP_Users_List_Table, WP_Comments_List_Table, etc
        $action = $wp_list_table->current_action();

        $allowed_actions = array("export");
        if (!in_array($action, $allowed_actions))
            return;

        // security check
        //check_admin_referer('bulk-posts');
        // make sure ids are submitted.  depending on the resource type, this may be 'media' or 'ids'
        /* if(isset($_REQUEST['post'])) {
          $post_ids = array_map('intval', $_REQUEST['post']);
          }

          if(empty($post_ids)) return; */

        // this is based on wp-admin/edit.php
        $sendback = remove_query_arg(array('exported', 'doexported', 'untrashed', 'deleted', 'ids'), wp_get_referer());
        if (!$sendback)
            $sendback = admin_url("edit.php?post_type=$post_type");

        $pagenum = $wp_list_table->get_pagenum();
        $sendback = add_query_arg('paged', $pagenum, $sendback);

        switch ($action) {
            case 'export':

                // if we set up user permissions/capabilities, the code might look like:
                //if ( !current_user_can($post_type_object->cap->export_post, $post_id) )
                //	wp_die( __('You are not allowed to export this post.') );

                $exported = 0;
                /* foreach( $post_ids as $post_id ) {

                  if ( !$this->perform_export($post_id) )
                  wp_die( __('Error exporting post.') );

                  $exported++;
                  } */

                //perform_export();
                //$sendback = add_query_arg( array('exported' => $exported, 'ids' => join(',', $post_ids) ), $sendback );

                $sendback = add_query_arg(array('doexported' => $exported), $sendback);

                break;

            default: return;
        }

        $sendback = remove_query_arg(array('action', 'action2', 'tags_input', 'post_author', 'comment_status', 'ping_status', '_status', 'post', 'bulk_edit', 'post_view'), $sendback);

        wp_redirect($sendback);
        exit();
    }
}

if (isset($_REQUEST['doexported']) && (int) $_REQUEST['doexported'] == 0) {

    global $post_type, $pagenow, $typenow;
    $post_type = $typenow;

    if (trim($post_type) == '') {

        if (isset($_REQUEST['post_type'])) {
            $post_type = $_REQUEST['post_type'];
        }
    }


    if ($pagenow == 'edit.php' && $post_type == 'news') {
        
        
      // require_once( ABSPATH . "wp-includes/pluggable.php" );

        news_perform_export();
    }
}


function get_news_meta_all($post_id){
    global $wpdb;

    $data   =   array();

    $wpdb->query(" SELECT `meta_key`, `meta_value`  FROM ".$wpdb->prefix."postmeta  WHERE meta_key!='_edit_last' and meta_key!='_edit_lock' and `post_id` = ".$post_id."  ");

    foreach($wpdb->last_result as $k => $v){
        $data[$v->meta_key] =   $v->meta_value;
    };

    return $data;
}

//==http://stackoverflow.com/questions/16722818/wordpress-admin-widget-that-exports-data
//http://wordpress.stackexchange.com/questions/10505/export-wordpress-table-to-excel
function news_perform_export() {

    global $wpdb,$post;
    // do whatever work needs to be done
    

    
    $sql='';
    
    $sql.='SELECT ps.* ';
    
    if (isset($_GET['news-cat']) && (int) $_GET['news-cat'] > (int) 0) {
        $sql.=' , tr1.term_taxonomy_id ';
    }
            
    $sql.=' FROM '.$wpdb->prefix.'posts as ps ';
    
    
    
    
    
     if (isset($_GET['news-cat']) && (int) $_GET['news-cat'] > (int) 0) {
        $sql.=' INNER JOIN '.$wpdb->prefix.'term_relationships AS tr1 ON ( ps.ID = tr1.object_id ) ';
    
     }
    
    
     if (isset($_GET['merchant_country_id']) && !empty($_GET['merchant_country_id'])) {
        $sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt1 ON ( ps.ID = mt1.post_id ) ';
    
     }
    if (isset($_GET['merchant_main_activity']) && !empty($_GET['merchant_main_activity'])) {
        $sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt2 ON ( ps.ID = mt2.post_id ) ';
    }
    
    $sql.=" WHERE 1=1 AND ps.post_type = 'news' AND (ps.post_status = 'publish' OR ps.post_status = 'future' OR ps.post_status = 'draft' OR ps.post_status = 'pending' OR ps.post_status = 'private') ";
    
    
    
    if (isset($_GET['news-cat']) && (int) $_GET['news-cat'] > (int) 0) {
        $sql.=' AND tr1.term_taxonomy_id='.(int) $_GET['news-cat'].' ';
    }
    
    
    if ( (isset($_GET['merchant_main_activity']) && !empty($_GET['merchant_main_activity'])) ||  isset($_GET['merchant_country_id']) && !empty($_GET['merchant_country_id']) ) {
    
        $sql.=" AND ( ";
    
    }
    
    
    
    
    if (isset($_GET['merchant_country_id']) && !empty($_GET['merchant_country_id'])) {
        $sql.="( mt1.meta_key = 'merchant_country_id' AND CAST(mt1.meta_value AS CHAR) = '".trim($_GET['merchant_country_id'])."' ) ";
    }
    
     if ( (isset($_GET['merchant_main_activity']) && !empty($_GET['merchant_main_activity'])) &&  isset($_GET['merchant_country_id']) && !empty($_GET['merchant_country_id']) ) {
            $sql.=" AND ";
     }
     
    if (isset($_GET['merchant_main_activity']) && !empty($_GET['merchant_main_activity'])) {
        $sql.=" ( mt2.meta_key = 'merchant_main_activity' AND CAST(mt2.meta_value AS CHAR) = '".$_GET['merchant_main_activity']."' ) ";
        
    }
    
    
     if ( (isset($_GET['merchant_main_activity']) && !empty($_GET['merchant_main_activity'])) ||  isset($_GET['merchant_country_id']) && !empty($_GET['merchant_country_id']) ) {
   
            $sql.=" ) ";
     }
     
    $sql.=' GROUP BY ps.ID ORDER BY ps.ID ASC ';
    
    
    
    //echo $sql;
    //die;
 

    
    $file = 'export'.rand();
    
    $export = mysql_query($sql) or die("Sql error : " . mysql_error());
    
    //echo "<pre>";
    //print_r(mysql_fetch_row($export));   
    //die;
    
    $fields = mysql_num_fields($export);
    $header='';$data='';
    for ($i = 0; $i < $fields; $i++) {
        $header .= mysql_field_name($export, $i) . "\t";
    }
    
   
    
    $cnt=0;
    while ($row = mysql_fetch_row($export)) {
        
        //echo "<pre>";
        //print_r($row);
        //die;
        
         $get_all_meta=get_news_meta_all($row[0]);
        
        // print_r($get_all_meta);
         //die;
         
         if($cnt==0) {
             if(!empty($get_all_meta)) {
                foreach($get_all_meta as $mkey=>$mval) {
                 $header .= $mkey . "\t"; 
                }
             }
         }
         
         $cnt++;
        
         
        $line = '';
        foreach ($row as $value) {
            if ((!isset($value) ) || ( $value == "" )) {
                $value = "\t";
            } else {
                $value = str_replace('"', '""', $value);
                $value = '"' . $value . '"' . "\t";
            }
            $line .= $value;
        }
        
         if(!empty($get_all_meta)) {
                foreach($get_all_meta as $mkey=>$mval) {
                    if ((!isset($mval) ) || ( $mval == "" )) {
                        $mval = "\t";
                    } else {
                        $mval = str_replace('"', '""', $mval);
                        $mval = '"' . $mval . '"' . "\t";
                    }
                    $line .= $mval;
                }
         }
        $data .= trim($line) . "\n";
        
        
        
    }
    $data = str_replace("\r", "", $data);
    if ($data == "") {
        $data = "\n(0) Records Found!\n";
    }

    $filename = $file . "_" . date("M-d-Y");

    header("Content-type: application/octet-stream");
    header("Content-disposition: filename=" . $filename . ".xls");
    header("Pragma: no-cache");
    header("Expires: 0");
    print "$header\n$data";
    die;
}



//==change title or metabox on the fly
//add_action('add_meta_boxes', 'news_change_meta_box_titles', 999);

function news_change_meta_box_titles() {
    global $wp_meta_boxes; // array of defined meta boxes
    // cycle through the array, change the titles you want

    //remove_meta_box('news-catdiv', 'clubs', 'side');
    //$wp_meta_boxes['clubs']['side']['core']['news-catdiv']['title']= 'Club Other Activities';
}

///=============add custom column==========


add_filter('manage_edit-news_columns', 'news_edit_columns');

function news_edit_columns($columns) {

    $columns = array(
        'cb' => '<input type="checkbox" />',
        'id' => __('ID'),
        'title' => __('Title'),
        //'club_status' => __('Type'),
        'post_status' => __('Status'),
        'date' => __('date')
    );

    return $columns;
}

add_action('manage_news_posts_custom_column', 'my_manage_news_columns', 10, 2);

function my_manage_news_columns($column_name, $post_id) {
    global $wpdb;
    global $post;

    switch ($column_name) {

        case 'id':
            echo $post_id;
            break;

        case 'post_status':
            echo $post->post_status;

            break;

       



        default:
            break;
    } // end switch
}

add_filter('manage_edit-news_sortable_columns', 'my_news_sortable_columns');

function my_news_sortable_columns($columns) {

    //$columns['id'] = 'id';
   
    $columns['post_status'] = 'post_status';


    return $columns;
}

/////////////===========add club active part================



/**
 * Add stylesheet to the page
 */
add_action('admin_enqueue_scripts', 'news_stylesheet_to_admin');

function news_stylesheet_to_admin() {
    wp_enqueue_style('news_css', plugins_url('/css/news.css', __FILE__));
}

/**
 * Add script to the page
 */
add_action('admin_enqueue_scripts', 'news_script_to_admin');

function news_script_to_admin($page) {
    if (is_admin()) {
        if ('post-new.php' == $page || 'post.php' == $page) {
            wp_register_script('news_js', plugins_url('/js/news_script.js', __FILE__), array('jquery'));
            // Enqueue Scripts that are needed on all the pages
            wp_enqueue_script('jquery');
            wp_enqueue_script('news_js');
        } else {
            return;
        }
    }
}


add_action('admin_init', 'meta_news_extra_fields_type_var');

function meta_news_extra_fields_type_var() {



    // please add other post type

    $post_types = array('news');



    foreach ($post_types as $post_type) {


       
        add_meta_box('news_contact_info', 'New Information', 'news_contact_info_setup', $post_type, 'normal', 'high');
        
    }

    // save metabox
    
    add_action('save_post', 'news_extra_field_save');
    
    
}


function news_contact_info_setup() {

    global $wpdb, $post;

    
    

    $news_user_id = get_post_meta($post->ID, 'news_user_id', TRUE);




 $roles = array( 'administrator');

$get_customer=get_user_by_role($roles);
   
    
    ?>






        <div class="left_label">
            <label for="news_user_id" class="">Select User</label>
        </div>
        <div class="right_label">
            <select name="news_user_id" id="news_user_id">
             
    <?php
    if (!empty($get_customer->results)) {
        foreach ($get_customer->results as $customer) {
            ?>
                        <option value="<?php echo $customer->ID; ?>" <?php if ($customer->ID == $news_user_id) { ?> selected <?php } ?>><?php echo $customer->display_name . " || " . $customer->user_email; ?></option>
        <?php }
    } ?>
            </select> <small>(News owner user)</small>

        </div>
        <div class="clear"></div>


        
        



    <?php
    echo '<input type="hidden" name="iws_adminnews_nonce" value="' . wp_create_nonce('iws-adminnews-nonce') . '" />';
}

// Display any errors
add_action('admin_notices', 'news_admin_notice_handler');

function news_admin_notice_handler() {

    global $post_type, $pagenow;
    
    $html='';

    if (isset($_GET['message'])) {

        if ((int) $_GET['message'] == (int) 4) {
            $html = '<div class="error">
                        <p><strong>Validation errors</strong></p>
                        <p>- Please select news user.</p></div>';
        }
    }



    if ($pagenow == 'edit.php' && $post_type == 'news' && isset($_REQUEST['doexported']) && (int) $_REQUEST['doexported']) {
        $message = _('News exported.');
        $html = "<div class='updated'><p>".$message."</p></div>";
    }

    echo $html;

}


function news_extra_field_save($post_id) {

    global $post,$wpdb;
    
        // don't do on autosave or when new posts are first created
    if (( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) || (isset($post) && !empty($post) && $post->post_status == 'auto-draft')) return $post_id;

    
    
    // authentication checks
    // make sure data came from our meta box
    
         
 
        

if($_POST && isset($_POST['iws_adminnews_nonce']) && wp_verify_nonce($_POST['iws_adminnews_nonce'],'iws-adminnews-nonce')){







    // check user permissions

    if ($_POST['post_type'] == 'page') {

        if (!current_user_can('edit_page', $post_id))
            return $post_id;
    } else {

        if (!current_user_can('edit_post', $post_id))
            return $post_id;
    }
    
    
    
      
    
    
    //===info all



    $news_user_id = (int) sanitize_text_field($_POST['news_user_id']);
    update_post_meta($post_id, 'news_user_id', esc_attr($news_user_id));

   $wpdb->update($wpdb->posts, array('post_author' => $news_user_id), array('ID' => $post_id));




    // just checking it's not empty - you could do other tests...
    if (empty($news_user_id) || (int) $news_user_id == (int) 0) {
        $meta_missing = true;
    }

    // on attempting to publish - check for completion and intervene if necessary
    if (( isset($_POST['publish']) || isset($_POST['save']) ) && $_POST['post_status'] == 'publish') {
        //  don't allow publishing while any of these are incomplete
        if ($meta_missing == true) {
            global $wpdb;
            $wpdb->update($wpdb->posts, array('post_status' => 'pending'), array('ID' => $post_id));
            // filter the query URL to change the published message
            add_filter('redirect_post_location', create_function('$location', 'return add_query_arg("message", "4", $location);'));
        }
    }

}

    return $post_id;
}


function get_latest_news(){
    
    global $wpdb,$post;
       
        
    $sql='';
    
    $sql.='SELECT ps.* ';
    
    if (isset($_GET['news-cat']) && (int) $_GET['news-cat'] > (int) 0) {
        $sql.=' , tr1.term_taxonomy_id ';
    }
            
    $sql.=' FROM '.$wpdb->prefix.'posts as ps ';
    
    
    
    
    
     /*if (isset($_GET['news-cat']) && (int) $_GET['news-cat'] > (int) 0) {
        $sql.=' INNER JOIN '.$wpdb->prefix.'term_relationships AS tr1 ON ( ps.ID = tr1.object_id ) ';
    
     }
    
    
     if (isset($_GET['merchant_country_id']) && !empty($_GET['merchant_country_id'])) {
        $sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt1 ON ( ps.ID = mt1.post_id ) ';
    
     }
    if (isset($_GET['merchant_main_activity']) && !empty($_GET['merchant_main_activity'])) {
        $sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt2 ON ( ps.ID = mt2.post_id ) ';
    }*/
    
    $sql.=" WHERE 1=1 AND ps.post_type = 'news' AND (ps.post_status = 'publish') and ps.post_parent=0 ";
    
    
    /*
    if (isset($_GET['news-cat']) && (int) $_GET['news-cat'] > (int) 0) {
        $sql.=' AND tr1.term_taxonomy_id='.(int) $_GET['news-cat'].' ';
    }
    
    
    if ( (isset($_GET['merchant_main_activity']) && !empty($_GET['merchant_main_activity'])) ||  isset($_GET['merchant_country_id']) && !empty($_GET['merchant_country_id']) ) {
    
        $sql.=" AND ( ";
    
    }
    
    
    
    
    if (isset($_GET['merchant_country_id']) && !empty($_GET['merchant_country_id'])) {
        $sql.="( mt1.meta_key = 'merchant_country_id' AND CAST(mt1.meta_value AS CHAR) = '".trim($_GET['merchant_country_id'])."' ) ";
    }
    
     if ( (isset($_GET['merchant_main_activity']) && !empty($_GET['merchant_main_activity'])) &&  isset($_GET['merchant_country_id']) && !empty($_GET['merchant_country_id']) ) {
            $sql.=" AND ";
     }
     
    if (isset($_GET['merchant_main_activity']) && !empty($_GET['merchant_main_activity'])) {
        $sql.=" ( mt2.meta_key = 'merchant_main_activity' AND CAST(mt2.meta_value AS CHAR) = '".$_GET['merchant_main_activity']."' ) ";
        
    }
    
    
     if ( (isset($_GET['merchant_main_activity']) && !empty($_GET['merchant_main_activity'])) ||  isset($_GET['merchant_country_id']) && !empty($_GET['merchant_country_id']) ) {
   
            $sql.=" ) ";
     } */
     
    $sql.=' GROUP BY ps.ID ORDER BY ps.ID DESC ';
    

    
    //echo $sql;
   // die;
 

    $get_news = mysql_query($sql) or die("Sql error : " . mysql_error());
    
    $lhtml='';
    
    $ncnt=1;
    if(mysql_num_rows($get_news)>0){
        while ($row = mysql_fetch_array($get_news)) {
            
            
            $active_cls='';
            if($ncnt==1){
                $active_cls='active';
            }
               
            
            $news_name=$row['post_title'];
            
            $news_link=get_permalink($row['ID']);
            
            $lhtml.='<div class="item '.$active_cls.'"><h2>';
            $lhtml.='<a href="'.$news_link.'" rel="bookmark">';
            $lhtml.=$news_name;
            $lhtml.='</a>';
            $lhtml.='</h2></div>';
            
            $ncnt++;
        
        }
    }
    
    return $lhtml;
}
add_shortcode('news_latest','get_latest_news');

?>