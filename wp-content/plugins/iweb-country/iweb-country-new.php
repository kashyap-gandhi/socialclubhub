<?php
require_once("iwebcountryclass.php");
$objMem = new iwebcountryClass();

global $wpdb, $post;

$addme = '';

$error_message = '';


$act_name = 'Add';
$btn = "Add New";

$countryid = "";
$country = '';
$fips104 = '';
$iso2 = '';
$iso3 = '';
$ison = '';
$internet = '';
$capital = '';
$mapreference = '';
$nationalitysingular = '';
$nationalityplural = '';
$currency = '';
$currencycode = '';
$population = '';


$act = '';

$ad_type = 1;

 $hidval = 1;


if ($_POST && isset($_POST['iws_adminiwebcountry_editadd_nonce']) && wp_verify_nonce($_POST['iws_adminiwebcountry_editadd_nonce'], 'iws-adminiwebcountry-editadd-nonce') && isset($_POST["addme"]) && (int) $_POST["addme"] > 0) {



    $addme = $_POST["addme"];

    




    if ($addme == 1) {

        $objMem->addNewIwebCountry($table_name = $wpdb->prefix . "countries", $_POST);


        header("Location:admin.php?page=iweb-country/iweb-country.php&info=saved");
        exit;
        
    } else if ($addme == 2) {



if(isset($_REQUEST["countryid"]) && (int) $_REQUEST["countryid"] > 0) {
    $countryid = $_REQUEST["countryid"];


        $sSQL = "select * from " . $wpdb->prefix . "countries where countryid=$countryid";
        $result = mysql_query($sSQL) or die('Error, query failed');
        if (mysql_num_rows($result) > 0) {
            if ($row = mysql_fetch_assoc($result)) {
                
                $country = trim($_POST['country']);


                $iso2 = trim($_POST['iso2']);
                $iso3 = trim($_POST['iso3']);





                if ($country == '') {
                    $error_message.='<p>Please enter country name.</p>';
                }



                if (isset($_POST["iso2"]) && trim($_POST["iso2"]) == '') {
                    $error_message.='<p>Please enter iso2.</p>';
                }


                if (isset($_POST["iso3"]) && trim($_POST["iso3"]) == '') {
                    $error_message.='<p>Please enter iso3.</p>';
                }






                if (trim($error_message) == '') {



                    $objMem->updIwebCountry($table_name = $wpdb->prefix . "countries", $_POST);


                    header("Location:admin.php?page=iweb-country/iweb-country.php&info=upd");
                    exit;
                }
            }
        } else { 
            header("Location:admin.php?page=iweb-country/iweb-country.php&info=notfound");
            exit;
        }
        } else { 
            header("Location:admin.php?page=iweb-country/iweb-country.php&info=notfound");
            exit;
        }
    }
}



if (isset($_REQUEST["act"]) && $_REQUEST["act"] == 'upd' && isset($_REQUEST["countryid"]) && (int) $_REQUEST["countryid"] > 0) {

    $act = $_REQUEST["act"];
    if ($act == "upd") {


        $act_name = 'Edit';

        $countryid = $_REQUEST["countryid"];
        $sSQL = "select * from " . $wpdb->prefix . "countries where countryid=$countryid";
        $result = mysql_query($sSQL) or die('Error, query failed');
        if (mysql_num_rows($result) > 0) {
            if ($row = mysql_fetch_assoc($result)) {

                $countryid = $row['countryid'];

                $country = $row['country'];

                $fips104 = $row['fips104'];

                $iso2 = $row['iso2'];
                $iso3 = $row['iso3'];


                $ison = $row['ison'];
                $internet = $row['internet'];
                $capital = $row['capital'];
                $mapreference = $row['mapreference'];
                $nationalitysingular = $row['nationalitysingular'];
                $nationalityplural = $row['nationalityplural'];
                $currency = $row['currency'];
                $currencycode = $row['currencycode'];
                $population = $row['population'];

                $btn = "Update";
                $hidval = 2;
            }
        } else {
            header("Location:admin.php?page=iweb-country/iweb-country.php&info=notfound");
            exit;
        }
    }
}


/* for state city
  $res_activities = get_all_activities();

  if($_POST){
  if(isset($_POST['is_active'])){
  $is_active= (int) $_POST['is_active'];
  }
  if(isset($_POST['ad_type'])){
  $ad_type= (int) $_POST['ad_type'];
  }
  } */
?>
<div xmlns="http://www.w3.org/1999/xhtml" class="wrap nosubsub">

    <div class="icon32" id="icon-edit"><br/></div>
    <h2>Country</h2>
    <div id="col-left">
        <div class="col-wrap">
            <div>
                <div class="form-wrap">
                    <h3><?php echo $act_name; ?> Country </h3>
                    <form class="validate" action="" method="post" id="addtag">



<?php if (trim($error_message) != '') { ?>
                            <style>.error_msg, .error_msg p { color:red; }</style>
                            <div class="error_msg"><?php echo $error_message; ?></div>

<?php } ?>


                        <div class="form-field">
                            <label for="country">Country</label>
                            <input type="text"  value="<?php echo $country; ?>" id="country" name="country"/>
                        </div>



                        <div class="form-field">
                            <label for="tag-name">iso2</label>

                            <input type="text"  id="iso2" name="iso2" value="<?php echo $iso2; ?>"/>
                        </div>
                        <div class="form-field">
                            <label for="tag-slug">iso3</label>
                            <input type="text"  value="<?php echo $iso3; ?>" id="iso3" name="iso3"/>
                        </div>



                        <?php /*  <div class="form-field">

                          <label for="parent">Activity</label>
                          <select class="postform" id="activity_id" name="activity_id" disabled="disabled">



                          <?php
                          if (!empty($res_activities)) {
                          foreach ($res_activities as $row_acti) {

                          ?>

                          <option value="<?php echo $row_acti->term_id; ?>"  <?php if ($row_acti->term_id == $activity_id) { ?> selected <?php } ?>><?php echo ucwords($row_acti->name); ?></option>
                          <?php  }
                          } ?>




                          </select>
                          </div> <?php */ ?>










                        <p class="submit">
                            <input type="submit" value="<?php echo $btn; ?>" class="button" id="submit" name="submit"/>
                            <input type="hidden" name="iws_adminiwebcountry_editadd_nonce" value="<?php echo wp_create_nonce('iws-adminiwebcountry-editadd-nonce'); ?>"/>
                            <input type="hidden" name="addme" value="<?php echo $hidval; ?>" />
                            <input type="hidden" name="countryid" value="<?php echo $countryid; ?>" />
                            <input type="hidden" name="act" value="<?php echo $act; ?>" />
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

