<?php

	global $wpdb;

	$table_name = $wpdb->prefix . "cities";

        if(isset($_REQUEST["info"])){
            $info=$_REQUEST["info"];

            if($info=="saved")
            {
                    echo "<div class='updated' id='message'><p><strong>Record Added</strong>.</p></div>";
            }

            if($info=="upd")
            {
                    echo "<div class='updated' id='message'><p><strong>Record Updated</strong>.</p></div>";
            }
            
            if($info=="notfound")
            {
                    echo "<div class='error' id='message'><p><strong>Could not found requested record</strong>.</p></div>";
            }

            if($info=="del")
            {
                    $delid=$_GET["did"];
                    $wpdb->query("delete from ".$table_name." where CityId=".$delid);
                    echo "<div class='updated' id='message'><p><strong>Record Deleted.</strong>.</p></div>";
            }
        }
?>

<script type="text/javascript">
	/* <![CDATA[ 
	jQuery(document).ready(function(){
		jQuery('#useradslist').dataTable({
        "order": [[ 0, "desc" ]]
    });
	});
	/* ]]> */

</script>

<style>
    .pager {
        padding-left: 0;
        margin: 20px 0;
        text-align: center;
        list-style: none
    }
    .pager li {
        display: inline
    }
    .pager li>a,
    .pager li>span {
        display: inline-block;
        padding: 5px 14px;
        background-color: #fff;
        border: 1px solid #ddd;
        border-radius: 15px
    }
    .pager li>a:hover,
    .pager li>a:focus {
        text-decoration: none;
        background-color: #eee
    }
    .pager .next>a,
    .pager .next>span {
        float: right
    }
    .pager .previous>a,
    .pager .previous>span {
        float: left
    }
    .pager .disabled>a,
    .pager .disabled>a:hover,
    .pager .disabled>a:focus,
    .pager .disabled>span {
        color: #777;
        cursor: not-allowed;
        background-color: #fff
    }
</style>



<div class="wrap">
   <h2>List of Records <a class="button add-new-h2" href="admin.php?page=iweb_city_add&act=add">Add New</a></h2>
	 <table class="wp-list-table widefat fixed " id="">
		<thead>
			<tr>
				<th><u>ID</u></th>
                                <th><u>Name</u></th>
                                <th><u>State</u></th>
                                <th><u>Country</u></th>
         
				
				
				
         <th><u>Action</u></th>
                      
			</tr>
		</thead>
		<tbody>
<?php
		$sql = "select ct.*, st.region, cnt.country  from ".$table_name." as ct left join ".$wpdb->prefix . "states as st on ct.RegionID=st.regionid left join ".$wpdb->prefix . "countries as cnt on st.countryid=cnt.countryid order by ct.City asc ";
                
                
                ///===my pagi
                
                $total_pages = mysql_num_rows(mysql_query($sql));
	
	
	
        
	
        $stages = 3;
        $limit = 15;
        $query_param_name='iwebcta';
	
        
        $current_url=current_page_url();
        
        $url_params = parse_url($current_url);
        
        
        if(!empty($url_params)){
           
            if(isset($url_params['query']) && $url_params['query']!=''){
                parse_str($url_params['query'],$query_params);
                
                if(isset($query_params[$query_param_name])){
                   unset($query_params[$query_param_name]);
               }
               
               if(isset($query_params['msg'])){
                   unset($query_params['msg']);
               }
               
               $url_params['query']=http_build_query($query_params);
            }
            
           
        }
        
        
        $currentUrl=$url_params['scheme']."://".$url_params['host'].$url_params['path'];
        $original_url=original_page_url();
        
        $separate_param='?';
        if(isset($url_params['query']) && trim($url_params['query'])!=''){
            
                $currentUrl.='?'.$url_params['query'];
                $separate_param='&';
            
        }
        
        
        $targetpage = $currentUrl;
        
        
	$page='';
        
        
        
        
        
        
	if(isset($_GET[$query_param_name]))
	{
		$page =  (int) $_GET[$query_param_name];
	}
	
	
	if($page){
		$start = ($page - 1) * $limit; 
	} else{
		$start = 0;	
		}	
	
    // Get page data
	$query1 = $sql." limit ".$limit." offset ".$start;
        
       
	$result = mysql_query($query1);
	
	// Initial page num setup
	if ($page == 0){$page = 1;}
	$prev = $page - 1;	
	$next = $page + 1;							
	$lastpage = ceil($total_pages/$limit);		
	$LastPagem1 = $lastpage - 1;					
	
	
	$paginate = '<ul class="pager">';
	if($lastpage > 1)
	{	
	

		// Previous
		if ($page > 1){
			$paginate.= "<li class='first-child'><a href='".$targetpage.$separate_param.$query_param_name."=".$prev."'>previous</a></li>";
		}else{
			$paginate.= "<li class='first-child'><a>previous</a></li>";	}
			

		
		// Pages	
		if ($lastpage < 7 + ($stages * 2))	// Not enough pages to breaking it up
		{	
			for ($counter = 1; $counter <= $lastpage; $counter++)
			{
				if ($counter == $page){
					$paginate.= "<li class='active'><a>$counter</a></li>";
				}else{
					$paginate.= "<li><a href='".$targetpage.$separate_param.$query_param_name."=".$counter."'>$counter</a></li>";}					
			}
		}
		elseif($lastpage > 5 + ($stages * 2))	// Enough pages to hide a few?
		{
			// Beginning only hide later pages
			if($page < 1 + ($stages * 2))		
			{
				for ($counter = 1; $counter < 4 + ($stages * 2); $counter++)
				{
					if ($counter == $page){
						$paginate.= "<li class='active'><a>$counter</a></li>";
					}else{
						$paginate.= "<li><a href='".$targetpage.$separate_param.$query_param_name."=".$counter."'>$counter</a></li>";}					
				}
				$paginate.= "...";
				$paginate.= "<a href='".$targetpage.$separate_param.$query_param_name."=".$LastPagem1."'>$LastPagem1</a>";
				$paginate.= "<a href='".$targetpage.$separate_param.$query_param_name."=".$lastpage."'>$lastpage</a>";		
			}
			// Middle hide some front and some back
			elseif($lastpage - ($stages * 2) > $page && $page > ($stages * 2))
			{
				$paginate.= "<li><a href='".$targetpage.$separate_param.$query_param_name."=1'>1</a></li>";
				$paginate.= "<li><a href='".$targetpage.$separate_param.$query_param_name."=2'>2</a></li>";
				$paginate.= "...";
				for ($counter = $page - $stages; $counter <= $page + $stages; $counter++)
				{
					if ($counter == $page){
						$paginate.= "<li class='active'><a>$counter</a></li>";
					}else{
						$paginate.= "<li><a href='".$targetpage.$separate_param.$query_param_name."=".$counter."'>$counter</a></li>";}					
				}
				$paginate.= "...";
				$paginate.= "<li><a href='".$targetpage.$separate_param.$query_param_name."=".$LastPagem1."'>$LastPagem1</a></li>";
				$paginate.= "<li><a href='".$targetpage.$separate_param.$query_param_name."=".$lastpage."'>$lastpage</a></li>";		
			}
			// End only hide early pages
			else
			{
				$paginate.= "<li><a href='".$targetpage.$separate_param.$query_param_name."=1'>1</a></li>";
				$paginate.= "<li><a href='".$targetpage.$separate_param.$query_param_name."=2'>2</a></li>";
				$paginate.= "...";
				for ($counter = $lastpage - (2 + ($stages * 2)); $counter <= $lastpage; $counter++)
				{
					if ($counter == $page){
						$paginate.= "<li class='active'><a>$counter</a></li>";
					}else{
						$paginate.= "<li><a href='".$targetpage.$separate_param.$query_param_name."=".$counter."'>$counter</a></li>";}					
				}
			}
		}
					
				// Next
		if ($page < $counter - 1){ 
			$paginate.= "<li class='last-child'><a href='".$targetpage.$separate_param.$query_param_name."=".$next."'>next</a></li>";
		}else{
			$paginate.= "<li><a>next</a></li>";
			}
			
	
	
	
}
$paginate.= "</ul>";
 

                //==end my pagi
                
                
		//$result = mysql_query($sql) or die ('Error, query failed');

		if (mysql_num_rows($result) > 0 )
		{

                        while ($row = mysql_fetch_assoc($result))
			{
				$id = $row['CityId'];
				
                                $City=$row['City'];
                                
                                $country=$row['country'];
                                $region=$row['region'];
                              
                                
                                
	?>
			<tr>
				<td><?php echo $id; ?></td>
				<td><?php echo $City; ?></td>
                                <td><?php echo $region; ?></td>
                                <td><?php echo $country; ?></td>
                                
				
                                <td><a href="admin.php?page=iweb_city_add&act=upd&CityId=<?php echo $id;?>">Edit</a> | <a href="admin.php?page=iweb-city/iweb-city.php&info=del&did=<?php echo $id; ?>">delete</a></td>
			</tr>
<?php }
	} else { ?>
			<tr>
                            <td colspan="5">No Record Found!</td>
			<tr>
	<?php } ?>
	</tbody>
	</table>
   <div style="float: right;">
       <?php echo $paginate; ?>
   </div>
   <div style="clear: both;"></div>
</div>