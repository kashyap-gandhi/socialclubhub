<?php
require_once("iwebcityclass.php");
$objMem = new iwebcityClass();

global $wpdb, $post;

$addme = '';

$error_message = '';


$act_name = 'Add';
$btn = "Add New";

$Longitude='';
$Latitude='';
$City = '';
$RegionID = '';
$CountryID = '';
$CityId='';




$act = '';

$ad_type = 1;

 $hidval = 1;


if ($_POST && isset($_POST['iws_adminiwebcity_editadd_nonce']) && wp_verify_nonce($_POST['iws_adminiwebcity_editadd_nonce'], 'iws-adminiwebcity-editadd-nonce') && isset($_POST["addme"]) && (int) $_POST["addme"] > 0) {



    $addme = $_POST["addme"];

    


    if ($addme == 1) {

        $objMem->addNewIwebCity($table_name = $wpdb->prefix . "cities", $_POST);


        header("Location:admin.php?page=iweb-city/iweb-city.php&info=saved");
        exit;
        
    } else if ($addme == 2) {



if(isset($_REQUEST["CityId"]) && (int) $_REQUEST["CityId"] > 0) {
    $CityId = $_REQUEST["CityId"];
        $sSQL = "select * from " . $wpdb->prefix . "cities where CityId=$CityId";
        $result = mysql_query($sSQL) or die('Error, query failed');
        if (mysql_num_rows($result) > 0) {
            if ($row = mysql_fetch_assoc($result)) {
                
                
                
                
                
                $Longitude=$_POST['Longitude'];
                $Latitude=$_POST['Latitude'];
                $City = trim($_POST['City']);
                $RegionID = $_POST['RegionID'];
                $CountryID = $_POST['CountryID'];
                
                
             




                if ($City == '') {
                    $error_message.='<p>Please enter city name.</p>';
                }



                if (isset($_POST["CountryID"]) && trim($_POST["CountryID"]) == '') {
                    $error_message.='<p>Please select country.</p>';
                }
                
                if (isset($_POST["RegionID"]) && trim($_POST["RegionID"]) == '') {
                    $error_message.='<p>Please select state.</p>';
                }


                if (isset($_POST["Longitude"]) && trim($_POST["Longitude"]) == '') {
                    $error_message.='<p>Please enter Longitude.</p>';
                }
                
                if (isset($_POST["Latitude"]) && trim($_POST["Latitude"]) == '') {
                    $error_message.='<p>Please enter Latitude.</p>';
                }






                if (trim($error_message) == '') {



                    $objMem->updIwebCity($table_name = $wpdb->prefix . "cities", $_POST);


                    header("Location:admin.php?page=iweb-city/iweb-city.php&info=upd");
                    exit;
                }
            }
        } else { 
            header("Location:admin.php?page=iweb-city/iweb-city.php&info=notfound");
            exit;
        }
        } else { 
            header("Location:admin.php?page=iweb-city/iweb-city.php&info=notfound");
            exit;
        }
    }
}



if (isset($_REQUEST["act"]) && $_REQUEST["act"] == 'upd' && isset($_REQUEST["CityId"]) && (int) $_REQUEST["CityId"] > 0) {

    $act = $_REQUEST["act"];
    if ($act == "upd") {


        $act_name = 'Edit';

        $CityId = $_REQUEST["CityId"];
        $sSQL = "select * from " . $wpdb->prefix . "cities where CityId=$CityId";
        $result = mysql_query($sSQL) or die('Error, query failed');
        if (mysql_num_rows($result) > 0) {
            if ($row = mysql_fetch_assoc($result)) {
                
                
                $Longitude=$row['Longitude'];
                $Latitude=$row['Latitude'];
                $City = $row['City'];
                $RegionID = $row['RegionID'];
                $CountryID = $row['CountryID'];
               
               

                $btn = "Update";
                $hidval = 2;
            }
        } else {
            header("Location:admin.php?page=iweb-city/iweb-city.php&info=notfound");
            exit;
        }
    }
}



  $res_country = get_all_country();
  
    $res_states = array();
    if ($CountryID > 0) {
        
        $res_states = get_states_from_country_by_id($CountryID);
    }

 
?>
<div xmlns="http://www.w3.org/1999/xhtml" class="wrap nosubsub">

    <div class="icon32" id="icon-edit"><br/></div>
    <h2>City</h2>
    <div id="col-left">
        <div class="col-wrap">
            <div>
                <div class="form-wrap">
                    <h3><?php echo $act_name; ?> City </h3>
                    <form class="validate" action="" method="post" id="addtag">



<?php if (trim($error_message) != '') { ?>
                            <style>.error_msg, .error_msg p { color:red; }</style>
                            <div class="error_msg"><?php echo $error_message; ?></div>

<?php } ?>
                            
                            
                            
                            
                            
                         <div class="form-field">

                          <label for="parent">Country</label>
                          <select class="postform" id="CountryID" name="CountryID" >



                          <?php
                          if (!empty($res_country)) {
                          foreach ($res_country as $row_country) {

                          ?>

                          <option value="<?php echo $row_country->countryid; ?>"  <?php if ($row_country->countryid == $CountryID) { ?> selected <?php } ?>><?php echo ucwords($row_country->country); ?></option>
                          <?php  }
                          } ?>




                          </select>
                          </div> 



 <div class="form-field">

                          <label for="parent">State</label>
                            
                             <select class="postform" name="RegionID" id="RegionID">
            <option value="0">---Select State---</option>

    <?php
    if (!empty($res_states)) {
        foreach ($res_states as $row_states) {
            ?>

                    <option value="<?php echo $row_states->regionid; ?>" <?php if ($row_states->regionid == $RegionID) { ?> selected <?php } ?>><?php echo ucfirst($row_states->region); ?></option>


                <?php
                }
            }
            ?>

        </select>
                 </div>             

                        <div class="form-field">
                            <label for="country">City</label>
                            <input type="text"  value="<?php echo $City; ?>" id="City" name="City"/>
                        </div>



                        <div class="form-field">
                            <label for="tag-name">Latitude</label>

                            <input type="text"  id="Latitude" name="Latitude" value="<?php echo $Latitude; ?>"/>
                        </div>
                            

<div class="form-field">
                            <label for="tag-name">Longitude</label>

                            <input type="text"  id="Longitude" name="Longitude" value="<?php echo $Longitude; ?>"/>
                        </div>



                        <p class="submit">
                            <input type="submit" value="<?php echo $btn; ?>" class="button" id="submit" name="submit"/>
                            <input type="hidden" name="iws_adminiwebcity_editadd_nonce" value="<?php echo wp_create_nonce('iws-adminiwebcity-editadd-nonce'); ?>"/>
                            <input type="hidden" name="addme" value="<?php echo $hidval; ?>" />
                            <input type="hidden" name="CityId" value="<?php echo $CityId; ?>" />
                            <input type="hidden" name="act" value="<?php echo $act; ?>" />
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
jQuery('#CountryID').change(function(e) {
		var data = {
			'action': 'get_states_of_country',
			'country': jQuery(this).val()
		};
		
		var state_ops = '<option value="">---Select State---</option>';
		
		
		jQuery("#RegionID").html(state_ops);
		

		// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
		jQuery.post(ajaxurl, data, function(response) {
			if(jQuery("#RegionID").length>0)
			{
				jQuery("#RegionID").html(response);
			}
		});
    });
    </script>
