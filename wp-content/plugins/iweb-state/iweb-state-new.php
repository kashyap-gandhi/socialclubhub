<?php
require_once("iwebstateclass.php");
$objMem = new iwebstateClass();

global $wpdb, $post;

$addme = '';

$error_message = '';


$act_name = 'Add';
$btn = "Add New";

$regionid='';
$region='';
$countryid = '';
$code = '';
$adm1code = '';




$act = '';

$ad_type = 1;

 $hidval = 1;


if ($_POST && isset($_POST['iws_adminiwebstate_editadd_nonce']) && wp_verify_nonce($_POST['iws_adminiwebstate_editadd_nonce'], 'iws-adminiwebstate-editadd-nonce') && isset($_POST["addme"]) && (int) $_POST["addme"] > 0) {



    $addme = $_POST["addme"];

    


    if ($addme == 1) {

        $objMem->addNewIwebState($table_name = $wpdb->prefix . "states", $_POST);


        header("Location:admin.php?page=iweb-state/iweb-state.php&info=saved");
        exit;
        
    } else if ($addme == 2) {



if(isset($_REQUEST["regionid"]) && (int) $_REQUEST["regionid"] > 0) {
    $regionid = $_REQUEST["regionid"];
        $sSQL = "select * from " . $wpdb->prefix . "states where regionid=$regionid";
        $result = mysql_query($sSQL) or die('Error, query failed');
        if (mysql_num_rows($result) > 0) {
            if ($row = mysql_fetch_assoc($result)) {
                
                $region = trim($_POST['region']);


                $countryid = trim($_POST['countryid']);
                $code = trim($_POST['code']);





                if ($region == '') {
                    $error_message.='<p>Please enter region name.</p>';
                }



                if (isset($_POST["countryid"]) && trim($_POST["countryid"]) == '') {
                    $error_message.='<p>Please select country.</p>';
                }


                if (isset($_POST["code"]) && trim($_POST["code"]) == '') {
                    $error_message.='<p>Please enter code.</p>';
                }






                if (trim($error_message) == '') {



                    $objMem->updIwebState($table_name = $wpdb->prefix . "states", $_POST);


                    header("Location:admin.php?page=iweb-state/iweb-state.php&info=upd");
                    exit;
                }
            }
        } else { 
            header("Location:admin.php?page=iweb-state/iweb-state.php&info=notfound");
            exit;
        }
        } else { 
            header("Location:admin.php?page=iweb-state/iweb-state.php&info=notfound");
            exit;
        }
    }
}



if (isset($_REQUEST["act"]) && $_REQUEST["act"] == 'upd' && isset($_REQUEST["regionid"]) && (int) $_REQUEST["regionid"] > 0) {

    $act = $_REQUEST["act"];
    if ($act == "upd") {


        $act_name = 'Edit';

        $regionid = $_REQUEST["regionid"];
        $sSQL = "select * from " . $wpdb->prefix . "states where regionid=$regionid";
        $result = mysql_query($sSQL) or die('Error, query failed');
        if (mysql_num_rows($result) > 0) {
            if ($row = mysql_fetch_assoc($result)) {

                $regionid = $row['regionid'];

                $countryid = $row['countryid'];

                $region = $row['region'];

                $code = $row['code'];
               

                $btn = "Update";
                $hidval = 2;
            }
        } else {
            header("Location:admin.php?page=iweb-state/iweb-state.php&info=notfound");
            exit;
        }
    }
}



  $res_country = get_all_country();

  if($_POST){
  if(isset($_POST['is_active'])){
  $is_active= (int) $_POST['is_active'];
  }
  if(isset($_POST['ad_type'])){
  $ad_type= (int) $_POST['ad_type'];
  }
  } 
?>
<div xmlns="http://www.w3.org/1999/xhtml" class="wrap nosubsub">

    <div class="icon32" id="icon-edit"><br/></div>
    <h2>State</h2>
    <div id="col-left">
        <div class="col-wrap">
            <div>
                <div class="form-wrap">
                    <h3><?php echo $act_name; ?> State </h3>
                    <form class="validate" action="" method="post" id="addtag">



<?php if (trim($error_message) != '') { ?>
                            <style>.error_msg, .error_msg p { color:red; }</style>
                            <div class="error_msg"><?php echo $error_message; ?></div>

<?php } ?>

	
	
	
	 <div class="form-field">

                          <label for="parent">Country</label>
                          <select class="postform" id="countryid" name="countryid" >



                          <?php
                          if (!empty($res_country)) {
                          foreach ($res_country as $row_country) {

                          ?>

                          <option value="<?php echo $row_country->countryid; ?>"  <?php if ($row_country->countryid == $countryid) { ?> selected <?php } ?>><?php echo ucwords($row_country->country); ?></option>
                          <?php  }
                          } ?>




                          </select>
                          </div> 
						  
						  
                        <div class="form-field">
                            <label for="country">Region</label>
                            <input type="text"  value="<?php echo $region; ?>" id="region" name="region"/>
                        </div>



                        <div class="form-field">
                            <label for="tag-name">Code</label>

                            <input type="text"  id="code" name="code" value="<?php echo $code; ?>"/>
                        </div>
                            
                            
                        









                        <p class="submit">
                            <input type="submit" value="<?php echo $btn; ?>" class="button" id="submit" name="submit"/>
                            <input type="hidden" name="iws_adminiwebstate_editadd_nonce" value="<?php echo wp_create_nonce('iws-adminiwebstate-editadd-nonce'); ?>"/>
                            <input type="hidden" name="addme" value="<?php echo $hidval; ?>" />
                            <input type="hidden" name="regionid" value="<?php echo $regionid; ?>" />
                            <input type="hidden" name="act" value="<?php echo $act; ?>" />
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

