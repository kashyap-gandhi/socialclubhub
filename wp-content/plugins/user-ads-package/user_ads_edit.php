<?php
ob_start();

global $wpdb, $post;

$edit_add_text='Add';


$obj_id=0;
$ads_package_id='';
$package_name='';
$package_ads_days='';
$package_ads_price='';
$start_date='';
$end_date='';
$cart_order_id='';
$order_number='';
$order_amount='';
$order_txn_id='';
$order_ip='';
$order_email='';
$order_first_name='';
$order_last_name='';
$order_date='';

$activity_id='';
$activity_name = '';

$user_name='';
$user_email='';
$user_id='';
$user_first_name='';
$user_last_name='';

$ads_target_link='';
$ads_image_id='';
$ads_content='';
$ads_title='';

$order_status='N/A';
$ad_type=1;


if(isset($_GET['id']) && (int) $_GET['id']>0){
                
    $obj_id = (int) $_GET['id'];
    $edit_add_text='Edit';
    
   
        $sSQL = "select * from " .$wpdb->prefix . "user_ads where id=$obj_id";
        $result = mysql_query($sSQL) or die('Error, query failed');
        if (mysql_num_rows($result) > 0) {
            if ($row = mysql_fetch_assoc($result)) {
               

                $ads_package_id = $row['ads_package_id'];

                $get_ad_post = get_post($ads_package_id);
                $get_ad_post_meta = get_post_meta($ads_package_id);

                $package_name = $get_ad_post->post_title;


                //$package_ads_days=(int) get_post_meta($ads_package_id,'ads_days',true);
                //$package_ads_price=(double) get_post_meta($ads_package_id,'ads_price',true);


                $package_ads_days = (int) $row['ads_days'];
                $package_ads_price = (double) $row['ads_price'];



                $start_date = date('d M,Y H:i:s',strtotime($row['ads_start_date']));
                $end_date = date('d M,Y H:i:s',strtotime($row['ads_end_date']));

                $is_active = $row['is_active'];
                $ad_type = (int) $row['ad_type'];


                $cart_order_id = $row['cart_order_id'];

                $get_cart_order_post = get_post($cart_order_id);
                $get_cart_order_post_meta = get_post_meta($cart_order_id);

                $order_number = $get_cart_order_post->post_title;
                $order_amount = get_post_meta($cart_order_id, 'wpsc_total_amount', true);
                
                
                $order_txn_id = get_post_meta($cart_order_id, 'wpsc_txn_id', true);
                
                
                $order_ip = get_post_meta($cart_order_id, 'wpsc_ipaddress', true);
                $order_email = get_post_meta($cart_order_id, 'wpsc_email_address', true);
                $order_first_name = get_post_meta($cart_order_id, 'wpsc_first_name', true);
                $order_last_name = get_post_meta($cart_order_id, 'wpsc_last_name', true);
                $order_date = date('d M,Y H:i:s',strtotime($get_cart_order_post->post_date));
                $order_status=get_post_meta($cart_order_id,'wpsc_order_status',true);
                
                $activity_id=$row['activity_id'];
                
                $activity_detail = get_activity_by_id($row['activity_id']);

                

                if (isset($activity_detail->name) && $activity_detail->name != '') {
                    $activity_name = $activity_detail->name;
                }
                
                
                $get_user = get_user_by( 'id', $row['user_id'] );
                
                if(!empty($get_user)){
                    $user_email=$get_user->user_email;
                    $user_first_name=$get_user->first_name;
                    $user_last_name=$get_user->last_name;
                    $user_name=$get_user->user_login;
                }
                
                $ads_target_link=$row['ads_target_link'];
                $ads_image_id=$row['_thumbnail_id'];
                $ads_content=$row['ads_content'];
                $ads_title=$row['ads_title'];

               
            }
        }
   
    

}

    
$res_activities = get_all_activities();
if($_POST){
    
        if(isset($_POST['ad_type'])){
            $ad_type= (int) $_POST['ad_type'];
        }
}

?>	



    <div class="col-sm-12 signUpBlock">
    
    
    <h3><?php echo $edit_add_text;?> Ad</h3>

    <?php
// show any error messages after form submission
    echo iws_show_error_messages();
    ?>
    <form id="iws_registration_form" class="form-horizontal" action="" method="POST">

        
        <input type="hidden" name="post_id" id="post_id" value="<?php echo $obj_id; ?>" />
         <h3>Package Details</h3>

        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Package Name'); ?>:</label>
            <div class="col-sm-8">
                <input name="" id="" value="<?php echo $package_name; ?>" class="form-control" type="text" readonly=""/>
            </div>
        </div>
        
        
        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Package Days'); ?>:</label>
            <div class="col-sm-8">
                <input name="" id="" value="<?php echo $package_ads_days; ?>" class="form-control" type="text" readonly=""/>
            </div>
        </div>
        
        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Package Price'); ?>($):</label>
            <div class="col-sm-8">
                <input name="" id="" value="<?php echo $package_ads_price; ?>" class="form-control" type="text" readonly=""/>
            </div>
        </div>
         
          <h3>Transaction Details</h3>
        
        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Transaction ID'); ?>:</label>
            <div class="col-sm-8">
                <input name="" id="" value="<?php echo $order_txn_id; ?>" class="form-control" type="text" readonly=""/>
            </div>
        </div>
          
          <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Payee First Name'); ?>:</label>
            <div class="col-sm-8">
                <input name="" id="" value="<?php echo $order_first_name; ?>" class="form-control" type="text" readonly=""/>
            </div>
        </div>
        
        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Payee Last Name'); ?>:</label>
            <div class="col-sm-8">
                <input name="" id="" value="<?php echo $order_last_name; ?>" class="form-control" type="text" readonly=""/>
            </div>
        </div>
          
          <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Payee Email'); ?>:</label>
            <div class="col-sm-8">
                <input name="" id="" value="<?php echo $order_email; ?>" class="form-control" type="text" readonly=""/>
            </div>
        </div>
          
          
           <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Order Status'); ?>:</label>
            <div class="col-sm-8">
                <input name="" id="" value="<?php echo $order_status; ?>" class="form-control" type="text" readonly=""/>
            </div>
        </div>
          
          
          
          <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Date'); ?>:</label>
            <div class="col-sm-8">
                <input name="" id="" value="<?php echo $order_date; ?>" class="form-control" type="text" readonly=""/>
            </div>
        </div>
          
          
          
          


        <h3>Activity</h3>

        <div class="form-group">
            <label for="" class="col-sm-4 control-label">Ad Activity</label>
     <div class="col-sm-8">
   

         <select name="activity_id" id="activity_id" class="form-control" disabled="disabled">
                             
    <?php
    if (!empty($res_activities)) {
        foreach ($res_activities as $row_acti) {
            ?>
                    <option value="<?php echo $row_acti->term_id; ?>" <?php if ($row_acti->term_id == $activity_id) { ?> selected <?php } ?>><?php echo ucwords($row_acti->name); ?></option>
        <?php }
    } ?>

        </select>
   </div>
 </div>
          
          
       <h3>Ad Details</h3>   
        
       
        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Start Date'); ?>:</label>
            <div class="col-sm-8">
                <input name="" id="" value="<?php echo $start_date; ?>" class="form-control" type="text" readonly=""/>
            </div>
        </div>
          
       
        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('End Date'); ?>:</label>
            <div class="col-sm-8">
                <input name="" id="" value="<?php echo $end_date; ?>" class="form-control" type="text" readonly=""/>
            </div>
        </div>
       
       
        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Ad Type'); ?>:</label>
            <div class="col-sm-8">
              <select class="form-control" id="ad_type" name="ad_type">
                                <option value="1" <?php if($ad_type=='' || $ad_type==0 || $ad_type==1){ ?> selected <?php } ?>>Text & Image</option>
                                <option value="2" <?php if($ad_type==2){ ?> selected <?php } ?>>Text</option>
                                <option value="3" <?php if($ad_type==3){ ?> selected <?php } ?>>Image</option>
                            </select>
            </div>
        </div>
          

        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Ad Title'); ?>:</label>
            <div class="col-sm-8">
                <input name="ads_title" id="ads_title" value="<?php
                       if (isset($_POST['ads_title'])) {
                           echo $_POST['ads_title'];
                       } else { echo $ads_title; } 
    ?>" class="form-control" type="text"/>
            </div>
        </div>


        
        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('About the Ad'); ?><br/><small>Please tell us about your ad so we can showcase your ad to our viewers.</small></label>
            <div class="col-sm-8">
                <textarea name="ads_content" id="ads_content" class="form-control" rows="5"><?php
                       if (isset($_POST['ads_content'])) {
                           echo $_POST['ads_content'];
                       } else { echo $ads_content; }
    ?></textarea>
            </div>
        </div>


        <div class="form-group">
            <label for="" class="col-sm-4 control-label"><?php _e('Ad Target Link'); ?>:</label>
            <div class="col-sm-8">
                <input name="ads_target_link" id="ads_target_link" value="<?php
                       if (isset($_POST['ads_target_link'])) {
                           echo $_POST['ads_target_link'];
                       } else { echo $ads_target_link; } 
    ?>" class="form-control" type="text"/>
            </div>
        </div>

        


        

        <h3>Upload picture of Ad</h3>                       

        <?php
        $allowed_ext = "jpg,gif,png";
        $post_id = $obj_id;
        $attr['name'] = '_thumbnail_id';
        $attr['max_size'] = "10000";
        $attr['count'] = 1;

        $type = 'adpost';

     
        
        $has_featured_image = false;
        $has_images = false;
        $has_avatar = false;
        

        if ($post_id) {

                   
            if (isset($_POST['iws_files']['_thumbnail_id'])) {
                 $thumb_id = $_POST['iws_files']['_thumbnail_id'][0]; 
                if ($thumb_id) {
                    $has_featured_image = true;
                    $featured_image = attach_html($thumb_id,$type); 
                    echo '<input type="hidden" name="iws_files[_thumbnail_id][]" value="'.$thumb_id.'">';
                }
            } else {

                    // it's a featured image then
                    $thumb_id = $ads_image_id;

                    if ($thumb_id) {
                        $has_featured_image = true;
                        $featured_image = attach_html($thumb_id,$type); 
                    }
            }
        } else {
            if (isset($_POST['iws_files']['_thumbnail_id'])) {
                echo $thumb_id = $_POST['iws_files']['_thumbnail_id'][0]; die;
                if ($thumb_id) {
                    $has_featured_image = true;
                    $featured_image = attach_html($thumb_id,$type); 
                    echo '<input type="hidden" name="iws_files[_thumbnail_id][]" value="'.$thumb_id.'">';
                }
            }
        }
        ?>


        <div class="iws-fields">
            <div id="iws-<?php echo $attr['name']; ?>-upload-container">
                <div class="iws-attachment-upload-filelist" data-type="file" >
                    <a id="iws-<?php echo $attr['name']; ?>-pickfiles" class="btn btn-danger btn-small button file-selector <?php echo ' iws_' . $attr['name']; ?>" href="#">Upload Image</a>

                    <ul class="iws-attachment-list thumbnails" >
    <?php
    if ($has_featured_image) {
        echo $featured_image;
    }

   
    
    ?>
                    </ul>
                </div>
            </div><!-- .container -->



        </div> <!-- .iws-fields -->

        
        <style>
            .thumbnail {
                border: none !important;
            }
        </style>
            
        
        
        <script type="text/javascript">
            jQuery(function($) {
                new IWS_Uploader('iws-<?php echo $attr['name']; ?>-pickfiles', 'iws-<?php echo $attr['name']; ?>-upload-container', <?php echo $attr['count']; ?>, '<?php echo $attr['name']; ?>', '<?php echo $allowed_ext; ?>', <?php echo $attr['max_size'] ?>);
                    
                    
                    
                jQuery(".iws-delete-file").on("click",function(e){
                       
                       
                    e.preventDefault();

                    if ( confirm( $(this).data('confirm') ) ) {
                        $.post('<?php echo admin_url('admin-ajax.php'); ?>',{action: 'iws_file_del'}, function() {
                            window.location.reload();
                        });
                    }
                       
                });
            });
        </script>





        <div class="form-group">

          <div class="col-sm-12 text-right">
                <input type="hidden" name="iws_userads_editadd_nonce" value="<?php echo wp_create_nonce('iws-userads-editadd-nonce'); ?>"/>

                <button id="iws_userads_editadd_submit" type="submit" class="btn btn-default sign-btn">Submit</button>
            </div>

        </div>
        
        
        <script>
            var ajaxurl='<?php echo admin_url('admin-ajax.php'); ?>';
            jQuery(document).ready(function(){
				
	jQuery('#deal_country_id').change(function(e) {
		var data = {
			'action': 'get_states_of_country',
			'country': jQuery(this).val()
		};
		
		var state_ops = '<option value="">---Select State---</option>';
		var city_ops = '<option value="">---Select City---</option>';
		
		jQuery("#deal_state_id").html(state_ops);
		jQuery("#deal_city_id").html(city_ops);

		// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
		jQuery.post(ajaxurl, data, function(response) {
			if(jQuery("#deal_state_id").length>0)
			{
				jQuery("#deal_state_id").html(response);
			}
		});
    });
	
	jQuery('#deal_state_id').change(function(e) {
		var data = {
			'action': 'get_cities_of_state',
			'state': jQuery(this).val()
			
		};
		
		
		var city_ops = '<option value="">---Select City---</option>';
		
		jQuery("#deal_city_id").html(city_ops);

		// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
		jQuery.post(ajaxurl, data, function(response) {
			if(jQuery("#deal_city_id").length>0)
			{
				jQuery("#deal_city_id").html(response);
			}
		});
    });
	
	jQuery('#deal_city_id').change(function(e) {
		var vlat=jQuery("#deal_city_id option:selected").attr('data-lat');
		var vlong=jQuery("#deal_city_id option:selected").attr('data-long');
		
		jQuery("#deal_lat").val(vlat);
		jQuery("#deal_long").val(vlong);
		
	});
	
});
        </script>


        
    </form>
</div>
<?php
echo ob_get_clean();
?>