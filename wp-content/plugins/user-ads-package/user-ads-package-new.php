<?php
require_once("useradspackageclass.php");
$objMem = new useradspackageClass();

 global $wpdb,$post;
 
 $addme='';
 
 $error_message='';
 
 

$btn = "Add New";
$id = "";
$hidval = 1;
$act_name='Add';
$ads_package_id='';
$package_name='';
$package_ads_days='';
$package_ads_price='';
$start_date='';
$end_date='';
$cart_order_id='';
$order_number='';
$order_amount='';
$order_txn_id='';
$order_ip='';
$order_email='';
$order_first_name='';
$order_last_name='';
$order_date='';

$activity_id='';
$activity_name = '';

$user_name='';
$user_email='';
$user_id='';
$user_first_name='';
$user_last_name='';

$ads_target_link='';
$ads_image_id='';
$ads_content='';
$ads_title='';

$act='';

$ad_type=1;


if($_POST && isset($_POST['iws_adminads_editadd_nonce']) && wp_verify_nonce($_POST['iws_adminads_editadd_nonce'],'iws-adminads-editadd-nonce') && isset($_POST["addme"]) && (int) $_POST["addme"] >0 && isset($_REQUEST["id"]) && (int) $_REQUEST["id"] > 0){
    
    
    
    $addme = $_POST["addme"];
    
    $id=$_REQUEST["id"];
    
    
    $sSQL = "select * from " .$wpdb->prefix . "user_ads where id=$id";
        $result = mysql_query($sSQL) or die('Error, query failed');
        if (mysql_num_rows($result) > 0) {
            if ($row = mysql_fetch_assoc($result)) {
        
   $prev_avatar = $row['_thumbnail_id'];

    if ($addme == 1) {
        //$objMem->addNewUserAds($table_name = $wpdb->prefix . "user_ads",$_POST);
        header("Location:admin.php?page=user-ads-package/user-ads-package.php&info=saved");
        exit;
    } else if ($addme == 2) {
        
        
        
        $ads_target_link=trim($_POST['ads_target_link']);
        $ads_content=trim($_POST['ads_content']);
        $ads_title=trim($_POST['ads_title']);
        $is_active= (int) $_POST['is_active'];
        
        $ad_type= (int) $_POST['ad_type'];
        
        if($ads_target_link==''){
            $error_message.='<p>Please enter a ad target link.</p>';
        }
        
        if(!valid_url($ads_target_link)){
            $error_message.='<p>Please enter a valid ad target link.</p>';
        }
        
        if($ad_type==2 || $ad_type==1){
            
            
            if(isset($_POST["ads_title"]) && trim($_POST["ads_title"])==''){
                $error_message.='<p>Please enter ad title.</p>';
            } else {

                if (strlen(trim($_POST["ads_title"]))>100) {
                    $error_message.='<p>Please enter ad title maximum 100 characters.</p>';
                }

                if (strlen(trim($_POST["ads_title"]))<3) {
                    $error_message.='<p>Please enter ad title minimum 3 characters.</p>';
                }
            }
            
            
            if(isset($_POST["ads_content"]) && trim($_POST["ads_content"])==''){
                $error_message.='<p>Please enter ad content.</p>';
            } else {

                if (strlen(trim($_POST["ads_content"]))>200) {
                    $error_message.='<p>Please enter ad content maximum 200 characters.</p>';
                }

                if (strlen(trim($_POST["ads_content"]))<5) {
                    $error_message.='<p>Please enter ad content minimum 5 characters.</p>';
                }
            }
        
        }
        
        if($ad_type==3 || $ad_type==1){
            if (isset($_POST['iws_files']['_thumbnail_id'][0]) && (int) $_POST['iws_files']['_thumbnail_id'][0]>0) {
                
            } elseif((int) $prev_avatar>0) { 
                 
            }  else {
                $error_message.='<p>Please upload aaaaaad image.</p>';
            }
        }
        
        
        
        if(trim($error_message)==''){
            
        //==thumbnail
        
            
            
            $attachment_id='';
        // set featured image if there's any
            if (isset($_POST['iws_files']['_thumbnail_id'][0]) && (int) $_POST['iws_files']['_thumbnail_id'][0]>0) {
                $attachment_id = $_POST['iws_files']['_thumbnail_id'][0];

                
                if (!empty($prev_avatar) && $prev_avatar>0) {
                    wp_delete_attachment($prev_avatar, true);
                }
                
            } else {
                $attachment_id=$prev_avatar;
            }
            
            mysql_query("update " .$wpdb->prefix . "user_ads set `ad_type`=".$ad_type.", `is_active`=".$is_active.", `_thumbnail_id`=".$attachment_id.", `ads_target_link`='".$ads_target_link."', `ads_content`='".$ads_content."', `ads_title`='".$ads_title."' where id=".$id);
            
            
    
        
        
        
        
        header("Location:admin.php?page=user-ads-package/user-ads-package.php&info=upd");
        exit;
        }
    } } }
}



if (isset($_REQUEST["act"]) && $_REQUEST["act"] == 'upd' && isset($_REQUEST["id"]) && (int) $_REQUEST["id"] > 0) {

    $act = $_REQUEST["act"];
    if ($act == "upd") {
        
        
        $act_name='Edit';
        
        $recid = $_REQUEST["id"];
        $sSQL = "select * from " .$wpdb->prefix . "user_ads where id=$recid";
        $result = mysql_query($sSQL) or die('Error, query failed');
        if (mysql_num_rows($result) > 0) {
            if ($row = mysql_fetch_assoc($result)) {
                $id = $row['ID'];

                $ads_package_id = $row['ads_package_id'];

                $get_ad_post = get_post($ads_package_id);
                $get_ad_post_meta = get_post_meta($ads_package_id);

                $package_name = $get_ad_post->post_title;


                //$package_ads_days=(int) get_post_meta($ads_package_id,'ads_days',true);
                //$package_ads_price=(double) get_post_meta($ads_package_id,'ads_price',true);


                $package_ads_days = (int) $row['ads_days'];
                $package_ads_price = (double) $row['ads_price'];



                $start_date = $row['ads_start_date'];
                $end_date = $row['ads_end_date'];

                $is_active = $row['is_active'];
                $ad_type = (int) $row['ad_type'];


                $cart_order_id = $row['cart_order_id'];

                $get_cart_order_post = get_post($cart_order_id);
                $get_cart_order_post_meta = get_post_meta($cart_order_id);

                $order_number = $get_cart_order_post->post_title;
                $order_amount = get_post_meta($cart_order_id, 'wpsc_total_amount', true);
                
                
                $order_txn_id = get_post_meta($cart_order_id, 'wpsc_txn_id', true);
                
                
                $order_ip = get_post_meta($cart_order_id, 'wpsc_ipaddress', true);
                $order_email = get_post_meta($cart_order_id, 'wpsc_email_address', true);
                $order_first_name = get_post_meta($cart_order_id, 'wpsc_first_name', true);
                $order_last_name = get_post_meta($cart_order_id, 'wpsc_last_name', true);
                $order_date = $get_cart_order_post->post_date;

                
                $activity_id=$row['activity_id'];
                
                $activity_detail = get_activity_by_id($row['activity_id']);

                

                if (isset($activity_detail->name) && $activity_detail->name != '') {
                    $activity_name = $activity_detail->name;
                }
                
                
                $get_user = get_user_by( 'id', $row['user_id'] );
                
                if(!empty($get_user)){
                    $user_email=$get_user->user_email;
                    $user_first_name=$get_user->first_name;
                    $user_last_name=$get_user->last_name;
                    $user_name=$get_user->user_login;
                }
                
                $ads_target_link=$row['ads_target_link'];
                $ads_image_id=$row['_thumbnail_id'];
                $ads_content=$row['ads_content'];
                $ads_title=$row['ads_title'];

                $btn = "Update";
                $hidval = 2;
            }
        }
    }
} 

if((int) $id=='' || (int) $id==0) {
    header("Location:admin.php?page=user-ads-package/user-ads-package.php&info=notfound");
    exit;
}

$res_activities = get_all_activities();

if($_POST){
    if(isset($_POST['is_active'])){
        $is_active= (int) $_POST['is_active'];
    }
        if(isset($_POST['ad_type'])){
            $ad_type= (int) $_POST['ad_type'];
        }
}

?>
<div xmlns="http://www.w3.org/1999/xhtml" class="wrap nosubsub">

    <div class="icon32" id="icon-edit"><br/></div>
    <h2>User Ads</h2>
    <div id="col-left">
        <div class="col-wrap">
            <div>
                <div class="form-wrap">
                    <h3><?php echo $act_name; ?> User Ads  |  <a href="post.php?post=<?php echo $order_number; ?>&action=edit" target="_blank">view order history</a></h3>
                    <form class="validate" action="" method="post" id="addtag">
                       
                        
                        
                        <?php if(trim($error_message)!=''){ ?>
                        <style>.error_msg, .error_msg p { color:red; }</style>
                        <div class="error_msg"><?php echo $error_message; ?></div>
                        
                        <?php } ?>
                        
                        
                        <div class="form-field">
                            <label for="email">User Email</label>
                            <input type="text" readonly="readonly" value="<?php echo $user_email; ?>" id="user_email" name="user_email"/>
                        </div>
                        
                        
                        
                        <div class="form-field">
                            <label for="tag-name">Package Name</label>

                            <input type="text" readonly="readonly" id="package_name" name="package_name" value="<?php echo $package_name; ?>"/>
                        </div>
                        <div class="form-field">
                            <label for="tag-slug">Package Days</label>
                            <input type="text" readonly="readonly" value="<?php echo $package_ads_days; ?>" id="ads_days" name="ads_days"/>
                        </div>
                        
                        <div class="form-field">
                            <label for="tag-slug">Package Price($)</label>
                            <input type="text" readonly="readonly" value="<?php echo $package_ads_price; ?>" id="ads_price" name="ads_price"/>
                        </div>
                        
                        
                         <div class="form-field">
                            <label for="email">Start Date</label>
                            <input type="text" readonly="readonly" value="<?php echo date('d,M Y H:i:s',strtotime($start_date)); ?>" id="ads_start_date" name="ads_start_date"/>
                        </div>
                        
                        
                        <div class="form-field">
                            <label for="email">End Date</label>
                            <input type="text" readonly="readonly" value="<?php echo date('d,M Y H:i:s',strtotime($end_date)); ?>" id="ads_end_date" name="ads_end_date"/>
                        </div>
                       
                        
                        
                        
                        
                        
                        <div class="form-field">
                            <label for="email">Status</label>
                            <select class="postform" id="is_active" name="is_active">
                                <option value="1" <?php if($is_active==1){ ?> selected <?php } ?>>Enable</option>
                                <option value="0" <?php if($is_active==0 || $is_active==''){ ?> selected <?php } ?>>Disable</option>
                            </select>
                        </div>
                       
                       
                        <div class="form-field">

                                <label for="parent">Activity</label>
                                <select class="postform" id="activity_id" name="activity_id" disabled="disabled">
                                        
                                   
                                          
        <?php
        if (!empty($res_activities)) {
            foreach ($res_activities as $row_acti) {
                
                ?>
               
                                <option value="<?php echo $row_acti->term_id; ?>"  <?php if ($row_acti->term_id == $activity_id) { ?> selected <?php } ?>><?php echo ucwords($row_acti->name); ?></option>
        <?php  }
    } ?>

                    
                                    
                                    
                                </select>
                        </div>
                        
                        
                        
                        <div class="form-field">
                            <label for="email">Ad Type</label>
                            <select class="postform" id="ad_type" name="ad_type">
                                <option value="1" <?php if($ad_type=='' || $ad_type==0 || $ad_type==1){ ?> selected <?php } ?>>Text & Image</option>
                                <option value="0" <?php if($ad_type==2){ ?> selected <?php } ?>>Text</option>
                                <option value="0" <?php if($ad_type==3){ ?> selected <?php } ?>>Image</option>
                            </select>
                        </div>
                       
                        
                        
                        
                        <div class="form-field">
                            <label for="email">Ads Title</label>
                            <input type="text" value="<?php if(isset($_POST['ads_title'])){ echo $_POST['ads_title']; } else { echo $ads_title; } ?>" id="ads_title" name="ads_title"/>
                        </div>
                        
                        <div class="form-field">
                            <label for="email">Ads Content</label>
                            <textarea row="5" cols="50" name="ads_content" id="ads_content"><?php if(isset($_POST['ads_content'])){ echo $_POST['ads_content']; } else { echo $ads_content; } ?></textarea>
                        </div>
                        <div class="form-field">
                            <label for="email">Ads Image</label>
                          
                        </div>
                        
                          <div class="form-field">
                            <label for="email">Ads Target Link</label>
                            <input type="text" value="<?php if(isset($_POST['ads_target_link'])){ echo $_POST['ads_target_link']; } else { echo $ads_target_link; } ?>" id="ads_target_link" name="ads_target_link"/>
                        </div>
                      
                        
        <?php        
                
                
        $allowed_ext = "jpg,gif,png";
        $post_id = $id;
        $attr['name'] = '_thumbnail_id';
        $attr['max_size'] = "10000";
        $attr['count'] = 1;

        $type = 'ads';

        



        
        $has_featured_image = false;
        $has_images = false;
        $has_avatar = false;
        

        if ($post_id) {

                   
            if (isset($_POST['iws_files']['_thumbnail_id'][0])) {
                 $thumb_id = $_POST['iws_files']['_thumbnail_id'][0]; 
                if ($thumb_id) {
                    $has_featured_image = true;
                    $featured_image = attach_html($thumb_id); 
                    echo '<input type="hidden" name="iws_files[_thumbnail_id][]" value="'.$thumb_id.'">';
                }
            } else {

                    // it's a featured image then
                    $thumb_id = $ads_image_id;

                    if ($thumb_id) {
                        $has_featured_image = true;
                        $featured_image = attach_html($thumb_id); 
                    }
            }
        } else {
            if (isset($_POST['iws_files']['_thumbnail_id'])) {
                echo $thumb_id = $_POST['iws_files']['_thumbnail_id'][0]; die;
                if ($thumb_id) {
                    $has_featured_image = true;
                    $featured_image = attach_html($thumb_id); 
                    echo '<input type="hidden" name="iws_files[_thumbnail_id][]" value="'.$thumb_id.'">';
                }
            }
        }
        ?>


        <div class="iws-fields">
            <div id="iws-<?php echo $attr['name']; ?>-upload-container">
                <div class="iws-attachment-upload-filelist" data-type="file" >
                    <a id="iws-<?php echo $attr['name']; ?>-pickfiles" class="btn btn-danger btn-small button file-selector <?php echo ' iws_' . $attr['name']; ?>" href="#">Upload Image</a>

                    <ul class="iws-attachment-list thumbnails" >
    <?php
    if ($has_featured_image) {
        echo $featured_image;
    }

    

    
    ?>
                    </ul>
                </div>
            </div><!-- .container -->



        </div> <!-- .iws-fields -->

        
        <style>
            .thumbnail {
                border: none !important;
            }
        </style>
            
        
        
        <script type="text/javascript">
            jQuery(function($) {
                new IWS_Uploader('iws-<?php echo $attr['name']; ?>-pickfiles', 'iws-<?php echo $attr['name']; ?>-upload-container', <?php echo $attr['count']; ?>, '<?php echo $attr['name']; ?>', '<?php echo $allowed_ext; ?>', <?php echo $attr['max_size'] ?>);
                    
                    
                    
                jQuery(".iws-delete-file").on("click",function(e){
                       
                       
                    e.preventDefault();

                    if ( confirm( $(this).data('confirm') ) ) {
                        $.post('<?php echo admin_url('admin-ajax.php'); ?>',{action: 'iws_file_del'}, function() {
                            window.location.reload();
                        });
                    }
                       
                });
            });
        </script>



                       
                        <p class="submit">
                            <input type="submit" value="<?php echo $btn; ?>" class="button" id="submit" name="submit"/>
                            <input type="hidden" name="iws_adminads_editadd_nonce" value="<?php echo wp_create_nonce('iws-adminads-editadd-nonce'); ?>"/>
                            <input type="hidden" name="addme" value="<?php echo $hidval; ?>" />
                            <input type="hidden" name="id" value="<?php echo $id; ?>" />
                            <input type="hidden" name="act" value="<?php echo $act; ?>" />
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

