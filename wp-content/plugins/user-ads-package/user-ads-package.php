<?php
/*
Plugin Name: User Ads Packages 
Plugin URI: 
Description: This plugin used for add edit delete and listing module at admin side.Also user can search and sort records.
Version: 1.0
Author: iWS
Author URI: 
*/


require_once("useradspackageclass.php");
require_once("user_ads_function.php");
$objMem = new useradspackageClass();

$table_name = $wpdb->prefix . "user_ads";

function adduseradspackage() {

	global $wpdb;

	$table_name = $wpdb->prefix . "user_ads";

	$MSQL = "show tables like '$table_name'";

	if($wpdb->get_var($MSQL) != $table_name)
	{

	   
           
           $sql="CREATE TABLE IF NOT EXISTS $table_name (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `ads_package_id` bigint(20) NOT NULL,
  `ads_price` double(10,2) NOT NULL,
  `ads_days` int(20) NOT NULL,
  `ads_start_date` datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
  `ads_end_date` datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
  `is_active` int(5) NOT NULL,
  `is_delete` int(5) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ";

		//require_once(ABSPATH . "wp-admin/includes/upgrade.php");
		dbDelta($sql);
	}

}
	/* Hook Plugin */
	register_activation_hook(__FILE__,'adduseradspackage');


	/* Creating Menus */
	function user_ads_package_Menu()
	{

		/* Adding menus */
		add_menu_page(__('User Ads List'),'User Ads List', 'manage_options','user-ads-package/user-ads-package.php', 'user_ads_list');

		/* Adding Sub menus */
		add_submenu_page('user-ads-package/user-ads-package.php', 'Add User Ads', 'Add User Ads', 'manage_options', 'user_ads_add', 'user_ads_add');

	wp_register_style('demo_table.css', plugin_dir_url(__FILE__) . 'css/demo_table.css');
	wp_enqueue_style('demo_table.css');

	wp_register_script('jquery.dataTables.js', plugin_dir_url(__FILE__) . 'js/jquery.dataTables.js', array('jquery'));
	wp_enqueue_script('jquery.dataTables.js');
        
        
        
        
    if(is_admin()){
    

    wp_enqueue_script('plupload-handlers');

    wp_enqueue_script('iws-adsupload', plugins_url('/js/iwsupload.js', __FILE__), array('jquery', 'plupload-handlers'));



    wp_localize_script('iws-adsupload', 'ProfileAJ', array(
        'confirmMsg' => __('Are you sure?', 'iws'),
        'nonce' => wp_create_nonce('iws_nonce'),
        'ajaxurl' => admin_url('admin-ajax.php'),
        'plupload' => array(
            'url' => admin_url('admin-ajax.php') . '?nonce=' . wp_create_nonce('iws_featured_img'),
            'flash_swf_url' => includes_url('js/plupload/plupload.flash.swf'),
            'filters' => array(array('title' => __('Allowed Files'), 'extensions' => '*')),
            'multipart' => true,
            'urlstream_upload' => true,
        )
    ));


    


// Enqueue Scripts that are needed on all the pages
    wp_enqueue_script('jquery');
    wp_enqueue_script('iws-adsupload');
    }
        
        

	}


add_action('admin_menu', 'user_ads_package_Menu');





function user_ads_list() {
	include "user-ads-package-list.php";
}


function user_ads_add() {
	include "user-ads-package-new.php";
}



function check_userads_owner($id){
    
     global $wpdb,$post,$current_user;

    $user = $current_user;
    
    
   
    
    
    $sql='';
    
    $sql.='SELECT usad.* ';
    
            
    $sql.=' FROM '.$wpdb->prefix.'user_ads usad, '.$wpdb->prefix.'posts as ps ';
    
    
    $sql.=" WHERE 1=1 AND ps.post_type = 'wpsc_cart_orders' AND (ps.post_status = 'publish') ";
    
    $sql.=" and usad.ID=".$id;
    
    $sql.=" and ps.post_author=".$user->ID;
    $sql.=" and usad.user_id=".$user->ID;
     
    $sql.=' GROUP BY usad.ID ORDER BY usad.ID DESC ';
    
    if(mysql_num_rows(mysql_query($sql))>0){
        return true;
    }
    return false;
    
        
}


function get_user_frontads(){
    global $wpdb,$post;
    
    
    // only show the registration form to non-logged-in members
    if (is_user_logged_in()) {

    } else {
        $redirect_link = wp_login_url();
        wp_redirect($redirect_link);
        exit;
    }
    
    
    
    
    if(isset($_GET['act']) && trim($_GET['act'])!=''){
        $act_type=trim($_GET['act']);
        switch ($act_type){            
           
            case 'edit':
                
                if(isset($_GET['id']) && (int) $_GET['id']>0){
                
                    $user_ads_id = (int) $_GET['id'];
                    $chk_owner=check_userads_owner($user_ads_id);
                    
                    if($chk_owner){
                        require_once 'user_ads_edit.php';
                    } else {
                        wp_redirect(original_page_url().'?msg=notowner');
                    }
                } else {
                    wp_redirect(original_page_url().'?msg=notfound');
                }
                
                break;
            default :
                break;
        }
    } else {
       require_once 'user_ads_list.php';
    }
    
    
    
    
}

add_shortcode('my_ads','get_user_frontads');





function iws_front_addedit_userads($post_id) {

    global $wpdb,$post,$current_user;

    $user = $current_user;
    
    
            
  
if($_POST && isset($_POST['post_id']) && (int) $_POST['post_id'] > (int) 0 && isset($_POST['iws_userads_editadd_nonce']) && wp_verify_nonce($_POST['iws_userads_editadd_nonce'],'iws-userads-editadd-nonce')){     

    
        
    $act='add';
    $post_id=(int) $_POST['post_id'];
    $msg='';
    
    
            
    $prev_avatar='';
    
    
    if(isset($_GET['act']) && trim($_GET['act'])=='edit'){
        if(isset($_GET['id']) && (int) $_GET['id']>0){
                
                    $user_ads_id = (int) $_GET['id'];
                    $chk_owner=check_userads_owner($user_ads_id);
                    
                    if($chk_owner){
                        
                        
                        $post_id=$user_ads_id;   
    
                        $sql='';

                        $sql.='SELECT usad.* ';
                        $sql.=' FROM '.$wpdb->prefix.'user_ads usad, '.$wpdb->prefix.'posts as ps ';
                        $sql.=" WHERE 1=1 AND ps.post_type = 'wpsc_cart_orders' AND (ps.post_status = 'publish') ";
                        $sql.=" and usad.ID=".$user_ads_id;
                        $sql.=" and ps.post_author=".$user->ID;
                        $sql.=" and usad.user_id=".$user->ID;
                        $sql.=' GROUP BY usad.ID ORDER BY usad.ID DESC ';

                        $get_ads_data=mysql_query($sql);

                        if(mysql_num_rows($get_ads_data)>0){
                            $rows=mysql_fetch_array($get_ads_data);

                            $prev_avatar=$rows['_thumbnail_id'];
                        }
                        
                        
                        
                       
                        
                        $act='edit';
                        $msg='useradsupdatesuccess';
                        
                    } else {
                        wp_redirect(original_page_url().'?msg=notowner');
                    }
                } else {
                    wp_redirect(original_page_url().'?msg=notfound');
                }
        }   
    
    
        
        
        
        $ads_content = trim($_POST["ads_content"]);
        $ads_title=trim($_POST['ads_title']);
        
        $ads_target_link = trim($_POST["ads_target_link"]);
        
        $ad_type= (int) $_POST['ad_type'];
       
             
        
        if($ads_target_link==''){
            iws_errors()->add('user_ads_website_empty', __('Please enter a ad target link.'));
        }
        
        if(!valid_url($ads_target_link)){
            iws_errors()->add('user_ads_website_valid', __('Please enter a valid ad target link.'));
        }
        
                    
                
        
        
        if($ad_type==2 || $ad_type==1){
        
            
            
            if(isset($_POST["ads_title"]) && trim($_POST["ads_title"])==''){
                iws_errors()->add('user_ads_title_empty', __('Please enter ad title.'));
            } else {

                if (strlen(trim($_POST["ads_title"]))>100) {
                    iws_errors()->add('user_ads_title_max_limit', __('Please enter ad title maximum 100 characters.'));
                }

                if (strlen(trim($_POST["ads_title"]))<3) {
                    iws_errors()->add('user_ads_title_min_limit', __('Please enter ad title minimum 3 characters.'));
                }
            }
            
            
            
            if(isset($_POST["ads_content"]) && trim($_POST["ads_content"])==''){
                iws_errors()->add('user_ads_content_empty', __('Please enter ad content.'));
            } else {

                if (strlen(trim($_POST["ads_content"]))>200) {
                    iws_errors()->add('user_ads_content_max_limit', __('Please enter ad content maximum 200 characters.'));
                }

                if (strlen(trim($_POST["ads_content"]))<5) {
                    iws_errors()->add('user_ads_content_min_limit', __('Please enter ad content minimum 5 characters.'));
                }
            }
        
        }
        
        if($ad_type==3 || $ad_type==1){
            if (isset($_POST['iws_files']['_thumbnail_id'][0]) && (int) $_POST['iws_files']['_thumbnail_id'][0]>0) {
                
            } elseif((int) $prev_avatar>0) { 
                 
            } else {
                iws_errors()->add('user_ads_photo_required', __('Please upload ad image.'));
            }
        }
       

        $errors = iws_errors()->get_error_messages();

        // only create the user in if there are no errors
        if (empty($errors)) {
       
        
        $ads_content = esc_textarea(sanitize_text_field($ads_content));

        
      
    //==thumbnail
        
           
            
            $attachment_id='';
        // set featured image if there's any
            if (isset($_POST['iws_files']['_thumbnail_id'][0]) && (int) $_POST['iws_files']['_thumbnail_id'][0]>0) {
                $attachment_id = $_POST['iws_files']['_thumbnail_id'][0];

                
                if (!empty($prev_avatar) && $prev_avatar>0) {
                    wp_delete_attachment($prev_avatar, true);
                }
                
            } else {
                $attachment_id=$prev_avatar;
            }
            
            mysql_query("update " .$wpdb->prefix . "user_ads set `ad_type`=".$ad_type.", `_thumbnail_id`=".$attachment_id.", `ads_target_link`='".$ads_target_link."', `ads_content`='".$ads_content."', `ads_title`='".$ads_title."' where id=".$post_id);
            
        
    
    

    

        wp_redirect(original_page_url().'?msg='.$msg);
    
         die; 
        
        }  

    }

    
}
add_action('init', 'iws_front_addedit_userads');




?>
