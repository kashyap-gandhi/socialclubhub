<?php

function associate_file($attach_id, $post_id) {
    wp_update_post(array(
        'ID' => $attach_id,
        'post_parent' => $post_id
    ));
}


add_action('wp_ajax_iws_insert_image', 'insert_image');
add_action('wp_ajax_nopriv_iws_insert_image', 'insert_image');

function insert_image() {
    upload_file(true);
}

add_action('wp_ajax_iws_file_upload', 'upload_file');
add_action('wp_ajax_nopriv_iws_file_upload', 'upload_file');

function upload_file($image_only = false) {
    $upload = array(
        'name' => $_FILES['iws_file']['name'],
        'type' => $_FILES['iws_file']['type'],
        'tmp_name' => $_FILES['iws_file']['tmp_name'],
        'error' => $_FILES['iws_file']['error'],
        'size' => $_FILES['iws_file']['size']
    );

    header('Content-Type: text/html; charset=' . get_option('blog_charset'));

    $attach = handle_upload($upload);

    if ($attach['success']) {

        $response = array('success' => true);

        if ($image_only) {
            $image_size = iws_get_option('insert_photo_size', 'iws_general', 'thumbnail');
            $image_type = iws_get_option('insert_photo_type', 'iws_general', 'link');

            if ($image_type == 'link') {
                $response['html'] = wp_get_attachment_link($attach['attach_id'], $image_size);
            } else {
                $response['html'] = wp_get_attachment_image($attach['attach_id'], $image_size);
            }
        } else {
            $response['html'] = attach_html($attach['attach_id']);
        }
        
        
       

        echo $response['html'];
        
        
        
    } else {
        echo 'error';
    }


    // $response = array('success' => false, 'message' => $attach['error']);
    // echo json_encode( $response );
    exit;
}

/**
 * Generic function to upload a file
 *
 * @param string $field_name file input field name
 * @return bool|int attachment id on success, bool false instead
 */
function handle_upload($upload_data) {

    $uploaded_file = wp_handle_upload($upload_data, array('test_form' => false));

    // If the wp_handle_upload call returned a local path for the image
    if (isset($uploaded_file['file'])) {
        $file_loc = $uploaded_file['file'];
        $file_name = basename($upload_data['name']);
        $file_type = wp_check_filetype($file_name);

        $attachment = array(
            'post_mime_type' => $file_type['type'],
            'post_title' => preg_replace('/\.[^.]+$/', '', basename($file_name)),
            'post_content' => '',
            'post_status' => 'inherit'
        );

        $attach_id = wp_insert_attachment($attachment, $file_loc);
        $attach_data = wp_generate_attachment_metadata($attach_id, $file_loc);
        wp_update_attachment_metadata($attach_id, $attach_data);

        return array('success' => true, 'attach_id' => $attach_id);
    }

    return array('success' => false, 'error' => $uploaded_file['error']);
}

function attach_html($attach_id, $type = NULL) {
    
    
    if (!$type) {
        $type = isset($_GET['type']) ? $_GET['type'] : 'image';
    }

    $attachment = get_post($attach_id);

    
    if (!$attachment) {
        return;
    }
    

    if (wp_attachment_is_image($attach_id)) {
        $image = wp_get_attachment_image_src($attach_id, 'thumbnail');
        $image = $image[0];
    } else {
        $image = wp_mime_type_icon($attach_id);
    }

    $html = '<li class="iws-image-wrap thumbnail">';
    $html .= sprintf('<div class="attachment-name"><img src="%s" alt="%s" /></div>', $image, esc_attr($attachment->post_title));

    /* if ( iws_get_option( 'image_caption', 'iws_general', 'off' ) == 'on' ) {
      $html .= '<div class="iws-file-input-wrap">';
      $html .= sprintf( '<input type="text" name="iws_files_data[%d][title]" value="%s" placeholder="%s">', $attach_id, esc_attr( $attachment->post_title ), __( 'Title', 'iws' ) );
      $html .= sprintf( '<textarea name="iws_files_data[%d][caption]" placeholder="%s">%s</textarea>', $attach_id, __( 'Caption', 'iws' ), esc_textarea( $attachment->post_excerpt ) );
      $html .= sprintf( '<textarea name="iws_files_data[%d][desc]" placeholder="%s">%s</textarea>', $attach_id, __( 'Description', 'iws' ), esc_textarea( $attachment->post_content ) );
      $html .= '</div>';
      } */

    $html .= sprintf('<input type="hidden" name="iws_files[%s][]" value="%d">', $type, $attach_id);
    $html .= sprintf('<div class="caption"><a href="#" class="btn btn-danger btn-small attachment-delete" data-attach_id="%d">%s</a></div>', $attach_id, __('Delete', 'iws'));
    $html .= '</li>';

    return $html;
}




add_action('wp_ajax_iws_file_del', 'delete_file');
add_action('wp_ajax_nopriv_iws_file_del', 'delete_file');

function delete_file() {
    check_ajax_referer('iws_nonce', 'nonce');

    $attach_id = isset($_POST['attach_id']) ? intval($_POST['attach_id']) : 0;
    $attachment = get_post($attach_id);

    //post author or editor role
    if (get_current_user_id() == $attachment->post_author || current_user_can('delete_private_pages')) {
        wp_delete_attachment($attach_id, true);
        echo 'success';
    }

    exit;
}


/**
 * User avatar wrapper for custom uploaded avatar
 *
 * @since 2.0
 *
 * @param string $avatar
 * @param mixed $id_or_email
 * @param int $size
 * @param string $default
 * @param string $alt
 * @return string image tag of the user avatar
 */
function iws_get_avatar($avatar, $id_or_email, $size, $default, $alt) {

    if (is_numeric($id_or_email)) {
        $user = get_user_by('id', $id_or_email);
    } elseif (is_object($id_or_email)) {
        if ($id_or_email->user_id != '0') {
            $user = get_user_by('id', $id_or_email->user_id);
        } else {
            return $avatar;
        }
    } else {
        $user = get_user_by('email', $id_or_email);
    }

    if (!$user) {
        return $avatar;
    }

    // see if there is a user_avatar meta field
    $user_avatar = get_user_meta($user->ID, 'user_avatar', true);
    if (empty($user_avatar)) {
        return $avatar;
    }

    return sprintf('<img src="%1$s" alt="%2$s" height="%3$s" width="%3$s" class="avatar">', esc_url($user_avatar), $alt, $size);
}

add_filter('get_avatar', 'iws_get_avatar', 99, 5);

function iws_update_avatar($user_id, $attachment_id) {

    $upload_dir = wp_upload_dir();
    $relative_url = wp_get_attachment_url($attachment_id);

    if (function_exists('wp_get_image_editor')) {
        // try to crop the photo if it's big
        $file_path = str_replace($upload_dir['baseurl'], $upload_dir['basedir'], $relative_url);

        // as the image upload process generated a bunch of images
        // try delete the intermediate sizes.
        $ext = strrchr($file_path, '.');
        $file_path_w_ext = str_replace($ext, '', $file_path);
        $small_url = $file_path_w_ext . '-avatar' . $ext;
        $relative_url = str_replace($upload_dir['basedir'], $upload_dir['baseurl'], $small_url);

        $editor = wp_get_image_editor($file_path);

        if (!is_wp_error($editor)) {
            $editor->resize(100, 100, true);
            $editor->save($small_url);

            // if the file creation successfull, delete the original attachment
            if (file_exists($small_url)) {
                wp_delete_attachment($attachment_id, true);
            }
        }
    }

    // delete any previous avatar
    $prev_avatar = get_user_meta($user_id, 'user_avatar', true);

    if (!empty($prev_avatar)) {
        $prev_avatar_path = str_replace($upload_dir['baseurl'], $upload_dir['basedir'], $prev_avatar);

        if (file_exists($prev_avatar_path)) {
            unlink($prev_avatar_path);
        }
    }

    // now update new user avatar
    update_user_meta($user_id, 'user_avatar', $relative_url);
}


add_action('wp_ajax_iws_delete_avatar', 'delete_avatar_ajax');

function delete_avatar_ajax() {
    $user_id = get_current_user_id();
    $avatar = get_user_meta($user_id, 'user_avatar', true);
    if ($avatar) {
        $upload_dir = wp_upload_dir();

        $full_url = str_replace($upload_dir['baseurl'], $upload_dir['basedir'], $avatar);

        if (file_exists($full_url)) {
            unlink($full_url);
            delete_user_meta($user_id, 'user_avatar');
        }
    }

    die();
}
?>