<?php
/**
 * The template part for displaying results in search pages
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */


?>

<div class="col-sm-12 clearfix mainpostrow">
    <?php the_title( sprintf( '<h2><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>


    <?php
    $thumb_src= '';
            $attach_id = get_post_thumbnail_id(get_the_ID());
            if($attach_id>0){
                $image_url = wp_get_attachment_image_src( $attach_id, 'medium' );
                if(isset($image_url[0]) && $image_url[0]!=''){
                    $thumb_src=$image_url[0];
                }
            }
            ?>
    
    <?php if($thumb_src!='') { ?>
<div class="col-sm-3" style="padding-left: 0px;"> <img src="<?php echo $thumb_src; ?>" class="img-responsive" /></div>
<div class="col-sm-7"><?php the_excerpt(); ?></div>
<div class="col-sm-12 posttypetax withimage"><?php twentyfifteen_entry_meta(); ?></div>

<?php } else { ?>
<div class="col-sm-12" style="padding-left: 0px;"><?php the_excerpt(); ?>
<div class="col-sm-12 posttypetax"><?php twentyfifteen_entry_meta(); ?></div>
</div>
<?php } ?>



</div>


	
