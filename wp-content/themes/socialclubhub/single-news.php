<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */


get_header(); ?>
 
	<div class="blogDetailPage clearfix">

            <div class="col-md-8 col-sm-12">


		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();  ?>
                    
                    


	<?php
		// Post thumbnail.
		//twentyfifteen_post_thumbnail();
	?>

	
		<?php
			if ( is_single() ) :
				the_title( '<h2 class="entry-title">', '</h2>' );
			else :
				the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
			endif;
		?>
	

        
        <div class="col-sm-12 posttypetax"><?php twentyfifteen_entry_meta(); ?></div>
        
        <div class="col-sm-12 clearfix" style="padding-left: 0px; padding-top: 15px;">
        
		<?php the_content(); ?>
	</div>

        
                
        <div class="news-pager clearfix">
       <?php

       if($post->post_parent>0){
        
           $parent_post_id=$post->post_parent;
           
       } else {
           
           $parent_post_id=$post->ID;
       }
       
       
       
news_paginate_parent_children($parent_post_id);
?>
            </div>
        
	



                
                <?php

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;
                        
                        
                        /*wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentyfifteen' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );*/
			// Previous/next post navigation.
			/*the_post_navigation( array(
				'next_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Next', 'twentyfifteen' ) . '</span> ' .
					'<span class="screen-reader-text">' . __( 'Next post:', 'twentyfifteen' ) . '</span> ' .
					'<span class="post-title">%title</span>',
				'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Previous', 'twentyfifteen' ) . '</span> ' .
					'<span class="screen-reader-text">' . __( 'Previous post:', 'twentyfifteen' ) . '</span> ' .
					'<span class="post-title">%title</span>',
			) );*/

		// End the loop.
		endwhile;
		?>

		</div>
                    
                    
                    <?php get_sidebar('ads'); ?>
                    
             

            </div>


<?php get_footer(); ?>

