<?php

function get_all_country() {
    global $wpdb, $post;


    $get_countries = "select * from " . $wpdb->prefix . "countries order by country asc";
    $res_countries = $wpdb->get_results($get_countries);

    return $res_countries;
}

function get_states_from_country_by_id($country_id) {

    global $wpdb, $post;

    $res_states = array();

    if ((int) $country_id > (int) 0) {
        $get_states = "select * from " . $wpdb->prefix . "states where countryid =" . $country_id . " order by region asc";
        $res_states = $wpdb->get_results($get_states);
    }

    return $res_states;
}

function get_cities_from_state_by_id($state_id) {

    global $wpdb, $post;

    $res_cities = array();

    if ((int) $state_id > (int) 0) {
        $get_cities = "select * from " . $wpdb->prefix . "cities where RegionID =" . $state_id . " order by City asc";
        $res_cities = $wpdb->get_results($get_cities);
    }

    return $res_cities;
}

add_action('wp_ajax_get_states_of_country', 'get_states_from_country');  //===for logged in user
add_action('wp_ajax_nopriv_get_states_of_country', 'get_states_from_country');  //==for not logged in user

function get_states_from_country() {
    global $wpdb;
    $cid = intval($_POST['country']);

    $res_states = get_states_from_country_by_id($cid);


    $state_ops = '<option value="">---' . __('Select State') . "---</option>";
    if (!empty($res_states)) {
        foreach ($res_states as $row_states) {
            $state_ops .= '<option value="' . $row_states->regionid . '">' . ucfirst($row_states->region) . "</option>";
        }
    }
    echo $state_ops;
    die(); // this is required to terminate immediately and return a proper response
}

add_action('wp_ajax_get_cities_of_state', 'get_cities_from_state');  //===for logged in user
add_action('wp_ajax_nopriv_get_cities_of_state', 'get_cities_from_state');  //==for not logged in user

function get_cities_from_state() {
    global $wpdb;
    $sid = intval($_POST['state']);


    $res_cities = get_cities_from_state_by_id($sid);


    $city_ops = '<option value="">---' . __('Select City') . "---</option>";
    if (!empty($res_cities)) {
        foreach ($res_cities as $row_cities) {

            $city_ops .= '<option value="' . $row_cities->CityId . '"  data-lat="' . $row_cities->Latitude . '"  data-long="' . $row_cities->Longitude . '" >' . ucfirst($row_cities->City) . "</option>";
        }
    }
    echo $city_ops;
    die(); // this is required to terminate immediately and return a proper response
}

function get_city_by_name($city_name='',$latitude='',$longitude='') {
    global $wpdb;
    
    $city_name=trim(strtolower($city_name));
    
    
    
    

            $search_by_miles = 3959;
            $search_by_kms = 6371;

            $latlong_field = "";

            if ($latitude != '' && $longitude != '') {

                $latlong_field = "(
                    $search_by_miles * acos (
                      cos ( radians(" . $latitude . ") )
                      * cos( radians( ct.Latitude ) )
                      * cos( radians( ct.Longitude ) - radians(" . $longitude . ") )
                      + sin ( radians(" . $latitude . ") )
                      * sin( radians( ct.Latitude ) )
                    )
                  ) AS city_distance";
            }

    $get_city = "select ct.* ";
    
    if($latlong_field!=''){
        $get_city.=', '.$latlong_field;
    }
    
    $get_city.=" from " . $wpdb->prefix . "cities as ct where LOWER(ct.City) like '" . $city_name . "%' ";
    
    $get_city.=" order by ";
    
    if($latlong_field!=''){
         $get_city.=" city_distance asc";
    } else {
        $get_city.=" ct.CityId asc";
    }
    
    $get_city.=", ct.City asc";
    
    $get_city.=" limit 1";
    
    //echo $get_city; die;
    $res_city = $wpdb->get_row($get_city);
    
    //echo "<pre>"; print_r($res_city);
    
    return $res_city;
}






function get_search_city_by_name($city_name='',$state_name='',$country_name='',$latitude='',$longitude='') {
    global $wpdb;
    
    $city_name=trim(strtolower($city_name));
    $state_name=trim(strtolower($state_name));
    $country_name=trim(strtolower($country_name));
    
    
    
    

            $search_by_miles = 3959;
            $search_by_kms = 6371;

            $latlong_field = "";

            if ($latitude != '' && $longitude != '') {

                $latlong_field = "(
                    $search_by_miles * acos (
                      cos ( radians(" . $latitude . ") )
                      * cos( radians( ct.Latitude ) )
                      * cos( radians( ct.Longitude ) - radians(" . $longitude . ") )
                      + sin ( radians(" . $latitude . ") )
                      * sin( radians( ct.Latitude ) )
                    )
                  ) AS city_distance";
            }

    $get_city = "select ct.* ";
    
    if($latlong_field!=''){
        $get_city.=', '.$latlong_field;
    }
    
    //$get_city.=" from " . $wpdb->prefix . "cities as ct where LOWER(ct.City) like '" . $city_name . "%' ";
    
    $get_city.=" from " . $wpdb->prefix . "cities as ct," . $wpdb->prefix . "states as st where ct.RegionID=st.regionid and LOWER(ct.City) like '" . $city_name . "%' and  ( LOWER(st.region) like '".$state_name."%' OR UPPER(st.code)='".  strtoupper($state_name)."' )";
    
     

    
    
    $get_city.=" order by ";
    
    if($latlong_field!=''){
         $get_city.=" city_distance asc";
    } else {
        $get_city.=" ct.CityId asc";
    }
    
    $get_city.=", ct.City asc";
    
    $get_city.=" limit 1";
    
  //  echo $get_city; die;
    $res_city = $wpdb->get_row($get_city);
    
    //echo "<pre>"; print_r($res_city); die;
    
    return $res_city;
}






function get_country_by_id($id=0) {
    global $wpdb;
    
    $result=array();
    
    if($id>0){

        $make_sql = "select * from " . $wpdb->prefix . "countries where countryid=".$id;
        $result = $wpdb->get_row($make_sql);

    }
    
    return $result;
}


function get_state_by_id($id=0) {
    global $wpdb;
    
    $result=array();
    
    if($id>0){

        $make_sql = "select * from " . $wpdb->prefix . "states where regionid=".$id;
        $result = $wpdb->get_row($make_sql);

    }
    
    return $result;
}


function get_city_by_id($id=0) {
    global $wpdb;
    
    $result=array();
    
    if($id>0){

        $make_sql = "select * from " . $wpdb->prefix . "cities where CityId=".$id;
        $result = $wpdb->get_row($make_sql);

    }
    
    return $result;
}


add_action('wp_ajax_getPopulatecity', 'get_search_by_city_text');  //===for logged in user
add_action('wp_ajax_nopriv_getPopulatecity', 'get_search_by_city_text');  //==for not logged in user

function get_search_by_city_text(){
    
    global $wpdb;
    
    $array = array();
    
    $search_city=trim(strip_tags($_REQUEST['search_city'])); 
    
    if($search_city!='' && strlen($search_city)>=3) {
        $get_city = "select ct.*, st.region  from " . $wpdb->prefix . "cities as ct," . $wpdb->prefix . "states as st where ct.RegionID=st.regionid and  LOWER(ct.City) like '" . strtolower($search_city) . "%'  order by  ct.City asc, st.region asc";
       

        //echo $get_city; die;
        $res_city = $wpdb->get_results($get_city);
        
        if(!empty($res_city)){
            
            foreach($res_city as $row){
                
                $array[]=$row->City.','.$row->region;
                
            }
            
        }
        
        
    }
    
    echo json_encode ($array); //Return the JSON Array
    die;
}

?>