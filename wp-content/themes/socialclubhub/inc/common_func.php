<?php

function get_all_activities() {

    global $wpdb, $post;

    $get_club_activities = "select tm.*, tt.description from " . $wpdb->prefix . "term_taxonomy tt, " . $wpdb->prefix . "terms tm where tt.term_id=tm.term_id and tt.taxonomy='club-activities' and tt.parent=0  order by tm.name asc";

    $res_activities = $wpdb->get_results($get_club_activities);

    return $res_activities;
}


function get_activity_by_id($id) {

    global $wpdb, $post;

    $get_club_activities = "select tm.*, tt.description from " . $wpdb->prefix . "term_taxonomy tt, " . $wpdb->prefix . "terms tm where tt.term_id=tm.term_id and tt.taxonomy='club-activities' and tt.parent=0 and tt.term_id=".$id." order by tm.name asc";

    $res_activities = $wpdb->get_row($get_club_activities);

    return $res_activities;
}

function get_user_by_role($roles=array()) {

    global $wpdb, $post;

    if (empty($roles)) {
        $roles = array('customer');
    }

    $meta_query = array(
        'key' => $wpdb->prefix . 'capabilities',
        'value' => '"(' . implode('|', array_map('preg_quote', $roles)) . ')"',
        'compare' => 'REGEXP'
    );
    $get_customer = new WP_User_Query(array('meta_query' => array($meta_query), 'orderby' => 'display_name', 'order' => 'ASC'));

    return $get_customer;
}

function get_post_all_meta_data($post_id) {
    global $wpdb;

    $data = array();

    $wpdb->query(" SELECT `meta_key`, `meta_value`  FROM " . $wpdb->prefix . "postmeta  WHERE meta_key!='_edit_last' and meta_key!='_edit_lock' and `post_id` = " . $post_id . "  ");

    foreach ($wpdb->last_result as $k => $v) {
        $data[$v->meta_key] = $v->meta_value;
    };

    return $data;
}

function generate_random_code($length=12) {

    $code = '';
    //Ascii Code for number, lowercase, uppercase and special characters
    $no = range(48, 57);
    $lo = range(97, 122);
    $up = range(65, 90);

    //exclude character I, l, 1, 0, O
    $eno = array(48, 49);
    $elo = array(108);
    $eup = array(73, 79);
    $no = array_diff($no, $eno);
    $lo = array_diff($lo, $elo);
    $up = array_diff($up, $eup);
    $chr = array_merge($no, $lo, $up);


    for ($i = 1; $i <= $length; $i++) {

        $code.= chr($chr[rand(0, count($chr) - 1)]);
    }
    return $code;
}

function valid_url($str)
{

   return ( ! preg_match('/^(http|https|ftp):\/\/([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i', $str)) ? FALSE : TRUE;
}
function contact_check($str)
{
        return ( ! preg_match("/^([0-9 +-])+$/i", $str)) ? FALSE : TRUE;
}

function current_page_url() {
	$pageURL = 'http';
	if( isset($_SERVER["HTTPS"]) ) {
		if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
	}
	$pageURL .= "://";
	if ($_SERVER["SERVER_PORT"] != "80") {
		$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	} else {
		$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	}
	return $pageURL;
}

function original_page_url(){
    
    $current_url=current_page_url();
        
        $url_params = parse_url($current_url);
             
        
        
        $currentUrl=$url_params['scheme']."://".$url_params['host'].$url_params['path'];
        
        return $original_url=$currentUrl;
        
}
?>