<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<div class="activityDetailPage clearfix">
 <div class="clearfix">
<?php get_sidebar('activity'); ?>



                        <div class="col-md-5 col-sm-9 clearfix">

                	

                          <?php
   global $wpdb;
   
                          $get_term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
                          
                          //echo "<pre>";    print_r($get_term);die;
                          
                          
                          
         if(!empty($get_term) && isset($get_term->term_id) && $get_term->term_id>0) {                
    
             
             $term_id=$get_term->term_id;
   
             
            $search_by_acitivity =$term_id;
             
             
             
             ///===query setup
             
             
             



 $search_by_miles = 3959;
    $search_by_kms = 6371;
     
    $socialclubhub = get_option("socialclubhub_theme_config");
     
    
    //==system default latlong
    $system_lat=25.761679800000;
    $system_long=-80.191790200000;
    
    $search_city_name='';
    
    if(isset($socialclubhub['system_lat']) && trim($socialclubhub['system_lat'])!=''){
        $system_lat=trim($socialclubhub['system_lat']);
    }
    
    
    if(isset($socialclubhub['system_long']) && trim($socialclubhub['system_long'])!=''){
        $system_long=trim($socialclubhub['system_long']);
    }
     
    
    //==user track latlong
    $user_lat='';    
    if(isset($_COOKIE['GASCHLAT'])){ $user_lat= trim(encrypt_decrypt_cookie('decrypt',$_COOKIE['GASCHLAT'])); } 
    
    $user_long='';    
    if(isset($_COOKIE['GASCHLONG'])){ $user_long= trim(encrypt_decrypt_cookie('decrypt',$_COOKIE['GASCHLONG'])); } 
    
    $user_state_name='';    
    if(isset($_COOKIE['GASCHST'])){ $user_state_name= trim(encrypt_decrypt_cookie('decrypt',$_COOKIE['GASCHST'])); } 
    
    $user_country_name='';    
    if(isset($_COOKIE['GASCHCNT'])){ $user_country_name= trim(encrypt_decrypt_cookie('decrypt',$_COOKIE['GASCHCNT'])); } 
    
    
    
    $user_city_name='';    
    if(isset($_COOKIE['GASCHCT'])){ 
       
        $user_city_name= trim(encrypt_decrypt_cookie('decrypt',$_COOKIE['GASCHCT']));
        
        if($user_lat=='' || $user_long==''){
            if(trim($user_city_name)!=''){
                $get_city_detail=get_city_by_name($user_city_name,$user_lat,$user_long);

                if(!empty($get_city)){

                    if(isset($row_cities->Latitude) && $row_cities->Latitude!=''){
                        $user_lat=$row_cities->Latitude;
                    }

                    if(isset($row_cities->Longitude) && $row_cities->Longitude!=''){
                        $user_long=$row_cities->Longitude;
                    }

                }
            }
        }
        
        if($user_lat=='' || $user_long==''){
            $user_lat=''; 
            $user_long=''; 
        }
    } 
    
    
    
    
    //==override with track city
    $search_city_name=$user_city_name;
    
    
    
    //==user db latlong
    $user_db_lat='';
    $user_db_long='';
    $user_db_city_name='';
     if (is_user_logged_in()) {

        global $current_user;

        $user = $current_user;
        
         $user_db_lat = trim(get_the_author_meta('user_lat', $user->ID));
         $user_db_long = trim(get_the_author_meta('user_long', $user->ID));
         
         
         
        $user_city_id = (int) get_the_author_meta('user_city_id', $user->ID);

        $get_city_detail = get_city_by_id($user_city_id);
        if (!empty($get_city_detail)) {
            $user_db_city_name = $get_city_detail->City;
            
            if($user_db_lat=='' || $user_db_long==''){
                
                    if(isset($get_city_detail->Latitude) && $get_city_detail->Latitude!=''){
                        $user_db_lat=$get_city_detail->Latitude;
                    }

                    if(isset($get_city_detail->Longitude) && $get_city_detail->Longitude!=''){
                        $user_db_long=$get_city_detail->Longitude;
                    }                
                
            }
            
        }
        
        
        if($user_db_lat=='' || $user_db_long==''){
             $user_db_lat='';
             $user_db_long='';
         }
         
     }
     
     //==override with profile city
     if($user_db_lat!='' && $user_db_long!='' && $user_db_city_name!=''){
         $user_lat=$user_db_lat;
         $user_long=$user_db_long;
         $search_city_name=$user_db_city_name;
     }
    
    
    //==user search latlong
    $user_search_lat='';    
    if(isset($_COOKIE['GASCHRLAT'])){ $user_search_lat= trim(encrypt_decrypt_cookie('decrypt',$_COOKIE['GASCHRLAT'])); } 
    
    $user_search_long='';    
    if(isset($_COOKIE['GASCHRLONG'])){ $user_search_long= trim(encrypt_decrypt_cookie('decrypt',$_COOKIE['GASCHRLONG'])); } 
    
    
   $user_search_city='';
    if (isset($_COOKIE['GASCHRCT'])) {
        $user_search_city = trim(encrypt_decrypt_cookie('decrypt', $_COOKIE['GASCHRCT'])); 

        if($user_search_lat=='' || $user_search_long==''){
            
            $get_city_detail = get_city_by_name($user_search_city);
            if (!empty($get_city_detail)) {
                
                
                if(isset($get_city_detail->Latitude) && $get_city_detail->Latitude!=''){
                    $user_search_lat=$get_city_detail->Latitude;
                }

                if(isset($get_city_detail->Longitude) && $get_city_detail->Longitude!=''){
                    $user_search_long=$get_city_detail->Longitude;
                }
               
            }            
        }        
    }
    
    if($user_search_lat!='' && $user_search_long!='' && $user_search_city!=''){
        $user_lat=$user_search_lat;
        $user_long=$user_search_long;
        $search_city_name=$user_search_city;
    }
    
    
    
    if($user_lat=='' || $user_long=='' || $search_city_name==''){
        
        $user_lat=$system_lat;
        $user_long=$system_long;
        
        if(isset($socialclubhub['system_city_id']) && (int) $socialclubhub['system_city_id']>0){
            
            $get_city_detail = get_city_by_id($socialclubhub['system_city_id']);

            if (!empty($get_city_detail)) {
                $search_city_name = $get_city_detail->City;
            }

        }
        
    }
    
    
    
    
    $search_city_name=strtolower(trim(strip_tags($search_city_name)));
    
    
    
    
    
    //radius setup
    $search_by_radius=100;
    
    if (isset($_COOKIE['GASCHRMIL'])) {
        $search_cache_radius = (int) trim(encrypt_decrypt_cookie('decrypt', $_COOKIE['GASCHRMIL']));    
    
        if($search_cache_radius>0){
            $search_by_radius=$search_cache_radius;
        }
    
    }
   //==end radius setup      
     
             
             //==query step
             
             
             
             
       $use_location=0;
    
    $latlong_field = "";
    

    if ($user_lat != '' && $user_long != '' && $use_location==1) {
        
        //==mt1===latitude &&  mt2===longitude
        
        $latlong_field = "IFNULL((
            $search_by_miles * acos (
              cos ( radians(" . $user_lat . ") )
              * cos( radians( CAST(mt1.meta_value as CHAR) ) )
              * cos( radians( CAST(mt2.meta_value as CHAR) ) - radians(" . $user_long . ") )
              + sin ( radians(" . $user_lat . ") )
              * sin( radians( CAST(mt1.meta_value as CHAR) ) )
            )
          ) ,0) AS city_distance ";
        
        
    }      
             
             
             
    $sql='';
    
    $sql.='SELECT ps.* ';
    
    
     if($latlong_field!=''){
        $sql.=', '.$latlong_field;
    }
    
    
     $sql.=' FROM '.$wpdb->prefix.'posts as ps ';
    
    
        
     if($latlong_field!=''){
        $sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt1 ON ( ps.ID = mt1.post_id AND mt1.meta_key="club_lat" ) ';
        $sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt2 ON ( ps.ID = mt2.post_id AND mt2.meta_key="club_long" ) ';
    }
    
    if($search_city_name!=''  && $use_location==1){
        //$sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt3 ON ( ps.ID = mt3.post_id AND mt3.meta_key = "club_city_id" AND CAST(mt3.meta_value AS SIGNED)>0 ) ';
        //$sql.=' INNER JOIN '.$wpdb->prefix.'cities AS ct ON ( CAST(mt3.meta_value AS SIGNED) = ct.CityId ) ';
    }
    
     if ((int) $search_by_acitivity > 0) {
        $sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt4 ON ( ps.ID = mt4.post_id ) ';
     }
    
            
   
     
    
    $sql.=" WHERE 1=1 AND ps.post_type = 'clubs' AND (ps.post_status = 'publish') ";
    
    
    
     if($search_city_name!='' && $use_location==1){
        //$sql.=" AND LOWER(ct.City) like '%" . $search_city_name . "%' ";
    }
    
     
    if ((int) $search_by_acitivity > 0) {
        $sql.=" AND mt4.meta_key = 'club_main_activity' AND CAST(mt4.meta_value AS SIGNED) = '".$search_by_acitivity."'  ";
    }
    
    
     $sql.=' GROUP BY ps.ID ';
    
    if($latlong_field!='' && $search_by_radius>0){
        $sql.=' having city_distance<='.$search_by_radius;
    }
    
    $sql.=' ORDER BY ';
    
    if($latlong_field!=''){
        $sql.=' city_distance asc ';
    } else {
        $sql.=' ps.ID ASC ';
    }
    
    
    //echo $sql;
    //die;
    
    
    
    
	 
	
	
	$total_pages = mysql_num_rows(mysql_query($sql));
	
	
	
        
	
        $stages = 3;
        $limit = 12;
        $query_param_name='cfp';
	
        
        $current_url=current_page_url();
        
        $url_params = parse_url($current_url);
        
        
        if(!empty($url_params)){
           
            if(isset($url_params['query']) && $url_params['query']!=''){
                parse_str($url_params['query'],$query_params);
                
                if(isset($query_params[$query_param_name])){
                   unset($query_params[$query_param_name]);
               }
               
               if(isset($query_params['msg'])){
                   unset($query_params['msg']);
               }
               
               $url_params['query']=http_build_query($query_params);
            }
            
           
        }
        
        
        $currentUrl=$url_params['scheme']."://".$url_params['host'].$url_params['path'];
        $original_url=original_page_url();
        
        $separate_param='?';
        if(isset($url_params['query']) && trim($url_params['query'])!=''){
            
                $currentUrl.='?'.$url_params['query'];
                $separate_param='&';
            
        }
        
        
        $targetpage = $currentUrl;
        
        
	$page='';
        
        
        
        
        
        
	if(isset($_GET[$query_param_name]))
	{
		$page =  (int) $_GET[$query_param_name];
	}
	
	
	if($page){
		$start = ($page - 1) * $limit; 
	} else{
		$start = 0;	
		}	
	
    // Get page data
	$query1 = $sql." limit ".$limit." offset ".$start;
        
       
	$result = mysql_query($query1);
	
	// Initial page num setup
	if ($page == 0){$page = 1;}
	$prev = $page - 1;	
	$next = $page + 1;							
	$lastpage = ceil($total_pages/$limit);		
	$LastPagem1 = $lastpage - 1;					
	
	
	$paginate = '<ul class="pager">';
	if($lastpage > 1)
	{	
	

		// Previous
		if ($page > 1){
			$paginate.= "<li class='first-child'><a href='".$targetpage.$separate_param.$query_param_name."=".$prev."'>previous</a></li>";
		}else{
			$paginate.= "<li class='first-child'><a>previous</a></li>";	}
			

		
		// Pages	
		if ($lastpage < 7 + ($stages * 2))	// Not enough pages to breaking it up
		{	
			for ($counter = 1; $counter <= $lastpage; $counter++)
			{
				if ($counter == $page){
					$paginate.= "<li class='active'><a>$counter</a></li>";
				}else{
					$paginate.= "<li><a href='".$targetpage.$separate_param.$query_param_name."=".$counter."'>$counter</a></li>";}					
			}
		}
		elseif($lastpage > 5 + ($stages * 2))	// Enough pages to hide a few?
		{
			// Beginning only hide later pages
			if($page < 1 + ($stages * 2))		
			{
				for ($counter = 1; $counter < 4 + ($stages * 2); $counter++)
				{
					if ($counter == $page){
						$paginate.= "<li class='active'><a>$counter</a></li>";
					}else{
						$paginate.= "<li><a href='".$targetpage.$separate_param.$query_param_name."=".$counter."'>$counter</a></li>";}					
				}
				$paginate.= "...";
				$paginate.= "<a href='".$targetpage.$separate_param.$query_param_name."=".$LastPagem1."'>$LastPagem1</a>";
				$paginate.= "<a href='".$targetpage.$separate_param.$query_param_name."=".$lastpage."'>$lastpage</a>";		
			}
			// Middle hide some front and some back
			elseif($lastpage - ($stages * 2) > $page && $page > ($stages * 2))
			{
				$paginate.= "<li><a href='".$targetpage.$separate_param.$query_param_name."=1'>1</a></li>";
				$paginate.= "<li><a href='".$targetpage.$separate_param.$query_param_name."=2'>2</a></li>";
				$paginate.= "...";
				for ($counter = $page - $stages; $counter <= $page + $stages; $counter++)
				{
					if ($counter == $page){
						$paginate.= "<li class='active'><a>$counter</a></li>";
					}else{
						$paginate.= "<li><a href='".$targetpage.$separate_param.$query_param_name."=".$counter."'>$counter</a></li>";}					
				}
				$paginate.= "...";
				$paginate.= "<li><a href='".$targetpage.$separate_param.$query_param_name."=".$LastPagem1."'>$LastPagem1</a></li>";
				$paginate.= "<li><a href='".$targetpage.$separate_param.$query_param_name."=".$lastpage."'>$lastpage</a></li>";		
			}
			// End only hide early pages
			else
			{
				$paginate.= "<li><a href='".$targetpage.$separate_param.$query_param_name."=1'>1</a></li>";
				$paginate.= "<li><a href='".$targetpage.$separate_param.$query_param_name."=2'>2</a></li>";
				$paginate.= "...";
				for ($counter = $lastpage - (2 + ($stages * 2)); $counter <= $lastpage; $counter++)
				{
					if ($counter == $page){
						$paginate.= "<li class='active'><a>$counter</a></li>";
					}else{
						$paginate.= "<li><a href='".$targetpage.$separate_param.$query_param_name."=".$counter."'>$counter</a></li>";}					
				}
			}
		}
					
				// Next
		if ($page < $counter - 1){ 
			$paginate.= "<li class='last-child'><a href='".$targetpage.$separate_param.$query_param_name."=".$next."'>next</a></li>";
		}else{
			$paginate.= "<li><a>next</a></li>";
			}
			
	
	
	
}
$paginate.= "</ul>";
 

?>
    
    
    <div class="blogDetailRightBLock">
    
    
    <h3>ACTIVITIES - <span class="orange"><?php echo $get_term->name; ?> CLUBS</span></h3>
    
    </div>

    <div class="blogDetailBlock">

        <p><?php echo $get_term->description; ?></p>

    </div>
                            <div class="clearfix"></div>
                            <div class="pull-right clearfix"><?php echo $paginate; ?></div>            
                            <div class="clearfix"></div>
    
    
    <?php if(mysql_num_rows($result)>0){
        
        $ic=1;
        
        if(isset($_GET[$query_param_name]))
	{
		$ic =  (int) $_GET[$query_param_name] + 1;
	}
        
        while ($row = mysql_fetch_array($result)) {
            
            
            $post_id=$row['ID'];
            
            $post_title=$row['post_title'];
            
            $post_link=get_permalink($post_id);;
            
             
            $attach_image_id = get_post_thumbnail_id($post_id);
            $thumb_src= esc_url( get_template_directory_uri() ).'/images/default_image.jpg';
            
            if ($attach_image_id>0) {
                
                $image_data=wp_get_attachment_image_src($attach_image_id, 'sch-thumb');
                if(isset($image_data[0]) && $image_data[0]!=''){
                    $thumb_src=$image_data[0];
                }
            }
            
            
            $club_city_id = get_post_meta($post_id, 'club_city_id', TRUE);
             $city_name='';    
    $get_city_detail=get_city_by_id($club_city_id);    
    if(!empty($get_city_detail)){
        $city_name=$get_city_detail->City;
    }
    
    
    $club_state_id = get_post_meta($post_id, 'club_state_id', TRUE);
    
    
    $state_name='';    
    $get_state_detail=get_state_by_id($club_state_id);    
    if(!empty($get_state_detail)){
        $state_name=$get_state_detail->region;
    }
    
   
          ?>
                            
                            <div class="activityWrap clubactivity">

                        <div class="activityBlock">

                            <div class="imgBlock">

            
             <a href="<?php echo $post_link; ?>"><img src="<?php echo $thumb_src; ?>" class="img-responsive" /></a></div>

                            <p><strong><?php echo $post_title; ?></strong></p>

                            <p><?php if($city_name!='' && $state_name!='') { echo $city_name.', '.$state_name; } elseif($state_name!='') { echo $state_name; } elseif($city_name!=''){ echo $city_name; } ?></p>

                        </div><!-- activityBlock -->

                    </div><!-- activityWrap -->
            
            <?php
                    
            
            
            
            
        }
        
        
        
    } else { ?>   No club has been added in <?php echo $get_term->name; ?>. <?php }
    
        }  ?>
                    
                            <div class="clearfix"></div>
                            <div class="pull-right clearfix"><?php echo $paginate; ?></div>            
                            <div class="clearfix"></div>
    
                       </div>

     <?php get_sidebar('ads'); ?>
     
         </div> <!--cleatfix-->
 </div>
         
<?php
get_footer();
