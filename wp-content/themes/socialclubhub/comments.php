<?php
/**
 * The template for displaying comments
 *
 * The area of the page that contains both current comments
 * and the comment form.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="col-sm-12 clearfix comments-area">

	<?php if ( have_comments() ) : ?>
		<h2 class="comments-title">
			<?php
				printf( _nx( 'One thought on &ldquo;%2$s&rdquo;', '%1$s thoughts on &ldquo;%2$s&rdquo;', get_comments_number(), 'comments title', 'twentyfifteen' ),
					number_format_i18n( get_comments_number() ), get_the_title() );
			?>
		</h2>

		<?php twentyfifteen_comment_nav(); ?>

		<ul class="comment-list">
			<?php
				/*wp_list_comments( array(
					'style'       => 'ul',
					'short_ping'  => true,
					'avatar_size' => 56,
				) );*/
                        
                        wp_list_comments('type=comment&callback=sch_theme_comment');
                        
			?>
		</ul><!-- .comment-list -->

		<?php twentyfifteen_comment_nav(); ?>

	<?php endif; // have_comments() ?>

	<?php
		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
	?>
		<div class="col-sm-12 no-comments"><?php _e( 'Comments are closed.', 'twentyfifteen' ); ?></div>
	<?php endif; ?>

	<?php 
        //https://codex.wordpress.org/Function_Reference/comment_form
        
        $args = array(
            'must_log_in'=>'<p class="must-log-in">' .  sprintf( __( 'You must be <a href="%s">logged in</a> to post a comment.' ), wp_login_url( apply_filters( 'the_permalink', get_permalink( ) ) ) ) . '</p>',
            'logged_in_as'=>'<p class="logged-in-as">' . sprintf( __( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>' ), wp_myprofile_url(), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) ) ) . '</p>',
    'comment_field' => '<p class="comment-form-comment"><label for="comment">' . _x('Comment', 'noun') . '</label><textarea class="form-control" id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea></p>',
            'class_submit'=>'btn btn-default sign-btn',
    'fields' => apply_filters('comment_form_default_fields', array(
        'author' => '<p class="comment-form-author">' . '<label for="author">' . __('Name', 'domainreference') . '</label> ' . ( $req ? '<span class="required">*</span>' : '' ) .  '<input class="form-control" id="author" name="author" type="text" value="' . esc_attr($commenter['comment_author']) . '" size="30"' . $aria_req . ' /></p>',
        'email' => '<p class="comment-form-email"><label for="email">' . __('Email', 'domainreference') . '</label> ' .  ( $req ? '<span class="required">*</span>' : '' ) . '<input class="form-control" id="email" name="email" type="text" value="' . esc_attr($commenter['comment_author_email']) . '" size="30"' . $aria_req . ' /></p>',
        'url' => '<p class="comment-form-url"><label for="url">' . __('Website', 'domainreference') . '</label>' .'<input class="form-control" id="url" name="url" type="text" value="' . esc_attr($commenter['comment_author_url']) .'" size="30" /></p>'
    ))
);
        
        comment_form($args); ?>

</div><!-- .comments-area -->
