
               <section class="media-section clearfix">
                  <div class="wrapbar wrapbar-top"></div>
                  <div class="wrapbar wrapbar-mid">
                     <h5 class="catname">clubs that have already joined the hub</h5>
                     <div class="mediagallery">
                        <div class="ca-wrapper">
                           
                            <?php echo do_shortcode('[club_latest]'); ?>
                           
                            
                            
                        </div>
                     </div>
                  </div>
                  <div class="wrapbar wrapbar-bot"></div>
               </section>
               
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.contentcarousel0235.js"></script>
      <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.easing.1.30235.js"></script>
      <script>
         jQuery(function($){
         
             $('.mediagallery').contentcarousel({
         
                 // speed for the sliding animation
         
                 sliderSpeed     : 500,
         
                 // easing for the sliding animation
         
                 sliderEasing    : 'easeOutExpo',
         
                 // speed for the item animation (open / close)
         
                 itemSpeed       : 500,
         
                 // easing for the item animation (open / close)
         
                 itemEasing      : 'easeOutExpo',
         
                 // number of items to scroll at a time
         
                 scroll          : 1
         
             });	
         
         });
         
      </script>

          
          
	
      