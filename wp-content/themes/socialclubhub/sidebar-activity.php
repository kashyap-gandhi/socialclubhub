<aside class="col-md-3 col-sm-3">
    
    
    <?php   if(is_user_logged_in()) {  ?>
    
                      <div class="sideBox">
                        <div class="sideHead">
                           <h4>My Dashboard:</h4>
                        </div>
                        <div class="sideContain">
    <?php  wp_nav_menu(array('container'=>'','menu_class' => 'sideNav','theme_location' => 'dashboard_profile_menu')); ?>
    </div></div><br/><br/>
    <?php } ?>
    
    
    <div class="sideBox" style="border: none; margin-bottom: 15px;">
          
        <div class="sideContain"><form name="frmclubsearch" id="frmclubsearch" method="get" action="<?php echo site_url(); ?>/search-clubs">
                
                <input type="text" name="club_text_search" id="club_text_search" placeholder="Search club" class="form-control" />
            
                
                <?php 
          global $wpdb;
   
         $get_term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
               $term_id=0;           
         if(!empty($get_term) && isset($get_term->term_id) && $get_term->term_id>0) {
             
             $term_id=$get_term->term_id;
         }
                ?>
                <input type="hidden" id="search_by_acitivity" name="search_by_acitivity" value="<?php echo $term_id; ?>" />
            </form></div>
    </div>
    
                     <div class="sideBox">
                        <div class="sideHead">
                           <h4>ACTIVITIES:</h4>
                        </div>
                        <div class="sideContain">
                           <ul class="sideNav">
                               
                               <?php $res_activity=get_all_activities();
                               
                               if(!empty($res_activity)) {
                                
                                   foreach($res_activity as $akey=>$aval) {
								   
								   $term_meta = get_option( "taxonomy_".$aval->term_id );
								   
								   if((int) $term_meta['show_to_left']==(int) 1) { 
								   
                                       ?>
                              <li><a href="<?php echo get_term_link($aval->slug,'club-activities'); ?>"><?php echo $aval->name; ?></a></li> 
                                       <?php }
                                   }
                                   
                               } ?>
                               
                             
                           </ul>
                        </div>
                         <?php
                          $socialclubhub = get_option("socialclubhub_theme_config");
                          ?>
                        <div class="sideFoot">
                           <a href="<?php if(isset($socialclubhub['all_activities_page_id'])) { echo get_permalink($socialclubhub['all_activities_page_id']);  }?>">VIEW ALL ACTIVITIES</a>
                        </div>
                     </div>
                  </aside>