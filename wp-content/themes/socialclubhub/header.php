<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Normalize -->
        <link href="<?php echo esc_url(get_template_directory_uri()); ?>/css/normalize.css" rel="stylesheet" >
        <!-- Bootstrap css -->
        <link href="<?php echo esc_url(get_template_directory_uri()); ?>/css/bootstrap.min.css" rel="stylesheet" >
        <!-- Custom Styles -->
        <link href="<?php echo esc_url(get_template_directory_uri()); ?>/css/style.css" rel="stylesheet" >
        <link href="<?php echo esc_url(get_template_directory_uri()); ?>/css/fonts.css" rel="stylesheet" >
        <link rel="stylesheet" id="font-awesome-css" href="<?php echo esc_url(get_template_directory_uri()); ?>/css/font-awesome.min.css?ver=4.5" type="text/css" media="screen">
        <!-- responsive style  -->
        <link href="<?php echo esc_url(get_template_directory_uri()); ?>/css/responsive.css" rel="stylesheet">
        <!-- Google Font -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700' rel='stylesheet' type='text/css'>


        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <!--[if lt IE 9]>
        <script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/html5shiv.js"></script>
        <script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/respond.min.js"></script>
        <![endif]-->


        <script>(function(){document.documentElement.className='js'})();</script>
        <?php wp_head(); ?>

        <script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/bootstrap.min.js"></script>
        <!--        <script src="<?php //echo esc_url( get_template_directory_uri() );   ?>/js/typeahead.min.js"></script>
        
        -->

        <script>var ajaxurl='<?php echo admin_url('admin-ajax.php');   ?>'; </script>
    </head>

    <body>

        <!-- Wrapper Start -->
        <div id="wrapper">

            <?php
            $socialclubhub = get_option("socialclubhub_theme_config");
            ?>

            <!--header-->

            <header>
                <div class="container">
                    <div class="headerTop clearfix">
                        <div class="col-sm-12">
                            <ul class="list-unstyled topSocial">
                                <li><a href="<?php
            if (isset($socialclubhub['facebook_profile_link'])) {
                echo $socialclubhub['facebook_profile_link'];
            }
            ?>"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/fbIcon.png"></a></li>
                                <li><a href="<?php
                                    if (isset($socialclubhub['googleplus_profile_link'])) {
                                        echo $socialclubhub['googleplus_profile_link'];
                                    }
            ?>"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/google+.png"></a></li>
                                <li><a href="<?php
                                       if (isset($socialclubhub['twitter_profile_link'])) {
                                           echo $socialclubhub['twitter_profile_link'];
                                       }
            ?>"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/tweet.png"></a></li>
                            </ul>


                            <?php
                            if (is_user_logged_in()) {
                                wp_nav_menu(array('container' => '', 'menu_class' => 'topNav', 'theme_location' => 'profile_menu'));
                            } else {
                                wp_nav_menu(array('container' => '', 'menu_class' => 'topNav', 'theme_location' => 'register_login_menu'));
                            }
                            ?>


                        </div>
                    </div>
                    <div class="headerBottom">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="logoBlock">
                                    <a href="<?php echo esc_url(home_url('/')); ?>"  rel="home" title="<?php bloginfo('name'); ?>"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo.png" class="img-responsive" alt="<?php bloginfo('name'); ?>"></a>
                                </div>
                                <div class="col-lg-8 col-sm-12 col-md-9 col-xs-12 pull-right">
                                    <div class="clearfix">



                                    <?php wp_nav_menu(array('container' => '', 'menu_class' => 'list-inline pull-right btnGroup', 'theme_location' => 'merchant_deal_menu')); ?>

                                    </div>

                                    <?php
                                    $location_find_msg = '';

                                    $user_search_city_name = '';
                                    $user_search_city_latitude = '';
                                    $user_search_city_longitude = '';
                                    $user_search_radius = 0;


                                    if (isset($_POST['changeuserlocation'])) {

                                        $current_search_location = '';
                                        if (isset($_POST['current_location']) && trim(strip_tags($_POST['current_location']))) {
                                            $current_search_location = trim(strip_tags($_POST['current_location']));
                                        }
                                        
                                        $current_search_country_location = '';
                                        if (isset($_POST['current_country_location']) && trim(strip_tags($_POST['current_country_location']))) {
                                            $current_search_country_location = trim(strip_tags($_POST['current_country_location']));
                                        }
                                        
                                        $current_search_state_location = '';
                                        if (isset($_POST['current_state_location']) && trim(strip_tags($_POST['current_state_location']))) {
                                            $current_search_state_location = trim(strip_tags($_POST['current_state_location']));
                                        }
                                        
                                        $current_search_lat_location = '';
                                        if (isset($_POST['current_lat_location']) && trim(strip_tags($_POST['current_lat_location']))) {
                                            $current_search_lat_location = trim(strip_tags($_POST['current_lat_location']));
                                        }
                                        $current_search_long_location = '';
                                        if (isset($_POST['current_long_location']) && trim(strip_tags($_POST['current_long_location']))) {
                                            $current_search_long_location = trim(strip_tags($_POST['current_long_location']));
                                        }

                                        $current_search_radius = 0;
                                        if (isset($_POST['user_search_radius']) && (int) $_POST['user_search_radius'] >= 0) {
                                            $current_search_radius = (int) $_POST['user_search_radius'];
                                        }

                                        if ($current_search_location != '') {

                                            $exp_loc = explode(',', $current_search_location);

                                            if (isset($exp_loc[0]) && trim($exp_loc[0]) != '') {

                                                //$get_search_city = get_city_by_name($exp_loc[0]);
                                                
                                                
                                                
                                                $get_search_city = get_search_city_by_name($exp_loc[0],$current_search_state_location,$current_search_country_location,$current_search_lat_location,$current_search_long_location);

                                                if (!empty($get_search_city)) {
                                                    
                                                    

                                                    $user_search_city_name = $get_search_city->City;

                                                    $user_search_city_latitude = $get_search_city->Latitude;
                                                    $user_search_city_longitude = $get_search_city->Longitude;

                                                    $user_search_radius = $current_search_radius;

                                                    $user_search_state_id = $get_search_city->RegionID;
                                                    $user_search_country_id = $get_search_city->CountryID;


                                                    setcookie('GASCHRMIL', encrypt_decrypt_cookie('encrypt', $user_search_radius), time() + (86400 * 3), "/", "", false, true);
                                                    setcookie('GASCHRLAT', encrypt_decrypt_cookie('encrypt', $user_search_city_latitude), time() + (86400 * 3), "/", "", false, true);
                                                    setcookie('GASCHRLONG', encrypt_decrypt_cookie('encrypt', $user_search_city_longitude), time() + (86400 * 3), "/", "", false, true);
                                                    setcookie('GASCHRCT', encrypt_decrypt_cookie('encrypt', $user_search_city_name), time() + (86400 * 3), "/", "", false, true);
                                                    setcookie('GASCHRST', encrypt_decrypt_cookie('encrypt', $user_search_state_id), time() + (86400 * 3), "/", "", false, true);
                                                    setcookie('GASCHRCNT', encrypt_decrypt_cookie('encrypt', $user_search_country_id), time() + (86400 * 3), "/", "", false, true);
                                                    
                                                    
                                                    
                                                    
        $get_term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
                          //echo "<pre>";    print_r($get_term);die;
                          
         if(!empty($get_term) && isset($get_term->term_id) && $get_term->term_id>0) { 
             echo "<script>window.location.href='".site_url()."/search-clubs/?search_by_acitivity=".$get_term->term_id."';</script>";
         } else {
              echo "<script>window.location.href=window.location.href;</script>";
         }
                                                    
                                                    
                                                    
                                                    
                                                   
                                                   
                                                    
                                                } else {
                                                    $location_find_msg = 'not';
                                                }
                                            }
                                        }
                                    }

                                    if (isset($_POST['changeuserlocation2'])) {

                                        $current_search_location = '';
                                        if (isset($_POST['current_location2']) && trim(strip_tags($_POST['current_location2']))) {
                                            $current_search_location = trim(strip_tags($_POST['current_location2']));
                                        }
                                        
                                        $current_search_country_location = '';
                                        if (isset($_POST['current_country_location2']) && trim(strip_tags($_POST['current_country_location2']))) {
                                            $current_search_country_location = trim(strip_tags($_POST['current_country_location2']));
                                        }
                                        
                                        $current_search_state_location = '';
                                        if (isset($_POST['current_state_location2']) && trim(strip_tags($_POST['current_state_location2']))) {
                                            $current_search_state_location = trim(strip_tags($_POST['current_state_location2']));
                                        }
                                        
                                        $current_search_lat_location = '';
                                        if (isset($_POST['current_lat_location2']) && trim(strip_tags($_POST['current_lat_location2']))) {
                                            $current_search_lat_location = trim(strip_tags($_POST['current_lat_location2']));
                                        }
                                        $current_search_long_location = '';
                                        if (isset($_POST['current_long_location2']) && trim(strip_tags($_POST['current_long_location2']))) {
                                            $current_search_long_location = trim(strip_tags($_POST['current_long_location2']));
                                        }

                                        $current_search_radius = 0;
                                        if (isset($_POST['user_search_radius2']) && (int) $_POST['user_search_radius2'] >= 0) {
                                            $current_search_radius = (int) $_POST['user_search_radius2'];
                                        }

                                        if ($current_search_location != '') {

                                            $exp_loc = explode(',', $current_search_location);

                                            if (isset($exp_loc[0]) && trim($exp_loc[0]) != '') {

                                                //$get_search_city = get_city_by_name($exp_loc[0]);
                                                
                                                $get_search_city = get_search_city_by_name($exp_loc[0],$current_search_state_location,$current_search_country_location,$current_search_lat_location,$current_search_long_location);

                                                if (!empty($get_search_city)) {

                                                    $user_search_city_name = $get_search_city->City;

                                                    $user_search_city_latitude = $get_search_city->Latitude;
                                                    $user_search_city_longitude = $get_search_city->Longitude;

                                                    $user_search_radius = $current_search_radius;

                                                    $user_search_state_id = $get_search_city->RegionID;
                                                    $user_search_country_id = $get_search_city->CountryID;


                                                    setcookie('GASCHRMIL', encrypt_decrypt_cookie('encrypt', $user_search_radius), time() + (86400 * 3), "/", "", false, true);
                                                    setcookie('GASCHRLAT', encrypt_decrypt_cookie('encrypt', $user_search_city_latitude), time() + (86400 * 3), "/", "", false, true);
                                                    setcookie('GASCHRLONG', encrypt_decrypt_cookie('encrypt', $user_search_city_longitude), time() + (86400 * 3), "/", "", false, true);
                                                    setcookie('GASCHRCT', encrypt_decrypt_cookie('encrypt', $user_search_city_name), time() + (86400 * 3), "/", "", false, true);
                                                    setcookie('GASCHRST', encrypt_decrypt_cookie('encrypt', $user_search_state_id), time() + (86400 * 3), "/", "", false, true);
                                                    setcookie('GASCHRCNT', encrypt_decrypt_cookie('encrypt', $user_search_country_id), time() + (86400 * 3), "/", "", false, true);
                                                    
                                                     $get_term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
                          //echo "<pre>";    print_r($get_term);die;
                          
         if(!empty($get_term) && isset($get_term->term_id) && $get_term->term_id>0) { 
             echo "<script>window.location.href='".site_url()."/search-clubs/?search_by_acitivity=".$get_term->term_id."';</script>";
         } else {
                echo "<script>window.location.href=window.location.href;</script>";

                }
                                                    
                                                } else {
                                                    $location_find_msg = 'not';
                                                }
                                            }
                                        }
                                    }
                                    
                                    
                                    
                                    
                                    //==track city==

                                    $current_city = '';
                                    if (isset($_COOKIE['GASCHCT'])) {
                                        $current_city = trim(encrypt_decrypt_cookie('decrypt', $_COOKIE['GASCHCT']));
                                    }
                                    $current_state = '';
                                    if (isset($_COOKIE['GASCHST'])) {
                                        $current_state = trim(encrypt_decrypt_cookie('decrypt', $_COOKIE['GASCHST']));
                                    }

                                    
                                    //==ovrride track city by user db city==
                                    $user_db_city_name = '';
                                    $user_db_state_name = '';

                                    if (is_user_logged_in()) {

                                        global $current_user;

                                        $user = $current_user;

                                        $user_state_id = esc_attr(get_the_author_meta('user_state_id', $user->ID));


                                        $get_state_detail = get_state_by_id($user_state_id);
                                        if (!empty($get_state_detail)) {
                                            $user_db_state_name = $get_state_detail->region;
                                        }

                                        $user_city_id = esc_attr(get_the_author_meta('user_city_id', $user->ID));


                                        $get_city_detail = get_city_by_id($user_city_id);
                                        if (!empty($get_city_detail)) {
                                            $user_db_city_name = $get_city_detail->City;
                                        }
                                    }

                                    ///==city setting
                                    if ($user_db_city_name != '') {
                                        $current_city = $user_db_city_name;
                                    }
                                    
                                    //==ovrride user db city by user search city==
                                    if($user_search_city_name!=''){
                                        $current_city=$user_search_city_name;
                                    } else {
                                    
                                        $search_cache_city='';
                                        if (isset($_COOKIE['GASCHRCT'])) {
                                             $search_cache_city = trim(encrypt_decrypt_cookie('decrypt', $_COOKIE['GASCHRCT'])); 
                                        }

                                        if($search_cache_city!=''){
                                            $current_city=$search_cache_city;
                                        } 
                                    }
                                    
                                    
                                    //==ovrride if city is blank 
                                    if($current_city==''){
                                        if(isset($socialclubhub['system_city_id']) && (int) $socialclubhub['system_city_id']>0){
                                            
                                            $get_city_detail = get_city_by_id($socialclubhub['system_city_id']);
                                            
                                            if (!empty($get_city_detail)) {
                                                $current_city = $get_city_detail->City;
                                            }
                                            
                                        }
                                    }
                                    
                                    
                                    
                                    //==radius setting
                                    
                                    $user_search_by_radius=100;
                                    
                                    
                                    if($user_search_radius>0){
                                        $user_search_by_radius=$user_search_radius;
                                    } else {
                                        $search_cache_radius=0;
                                        if (isset($_COOKIE['GASCHRMIL'])) {
                                             $search_cache_radius = (int) trim(encrypt_decrypt_cookie('decrypt', $_COOKIE['GASCHRMIL'])); 
                                        }


                                        if($search_cache_radius>0){
                                            $user_search_by_radius=$search_cache_radius;
                                        }
                                    }
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    ?>

<?php if ($location_find_msg == 'not') { ?><p style="color: red;">Unable to find search city.</p><?php } ?>
                                    <div class="clearfix setpBox col-sm-12 pull-right">

                                        <form name="frmuserlocation" id="frmuserlocation" action="" method="post">                               
                                            <p class="pull-left">Step 1. Go to Activities | Step 2. My Location/Destination</p>
                                            <input type="text" name="current_location" id="current_location" placeholder="Enter City" value="<?php echo $current_city; ?>" class="locationBox" />
                                            <input type="hidden" name="current_state_location" id="current_state_location" />
                                            <input type="hidden" name="current_country_location" id="current_country_location" />
                                            <input type="hidden" name="current_lat_location" id="current_lat_location" />
                                            <input type="hidden" name="current_long_location" id="current_long_location" />
                                            
                                            
                                            <p class="pull-left">| Step 3.Search Radius</p>
                                            <select class="searchRadius" name="user_search_radius" id="user_search_radius">
                                                <option value="0">In Radius</option>                            
                                                <option value="10" <?php if ($user_search_by_radius == 10) { ?> selected <?php } ?>>10 miles</option>
                                                <option value="20" <?php if ($user_search_by_radius == 20) { ?> selected <?php } ?>>20 miles</option>
                                                <option value="30" <?php if ($user_search_by_radius == 30) { ?> selected <?php } ?>>30 miles</option>
                                                <option value="50" <?php if ($user_search_by_radius == 50) { ?> selected <?php } ?>>50 miles</option>
                                                <option value="100" <?php if ($user_search_by_radius == 100) { ?> selected <?php } ?>>100 miles</option>
                                            </select>
                                            <button class="btn btn-default sign-btn" style="padding: 3px 12px;" name="changeuserlocation" id="changeuserlocation">GO</button>
                                        </form>
                                    </div>

                                    <div class="clearfix setpBoxMobile visible-xs">
                                        <form name="frmuserlocation2" id="frmuserlocation2" action="" method="post">
                                            <p>Step 1. Go to Activities</p>
                                            <p>Step 2. My Location/Destination</p>
                                            <div class="form-group">
                                                <input type="text" name="current_location2" id="current_location2" placeholder="Enter City" value="<?php echo $current_city; ?>" class="locationBox form-control" />
                                                 <input type="hidden" name="current_state_location2" id="current_state_location2" />
                                            <input type="hidden" name="current_country_location2" id="current_country_location2" />
                                            <input type="hidden" name="current_lat_location2" id="current_lat_location2" />
                                            <input type="hidden" name="current_long_location2" id="current_long_location2" />
                                            
                                            </div>
                                            <p>Step 3.Search Radius</p>
                                            <div class="form-group">
                                                <select class="form-control" name="user_search_radius2" id="user_search_radius2">
                                                    <option value="0">In Radius</option>                            
                                                    <option value="10" <?php if ($user_search_by_radius == 10) { ?> selected <?php } ?>>10 miles</option>
                                                    <option value="20" <?php if ($user_search_by_radius == 20) { ?> selected <?php } ?>>20 miles</option>
                                                    <option value="30" <?php if ($user_search_by_radius == 30) { ?> selected <?php } ?>>30 miles</option>
                                                    <option value="50" <?php if ($user_search_by_radius == 50) { ?> selected <?php } ?>>50 miles</option>
                                                    <option value="100" <?php if ($user_search_by_radius == 100) { ?> selected <?php } ?>>100 miles</option>
                                                </select>
                                            </div>
                                            <button class="btn btn-default sign-btn" name="changeuserlocation2" id="changeuserlocation2">GO</button>
                                        </form>
                                    </div>



<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script>
google.load("maps", "3.x", {callback: cinitialize, other_params:'sensor=false&libraries=places'});

		
                
jQuery(window).resize(function() {
   cinitialize(); 
});
    function cinitialize() {
        
        
        var input = document.getElementById('current_location');

        if(jQuery("div.setpBoxMobile").is(':visible')){
              var input = document.getElementById('current_location2');
        } else {
              var input = document.getElementById('current_location');
        }

        var autocomplete = new google.maps.places.Autocomplete(input, { types: ['(cities)']});
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
			console.log(place.formatted_address);
            if (!place.geometry) {
                console.log('no location');
                return;
            } else {
                
                var lat=place.geometry.location.lat();
                var lng=place.geometry.location.lng();
                                        
                                        jQuery("#current_lat_location").val(lat);
                                        jQuery("#current_lat_location2").val(lat);
                                        
                                        jQuery("#current_long_location").val(lng);
                                        jQuery("#current_long_location2").val(lng);
                                        
                                        
                                        
					jQuery("#current_location").val(place.name);
                                        jQuery("#current_location2").val(place.name);
					console.log(jQuery("#current_location2").val());
					
					var foradd=place.formatted_address;
					var expc=foradd.split(',');
					
					if(expc[1]!='' && expc[1]!=undefined && expc[1]!=null) {
						jQuery("#current_state_location").val(jQuery.trim(expc[1]).replace(/[0-9]/g, ''));
                                                jQuery("#current_state_location2").val(jQuery.trim(expc[1]).replace(/[0-9]/g, ''));
					}
					
					
			}
			
            //console.log(place.geometry.location);
        });
        
        
        
    }
		
</script>                


<!--     <script>
jQuery(document).ready(function(){



jQuery('input#current_location').typeahead({
name: 'country',
remote : '<?php //echo admin_url('admin-ajax.php');   ?>?action=getPopulatecity&search_city=%QUERY'

});



});
</script>
                                    -->




                                </div>
                            </div>
                        </div>
                        <!--<div class="form-group col-sm-12 visible-sm visible-xs">
                        <select class="form-control">
                        <option>home</option>
                        <option>activities</option>
                        <option>events</option>
                        <option>message board</option>
                        <option>blog</option>
                        <option>Free Membership</option>
                        </select>
                        </div>-->
                        <div class="clearfix">
                            <div class="col-sm-12">
                                <div class="nav_date pull-left">
                                    <script type="text/javascript">
                                        <!--
                                        var mydate=new Date()

                                        var day=mydate.getDay()

                                        var month=mydate.getMonth()

                                        var daym=mydate.getDate()

                                        if (daym<10)

                                        daym="0"+daym

                                    var montharray=new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec")

                                    document.write("<span class='gabfire_day'>"+daym+"</span><span class='gabfire_month'>"+montharray[month]+"</span>")

                                    // -->

                                    </script>
                                </div>


                                <nav class="navbar navbar-default">
                                    <div class="container-fluid">
                                        <!-- Brand and toggle get grouped for better mobile display -->
                                        <div class="navbar-header">
                                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                                <span class="sr-only">Toggle navigation</span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                        </div>
                                        <!-- Collect the nav links, forms, and other content for toggling -->
                                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">


<?php wp_nav_menu(array('container' => '', 'menu_class' => 'nav navbar-nav pull-right', 'theme_location' => 'main_menu')); ?>

                                        </div>
                                        <!-- /.navbar-collapse -->
                                    </div>
                                    <!-- /.container-fluid -->
                                </nav>



                                <section id="bootstartCarousel" class="carousel slide">
                                    <div class="carousel-inner">

<?php echo do_shortcode('[news_latest]'); ?>

                                    </div>
                                    <a class="left carousel-control" href="#bootstartCarousel" data-slide="prev">&lsaquo;</a>
                                    <a class="right carousel-control" href="#bootstartCarousel" data-slide="next">&rsaquo;</a>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

            <!--end header-->

            <div class="container">

                <?php
                if (is_front_page()) {

                    /* ?>

                      <div class="bannerWrap clearfix">
                      <div class="col-sm-12">
                      <section class="featured-cycle">
                      <div class="cycle-slideshow" data-cycle-fx="fade" data-cycle-timeout="10000" data-cycle-slides="> div" data-cycle-pager="#no-template-pager" data-cycle-pager-template="">
                      <div>
                      <div class="slides-big-left pull-left">
                      <a href="#" rel="bookmark">
                      <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/slide1A.png" class="aligncenter" alt="Occupy Wall Street driven by intellectual children" title="Occupy Wall Street driven by intellectual children"/>
                      </a>
                      <div class="slider-caption">
                      <h2 class="fea_posttitle">
                      <a href="#" rel="bookmark" title="">Lorem Ipsum </a>
                      </h2>
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                      </div>
                      </div>
                      <div class="slides-big-right pull-right">
                      <div class="slides-right-top pull-left">
                      <a href="#" rel="bookmark"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/slide1B.png" class="aligncenter" width="100" alt="" title=""/></a>
                      <div class="slider-caption">
                      <h2 class="fea_posttitle">
                      <a href="#" rel="bookmark" title="">Lorem Ipsum is simply dummy</a>
                      </h2>
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                      </div>
                      </div>
                      <div class="slides-right-bottom pull-left">
                      <div class="slides-right-bottom-left pull-left">
                      <a href="#" rel="bookmark"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/slide1C.png" class="aligncenter" alt="" title=""/></a>
                      <div class="slider-caption">
                      <h2 class="fea_posttitle">
                      <a href="#" rel="bookmark" title="">Lorem Ipsum is simply dummy text</a>
                      </h2>
                      </div>
                      </div>
                      <div class="slides-right-bottom-right pull-right">
                      <a href="#" rel="bookmark">
                      <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/slide1D.png" class="aligncenter" alt="" title=""/>
                      </a>
                      <div class="slider-caption">
                      <h2 class="fea_posttitle"><a href="#" rel="bookmark" title="">Lorem Ipsum is simply dummy text</a></h2>
                      </div>
                      </div>
                      </div>
                      </div>
                      </div>
                      <div>
                      <div class="slides-big-left pull-left">
                      <a href="#" rel="bookmark">
                      <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/slide1A.png" class="aligncenter" alt="Occupy Wall Street driven by intellectual children" title="Occupy Wall Street driven by intellectual children"/>
                      </a>
                      <div class="slider-caption">
                      <h2 class="fea_posttitle">
                      <a href="#" rel="bookmark" title="">Lorem Ipsum </a>
                      </h2>
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                      </div>
                      </div>
                      <div class="slides-big-right pull-right">
                      <div class="slides-right-top pull-left">
                      <a href="#" rel="bookmark"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/slide1B.png" class="aligncenter" width="100" alt="" title=""/></a>
                      <div class="slider-caption">
                      <h2 class="fea_posttitle">
                      <a href="#" rel="bookmark" title="">Lorem Ipsum is simply dummy</a>
                      </h2>
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                      </div>
                      </div>
                      <div class="slides-right-bottom pull-left">
                      <div class="slides-right-bottom-left pull-left">
                      <a href="#" rel="bookmark"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/slide1C.png" class="aligncenter" alt="" title=""/></a>
                      <div class="slider-caption">
                      <h2 class="fea_posttitle">
                      <a href="#" rel="bookmark" title="">Lorem Ipsum is simply dummy text</a>
                      </h2>
                      </div>
                      </div>
                      <div class="slides-right-bottom-right pull-right">
                      <a href="#" rel="bookmark">
                      <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/slide1D.png" class="aligncenter" alt="" title=""/>
                      </a>
                      <div class="slider-caption">
                      <h2 class="fea_posttitle"><a href="#" rel="bookmark" title="">Lorem Ipsum is simply dummy text</a></h2>
                      </div>
                      </div>
                      </div>
                      </div>
                      </div>
                      <div>
                      <div class="slides-big-left pull-left">
                      <a href="#" rel="bookmark">
                      <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/slide1A.png" class="aligncenter" alt="Occupy Wall Street driven by intellectual children" title="Occupy Wall Street driven by intellectual children"/>
                      </a>
                      <div class="slider-caption">
                      <h2 class="fea_posttitle">
                      <a href="#" rel="bookmark" title="">Lorem Ipsum </a>
                      </h2>
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                      </div>
                      </div>
                      <div class="slides-big-right pull-right">
                      <div class="slides-right-top pull-left">
                      <a href="#" rel="bookmark"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/slide1B.png" class="aligncenter" width="100" alt="" title=""/></a>
                      <div class="slider-caption">
                      <h2 class="fea_posttitle">
                      <a href="#" rel="bookmark" title="">Lorem Ipsum is simply dummy</a>
                      </h2>
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                      </div>
                      </div>
                      <div class="slides-right-bottom pull-left">
                      <div class="slides-right-bottom-left pull-left">
                      <a href="#" rel="bookmark"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/slide1C.png" class="aligncenter" alt="" title=""/></a>
                      <div class="slider-caption">
                      <h2 class="fea_posttitle">
                      <a href="#" rel="bookmark" title="">Lorem Ipsum is simply dummy text</a>
                      </h2>
                      </div>
                      </div>
                      <div class="slides-right-bottom-right pull-right">
                      <a href="#" rel="bookmark">
                      <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/slide1D.png" class="aligncenter" alt="" title=""/>
                      </a>
                      <div class="slider-caption">
                      <h2 class="fea_posttitle"><a href="#" rel="bookmark" title="">Lorem Ipsum is simply dummy text</a></h2>
                      </div>
                      </div>
                      </div>
                      </div>
                      </div>
                      <div>
                      <div class="slides-big-left pull-left">
                      <a href="#" rel="bookmark">
                      <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/slide1A.png" class="aligncenter" alt="Occupy Wall Street driven by intellectual children" title="Occupy Wall Street driven by intellectual children"/>
                      </a>
                      <div class="slider-caption">
                      <h2 class="fea_posttitle">
                      <a href="#" rel="bookmark" title="">Lorem Ipsum </a>
                      </h2>
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                      </div>
                      </div>
                      <div class="slides-big-right pull-right">
                      <div class="slides-right-top pull-left">
                      <a href="#" rel="bookmark"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/slide1B.png" class="aligncenter" width="100" alt="" title=""/></a>
                      <div class="slider-caption">
                      <h2 class="fea_posttitle">
                      <a href="#" rel="bookmark" title="">Lorem Ipsum is simply dummy</a>
                      </h2>
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                      </div>
                      </div>
                      <div class="slides-right-bottom pull-left">
                      <div class="slides-right-bottom-left pull-left">
                      <a href="#" rel="bookmark"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/slide1C.png" class="aligncenter" alt="" title=""/></a>
                      <div class="slider-caption">
                      <h2 class="fea_posttitle">
                      <a href="#" rel="bookmark" title="">Lorem Ipsum is simply dummy text</a>
                      </h2>
                      </div>
                      </div>
                      <div class="slides-right-bottom-right pull-right">
                      <a href="#" rel="bookmark">
                      <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/slide1D.png" class="aligncenter" alt="" title=""/>
                      </a>
                      <div class="slider-caption">
                      <h2 class="fea_posttitle"><a href="#" rel="bookmark" title="">Lorem Ipsum is simply dummy text</a></h2>
                      </div>
                      </div>
                      </div>
                      </div>
                      </div>
                      <div>
                      <div class="slides-big-left pull-left">
                      <a href="#" rel="bookmark">
                      <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/slide1A.png" class="aligncenter" alt="Occupy Wall Street driven by intellectual children" title="Occupy Wall Street driven by intellectual children"/>
                      </a>
                      <div class="slider-caption">
                      <h2 class="fea_posttitle">
                      <a href="#" rel="bookmark" title="">Lorem Ipsum </a>
                      </h2>
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                      </div>
                      </div>
                      <div class="slides-big-right pull-right">
                      <div class="slides-right-top pull-left">
                      <a href="#" rel="bookmark"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/slide1B.png" class="aligncenter" width="100" alt="" title=""/></a>
                      <div class="slider-caption">
                      <h2 class="fea_posttitle">
                      <a href="#" rel="bookmark" title="">Lorem Ipsum is simply dummy</a>
                      </h2>
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                      </div>
                      </div>
                      <div class="slides-right-bottom pull-left">
                      <div class="slides-right-bottom-left pull-left">
                      <a href="#" rel="bookmark"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/slide1C.png" class="aligncenter" alt="" title=""/></a>
                      <div class="slider-caption">
                      <h2 class="fea_posttitle">
                      <a href="#" rel="bookmark" title="">Lorem Ipsum is simply dummy text</a>
                      </h2>
                      </div>
                      </div>
                      <div class="slides-right-bottom-right pull-right">
                      <a href="#" rel="bookmark">
                      <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/slide1D.png" class="aligncenter" alt="" title=""/>
                      </a>
                      <div class="slider-caption">
                      <h2 class="fea_posttitle"><a href="#" rel="bookmark" title="">Lorem Ipsum is simply dummy text</a></h2>
                      </div>
                      </div>
                      </div>
                      </div>
                      </div>
                      </div>
                      <div id="no-template-pager" class="cycle-pager external">
                      <a href='#' class='featured_pager'>Hobby</a>
                      <a href='#' class='featured_pager'>Bike/Cycling</a>
                      <a href='#' class='featured_pager'>Photography</a>
                      <a href='#' class='featured_pager'>Dancing</a>
                      <a href='#' class='featured_pager last-cycle-pager'>Cooking</a>
                      </div>
                      </section>
                      </div>
                      </div>
                      <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.cycle2.min0235.js"></script>
                      <?php */
                    ?>
                    <div class="bannerWrap clearfix">
                        <div class="col-sm-12">
                            <section class="featured-cycle">
    <?php
    if (isset($socialclubhub['homepage_slider_code']) && $socialclubhub['homepage_slider_code'] != '') {
        echo do_shortcode(stripslashes($socialclubhub['homepage_slider_code']));
    }
    ?>

                            </section>
                        </div>
                    </div>


<?php } ?>

                <div class="containArea clearfix">


