
 </div><!--contentarea-->
 </div><!--container-->
 </div> <!-- Wrapper End -->
      
      <footer>
         <div class="footTop clearfix">
            <div class="container">
               <div class="row">
                  <div class="col-sm-4">
                     <h4>Quick Links</h4>
                     <?php wp_nav_menu(array('container'=>'','menu_class' => 'footNav','theme_location' => 'footer_link_menu')); ?>
                     
                     
                     
                  </div>
                  <div class="col-sm-4">
                     <h4>Contact Us</h4>
                     <p>social Club Hub, Inc.</p>
                     <p>Palm Beach Gardens, Florida</p>
                     <p>Email: info@socialclubhub.com</p>
                     <div class="newsletterBlock">
                        <p>register For Our Newsletter:</p>
                        <div class="form-group">
                           <input type="text" class="form-control" placeholder="">
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-4">
                     <h4>Links</h4>
<!--                     <a href="#" class="footbtn">join now - individual</a><br><br><br>
                     <a href="#" class="footbtn">join now - clubs only</a>-->
                     
                     <?php wp_nav_menu(array('container'=>'','menu_class' => '','theme_location' => 'footer_signup_menu')); ?>
                     
                  </div>
               </div>
            </div>
         </div>
         <div class="footbottom">
            <div class="container">
               <div class="row">
                  <div class="col-sm-12 text-center">
                     <p>All copyright reserved &copy; 2015 SOCIAL CLUB HUB</p>
                  </div>
               </div>
            </div>
         </div>
      </footer>
      
      
<?php wp_footer(); ?>

</body>
</html>
