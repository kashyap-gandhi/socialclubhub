<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */


get_header(); ?>

		


        	<div class="blogDetailPage clearfix">

            	<div class="col-md-8 col-sm-12">
                <?php
		// Start the loop.
		while ( have_posts() ) : the_post();

                
                 $post_id = get_the_ID(); 
                 
                  $thumb_src= esc_url( get_template_directory_uri() ).'/images/default_image.jpg';
            $attach_id = get_post_thumbnail_id($post_id);
            if($attach_id>0){
                $image_url = wp_get_attachment_image_src( $attach_id, 'medium' );
                if(isset($image_url[0]) && $image_url[0]!=''){
                    $thumb_src=$image_url[0];
                }
            }
                 
    $club_first_name = get_post_meta($post_id, 'club_first_name', TRUE);
    $club_last_name = get_post_meta($post_id, 'club_last_name', TRUE);

    $club_email = get_post_meta($post_id, 'club_email', TRUE);
    $club_phone_number = get_post_meta($post_id, 'club_phone_number', TRUE);

    $club_no_of_members = get_post_meta($post_id, 'club_no_of_members', TRUE);
    $club_event_host = get_post_meta($post_id, 'club_event_host', TRUE);
    $club_website = get_post_meta($post_id, 'club_website', TRUE);

    $club_user_id = get_post_meta($post_id, 'club_user_id', TRUE);

    $club_address = get_post_meta($post_id, 'club_adderss', TRUE);
    
    $club_zip_code = get_post_meta($post_id, 'club_zip_code', TRUE);
    
    $club_city_id = get_post_meta($post_id, 'club_city_id', TRUE);
    
    $city_name='';    
    $get_city_detail=get_city_by_id($club_city_id);    
    if(!empty($get_city_detail)){
        $city_name=$get_city_detail->City;
    }
    
    
    $club_state_id = get_post_meta($post_id, 'club_state_id', TRUE);
    
    
    $state_name='';    
    $get_state_detail=get_state_by_id($club_state_id);    
    if(!empty($get_state_detail)){
        $state_name=$get_state_detail->region;
    }
    
    
    $club_country_id = get_post_meta($post_id, 'club_country_id', TRUE);
    
    
    $country_name='';    
    $get_country_detail=get_country_by_id($club_country_id);    
    if(!empty($get_country_detail)){
        $country_name=$get_country_detail->country;
    }
    
    
    
    $club_lat = get_post_meta($post_id, 'club_lat', TRUE);
    $club_long = get_post_meta($post_id, 'club_long', TRUE);

    
    $club_main_activity = get_post_meta($post_id, 'club_main_activity', TRUE);
    

    $get_other_activity = get_post_meta($post_id, 'club_other_activities', TRUE);

    if ($get_other_activity != '') {
        $club_other_activity = explode(',', $get_other_activity);
    }
           
    
    $user_lat='';
    
    if(isset($_COOKIE['GASCHLAT'])){ $user_lat= encrypt_decrypt_cookie('decrypt',$_COOKIE['GASCHLAT']); } 
    
    $user_long='';
    
    if(isset($_COOKIE['GASCHLONG'])){ $user_long= encrypt_decrypt_cookie('decrypt',$_COOKIE['GASCHLONG']); } 
    
    
    $socialclubhub = get_option("socialclubhub_theme_config");
    
    
    $system_lat=25.761679800000;
    $system_long=-80.191790200000;
    
    if(isset($socialclubhub['system_lat']) && trim($socialclubhub['system_lat'])!=''){
        $system_lat=trim($socialclubhub['system_lat']);
    }
    
    
    if(isset($socialclubhub['system_long']) && trim($socialclubhub['system_long'])!=''){
        $system_long=trim($socialclubhub['system_long']);
    }
    
                 
                 ?>
                    
                	<h3 class="blogTitle"><?php echo the_title(); ?></h3>

                    <div class="blogDetailBlock">
                        
                         <div class="col-sm-12 pull-right text-right">
                        <?php if (function_exists('wpfp_link')) { wpfp_link(); } ?>
                            </div>

                    	<div class="blogImg clearfix ">

                          
                            
                            <img src="<?php echo $thumb_src; ?>" class="img-responsive">

                        </div>
                       
                        
                        <?php nl2br(the_content()); ?>

                    </div>

                    

                        <div class="row" style="clear: both;">

                    	<div class="mapBloc clearfix">

                    		<div class="col-md-6 col-sm-6">

                                <div class="blogDetailRightBLock">

                                    <h3>address</h3>

                                </div>

                                <p>

                                	<strong><?php the_title(); ?></strong><br>

                                    <?php echo $club_address; ?><br><br>
                                       <?php echo $city_name.', '.$state_name.' - '.$club_zip_code;?><br>
                                       <?php echo $country_name; ?><br/>
                                    <?php echo $club_phone_number;?><br>

                                    <a href="<?php echo $club_website;?>"><?php echo $club_website; ?></a>

                                </p>

                                
                                
                               <!---map show-->
                               <script>
           
     
                        function rad(x) {
                            return x * Math.PI / 180;
                        };

                        function getDistance(system_lat,system_long,store_lat,store_long) {
                            // alert(system_lat+","+system_long+","+store_lat+","+store_long);
                            //var R = 6378137; // Earth’s mean radius in meter
                            var R = 3961.3; // Earth’s mean radius in kilometer
                            var dLat = rad(store_lat - system_lat);
                            var dLong = rad(store_long - system_long);
                            var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                                Math.cos(rad(system_lat)) * Math.cos(rad(store_lat)) *
                                Math.sin(dLong / 2) * Math.sin(dLong / 2);
                            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                            var d = R * c;
                            return d.toFixed(2); // returns the distance in meter
                        }
                        
                        
    //var google_key='AIzaSyAE3Vx4HJoGXk2AEF0WA6nlUy_c_L_1fKY';
  var google_key='';
  
    var placeSearch, autocomplete,map, infowindow, service;
    var marker,geocoder;


    var gmarkers = []; 
    var ib = [];
    
    var directionDisplay;
  var directionsService;
  
  var origin = null;
  var destination = null;
  var waypoints = [];
  



    var system_lat='<?php echo $system_lat; ?>';
    var system_long='<?php echo $system_long; ?>';


    var store_lat =   '<?php echo $club_lat; ?>';
    var store_long =    '<?php echo $club_long; ?>';
    
   

    var vlat='<?php echo $user_lat; ?>';
    var vlon='<?php echo $user_long; ?>';

    
    if(vlat!='' && vlon!=''){
        
        var distance=getDistance(system_lat,system_long,vlat,vlon);
        console.log('distance:'+distance);
        if(distance<=1000){
            system_lat=vlat;
            system_long=vlon; 
        }
    }
    



function loadScript()
{
    if ( typeof map == 'undefined') {
        
        var script1 = document.createElement('script');
        script1.type = 'text/javascript';
        if(google_key!=''){
        script1.src = 'http://www.google.com/jsapi?key='+google_key;
        } else {
         script1.src = 'http://www.google.com/jsapi';
        }
        document.body.appendChild(script1);
        
        var script = document.createElement('script');
        script.type = 'text/javascript';
        if(google_key!=''){
            script.src = 'http://maps.google.com/maps/api/js?key='+google_key+'&v=3.exp&sensor=false&callback=initmap';
        } else {
            script.src = 'http://maps.google.com/maps/api/js?v=3.exp&sensor=false&callback=initialize';
        }
        document.body.appendChild(script);
        
        
       
    } 
}



    function initialize() {
      //loadScript2(icon_path+'js/infobox.js',function(){});
    
    geocoder = new google.maps.Geocoder();
    directionsDisplay = new google.maps.DirectionsRenderer();
    directionsService = new google.maps.DirectionsService();
    
    var systemLatlng = new google.maps.LatLng( store_lat, store_long);
    var myOptions = {
      zoom:13,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      center: systemLatlng
    }
    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
    directionsDisplay.setMap(map);
    directionsDisplay.setPanel(document.getElementById("directionsPanel"));
    
    
    origin= new google.maps.LatLng( store_lat, store_long);
      gmarkers.push(new google.maps.Marker({
        position:origin, 
        map: map,
        icon: "http://maps.google.com/mapfiles/marker" + String.fromCharCode(gmarkers.length + 65) + ".png",
        content:'sdsdsdsd'
      }));
    
    destination= new google.maps.LatLng( system_lat, system_long);
      gmarkers.push(new google.maps.Marker({
        position: destination, 
        map: map,
        icon: "http://maps.google.com/mapfiles/marker" + String.fromCharCode(gmarkers.length + 65) + ".png"
      }));
    
    
    var mode;
    //mode = google.maps.DirectionsTravelMode.BICYCLING;
    mode = google.maps.DirectionsTravelMode.DRIVING;
    //mode = google.maps.DirectionsTravelMode.WALKING;
      
     
    

var fromAddress;
    fromAddress = '<?php echo $club_address; ?>';
    fromAddress += " " + '<?php echo $city_name; ?>';
    fromAddress += ", " + '<?php echo $state_name; ?>';
    fromAddress += " " + '<?php echo $club_zip_code; ?>';

      //origin=fromAddress;
    
    var request = {
        origin: origin,
        destination: destination,
        waypoints: waypoints,
        travelMode: mode,
        optimizeWaypoints: true,
        avoidHighways: true,
        avoidTolls: true
    };
    directionsService.route(request, function(response, status) {
      if (status == google.maps.DirectionsStatus.OK) {
          jQuery('#map_canvas,#directionlabel,#directionsPanel').show();
          
          google.maps.event.trigger(map, 'resize');
        directionsDisplay.setDirections(response);
      }
    });
    
    clearMarkers();
  }
  
  function clearMarkers() {
    for (var i = 0; i < gmarkers.length; i++) {
      gmarkers[i].setMap(null);
    }
    gmarkers = [];
    origin = null;
    destination = null;
    waypoints = [];
  }
  
  function reset() {
    clearMarkers();
    directionsDisplay.setMap(null);
    directionsDisplay.setPanel(null);
    directionsDisplay = new google.maps.DirectionsRenderer();
    directionsDisplay.setMap(map);
    directionsDisplay.setPanel(document.getElementById("directionsPanel"));    
  }
  
  
   jQuery(document).ready(function(){
        if(store_long!='' && store_lat!=''){ 
            loadScript(); }
    });


                       
                   </script>
                   
                               <!---map show-->
                   <div id="map_canvas" style="width: 336px; height: 473px; display: none; "></div>         

                        	</div>

                        	<div class="col-md-6 col-sm-6">

                                    <div class="blogDetailRightBLock" id="directionlabel" style="display:none;">

                                    <h3>directions</h3>

                                </div>

                                
                                
                                <div id="directionsPanel" style="margin-top:10px; width: 343px; height: 624px; overflow-y: scroll; display: none;"></div>
                                
                                
                        	</div>

                        </div>

                    </div>
<?php 
                    // End the loop.
		endwhile;
		?>

                        <?php get_sidebar('similar-clubs'); ?>

                </div>
                    
                    
                    <?php get_sidebar('ads'); ?>
                    
             

            </div>


               

		
<?php
get_footer();
 ?>
