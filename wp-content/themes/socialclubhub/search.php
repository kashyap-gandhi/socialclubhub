<?php
/**
 * The template for displaying search results pages.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	
        	

		<?php if ( have_posts() ) : ?>


<div class="blogDetailPage clearfix">

            	<div class="col-md-8 col-sm-12">
        
			<h2><?php printf( __( 'Search Results for: %s', 'twentyfifteen' ), get_search_query() ); ?></h2>
			

			<?php
			// Start the loop.
			while ( have_posts() ) : the_post(); ?>

				<?php
				/*
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'content', 'search' );

			// End the loop.
			endwhile;

			// Previous/next page navigation.
			the_posts_pagination( array(
				'prev_text'          => __( 'Previous', 'twentyfifteen' ),
				'next_text'          => __( 'Next', 'twentyfifteen' ),
				'before_page_number' => '',
			) );
                        
                        
                        ?>
                        
                          </div>
                    
                    
                    <?php get_sidebar('ads'); ?>
                    
             

            </div>


               

		
<?php
get_footer();
 ?>

                        
                        
                        <?php

		// If no content, include the "No posts found" template.
		else :
			get_template_part( 'content', 'none' );

		endif;
		?>


              