<?php

/**
 * Search 
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<form role="search" method="get" class="form-inline" id="bbp-search-form" action="<?php bbp_search_url(); ?>">
	<div class="form-group">
		<label class="screen-reader-text hidden" for="bbp_search"><?php _e( 'Search for:', 'bbpress' ); ?></label>
		<input type="hidden" name="action" value="bbp-search-request" />
		<input  class="form-control" tabindex="<?php bbp_tab_index(); ?>" type="text" value="<?php echo esc_attr( bbp_get_search_terms() ); ?>" name="bbp_search" id="bbp_search" />
		
	</div>
    
  
    <button tabindex="<?php bbp_tab_index(); ?>" type="submit" id="bbp_search_submit" class="btn btn-default sign-btn"><?php esc_attr_e( 'Search', 'bbpress' ); ?></button>
</form>
