<?php $post_id=get_the_ID(); 


$similar_clubs=array();

if($post_id>0){
    $similar_clubs=get_similar_clubs($post_id);
}

if(!empty ($similar_clubs)) {
?>

<div class="row" style="clear: both;">

    <div class="col-md-12 col-sm-12 areaWrap">

            <div class="blogDetailRightBLock">

            <h3>similar clubs in your area</h3>

            </div>


                            <?php foreach($similar_clubs as $row) { 
                                
                                $post_id=$row->ID;
                                
                                $thumb_src= esc_url( get_template_directory_uri() ).'/images/default_image.jpg';
                                $attach_id = get_post_thumbnail_id( $post_id );
                                if($attach_id>0){
                                    $image_url = wp_get_attachment_image_src( $attach_id, 'sch-thumb' );
                                    if(isset($image_url[0]) && $image_url[0]!=''){
                                        $thumb_src=$image_url[0];
                                    }
                                }

                                $club_name=$row->post_title;

                                $club_link=get_permalink($post_id);
                                
                                
                                $club_address = get_post_meta($post_id, 'club_adderss', TRUE);
    
    $club_zip_code = get_post_meta($post_id, 'club_zip_code', TRUE);
    
    $club_city_id = get_post_meta($post_id, 'club_city_id', TRUE);
    
    $city_name='';    
    $get_city_detail=get_city_by_id($club_city_id);    
    if(!empty($get_city_detail)){
        $city_name=$get_city_detail->City;
    }
    
    
    $club_state_id = get_post_meta($post_id, 'club_state_id', TRUE);
    
    
    $state_name='';    
    $get_state_detail=get_state_by_id($club_state_id);    
    if(!empty($get_state_detail)){
        $state_name=$get_state_detail->region;
    }
    
    
    $club_country_id = get_post_meta($post_id, 'club_country_id', TRUE);
    
    
    $country_name='';    
    $get_country_detail=get_country_by_id($club_country_id);    
    if(!empty($get_country_detail)){
        $country_name=$get_country_detail->country;
    }
    
                                
                                
                                ?>
                            
                            <div class="areaBLock">

                            	<div class="imgBlock">

                                    <a href="<?php echo $club_link; ?>"><img src="<?php echo $thumb_src; ?>" class="img-responsive"></a>             

                                </div>

                                <div class="textBlock">

                                	<p><?php echo $club_name; ?></p>

                                    <p><?php echo $club_address;?><br>

                                    <?php echo $city_name;?>, <?php echo $state_name; ?> <?php echo $club_zip_code; ?></p>

                                    <a href="<?php echo $club_link; ?>" class="readMore">Read More</a>

                                </div>

                            </div>
                            
                            
                            <?php } ?>

        </div>

    </div>
<?php } ?>