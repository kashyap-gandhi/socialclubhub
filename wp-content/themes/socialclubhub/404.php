<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
get_header();
?>
<div class="clearfix">
    <section class="col-sm-12 text-center">
        <h2><?php _e('Oops! That page can&rsquo;t be found.', 'twentyfifteen'); ?></h2>

        <div class="page-content text-center">
            <p><?php _e('It looks like nothing was found at this location. Maybe try a search?', 'twentyfifteen'); ?></p>

            <?php get_search_form(); ?>

        </div><!-- .page-content -->


    </section>
</div> <!--cleatfix-->


<?php get_sidebar('clubs'); ?>
<?php get_sidebar('introvideo'); ?>


<?php
get_footer();
