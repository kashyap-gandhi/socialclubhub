<?php
/**
 * Template Name: Activity Page
 */

get_header(); ?>
 <div class="activityDetailPage clearfix">
 <div class="clearfix">
<?php get_sidebar('activity'); ?>

  <div class="col-md-5 col-sm-9 clearfix">

   <div class="blogDetailRightBLock">    
    
    <h3>ACTIVITIES</h3>
    
    </div>

    <div class="blogDetailBlock">


                          
   <?php 
				// Start the Loop.
				while ( have_posts() ) : the_post();

					 the_content(); 
				endwhile;
			?>
  

                      
    </div>
       
     
      <?php echo do_shortcode('[activities_all]'); ?>
      
       </div>
     <?php get_sidebar('ads'); ?>
     
         </div> <!--cleatfix-->
 </div>  
<?php
get_footer();
