
     
     <div class="col-md-12 col-sm-12 areaWrap">

           
         <!--local loop start-->

<?php


    
    

    $latlong_field = "";
    

    if ($user_lat != '' && $user_long != '') {
        
        //==mt1===latitude &&  mt2===longitude
        
        $latlong_field = "(
            $search_by_miles * acos (
              cos ( radians(" . $user_lat . ") )
              * cos( radians( CAST(mt1.meta_value as CHAR) ) )
              * cos( radians( CAST(mt2.meta_value as CHAR) ) - radians(" . $user_long . ") )
              + sin ( radians(" . $user_lat . ") )
              * sin( radians( CAST(mt1.meta_value as CHAR) ) )
            )
          ) AS city_distance";
        
        
    }
    
         
    $sql='';
    
    $sql.='SELECT ps.* ';
    
    if($latlong_field!=''){
        $sql.=', '.$latlong_field;
    }
    
    
    $sql.=' FROM '.$wpdb->prefix.'posts as ps ';
    
    if($latlong_field!=''){
        $sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt1 ON ( ps.ID = mt1.post_id AND mt1.meta_key="deal_lat" ) ';
        $sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt2 ON ( ps.ID = mt2.post_id AND mt2.meta_key="deal_long" ) ';
    }
    
    if($search_city_name!=''){
        $sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt3 ON ( ps.ID = mt3.post_id AND mt3.meta_key = "deal_city_id" AND CAST(mt3.meta_value AS SIGNED)>0 ) ';
        $sql.=' INNER JOIN '.$wpdb->prefix.'cities AS ct ON ( CAST(mt3.meta_value AS SIGNED) = ct.CityId ) ';
    }
    
    
    
     if ((int) $search_by_acitivity > 0) {
        $sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt4 ON ( ps.ID = mt4.post_id ) ';
     }
    
     if($deal_text_search!=''){
        $sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt7 ON ( ps.ID = mt7.post_id AND mt7.meta_key="deal_company_name") ';
    }
    
     $sql.=" WHERE 1=1 AND ps.post_type = 'deals_discount' AND (ps.post_status = 'publish' ) ";
    
    if($deal_text_search!=''){
        $sql.= " AND (ps.post_title like '%".$deal_text_search."%' OR  ps.post_content like '%".$deal_text_search."%' OR mt7.meta_value like '%".$deal_text_search."') ";
    }
  
    
    if($search_city_name!=''){
        $sql.=" AND LOWER(ct.City) like '%" . $search_city_name . "%' ";
    }
    
     
    if ((int) $search_by_acitivity > 0) {
        $sql.=" AND mt4.meta_key = 'deal_main_activity' AND CAST(mt4.meta_value AS SIGNED) = '".$search_by_acitivity."'  ";
    }
    
    
    
     
    $sql.=' GROUP BY ps.ID ';
    
    if($latlong_field!='' && $search_by_radius>0){
        $sql.=' having city_distance<='.$search_by_radius;
    }
    
    $sql.=' ORDER BY ';
    
    if($latlong_field!=''){
        $sql.=' city_distance asc ';
    } else {
        $sql.=' ps.ID ASC ';
    }
    
    
    
    //echo $sql;
    //die;
    
  	 
	
	
	$total_pages = mysql_num_rows(mysql_query($sql));
	
	
	
        
	
        $stages = 3;
        $limit = 12;
        $query_param_name='dlp';
	
        
        $current_url=current_page_url();
        
        $url_params = parse_url($current_url);
        
        
        if(!empty($url_params)){
           
            if(isset($url_params['query']) && $url_params['query']!=''){
                parse_str($url_params['query'],$query_params);
                
                if(isset($query_params[$query_param_name])){
                   unset($query_params[$query_param_name]);
               }
               
               if(isset($query_params['msg'])){
                   unset($query_params['msg']);
               }
               
               $url_params['query']=http_build_query($query_params);
            }
            
           
        }
        
        
        $currentUrl=$url_params['scheme']."://".$url_params['host'].$url_params['path'];
        $original_url=original_page_url();
        
        $separate_param='?';
        if(isset($url_params['query']) && trim($url_params['query'])!=''){
            
                $currentUrl.='?'.$url_params['query'];
                $separate_param='&';
            
        }
        
        
        $targetpage = $currentUrl;
        
        
	$page='';
        
        
        
        
        
        
	if(isset($_GET[$query_param_name]))
	{
		$page =  (int) $_GET[$query_param_name];
	}
	
	
	if($page){
		$start = ($page - 1) * $limit; 
	} else{
		$start = 0;	
		}	
	
    // Get page data
	$query1 = $sql." limit ".$limit." offset ".$start;
        
       
	$result = mysql_query($query1);
	
	// Initial page num setup
	if ($page == 0){$page = 1;}
	$prev = $page - 1;	
	$next = $page + 1;							
	$lastpage = ceil($total_pages/$limit);		
	$LastPagem1 = $lastpage - 1;					
	
	
	$paginate = '<ul class="pager">';
	if($lastpage > 1)
	{	
	

		// Previous
		if ($page > 1){
			$paginate.= "<li class='first-child'><a href='".$targetpage.$separate_param.$query_param_name."=".$prev."'>previous</a></li>";
		}else{
			$paginate.= "<li class='first-child'><a>previous</a></li>";	}
			

		
		// Pages	
		if ($lastpage < 7 + ($stages * 2))	// Not enough pages to breaking it up
		{	
			for ($counter = 1; $counter <= $lastpage; $counter++)
			{
				if ($counter == $page){
					$paginate.= "<li class='active'><a>$counter</a></li>";
				}else{
					$paginate.= "<li><a href='".$targetpage.$separate_param.$query_param_name."=".$counter."'>$counter</a></li>";}					
			}
		}
		elseif($lastpage > 5 + ($stages * 2))	// Enough pages to hide a few?
		{
			// Beginning only hide later pages
			if($page < 1 + ($stages * 2))		
			{
				for ($counter = 1; $counter < 4 + ($stages * 2); $counter++)
				{
					if ($counter == $page){
						$paginate.= "<li class='active'><a>$counter</a></li>";
					}else{
						$paginate.= "<li><a href='".$targetpage.$separate_param.$query_param_name."=".$counter."'>$counter</a></li>";}					
				}
				$paginate.= "...";
				$paginate.= "<a href='".$targetpage.$separate_param.$query_param_name."=".$LastPagem1."'>$LastPagem1</a>";
				$paginate.= "<a href='".$targetpage.$separate_param.$query_param_name."=".$lastpage."'>$lastpage</a>";		
			}
			// Middle hide some front and some back
			elseif($lastpage - ($stages * 2) > $page && $page > ($stages * 2))
			{
				$paginate.= "<li><a href='".$targetpage.$separate_param.$query_param_name."=1'>1</a></li>";
				$paginate.= "<li><a href='".$targetpage.$separate_param.$query_param_name."=2'>2</a></li>";
				$paginate.= "...";
				for ($counter = $page - $stages; $counter <= $page + $stages; $counter++)
				{
					if ($counter == $page){
						$paginate.= "<li class='active'><a>$counter</a></li>";
					}else{
						$paginate.= "<li><a href='".$targetpage.$separate_param.$query_param_name."=".$counter."'>$counter</a></li>";}					
				}
				$paginate.= "...";
				$paginate.= "<li><a href='".$targetpage.$separate_param.$query_param_name."=".$LastPagem1."'>$LastPagem1</a></li>";
				$paginate.= "<li><a href='".$targetpage.$separate_param.$query_param_name."=".$lastpage."'>$lastpage</a></li>";		
			}
			// End only hide early pages
			else
			{
				$paginate.= "<li><a href='".$targetpage.$separate_param.$query_param_name."=1'>1</a></li>";
				$paginate.= "<li><a href='".$targetpage.$separate_param.$query_param_name."=2'>2</a></li>";
				$paginate.= "...";
				for ($counter = $lastpage - (2 + ($stages * 2)); $counter <= $lastpage; $counter++)
				{
					if ($counter == $page){
						$paginate.= "<li class='active'><a>$counter</a></li>";
					}else{
						$paginate.= "<li><a href='".$targetpage.$separate_param.$query_param_name."=".$counter."'>$counter</a></li>";}					
				}
			}
		}
					
				// Next
		if ($page < $counter - 1){ 
			$paginate.= "<li class='last-child'><a href='".$targetpage.$separate_param.$query_param_name."=".$next."'>next</a></li>";
		}else{
			$paginate.= "<li><a>next</a></li>";
			}
			
	
	
	
}
$paginate.= "</ul>";
 

   
?>
         
         
         
          <div class="searchboxdiv">

                <div class="col-md-6 col-sm-12 col-xs-12 pull-left text-left left"><h2>Search Deals</h2></div>
                
                <div class="col-md-6 col-sm-12 col-xs-12 pull-right right">
                    <form class="form-inline" name="fromsearchdealsdiscounts" id="fromsearchdealsdiscounts" action="" method="get">
                    
                    <div class="form-group"><h3>Search:</h3></div>
                    <div class="form-group">
                        <select class="form-control" name="search_by_acitivity" id="search_by_acitivity">
                            <option value="0">---Select Activity---</option>                    
                            <?php
                            if (!empty($res_activities)) {
                                foreach ($res_activities as $row_acti) {
                                    ?>
                                            <option value="<?php echo $row_acti->term_id; ?>" <?php if ($row_acti->term_id == $search_by_acitivity) { ?> selected <?php } ?>><?php echo ucwords($row_acti->name); ?></option>
                                <?php }
                            } ?>
                        </select>
                    </div>
                    
                    <div class="form-group">
                        <input type="text" name="deal_text_search" id="deal_text_search" placeholder="Search" value="<?php echo $deal_text_search; ?>" class="form-control" />
                    </div>
                    
                    
                    
                        <input type="hidden" name="search_type" id="search_type" value="search" />
                        <button type="submit" class="btn btn-default sign-btn">search</button>
                   
                    
                    </form>
                    
                </div>
                
                <div class="clearfix" style="clear:both;"></div>
                
            </div>
     
         
          <div class="clearfix"></div>
                            <div class="col-sm-12 pull-right clearfix"><?php echo $paginate; ?></div>            
                            <div class="clearfix"></div>


         <div class="col-sm-12 clearfix" style="clear:both;margin-top: 20px; ">
         
            
            
            
            <?php
            
            if(mysql_num_rows($result)>0){
            
                
                
               while ($row = mysql_fetch_object($result)) {
                    
                    $post_id=$row->ID;
                    
                   
                    
                    
                    $thumb_src= esc_url( get_template_directory_uri() ).'/images/default_image.jpg';
                    $attach_id = get_post_thumbnail_id($post_id);
                    if($attach_id>0){
                        $image_url = wp_get_attachment_image_src( $attach_id, 'sch-thumb' );
                        if(isset($image_url[0]) && $image_url[0]!=''){
                            $thumb_src=$image_url[0];
                        }
                    }
                    
                    
                    $deal_name=$row->post_title;                   
                    
                    
                    $deal_link='';
                    
                    $deal_adderss = get_post_meta($post_id, 'deal_adderss', TRUE);
    
                    $deal_zip_code = get_post_meta($post_id, 'deal_zip_code', TRUE);

                    $deal_city_id = get_post_meta($post_id, 'deal_city_id', TRUE);

                    $city_name='';    
                    $get_city_detail=get_city_by_id($deal_city_id);    
                    if(!empty($get_city_detail)){
                        $city_name=$get_city_detail->City;
                    }


                    $deal_state_id = get_post_meta($post_id, 'deal_state_id', TRUE);
                    
                    
                    
                    $state_name='';    
                    $get_state_detail=get_state_by_id($deal_state_id);    
                    if(!empty($get_state_detail)){
                        $state_name=$get_state_detail->region;
                    }

                    $deal_country_id = get_post_meta($post_id, 'deal_country_id', TRUE);

                    $country_name='';    
                    $get_country_detail=get_country_by_id($deal_country_id);    
                    if(!empty($get_country_detail)){
                        $country_name=$get_country_detail->country;
                    }

                    $deal_lat = get_post_meta($post_id, 'deal_lat', TRUE);
                    $deal_long = get_post_meta($post_id, 'deal_long', TRUE);



                    $deal_fax_number = get_post_meta($post_id, 'deal_fax_number', TRUE);
                    $deal_phone_number = get_post_meta($post_id, 'deal_phone_number', TRUE);


                    $deal_website = get_post_meta($post_id, 'deal_website', TRUE);

                    $deal_company_name=get_post_meta($post_id,'deal_company_name',TRUE);
                    
                    $deal_code=get_post_meta($post_id,'deal_code',TRUE);
                    $deal_discount=get_post_meta($post_id,'deal_discount',TRUE);
                    $deal_discount_type=get_post_meta($post_id,'deal_discount_type',TRUE);
             
            ?>
            
            <div class="areaBLock" style="height: auto;">

                            	<div class="imgBlock">

                                    <a href="<?php echo $deal_link; ?>"><img src="<?php echo $thumb_src; ?>" class="img-responsive"></a>             

                                </div>

                                <div class="textBlock">

                                	<p><?php echo $deal_name; ?></p>
                                        
                                        <p><?php echo $deal_company_name; ?></p>
                                        
                                        <p class="coupon_bg"><?php echo $deal_code; ?></p>
                                        

                                    <p><?php echo $deal_adderss;?><br>

                                    <?php echo $city_name;?>, <?php echo $state_name; ?> <?php echo $deal_zip_code; ?></p>
                                    
                                    
                                    <p><?php echo $deal_phone_number; ?></p>
                                    <p><?php echo $deal_fax_number; ?></p>
                                    
<!--                                    <a href="<?php echo $deal_link; ?>" class="readMore">Read More</a>-->

                                </div>

                            </div>
                  
             
             <?php }
            } else { ?>
             
             <div class="text-center">No deals found in your search criteria. Please refine your search again.</div>
             <?php } ?>
             
            
            
            
         </div>
         <!--local loop end-->
         
         
         
         
     </div>

   
     
    