<?php
/**
 * Template Name: Deals Discount Page
 */

get_header(); ?>
 <div class="clearfix">

                        <div class="col-sm-12 clearfix">

                          
   <?php 
				// Start the Loop.
				while ( have_posts() ) : the_post();

					 the_content(); 
				endwhile;
			?>
  

                        </div>
     
    
     
     
     <?php
     
     
    //==track user latlong  if blank than check for if user have city but no latlong then fetch city lat long else use  system latlong
    //==if user search with city name then use city lat long 
    //
    //and find  in that default radius or search radius
    
    
     global $wpdb;    
    
     
    $search_by_miles = 3959;
    $search_by_kms = 6371;
     
    $socialclubhub = get_option("socialclubhub_theme_config");
     
    
    //==system default latlong
    $system_lat=25.761679800000;
    $system_long=-80.191790200000;
    
    $search_city_name='';
    
    if(isset($socialclubhub['system_lat']) && trim($socialclubhub['system_lat'])!=''){
        $system_lat=trim($socialclubhub['system_lat']);
    }
    
    
    if(isset($socialclubhub['system_long']) && trim($socialclubhub['system_long'])!=''){
        $system_long=trim($socialclubhub['system_long']);
    }
     
    
    //==user track latlong
    $user_lat='';    
    if(isset($_COOKIE['GASCHLAT'])){ $user_lat= trim(encrypt_decrypt_cookie('decrypt',$_COOKIE['GASCHLAT'])); } 
    
    $user_long='';    
    if(isset($_COOKIE['GASCHLONG'])){ $user_long= trim(encrypt_decrypt_cookie('decrypt',$_COOKIE['GASCHLONG'])); } 
    
    $user_state_name='';    
    if(isset($_COOKIE['GASCHST'])){ $user_state_name= trim(encrypt_decrypt_cookie('decrypt',$_COOKIE['GASCHST'])); } 
    
    $user_country_name='';    
    if(isset($_COOKIE['GASCHCNT'])){ $user_country_name= trim(encrypt_decrypt_cookie('decrypt',$_COOKIE['GASCHCNT'])); } 
    
    
    
    $user_city_name='';    
    if(isset($_COOKIE['GASCHCT'])){ 
       
        $user_city_name= trim(encrypt_decrypt_cookie('decrypt',$_COOKIE['GASCHCT']));
        
        if($user_lat=='' || $user_long==''){
            if(trim($user_city_name)!=''){
                $get_city_detail=get_city_by_name($user_city_name,$user_lat,$user_long);

                if(!empty($get_city)){

                    if(isset($row_cities->Latitude) && $row_cities->Latitude!=''){
                        $user_lat=$row_cities->Latitude;
                    }

                    if(isset($row_cities->Longitude) && $row_cities->Longitude!=''){
                        $user_long=$row_cities->Longitude;
                    }

                }
            }
        }
        
        if($user_lat=='' || $user_long==''){
            $user_lat=''; 
            $user_long=''; 
        }
    } 
    
    
    
    
    //==override with track city
    $search_city_name=$user_city_name;
    
    
    
    //==user db latlong
    $user_db_lat='';
    $user_db_long='';
    $user_db_city_name='';
     if (is_user_logged_in()) {

        global $current_user;

        $user = $current_user;
        
         $user_db_lat = trim(get_the_author_meta('user_lat', $user->ID));
         $user_db_long = trim(get_the_author_meta('user_long', $user->ID));
         
         
         
        $user_city_id = (int) get_the_author_meta('user_city_id', $user->ID);

        $get_city_detail = get_city_by_id($user_city_id);
        if (!empty($get_city_detail)) {
            $user_db_city_name = $get_city_detail->City;
            
            if($user_db_lat=='' || $user_db_long==''){
                
                    if(isset($get_city_detail->Latitude) && $get_city_detail->Latitude!=''){
                        $user_db_lat=$get_city_detail->Latitude;
                    }

                    if(isset($get_city_detail->Longitude) && $get_city_detail->Longitude!=''){
                        $user_db_long=$get_city_detail->Longitude;
                    }                
                
            }
            
        }
        
        
        if($user_db_lat=='' || $user_db_long==''){
             $user_db_lat='';
             $user_db_long='';
         }
         
     }
     
     //==override with profile city
     if($user_db_lat!='' && $user_db_long!='' && $user_db_city_name!=''){
         $user_lat=$user_db_lat;
         $user_long=$user_db_long;
         $search_city_name=$user_db_city_name;
     }
    
    
    //==user search latlong
    $user_search_lat='';    
    if(isset($_COOKIE['GASCHRLAT'])){ $user_search_lat= trim(encrypt_decrypt_cookie('decrypt',$_COOKIE['GASCHRLAT'])); } 
    
    $user_search_long='';    
    if(isset($_COOKIE['GASCHRLONG'])){ $user_search_long= trim(encrypt_decrypt_cookie('decrypt',$_COOKIE['GASCHRLONG'])); } 
    
    
   $user_search_city='';
    if (isset($_COOKIE['GASCHRCT'])) {
        $user_search_city = trim(encrypt_decrypt_cookie('decrypt', $_COOKIE['GASCHRCT'])); 

        if($user_search_lat=='' || $user_search_long==''){
            
            $get_city_detail = get_city_by_name($user_search_city);
            if (!empty($get_city_detail)) {
                
                
                if(isset($get_city_detail->Latitude) && $get_city_detail->Latitude!=''){
                    $user_search_lat=$get_city_detail->Latitude;
                }

                if(isset($get_city_detail->Longitude) && $get_city_detail->Longitude!=''){
                    $user_search_long=$get_city_detail->Longitude;
                }
               
            }            
        }        
    }
    
    if($user_search_lat!='' && $user_search_long!='' && $user_search_city!=''){
        $user_lat=$user_search_lat;
        $user_long=$user_search_long;
        $search_city_name=$user_search_city;
    }
    
    
    
    if($user_lat=='' || $user_long=='' || $search_city_name==''){
        
        $user_lat=$system_lat;
        $user_long=$system_long;
        
        if(isset($socialclubhub['system_city_id']) && (int) $socialclubhub['system_city_id']>0){
            
            $get_city_detail = get_city_by_id($socialclubhub['system_city_id']);

            if (!empty($get_city_detail)) {
                $search_city_name = $get_city_detail->City;
            }

        }
        
    }
    
    
    
    
    $search_city_name=strtolower(trim(strip_tags($search_city_name)));
    
    
    //==post parameter setup
    
    
    $search_by_acitivity= (int) 0;
    $deal_text_search='';
     
     
    if($_REQUEST){
        
        if(isset($_REQUEST['search_by_acitivity']) && (int) $_REQUEST['search_by_acitivity'] >0){
            $search_by_acitivity=(int) $_REQUEST['search_by_acitivity'];
        }
        
        if(isset($_REQUEST['deal_text_search']) && trim($_REQUEST['deal_text_search'])!=''){
            $deal_text_search=trim(strip_tags($_REQUEST['deal_text_search']));
        }
        
    }
    
    
    
   
    
    //radius setup
    $search_by_radius=100;
    
    if (isset($_COOKIE['GASCHRMIL'])) {
        $search_cache_radius = (int) trim(encrypt_decrypt_cookie('decrypt', $_COOKIE['GASCHRMIL']));    
    
        if($search_cache_radius>0){
            $search_by_radius=$search_cache_radius;
        }
    
    }
   //==end radius setup      
     
   
     
    
     $res_activities = get_all_activities();
    
     
     $allow_types=array('local','regional','national','search');
     
     $search_type='';
     $show_search_list=0;
     
     if(isset($_REQUEST['search_type']) && trim(strip_tags($_REQUEST['search_type']))!='' ) {
         
         $search_type=trim(strip_tags($_REQUEST['search_type']));
         
         if(in_array($search_type, $allow_types)){
             $show_search_list=1;
         }
         
     }
     
     
     
     if($show_search_list==1){
         require_once 'dealsdiscount_list.php';
     } else {
         require_once 'dealsdiscount_list_local.php';
     }
     
     ?>
     
   
     
     
         </div> <!--cleatfix-->

         
<?php
get_footer();
