
     
     <div class="col-md-12 col-sm-12 areaWrap">

           
         <!--local loop start-->

<?php


    
    

    $latlong_field = "";
    

    if ($user_lat != '' && $user_long != '') {
        
        //==mt1===latitude &&  mt2===longitude
        
        $latlong_field = "(
            $search_by_miles * acos (
              cos ( radians(" . $user_lat . ") )
              * cos( radians( CAST(mt1.meta_value as CHAR) ) )
              * cos( radians( CAST(mt2.meta_value as CHAR) ) - radians(" . $user_long . ") )
              + sin ( radians(" . $user_lat . ") )
              * sin( radians( CAST(mt1.meta_value as CHAR) ) )
            )
          ) AS city_distance";
        
        
    }
    
         
    $sql='';
    
    $sql.='SELECT ps.* ';
    
    if($latlong_field!=''){
        $sql.=', '.$latlong_field;
    }
    
    
    $sql.=' FROM '.$wpdb->prefix.'posts as ps ';
    
    if($latlong_field!=''){
        $sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt1 ON ( ps.ID = mt1.post_id AND mt1.meta_key="deal_lat" ) ';
        $sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt2 ON ( ps.ID = mt2.post_id AND mt2.meta_key="deal_long" ) ';
    }
    
    if($search_city_name!=''){
        $sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt3 ON ( ps.ID = mt3.post_id AND mt3.meta_key = "deal_city_id" AND CAST(mt3.meta_value AS SIGNED)>0 ) ';
        $sql.=' INNER JOIN '.$wpdb->prefix.'cities AS ct ON ( CAST(mt3.meta_value AS SIGNED) = ct.CityId ) ';
    }
    
    
    
     if ((int) $search_by_acitivity > 0) {
        $sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt4 ON ( ps.ID = mt4.post_id ) ';
     }
    
     
     if($deal_text_search!=''){
        $sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt7 ON ( ps.ID = mt7.post_id AND mt7.meta_key="deal_company_name") ';
    }
    
     $sql.=" WHERE 1=1 AND ps.post_type = 'deals_discount' AND (ps.post_status = 'publish' ) ";
    
    if($deal_text_search!=''){
        $sql.= " AND (ps.post_title like '%".$deal_text_search."%' OR  ps.post_content like '%".$deal_text_search."%' OR mt7.meta_value like '%".$deal_text_search."') ";
    }
  
    
    if($search_city_name!=''){
        $sql.=" AND LOWER(ct.City) like '%" . $search_city_name . "%' ";
    }
    
     
    if ((int) $search_by_acitivity > 0) {
        $sql.=" AND mt4.meta_key = 'deal_main_activity' AND CAST(mt4.meta_value AS SIGNED) = '".$search_by_acitivity."'  ";
    }
    
    
    
     
    $sql.=' GROUP BY ps.ID ';
    
    if($latlong_field!='' && $search_by_radius>0){
        $sql.=' having city_distance<='.$search_by_radius;
    }
    
    $sql.=' ORDER BY ';
    
    if($latlong_field!=''){
        $sql.=' city_distance asc ';
    } else {
        $sql.=' ps.ID ASC ';
    }
    
    $sql.=' limit 6 ';
    
    //echo $sql;
    //die;
    
    $res_local = $wpdb->get_results($sql);
    
    //print_r($res_local);

    $local_deals=array();
    $local_states=array();
?>
         
         
         
          <div class="searchboxdiv">

                <div class="col-md-6 col-sm-12 col-xs-12 pull-left text-left left"><h2>Local Deals</h2></div>
                
                <div class="col-md-6 col-sm-12 col-xs-12 pull-right right">
                    <form class="form-inline" name="fromsearchdealsdiscounts" id="fromsearchdealsdiscounts" action="" method="get">
                    
                    <div class="form-group"><h3>Search:</h3></div>
                    <div class="form-group">
                        <select class="form-control" name="search_by_acitivity" id="search_by_acitivity">
                            <option value="0">---Select Activity---</option>                    
                            <?php
                            if (!empty($res_activities)) {
                                foreach ($res_activities as $row_acti) {
                                    ?>
                                            <option value="<?php echo $row_acti->term_id; ?>" <?php if ($row_acti->term_id == $search_by_acitivity) { ?> selected <?php } ?>><?php echo ucwords($row_acti->name); ?></option>
                                <?php }
                            } ?>
                        </select>
                    </div>
                    
                    <div class="form-group">
                        <input type="text" name="deal_text_search" id="deal_text_search" placeholder="Search" value="<?php echo $deal_text_search; ?>" class="form-control" />
                    </div>
                    
                    
                        <input type="hidden" name="search_type" id="search_type" value="local" />
                        <button type="submit" class="btn btn-default sign-btn">search</button>
                    
                    </form>
                    
                </div>
                
                <div class="clearfix" style="clear:both;"></div>
                
            </div>
     
         


         <div class="col-sm-12 clearfix" style="clear:both;margin-top: 20px; ">
         
            
            
            
            <?php
            
            if(!empty($res_local)) {
            
                
                
                foreach($res_local as $row) {
                    
                    $post_id=$row->ID;
                    
                    $local_deals[]=$post_id;
                    
                    
                    $thumb_src= esc_url( get_template_directory_uri() ).'/images/default_image.jpg';
                    $attach_id = get_post_thumbnail_id($post_id);
                    if($attach_id>0){
                        $image_url = wp_get_attachment_image_src( $attach_id, 'sch-thumb' );
                        if(isset($image_url[0]) && $image_url[0]!=''){
                            $thumb_src=$image_url[0];
                        }
                    }
                    
                    
                    $deal_name=$row->post_title;                   
                    
                    
                    $deal_link='';
                    
                    $deal_adderss = get_post_meta($post_id, 'deal_adderss', TRUE);
    
                    $deal_zip_code = get_post_meta($post_id, 'deal_zip_code', TRUE);

                    $deal_city_id = get_post_meta($post_id, 'deal_city_id', TRUE);

                    $city_name='';    
                    $get_city_detail=get_city_by_id($deal_city_id);    
                    if(!empty($get_city_detail)){
                        $city_name=$get_city_detail->City;
                    }


                    $deal_state_id = get_post_meta($post_id, 'deal_state_id', TRUE);
                    
                    $local_states[]=$deal_state_id;
                    
                    $state_name='';    
                    $get_state_detail=get_state_by_id($deal_state_id);    
                    if(!empty($get_state_detail)){
                        $state_name=$get_state_detail->region;
                    }

                    $deal_country_id = get_post_meta($post_id, 'deal_country_id', TRUE);

                    $country_name='';    
                    $get_country_detail=get_country_by_id($deal_country_id);    
                    if(!empty($get_country_detail)){
                        $country_name=$get_country_detail->country;
                    }

                    $deal_lat = get_post_meta($post_id, 'deal_lat', TRUE);
                    $deal_long = get_post_meta($post_id, 'deal_long', TRUE);



                    $deal_fax_number = get_post_meta($post_id, 'deal_fax_number', TRUE);
                    $deal_phone_number = get_post_meta($post_id, 'deal_phone_number', TRUE);


                    $deal_website = get_post_meta($post_id, 'deal_website', TRUE);
                    
                    $deal_company_name=get_post_meta($post_id,'deal_company_name',TRUE);
                    
                    $deal_code=get_post_meta($post_id,'deal_code',TRUE);
                    $deal_discount=get_post_meta($post_id,'deal_discount',TRUE);
                    $deal_discount_type=get_post_meta($post_id,'deal_discount_type',TRUE);

             
            ?>
            
            <div class="areaBLock" style="height: auto;">

                            	<div class="imgBlock">

                                    <a href="<?php echo $deal_link; ?>"><img src="<?php echo $thumb_src; ?>" class="img-responsive"></a>             

                                </div>

                                <div class="textBlock">

                                	<p><?php echo $deal_name; ?></p>
                                        
                                        <p><?php echo $deal_company_name; ?></p>
                                        
                                        <p class="coupon_bg"><?php echo $deal_code; ?></p>
                                        

                                    <p><?php echo $deal_adderss;?><br>

                                    <?php echo $city_name;?>, <?php echo $state_name; ?> <?php echo $deal_zip_code; ?></p>
                                    
                                    
                                    <p><?php echo $deal_phone_number; ?></p>
                                    <p><?php echo $deal_fax_number; ?></p>
                                    
<!--                                    <a href="<?php echo $deal_link; ?>" class="readMore">Read More</a>-->

                                </div>

                            </div>
                  
             
             <?php }
            } ?>
             
            
            
            
         </div>
         <!--local loop end-->
         
         
         
         
     </div>

   
     
     
       
     
     <div class="col-md-12 col-sm-12 areaWrap">

           
         <!--regional loop start-->

<?php


    
    

    $latlong_field = "";
    

    if ($user_lat != '' && $user_long != '') {
        
        //==mt1===latitude &&  mt2===longitude
        
        $latlong_field = "(
            $search_by_miles * acos (
              cos ( radians(" . $user_lat . ") )
              * cos( radians( CAST(mt1.meta_value as CHAR) ) )
              * cos( radians( CAST(mt2.meta_value as CHAR) ) - radians(" . $user_long . ") )
              + sin ( radians(" . $user_lat . ") )
              * sin( radians( CAST(mt1.meta_value as CHAR) ) )
            )
          ) AS city_distance";
        
        
    }
    
         
    $sql='';
    
    $sql.='SELECT ps.* ';
    
    if($latlong_field!=''){
        $sql.=', '.$latlong_field;
    }
    
    
    $sql.=' FROM '.$wpdb->prefix.'posts as ps ';
    
    if($latlong_field!=''){
        $sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt1 ON ( ps.ID = mt1.post_id AND mt1.meta_key="deal_lat" ) ';
        $sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt2 ON ( ps.ID = mt2.post_id AND mt2.meta_key="deal_long" ) ';
    }
    
    if($search_city_name!=''){
        $sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt3 ON ( ps.ID = mt3.post_id AND mt3.meta_key = "deal_city_id" AND CAST(mt3.meta_value AS SIGNED)>0 ) ';
        $sql.=' INNER JOIN '.$wpdb->prefix.'cities AS ct ON ( CAST(mt3.meta_value AS SIGNED) = ct.CityId ) ';
        
         $sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt5 ON ( ps.ID = mt5.post_id  ) ';
    }
    
    
    
     if ((int) $search_by_acitivity > 0) {
        $sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt4 ON ( ps.ID = mt4.post_id ) ';
     }
    
     if($deal_text_search!=''){
        $sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt7 ON ( ps.ID = mt7.post_id AND mt7.meta_key="deal_company_name") ';
    }
    
     $sql.=" WHERE 1=1 AND ps.post_type = 'deals_discount' AND (ps.post_status = 'publish' ) ";
    
    if($deal_text_search!=''){
        $sql.= " AND (ps.post_title like '%".$deal_text_search."%' OR  ps.post_content like '%".$deal_text_search."%' OR mt7.meta_value like '%".$deal_text_search."') ";
    }
    
    
    if(!empty($local_deals)){
        
        
        $exp_deals=implode(',', $local_deals);
        $sql.=" AND ps.ID NOT IN (".$exp_deals.")";
    }
    
    if($search_city_name!=''){
        //$sql.=" AND LOWER(ct.City) like '%" . $search_city_name . "%' ";
        
       $local_states=array_unique(array_filter($local_states));
        if(!empty($local_states)){
            $exp_state=implode(',', $local_states);
                $sql.=" AND mt5.meta_key = 'deal_state_id' AND CAST(mt5.meta_value AS SIGNED) IN(".$exp_state.")  ";
         }
    }
    
     
    if ((int) $search_by_acitivity > 0) {
        $sql.=" AND mt4.meta_key = 'deal_main_activity' AND CAST(mt4.meta_value AS SIGNED) = '".$search_by_acitivity."'  ";
    }
    
    
    
     
    $sql.=' GROUP BY ps.ID ';
    
    if($latlong_field!='' && $search_by_radius>0){
        //$sql.=' having city_distance<='.$search_by_radius;
    }
    
    $sql.=' ORDER BY ';
    
    /*if($latlong_field!=''){
        $sql.=' city_distance asc ';
    } else {*/
        $sql.=' ps.ID ASC ';
    //}
    
    $sql.=' limit 6 ';
    
    //echo $sql;
    //die;
    
    $res_regional = $wpdb->get_results($sql);
    
    //print_r($res_regional);

    $regional_deals=array();
    $regional_country=array();
?>
         
         
         
          <div class="searchboxdiv">

                <div class="col-md-6 col-sm-12 col-xs-12 pull-left text-left left"><h2>Regional Deals</h2></div>
                
                <div class="col-md-6 col-sm-12 col-xs-12 pull-right right">
                    <form class="form-inline" name="fromsearchdealsdiscounts" id="fromsearchdealsdiscounts" action="" method="get">
                    
                    <div class="form-group"><h3>Search:</h3></div>
                    <div class="form-group">
                        <select class="form-control" name="search_by_acitivity" id="search_by_acitivity">
                            <option value="0">---Select Activity---</option>                    
                            <?php
                            if (!empty($res_activities)) {
                                foreach ($res_activities as $row_acti) {
                                    ?>
                                            <option value="<?php echo $row_acti->term_id; ?>" <?php if ($row_acti->term_id == $search_by_acitivity) { ?> selected <?php } ?>><?php echo ucwords($row_acti->name); ?></option>
                                <?php }
                            } ?>
                        </select>
                    </div>
                    
                    <div class="form-group">
                        <input type="text" name="deal_text_search" id="deal_text_search" placeholder="Search" value="<?php echo $deal_text_search; ?>" class="form-control" />
                    </div>
                    
                    
                        <input type="hidden" name="search_type" id="search_type" value="regional" />
                        <button type="submit" class="btn btn-default sign-btn">search</button>
                    
                    </form>
                    
                </div>
                
                <div class="clearfix" style="clear:both;"></div>
                
            </div>
     
         


         <div class="col-sm-12 clearfix" style="clear:both;margin-top: 20px; ">
         
            
            
            
            <?php
            
            if(!empty($res_regional)) {
            
                
                
                foreach($res_regional as $row) {
                    
                    $post_id=$row->ID;
                    
                    $regional_deals[]=$post_id;
                    
                    
                    $thumb_src= esc_url( get_template_directory_uri() ).'/images/default_image.jpg';
                    $attach_id = get_post_thumbnail_id($post_id);
                    if($attach_id>0){
                        $image_url = wp_get_attachment_image_src( $attach_id, 'sch-thumb' );
                        if(isset($image_url[0]) && $image_url[0]!=''){
                            $thumb_src=$image_url[0];
                        }
                    }
                    
                    
                    $deal_name=$row->post_title;                   
                    
                    
                    $deal_link='';
                    
                    $deal_adderss = get_post_meta($post_id, 'deal_adderss', TRUE);
    
                    $deal_zip_code = get_post_meta($post_id, 'deal_zip_code', TRUE);

                    $deal_city_id = get_post_meta($post_id, 'deal_city_id', TRUE);

                    $city_name='';    
                    $get_city_detail=get_city_by_id($deal_city_id);    
                    if(!empty($get_city_detail)){
                        $city_name=$get_city_detail->City;
                    }


                    $deal_state_id = get_post_meta($post_id, 'deal_state_id', TRUE);
                    
                    
                    
                    $state_name='';    
                    $get_state_detail=get_state_by_id($deal_state_id);    
                    if(!empty($get_state_detail)){
                        $state_name=$get_state_detail->region;
                    }

                    $deal_country_id = get_post_meta($post_id, 'deal_country_id', TRUE);

                    $regional_country[]=$deal_country_id;
                    
                    $country_name='';    
                    $get_country_detail=get_country_by_id($deal_country_id);    
                    if(!empty($get_country_detail)){
                        $country_name=$get_country_detail->country;
                    }

                    $deal_lat = get_post_meta($post_id, 'deal_lat', TRUE);
                    $deal_long = get_post_meta($post_id, 'deal_long', TRUE);



                    $deal_fax_number = get_post_meta($post_id, 'deal_fax_number', TRUE);
                    $deal_phone_number = get_post_meta($post_id, 'deal_phone_number', TRUE);


                    $deal_website = get_post_meta($post_id, 'deal_website', TRUE);
                    
                    $deal_company_name=get_post_meta($post_id,'deal_company_name',TRUE);

                    $deal_code=get_post_meta($post_id,'deal_code',TRUE);
                    $deal_discount=get_post_meta($post_id,'deal_discount',TRUE);
                    $deal_discount_type=get_post_meta($post_id,'deal_discount_type',TRUE);
            ?>
            
             <div class="areaBLock" style="height: auto;">

                            	<div class="imgBlock">

                                    <a href="<?php echo $deal_link; ?>"><img src="<?php echo $thumb_src; ?>" class="img-responsive"></a>             

                                </div>

                                <div class="textBlock">

                                	<p><?php echo $deal_name; ?></p>
                                        
                                        <p><?php echo $deal_company_name; ?></p>
                                        
                                        <p class="coupon_bg"><?php echo $deal_code; ?></p>
                                        

                                    <p><?php echo $deal_adderss;?><br>

                                    <?php echo $city_name;?>, <?php echo $state_name; ?> <?php echo $deal_zip_code; ?></p>
                                    
                                    
                                    <p><?php echo $deal_phone_number; ?></p>
                                    <p><?php echo $deal_fax_number; ?></p>
                                    
<!--                                    <a href="<?php echo $deal_link; ?>" class="readMore">Read More</a>-->

                                </div>

                            </div>
                  
             
             <?php }
            } ?>
             
            
            
            
         </div>
         <!--regional loop end-->
         
     </div>

     
     
     
     
     
         
     <div class="col-md-12 col-sm-12 areaWrap">

           
         <!--national loop start-->

<?php


    
    

    $latlong_field = "";
    

    if ($user_lat != '' && $user_long != '') {
        
        //==mt1===latitude &&  mt2===longitude
        
        $latlong_field = "(
            $search_by_miles * acos (
              cos ( radians(" . $user_lat . ") )
              * cos( radians( CAST(mt1.meta_value as CHAR) ) )
              * cos( radians( CAST(mt2.meta_value as CHAR) ) - radians(" . $user_long . ") )
              + sin ( radians(" . $user_lat . ") )
              * sin( radians( CAST(mt1.meta_value as CHAR) ) )
            )
          ) AS city_distance";
        
        
    }
    
         
    $sql='';
    
    $sql.='SELECT ps.* ';
    
    if($latlong_field!=''){
        $sql.=', '.$latlong_field;
    }
    
    
    $sql.=' FROM '.$wpdb->prefix.'posts as ps ';
    
    if($latlong_field!=''){
        $sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt1 ON ( ps.ID = mt1.post_id AND mt1.meta_key="deal_lat" ) ';
        $sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt2 ON ( ps.ID = mt2.post_id AND mt2.meta_key="deal_long" ) ';
    }
    
    if($search_city_name!=''){
        $sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt3 ON ( ps.ID = mt3.post_id AND mt3.meta_key = "deal_city_id" AND CAST(mt3.meta_value AS SIGNED)>0 ) ';
        $sql.=' INNER JOIN '.$wpdb->prefix.'cities AS ct ON ( CAST(mt3.meta_value AS SIGNED) = ct.CityId ) ';
        
         $sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt5 ON ( ps.ID = mt5.post_id  ) ';
    }
    
    
    
     if ((int) $search_by_acitivity > 0) {
        $sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt4 ON ( ps.ID = mt4.post_id ) ';
     }
     
     if($deal_text_search!=''){
        $sql.=' INNER JOIN '.$wpdb->prefix.'postmeta AS mt7 ON ( ps.ID = mt7.post_id AND mt7.meta_key="deal_company_name") ';
    }
    
    
     $sql.=" WHERE 1=1 AND ps.post_type = 'deals_discount' AND (ps.post_status = 'publish' ) ";
    
    if($deal_text_search!=''){
        $sql.= " AND (ps.post_title like '%".$deal_text_search."%' OR  ps.post_content like '%".$deal_text_search."%' OR mt7.meta_value like '%".$deal_text_search."') ";
    }
    
    
    $merge_array=array_merge($local_deals,$regional_deals);
    
    if(!empty($merge_array)){
        $exp_deals=implode(',', $merge_array);
        $sql.=" AND ps.ID NOT IN (".$exp_deals.")";
    }
    
    if($search_city_name!=''){
        //$sql.=" AND LOWER(ct.City) like '%" . $search_city_name . "%' ";
        $regional_country=array_unique(array_filter($regional_country));
        if(!empty($regional_country)){
            $exp_country=implode(',', $regional_country);
                $sql.=" AND mt5.meta_key = 'deal_country_id' AND CAST(mt5.meta_value AS SIGNED) IN(".$exp_country.")  ";
         }
    }
    
     
    if ((int) $search_by_acitivity > 0) {
        $sql.=" AND mt4.meta_key = 'deal_main_activity' AND CAST(mt4.meta_value AS SIGNED) = '".$search_by_acitivity."'  ";
    }
    
    
    
     
    $sql.=' GROUP BY ps.ID ';
    
    if($latlong_field!='' && $search_by_radius>0){
       // $sql.=' having city_distance<='.$search_by_radius;
    }
    
    $sql.=' ORDER BY ';
    
    /*if($latlong_field!=''){
        $sql.=' city_distance asc ';
    } else {*/
        $sql.=' ps.ID ASC ';
    //}
    
    $sql.=' limit 6 ';
    
    //echo $sql;
    //die;
    
    $res_national = $wpdb->get_results($sql);
    
    //print_r($res_regional);

    
?>
         
         
         
          <div class="searchboxdiv">

                <div class="col-md-6 col-sm-12 col-xs-12 pull-left text-left left"><h2>National Deals</h2></div>
                
                <div class="col-md-6 col-sm-12 col-xs-12 pull-right right">
                    <form class="form-inline" name="fromsearchdealsdiscounts" id="fromsearchdealsdiscounts" action="" method="get">
                    
                    <div class="form-group"><h3>Search:</h3></div>
                    <div class="form-group">
                        <select class="form-control" name="search_by_acitivity" id="search_by_acitivity">
                            <option value="0">---Select Activity---</option>                    
                            <?php
                            if (!empty($res_activities)) {
                                foreach ($res_activities as $row_acti) {
                                    ?>
                                            <option value="<?php echo $row_acti->term_id; ?>" <?php if ($row_acti->term_id == $search_by_acitivity) { ?> selected <?php } ?>><?php echo ucwords($row_acti->name); ?></option>
                                <?php }
                            } ?>
                        </select>
                    </div>
                    
                    <div class="form-group">
                        <input type="text" name="deal_text_search" id="deal_text_search" placeholder="Search" value="<?php echo $deal_text_search; ?>" class="form-control" />
                    </div>
                    
                    
                        <input type="hidden" name="search_type" id="search_type" value="national" />
                        <button type="submit" class="btn btn-default sign-btn">search</button>
                    
                    </form>
                    
                </div>
                
                <div class="clearfix" style="clear:both;"></div>
                
            </div>
     
         


         <div class="col-sm-12 clearfix" style="clear:both;margin-top: 20px; ">
         
            
            
            
            <?php
            
            if(!empty($res_national)) {
            
                
                
                foreach($res_national as $row) {
                    
                    $post_id=$row->ID;
                    
                    
                    
                    
                    $thumb_src= esc_url( get_template_directory_uri() ).'/images/default_image.jpg';
                    $attach_id = get_post_thumbnail_id($post_id);
                    if($attach_id>0){
                        $image_url = wp_get_attachment_image_src( $attach_id, 'sch-thumb' );
                        if(isset($image_url[0]) && $image_url[0]!=''){
                            $thumb_src=$image_url[0];
                        }
                    }
                    
                    
                    $deal_name=$row->post_title;                   
                    
                    
                    $deal_link='';
                    
                    $deal_adderss = get_post_meta($post_id, 'deal_adderss', TRUE);
    
                    $deal_zip_code = get_post_meta($post_id, 'deal_zip_code', TRUE);

                    $deal_city_id = get_post_meta($post_id, 'deal_city_id', TRUE);

                    $city_name='';    
                    $get_city_detail=get_city_by_id($deal_city_id);    
                    if(!empty($get_city_detail)){
                        $city_name=$get_city_detail->City;
                    }


                    $deal_state_id = get_post_meta($post_id, 'deal_state_id', TRUE);
                    
                    
                    
                    $state_name='';    
                    $get_state_detail=get_state_by_id($deal_state_id);    
                    if(!empty($get_state_detail)){
                        $state_name=$get_state_detail->region;
                    }

                    $deal_country_id = get_post_meta($post_id, 'deal_country_id', TRUE);

                    
                    
                    $country_name='';    
                    $get_country_detail=get_country_by_id($deal_country_id);    
                    if(!empty($get_country_detail)){
                        $country_name=$get_country_detail->country;
                    }

                    $deal_lat = get_post_meta($post_id, 'deal_lat', TRUE);
                    $deal_long = get_post_meta($post_id, 'deal_long', TRUE);



                    $deal_fax_number = get_post_meta($post_id, 'deal_fax_number', TRUE);
                    $deal_phone_number = get_post_meta($post_id, 'deal_phone_number', TRUE);


                    $deal_website = get_post_meta($post_id, 'deal_website', TRUE);
                    
                    $deal_company_name=get_post_meta($post_id,'deal_company_name',TRUE);
                    
                    $deal_code=get_post_meta($post_id,'deal_code',TRUE);
                    $deal_discount=get_post_meta($post_id,'deal_discount',TRUE);
                    $deal_discount_type=get_post_meta($post_id,'deal_discount_type',TRUE);

             
            ?>
            
            <div class="areaBLock" style="height: auto;">

                            	<div class="imgBlock">

                                    <a href="<?php echo $deal_link; ?>"><img src="<?php echo $thumb_src; ?>" class="img-responsive"></a>             

                                </div>

                                <div class="textBlock">
                                    
                                	<p><?php echo $deal_name; ?></p>

                                        <p><?php echo $deal_company_name; ?></p>
                                        
                                        <p class="coupon_bg"><?php echo $deal_code; ?></p>
                                        
                                    <p><?php echo $deal_adderss;?><br>

                                    <?php echo $city_name;?>, <?php echo $state_name; ?> <?php echo $deal_zip_code; ?></p>
                                    
                                    
                                    <p><?php echo $deal_phone_number; ?></p>
                                    <p><?php echo $deal_fax_number; ?></p>
                                    
<!--                                    <a href="<?php echo $deal_link; ?>" class="readMore">Read More</a>-->

                                </div>

                            </div>
                  
             
             <?php }
            } ?>
             
            
            
            
         </div>
         <!--national loop end-->
                  
     </div>
     
     