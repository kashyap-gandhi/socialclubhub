<?php    $socialclubhub = get_option("socialclubhub_theme_config");  

$video_id_1='';
if(isset($socialclubhub['intro_youtube_video_id_1']) && trim($socialclubhub['intro_youtube_video_id_1'])!='') { 
    $video_id_1=trim($socialclubhub['intro_youtube_video_id_1']);
}
$video_id_2='';
if(isset($socialclubhub['intro_youtube_video_id_2']) && trim($socialclubhub['intro_youtube_video_id_2'])!='') { 
    $video_id_2=trim($socialclubhub['intro_youtube_video_id_2']);
}
$video_id_3='';
if(isset($socialclubhub['intro_youtube_video_id_3']) && trim($socialclubhub['intro_youtube_video_id_3'])!='') { 
    $video_id_3=trim($socialclubhub['intro_youtube_video_id_3']);
}
?>

                <section class="videoBlock clearfix">
                  <div class="col-sm-4">
                    
                    <?php if($video_id_1!='') { ?><a rel="wp-video-lightbox" href="https://www.youtube.com/watch?v=<?php echo $video_id_1; ?>&amp;width=640&amp;height=480" title=""><?php } ?>
                            
                          <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/video1.png" class="img-responsive">
                    <?php if($video_id_1!='') { ?></a><?php } ?>
                     <h5 class="videoTitle">For Members</h5>
                  </div>
                  <div class="col-sm-4">
                      <?php if($video_id_2!='') { ?><a rel="wp-video-lightbox" href="https://www.youtube.com/watch?v=<?php echo $video_id_2; ?>&amp;width=640&amp;height=480" title=""><?php } ?>
                     <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/video2.png" class="img-responsive">
                     <?php if($video_id_2!='') { ?></a><?php } ?>
                     <h5 class="videoTitle">For Clubs</h5>
                  </div>
                  <div class="col-sm-4">
                    <?php if($video_id_3!='') { ?><a rel="wp-video-lightbox" href="https://www.youtube.com/watch?v=<?php echo $video_id_3; ?>&amp;width=640&amp;height=480" title=""><?php } ?>
                     <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/video3.png" class="img-responsive">
                     <?php if($video_id_3!='') { ?> </a> <?php } ?>
                     <h5 class="videoTitle">Site Navigation</h5>
                  </div>
               </section>
           