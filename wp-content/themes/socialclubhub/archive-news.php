<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

//http://code.tutsplus.com/tutorials/a-guide-to-wordpress-custom-post-types-taxonomies-admin-columns-filters-and-archives--wp-27898
get_header(); ?>

	<div class="blogDetailPage clearfix">

            <div class="col-md-8 col-sm-12">
                
                
		<?php if (have_posts() ) : ?>

			<h2><?php printf( __( 'News', 'twentyfifteen' )); ?></h2>
				

			<?php
			// Start the loop.
			while ( have_posts() ) : the_post(); ?>
                        
                        
                        
                        
                        
<div class="col-sm-12 clearfix mainpostrow">
    <?php the_title( sprintf( '<h2><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>


    <?php
    $thumb_src= '';
            $attach_id = get_post_thumbnail_id(get_the_ID());
            if($attach_id>0){
                $image_url = wp_get_attachment_image_src( $attach_id, 'medium' );
                if(isset($image_url[0]) && $image_url[0]!=''){
                    $thumb_src=$image_url[0];
                }
            }
            ?>
    
    <?php if($thumb_src!='') { ?>
<div class="col-sm-3" style="padding-left: 0px;"> <img src="<?php echo $thumb_src; ?>" class="img-responsive" /></div>
<div class="col-sm-7"><?php the_excerpt(); ?></div>
    <div class="col-sm-12 posttypetax withimage"><?php twentyfifteen_entry_meta(); ?></div>

<?php } else { ?>
<div class="col-sm-12" style="padding-left: 0px;"><?php the_excerpt(); ?>
    <div class="col-sm-12 posttypetax"><?php twentyfifteen_entry_meta(); ?></div>
</div>
<?php } ?>



</div>


                        
                        
                        <?php 

				

			// End the loop.
			endwhile;

			// Previous/next page navigation.
			the_posts_pagination( array(
				'prev_text'          => __( 'Previous', 'twentyfifteen' ),
				'next_text'          => __( 'Next', 'twentyfifteen' ),
				'before_page_number' => '',
			) );

		// If no content, include the "No posts found" template.
		else : ?>
			
                        <div class="page-content text-center">
                             <h2><?php _e( 'Nothing Found', 'twentyfifteen' ); ?></h2>
                        <p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'twentyfifteen' ); ?></p>
			<?php get_search_form(); ?>
                        </div>
                        
		<?php endif; ?>

		 </div>
                    
                    
                    <?php get_sidebar('ads'); ?>
                    
             

            </div>


<?php get_footer(); ?>
