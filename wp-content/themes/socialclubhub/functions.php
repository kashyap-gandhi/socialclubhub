<?php
/**
 * Twenty Fifteen functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */



/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since Twenty Fifteen 1.0
 */
if ( ! isset( $content_width ) ) {
	$content_width = 660;
}

/**
 * Twenty Fifteen only works in WordPress 4.1 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.1-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'twentyfifteen_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * @since Twenty Fifteen 1.0
 */
function twentyfifteen_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on twentyfifteen, use a find and replace
	 * to change 'twentyfifteen' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'twentyfifteen', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 150, 150, true );
        
        //https://perishablepress.com/bbpress-theme-template-files/
        //https://codex.bbpress.org/layout-and-functionality-examples-you-can-use/
        add_theme_support( 'bbpress' );
        
        
        
	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'register_login_menu' => __( 'Register Login Menu',      'twentyfifteen' ),
		'profile_menu'  => __( 'Profile Menu', 'twentyfifteen' ),
                'dashboard_profile_menu'  => __( 'Dashboard Menu', 'twentyfifteen' ),
                'merchant_deal_menu'  => __( 'Merchant Deals Menu', 'twentyfifteen' ),
                'main_menu'  => __( 'Main Menu', 'twentyfifteen' ),
                'footer_link_menu'  => __( 'Footer Menu', 'twentyfifteen' ),
                'footer_signup_menu'  => __( 'Footer Join Menu', 'twentyfifteen' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	/*add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
	) );*/

	//$color_scheme  = twentyfifteen_get_color_scheme();
	//$default_color = trim( $color_scheme[0], '#' );

	// Setup the WordPress core custom background feature.
	/*add_theme_support( 'custom-background', apply_filters( 'twentyfifteen_custom_background_args', array(
		'default-color'      => $default_color,
		'default-attachment' => 'fixed',
	) ) );*/

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
	//add_editor_style( array( 'css/editor-style.css', 'genericons/genericons.css', twentyfifteen_fonts_url() ) );
}
endif; // twentyfifteen_setup
add_action( 'after_setup_theme', 'twentyfifteen_setup' );


add_image_size('sch-thumb',100,100,false); 
add_image_size('sch-medium',300,300,false); 



add_filter('image_size_names_choose', 'sch_image_sizes');
function sch_image_sizes($sizes) {
        $addsizes = array(
            "sch-thumb" => __( "Sch Thumb"),
            "sch-medium" => __( "Sch Medium")
        );
    $newsizes = array_merge($sizes, $addsizes);
    return $newsizes;
}

function setup_theme_admin_menus() {
    add_submenu_page('themes.php', 
        'Socialclubhub Theme Setup', 'Theme Setup', 'manage_options', 
        'socialclubhub-theme-setup', 'theme_front_page_settings'); 
}
 
// This tells WordPress to call the function named "setup_theme_admin_menus"
// when it's time to create the menu pages.
add_action("admin_menu", "setup_theme_admin_menus");

function theme_front_page_settings() {
    
    if (!current_user_can('manage_options')) {
        wp_die('You do not have sufficient permissions to access this page.');
    }

    
    
if (isset($_POST["sch_theme_update_setting"])) {
    // Do the saving
    $socialclubhub_setup = $_POST["socialclubhub"];   
    
    
    update_option("socialclubhub_theme_config", $socialclubhub_setup);
    ?>

    <div id="message" class="updated">Settings saved</div>

<?php
}
    $socialclubhub = get_option("socialclubhub_theme_config");
    
    //print_r($socialclubhub); die;
    
     $args = array(
	'sort_order' => 'ASC',
	'sort_column' => 'post_title',
	'hierarchical' => 1,
	'exclude' => '',
	'include' => '',
	'meta_key' => '',
	'meta_value' => '',
	'authors' => '',
	'child_of' => 0,
	'parent' => -1,
	'exclude_tree' => '',
	'number' => '',
	'offset' => 0,
	'post_type' => 'page',
	'post_status' => 'publish'
); 
    $posts = get_pages($args); 
    ?>

    <div class="wrap">
    <?php screen_icon(); ?> <h2>Socialclubhub Theme Setup</h2>
 
    <form method="POST" action="" name="schthemesetup" id="schthemesetup">
        <table class="form-table">
            <tr valign="top">
                <th scope="row">
                    <label for="homepage_slider_code">
                        Home Page Slider Code
                    </label>
                </th>
                <td>                    
                    <input type="text" name="socialclubhub[homepage_slider_code]" id="socialclubhub[homepage_slider_code]" value='<?php echo stripslashes($socialclubhub['homepage_slider_code']); ?>' />
                </td>
            </tr>                
            <tr valign="top">
                <th scope="row">
                    <label for="loginpage_setup">
                        Login Page 
                    </label>
                </th>
                <td>                    
                    

                    <select name="socialclubhub[login_page_id]" id="socialclubhub[login_page_id]">
                        <?php foreach ($posts as $post) { ?>
                        <option value="<?php echo $post->ID; ?>" <?php if(isset($socialclubhub['login_page_id']) && $socialclubhub['login_page_id']==$post->ID) { ?> selected <?php } ?>>
                                <?php echo $post->post_title; ?>
                            </option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
            
            <tr valign="top">
                <th scope="row">
                    <label for="registerpage_setup">
                        Registration Page 
                    </label>
                </th>
                <td>                    
                    

                    <select name="socialclubhub[register_page_id]" id="socialclubhub[register_page_id]">
                        <?php foreach ($posts as $post) { ?>
                            <option value="<?php echo $post->ID; ?>"  <?php if(isset($socialclubhub['register_page_id']) && $socialclubhub['register_page_id']==$post->ID) { ?> selected <?php } ?>>
                                <?php echo $post->post_title; ?>
                            </option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
            
            
            <tr valign="top">
                <th scope="row">
                    <label for="registerpage_setup">
                        Forgot Password Page 
                    </label>
                </th>
                <td>                    
                    

                    <select name="socialclubhub[forgotpassword_page_id]" id="socialclubhub[forgotpassword_page_id]">
                        <?php foreach ($posts as $post) { ?>
                            <option value="<?php echo $post->ID; ?>"  <?php if(isset($socialclubhub['forgotpassword_page_id']) && $socialclubhub['forgotpassword_page_id']==$post->ID) { ?> selected <?php } ?>>
                                <?php echo $post->post_title; ?>
                            </option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
            
             <tr valign="top">
                <th scope="row">
                    <label for="registerpage_setup">
                        Resend Verification Page 
                    </label>
                </th>
                <td>                    
                    

                    <select name="socialclubhub[resend_verification_page_id]" id="socialclubhub[resend_verification_page_id]">
                        <?php foreach ($posts as $post) { ?>
                            <option value="<?php echo $post->ID; ?>"  <?php if(isset($socialclubhub['resend_verification_page_id']) && $socialclubhub['resend_verification_page_id']==$post->ID) { ?> selected <?php } ?>>
                                <?php echo $post->post_title; ?>
                            </option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
            
            <tr valign="top">
                <th scope="row">
                    <label for="registerpage_setup">
                        My Profile Page (after login redirect)
                    </label>
                </th>
                <td>                    
                    

                    <select name="socialclubhub[myprofile_page_id]" id="socialclubhub[myprofile_page_id]">
                        <?php foreach ($posts as $post) { ?>
                            <option value="<?php echo $post->ID; ?>"  <?php if(isset($socialclubhub['myprofile_page_id']) && $socialclubhub['myprofile_page_id']==$post->ID) { ?> selected <?php } ?>>
                                <?php echo $post->post_title; ?>
                            </option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
            
            
            <tr valign="top">
                <th scope="row">
                    <label for="registerpage_setup">
                        Terms & Condition Page
                    </label>
                </th>
                <td>                    
                    

                    <select name="socialclubhub[terms_page_id]" id="socialclubhub[terms_page_id]">
                        <?php foreach ($posts as $post) { ?>
                            <option value="<?php echo $post->ID; ?>"  <?php if(isset($socialclubhub['terms_page_id']) && $socialclubhub['terms_page_id']==$post->ID) { ?> selected <?php } ?>>
                                <?php echo $post->post_title; ?>
                            </option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
            
             
            <tr valign="top">
                <th scope="row">
                    <label for="registerpage_setup">
                        All Activities Page
                    </label>
                </th>
                <td>                    
                    

                    <select name="socialclubhub[all_activities_page_id]" id="socialclubhub[all_activities_page_id]">
                        <?php foreach ($posts as $post) { ?>
                            <option value="<?php echo $post->ID; ?>"  <?php if(isset($socialclubhub['all_activities_page_id']) && $socialclubhub['all_activities_page_id']==$post->ID) { ?> selected <?php } ?>>
                                <?php echo $post->post_title; ?>
                            </option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
            
            
        </table>
 
        <h3>Social Link Setup</h3>
        
        <table class="form-table">
            <tr valign="top">
                <th scope="row">
                    <label for="homepage_slider_code">
                        Facebook Link
                    </label>
                </th>
                <td>                    
                    <input type="text" name="socialclubhub[facebook_profile_link]" id="socialclubhub[facebook_profile_link]" value="<?php if(isset($socialclubhub['facebook_profile_link'])) { echo $socialclubhub['facebook_profile_link']; } ?>" />
                </td>
            </tr>                
            <tr valign="top">
                <th scope="row">
                    <label for="homepage_slider_code">
                        Google+ Link
                    </label>
                </th>
                <td>                    
                    <input type="text" name="socialclubhub[googleplus_profile_link]" id="socialclubhub[googleplus_profile_link]" value="<?php if(isset($socialclubhub['googleplus_profile_link'])) { echo $socialclubhub['googleplus_profile_link']; } ?>" />
                </td>
            </tr>                
            
            <tr valign="top">
                <th scope="row">
                    <label for="homepage_slider_code">
                        Twitter Link
                    </label>
                </th>
                <td>                    
                    <input type="text" name="socialclubhub[twitter_profile_link]" id="socialclubhub[twitter_profile_link]" value="<?php if(isset($socialclubhub['twitter_profile_link'])) { echo $socialclubhub['twitter_profile_link']; } ?>" />
                </td>
            </tr>                
        </table>
 
       <h3>Intro Video <small>(insert youtube video id only)</small></h3>
        
        <table class="form-table">
            <tr valign="top">
                <th scope="row">
                    <label for="homepage_slider_code">
                        Intro Video ID 1
                    </label>
                </th>
                <td>                    
                    <input type="text" name="socialclubhub[intro_youtube_video_id_1]" id="socialclubhub[intro_youtube_video_id_1]" value="<?php if(isset($socialclubhub['intro_youtube_video_id_1'])) { echo $socialclubhub['intro_youtube_video_id_1']; } ?>" />
                </td>
            </tr>                
            <tr valign="top">
                <th scope="row">
                    <label for="homepage_slider_code">
                        Intro Video ID 2
                    </label>
                </th>
                <td>                    
                    <input type="text" name="socialclubhub[intro_youtube_video_id_2]" id="socialclubhub[intro_youtube_video_id_2]" value="<?php if(isset($socialclubhub['intro_youtube_video_id_2'])) { echo $socialclubhub['intro_youtube_video_id_2']; } ?>" />
                </td>
            </tr>                
            
            <tr valign="top">
                <th scope="row">
                    <label for="homepage_slider_code">
                        Intro Video ID 3
                    </label>
                </th>
                <td>                    
                    <input type="text" name="socialclubhub[intro_youtube_video_id_3]" id="socialclubhub[intro_youtube_video_id_3]" value="<?php if(isset($socialclubhub['intro_youtube_video_id_3'])) { echo $socialclubhub['intro_youtube_video_id_3']; } ?>" />
                </td>
            </tr>                
        </table>
       
       
       
       <h3>Location Setup</h3>
       
       
       <?php
       
       
    $system_city_id = 0;    
    if(isset($socialclubhub['system_city_id'])) { $system_city_id=$socialclubhub['system_city_id']; }
    
    $system_state_id = 0;    
    if(isset($socialclubhub['system_state_id'])) { $system_state_id=$socialclubhub['system_state_id']; }
    
    $system_country_id = 0;    
    if(isset($socialclubhub['system_country_id'])) { $system_country_id=$socialclubhub['system_country_id']; }
    
    
  
    
    $res_countries = get_all_country();


    $res_states = array();
    if ($system_country_id > 0) {
        
        $res_states = get_states_from_country_by_id($system_country_id);
    }

    $res_cities = array();
    if ($system_state_id > 0) {
        
        $res_cities = get_cities_from_state_by_id($system_state_id);
    }
       
       ?>
       
        
        <table class="form-table">
            <tr valign="top">
                <th scope="row">
                    <label for="">
                        Default Country
                    </label>
                </th>
                <td>                    
                   <select name="socialclubhub[system_country_id]" id="system_country_id">
                        <option value="0">---Select Country---</option>   
                <?php if (!empty($res_countries)) {
                    foreach ($res_countries as $row_country) { ?>

                                <option value="<?php echo $row_country->countryid; ?>" <?php if ($row_country->countryid == $system_country_id) { ?> selected <?php } ?>><?php echo ucfirst($row_country->country); ?></option>

                    <?php }
                } ?>

                    </select> 
                    
                </td>
            </tr>
            
            <tr valign="top">
                <th scope="row">
                    <label for="">
                        Default State
                    </label>
                </th>
                <td>                    
                   <select name="socialclubhub[system_state_id]" id="system_state_id">
                       <option value="0">---Select State---</option>

    <?php
    if (!empty($res_states)) {
        foreach ($res_states as $row_states) {
            ?>

                    <option value="<?php echo $row_states->regionid; ?>" <?php if ($row_states->regionid == $system_state_id) { ?> selected <?php } ?>><?php echo ucfirst($row_states->region); ?></option>


                <?php
                }
            }
            ?>


                    </select> 
                    
                </td>
            </tr>
            
            <tr valign="top">
                <th scope="row">
                    <label for="">
                        Default City
                    </label>
                </th>
                <td>                    
                   <select name="socialclubhub[system_city_id]" id="system_city_id">
                         <option value="0">---Select City---</option>

            <?php
            if (!empty($res_cities)) {
                foreach ($res_cities as $row_cities) {
                    ?>

                    <option value="<?php echo $row_cities->CityId; ?>" data-lat="<?php echo $row_cities->Latitude; ?>" data-long="<?php echo $row_cities->Longitude; ?>" <?php if ($row_cities->CityId == $system_city_id) { ?> selected <?php } ?>><?php echo ucfirst($row_cities->City); ?></option>


                <?php }
            } ?>

                    </select> 
                    
                </td>
            </tr>
            
            <tr valign="top">
                <th scope="row">
                    <label for="">
                        System Latitude
                    </label>
                </th>
                <td>                    
                    <input type="text" name="socialclubhub[system_lat]" id="system_lat" value="<?php if(isset($socialclubhub['system_lat'])) { echo $socialclubhub['system_lat']; } ?>" />
                </td>
            </tr>   
            
            
            <tr valign="top">
                <th scope="row">
                    <label for="">
                        System Longitude
                    </label>
                </th>
                <td>                    
                    <input type="text" name="socialclubhub[system_long]" id="system_long" value="<?php if(isset($socialclubhub['system_long'])) { echo $socialclubhub['system_long']; } ?>" />
                </td>
            </tr>   
            
            
        </table>
       
       <script>
       jQuery(document).ready(function(){

				
				
	jQuery('#system_country_id').change(function(e) {
		var data = {
			'action': 'get_states_of_country',
			'country': jQuery(this).val()
		};
		
		var state_ops = '<option value="">---Select State---</option>';
		var city_ops = '<option value="">---Select City---</option>';
		
		jQuery("#system_state_id").html(state_ops);
		jQuery("#system_city_id").html(city_ops);

		// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
		jQuery.post(ajaxurl, data, function(response) {
			if(jQuery("#system_state_id").length>0)
			{
				jQuery("#system_state_id").html(response);
			}
		});
    });
	
	jQuery('#system_state_id').change(function(e) {
		var data = {
			'action': 'get_cities_of_state',
			'state': jQuery(this).val()
			
		};
		
		
		var city_ops = '<option value="">---Select City---</option>';
		
		jQuery("#system_city_id").html(city_ops);

		// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
		jQuery.post(ajaxurl, data, function(response) {
			if(jQuery("#system_city_id").length>0)
			{
				jQuery("#system_city_id").html(response);
			}
		});
    });
	
	jQuery('#system_city_id').change(function(e) {
		var vlat=jQuery("#system_city_id option:selected").attr('data-lat');
		var vlong=jQuery("#system_city_id option:selected").attr('data-long');
		
		jQuery("#system_lat").val(vlat);
		jQuery("#system_long").val(vlong);
		
	});
	
});
</script>

       
       
        <input type="hidden" name="sch_theme_update_setting" value="schthemesetup" />
        <input type="submit" name="sch_theme_submit" id="sch_theme_submit" class="button" value="Update" />
        
    </form>
     
    
 
</div>


<?php


}

/**
 * Register widget area.
 *
 * @since Twenty Fifteen 1.0
 *
 * @link https://codex.wordpress.org/Function_Reference/register_sidebar
 */
function twentyfifteen_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Widget Area', 'twentyfifteen' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'twentyfifteen' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'twentyfifteen_widgets_init' );

if ( ! function_exists( 'twentyfifteen_fonts_url' ) ) :
/**
 * Register Google fonts for Twenty Fifteen.
 *
 * @since Twenty Fifteen 1.0
 *
 * @return string Google fonts URL for the theme.
 */
function twentyfifteen_fonts_url() {
	$fonts_url = '';
	$fonts     = array();
	$subsets   = 'latin,latin-ext';

	/* translators: If there are characters in your language that are not supported by Noto Sans, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Noto Sans font: on or off', 'twentyfifteen' ) ) {
		$fonts[] = 'Noto Sans:400italic,700italic,400,700';
	}

	/* translators: If there are characters in your language that are not supported by Noto Serif, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Noto Serif font: on or off', 'twentyfifteen' ) ) {
		$fonts[] = 'Noto Serif:400italic,700italic,400,700';
	}

	/* translators: If there are characters in your language that are not supported by Inconsolata, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Inconsolata font: on or off', 'twentyfifteen' ) ) {
		$fonts[] = 'Inconsolata:400,700';
	}

	/* translators: To add an additional character subset specific to your language, translate this to 'greek', 'cyrillic', 'devanagari' or 'vietnamese'. Do not translate into your own language. */
	$subset = _x( 'no-subset', 'Add new subset (greek, cyrillic, devanagari, vietnamese)', 'twentyfifteen' );

	if ( 'cyrillic' == $subset ) {
		$subsets .= ',cyrillic,cyrillic-ext';
	} elseif ( 'greek' == $subset ) {
		$subsets .= ',greek,greek-ext';
	} elseif ( 'devanagari' == $subset ) {
		$subsets .= ',devanagari';
	} elseif ( 'vietnamese' == $subset ) {
		$subsets .= ',vietnamese';
	}

	if ( $fonts ) {
		$fonts_url = add_query_arg( array(
			'family' => urlencode( implode( '|', $fonts ) ),
			'subset' => urlencode( $subsets ),
		), '//fonts.googleapis.com/css' );
	}

	return $fonts_url;
}
endif;

/**
 * Enqueue scripts and styles.
 *
 * @since Twenty Fifteen 1.0
 */
function twentyfifteen_scripts() {
	// Add custom fonts, used in the main stylesheet.
	//wp_enqueue_style( 'twentyfifteen-fonts', twentyfifteen_fonts_url(), array(), null );

	// Add Genericons, used in the main stylesheet.
	//wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.2' );

	// Load our main stylesheet.
	wp_enqueue_style( 'twentyfifteen-style', get_stylesheet_uri() );

	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'twentyfifteen-ie', get_template_directory_uri() . '/css/ie.css', array( 'twentyfifteen-style' ), '20141010' );
	wp_style_add_data( 'twentyfifteen-ie', 'conditional', 'lt IE 9' );

	// Load the Internet Explorer 7 specific stylesheet.
	wp_enqueue_style( 'twentyfifteen-ie7', get_template_directory_uri() . '/css/ie7.css', array( 'twentyfifteen-style' ), '20141010' );
	wp_style_add_data( 'twentyfifteen-ie7', 'conditional', 'lt IE 8' );

	wp_enqueue_script( 'twentyfifteen-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20141010', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'twentyfifteen-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20141010' );
	}

	wp_enqueue_script( 'twentyfifteen-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20141212', true );
	wp_localize_script( 'twentyfifteen-script', 'screenReaderText', array(
		'expand'   => '<span class="screen-reader-text">' . __( 'expand child menu', 'twentyfifteen' ) . '</span>',
		'collapse' => '<span class="screen-reader-text">' . __( 'collapse child menu', 'twentyfifteen' ) . '</span>',
	) );
}
add_action( 'wp_enqueue_scripts', 'twentyfifteen_scripts' );

/**
 * Add featured image as background image to post navigation elements.
 *
 * @since Twenty Fifteen 1.0
 *
 * @see wp_add_inline_style()
 */
function twentyfifteen_post_nav_background() {
	if ( ! is_single() ) {
		return;
	}

	$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );
	$css      = '';

	if ( is_attachment() && 'attachment' == $previous->post_type ) {
		return;
	}

	if ( $previous &&  has_post_thumbnail( $previous->ID ) ) {
		$prevthumb = wp_get_attachment_image_src( get_post_thumbnail_id( $previous->ID ), 'post-thumbnail' );
		$css .= '
			.post-navigation .nav-previous { background-image: url(' . esc_url( $prevthumb[0] ) . '); }
			.post-navigation .nav-previous .post-title, .post-navigation .nav-previous a:hover .post-title, .post-navigation .nav-previous .meta-nav { color: #fff; }
			.post-navigation .nav-previous a:before { background-color: rgba(0, 0, 0, 0.4); }
		';
	}

	if ( $next && has_post_thumbnail( $next->ID ) ) {
		$nextthumb = wp_get_attachment_image_src( get_post_thumbnail_id( $next->ID ), 'post-thumbnail' );
		$css .= '
			.post-navigation .nav-next { background-image: url(' . esc_url( $nextthumb[0] ) . '); }
			.post-navigation .nav-next .post-title, .post-navigation .nav-next a:hover .post-title, .post-navigation .nav-next .meta-nav { color: #fff; }
			.post-navigation .nav-next a:before { background-color: rgba(0, 0, 0, 0.4); }
		';
	}

	wp_add_inline_style( 'twentyfifteen-style', $css );
}
add_action( 'wp_enqueue_scripts', 'twentyfifteen_post_nav_background' );

/**
 * Display descriptions in main navigation.
 *
 * @since Twenty Fifteen 1.0
 *
 * @param string  $item_output The menu item output.
 * @param WP_Post $item        Menu item object.
 * @param int     $depth       Depth of the menu.
 * @param array   $args        wp_nav_menu() arguments.
 * @return string Menu item with possible description.
 */
function twentyfifteen_nav_description( $item_output, $item, $depth, $args ) {
	if ( 'primary' == $args->theme_location && $item->description ) {
		$item_output = str_replace( $args->link_after . '</a>', '<div class="menu-item-description">' . $item->description . '</div>' . $args->link_after . '</a>', $item_output );
	}

	return $item_output;
}
add_filter( 'walker_nav_menu_start_el', 'twentyfifteen_nav_description', 10, 4 );

/**
 * Add a `screen-reader-text` class to the search form's submit button.
 *
 * @since Twenty Fifteen 1.0
 *
 * @param string $html Search form HTML.
 * @return string Modified search form HTML.
 */
function twentyfifteen_search_form_modify( $html ) {
	return str_replace( 'class="search-submit"', 'class="search-submit screen-reader-text"', $html );
}
//add_filter( 'get_search_form', 'twentyfifteen_search_form_modify' );


function sch_search_form( $form ) {
	
        $form='<form role="search" method="get" id="searchform" class="form-horizontal" action="' . home_url( '/' ) . '" >
                <div class="form-group">
<!--                    <label for="" class="col-sm-4 control-label" for="s">' . __( 'Search for:' ) . '</label>-->
                    <div class="col-lg-4 col-lg-offset-4">
                        <input type="text" value="' . get_search_query() . '" name="s" id="s" class="form-control" />
                    </div>
                </div>
                <div class="form-group">

                    <div class="col-lg-4 col-lg-offset-4">
                        <button type="submit" id="searchsubmit" class="btn btn-default sign-btn">'. esc_attr__( 'Search' ) .'</button>
                    </div></div>
            </form>';

	return $form;
}

add_filter( 'get_search_form', 'sch_search_form' );

function schSearchFilter($query) {
	
    if ($query->is_search) {
        $query->set('post_type', array('clubs','deals_discount','merchant_links','news','post'));
    };
    return $query;
};

add_filter('pre_get_posts','schSearchFilter');

/*
 * remove wordpress generator
 */
remove_action('wp_head', 'wp_generator');



/**
 * Globalization
 */

require get_template_directory() . '/inc/globalization.php';

/*
 * Track user location
 */

require get_template_directory() . '/inc/iwsip/ip.codehelper.io.php';
require get_template_directory() . '/inc/iwsip/php_fast_cache.php';



function encrypt_decrypt_cookie($action, $string) {
    $output = false;

    $encrypt_method = "AES-256-CBC";
    $secret_key = 'iWebSol@123';
    $secret_iv = 'iWS';

    // hash
    $key = hash('sha256', $secret_key);

    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);

    if ($action == 'encrypt') {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
    } else if ($action == 'decrypt') {
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }

    return $output;
}



function IWS_trackvisitor(){

    $_ip = new ip_codehelper();

    $real_client_ip_address = $_ip->getRealIP();
    
    //$visitor_location       = $_ip->getLocation($real_client_ip_address);
    
    

/*
    if(!empty($visitor_location)){
        
        if(isset($visitor_location['IP']) && $visitor_location['IP']!=''){

            $guest_ip   = $visitor_location['IP'];

            $guest_country = '';
            if(isset($visitor_location['CountryName']) && $visitor_location['CountryName']!='' && strtolower($visitor_location['CountryName'])!='unknown'){
                $guest_country = $visitor_location['CountryName'];
            }

            $guest_city  = '';
            if(isset($visitor_location['CityName']) && $visitor_location['CityName']!=''  && strtolower($visitor_location['CityName'])!='unknown'){
                $guest_city  = $visitor_location['CityName'];
            }

            $guest_state = '';
            if(isset($visitor_location['RegionName']) && $visitor_location['RegionName']!=''  && strtolower($visitor_location['RegionName'])!='unknown'){
                $guest_state = $visitor_location['RegionName'];
            }

            $guest_lat = '';
            if(isset($visitor_location['CityLatitude']) && $visitor_location['CityLatitude']!=''){
                $guest_lat = $visitor_location['CityLatitude'];
            }

            $guest_long = '';
            if(isset($visitor_location['CityLongitude']) && $visitor_location['CityLongitude']!=''){
                $guest_long = $visitor_location['CityLongitude'];
            }
            
            
            
            
            
            if($guest_lat=='' || $guest_long==''){
                if($guest_city!=''){
                    
                    $get_city=get_city_by_name($guest_city);
                    
                    if(!empty($get_city)){
                        
                        if(isset($get_city->Latitude) && $get_city->Latitude!=''){
                            $guest_lat=$get_city->Latitude;
                        }
                        
                        if(isset($get_city->Longitude) && $get_city->Longitude!=''){
                            $guest_long=$get_city->Longitude;
                        }
                        
                    }
                    
                }
            }
            
            
            setcookie('GASCHLAT', encrypt_decrypt_cookie('encrypt',$guest_lat) , time() + (86400 * 3), "/","",false,true); // 86400 = 1 day
            setcookie('GASCHLONG', encrypt_decrypt_cookie('encrypt',$guest_long) , time() + (86400 * 3), "/","",false,true); // 86400 = 1 day
            setcookie('GASCHCT', encrypt_decrypt_cookie('encrypt',$guest_city) , time() + (86400 * 3), "/","",false,true); // 86400 = 1 day
            setcookie('GASCHST', encrypt_decrypt_cookie('encrypt',$guest_state) , time() + (86400 * 3), "/","",false,true); // 86400 = 1 day
            setcookie('GASCHCNT', encrypt_decrypt_cookie('encrypt',$guest_country) , time() + (86400 * 3), "/","",false,true); // 86400 = 1 day
            
            
            
        
        }
    } */
    
    $visitor_location       = $_ip->getip2location($real_client_ip_address);
    
    if(!empty($visitor_location)){
        
        if(isset($visitor_location['geoplugin_request']) && $visitor_location['geoplugin_request']!=''){

            $guest_ip   = $visitor_location['geoplugin_request'];

            $guest_country = '';
            if(isset($visitor_location['geoplugin_countryName']) && $visitor_location['geoplugin_countryName']!='' && strtolower($visitor_location['geoplugin_countryName'])!='unknown'){
                $guest_country = $visitor_location['geoplugin_countryName'];
            }

            $guest_city  = '';
            if(isset($visitor_location['geoplugin_city']) && $visitor_location['geoplugin_city']!=''  && strtolower($visitor_location['geoplugin_city'])!='unknown'){
                $guest_city  = $visitor_location['geoplugin_city'];
            }

            $guest_state = '';
            if(isset($visitor_location['geoplugin_regionName']) && $visitor_location['geoplugin_regionName']!=''  && strtolower($visitor_location['geoplugin_regionName'])!='unknown'){
                $guest_state = $visitor_location['geoplugin_regionName'];
            }

            $guest_lat = '';
            if(isset($visitor_location['geoplugin_latitude']) && $visitor_location['geoplugin_latitude']!=''){
                $guest_lat = $visitor_location['geoplugin_latitude'];
            }

            $guest_long = '';
            if(isset($visitor_location['geoplugin_longitude']) && $visitor_location['geoplugin_longitude']!=''){
                $guest_long = $visitor_location['geoplugin_longitude'];
            }
            
            
            
            
            
            if($guest_lat=='' || $guest_long==''){
                if($guest_city!=''){
                    
                    $get_city=get_city_by_name($guest_city);
                    
                    if(!empty($get_city)){
                        
                        if(isset($get_city->Latitude) && $get_city->Latitude!=''){
                            $guest_lat=$get_city->Latitude;
                        }
                        
                        if(isset($get_city->Longitude) && $get_city->Longitude!=''){
                            $guest_long=$get_city->Longitude;
                        }
                        
                    }
                    
                }
            }
            
            
            setcookie('GASCHLAT', encrypt_decrypt_cookie('encrypt',$guest_lat) , time() + (86400 * 3), "/","",false,true); // 86400 = 1 day
            setcookie('GASCHLONG', encrypt_decrypt_cookie('encrypt',$guest_long) , time() + (86400 * 3), "/","",false,true); // 86400 = 1 day
            setcookie('GASCHCT', encrypt_decrypt_cookie('encrypt',$guest_city) , time() + (86400 * 3), "/","",false,true); // 86400 = 1 day
            setcookie('GASCHST', encrypt_decrypt_cookie('encrypt',$guest_state) , time() + (86400 * 3), "/","",false,true); // 86400 = 1 day
            setcookie('GASCHCNT', encrypt_decrypt_cookie('encrypt',$guest_country) , time() + (86400 * 3), "/","",false,true); // 86400 = 1 day
            
            
            
        
        }
    }
    
}

add_action('init', 'IWS_trackvisitor');





/**
 * Implement the Custom Header feature.
 *
 * @since Twenty Fifteen 1.0
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 *
 * @since Twenty Fifteen 1.0
 */
require get_template_directory() . '/inc/template-tags.php';

/**   http://bavotasan.com/
 * Customizer additions.
 *
 * @since Twenty Fifteen 1.0
 */
//require get_template_directory() . '/inc/customizer.php';


/**
 * Common Function
 */

require get_template_directory() . '/inc/common_func.php';









function wp_get_attachment_medium_url( $post_id = 0 ) {
	$post_id = (int) $post_id;
	if ( !$post = get_post( $post_id ) )
		return false;
	if ( !$url = wp_get_attachment_url( $post->ID ) )
		return false;

	$sized = image_downsize( $post_id, 'medium' );
	if ( $sized )
		return $sized[0];

	if ( !$medium = wp_get_attachment_medium_file( $post->ID ) )
		return false;

	$url = str_replace(basename($url), basename($medium), $url);

	return apply_filters( 'wp_get_attachment_medium_url', $url, $post->ID );
}

function wp_get_attachment_medium_file( $post_id = 0 ) {
	$post_id = (int) $post_id;
	if ( !$post = get_post( $post_id ) )
		return false;
	if ( !is_array( $imagedata = wp_get_attachment_metadata( $post->ID ) ) )
		return false;

	$file = get_attached_file( $post->ID );

	if ( !empty($imagedata['medium']) && ($thumbfile = str_replace(basename($file), $imagedata['medium'], $file)) && file_exists($mediumfile) )
		return apply_filters( 'wp_get_attachment_medium_file', $mediumfile, $post->ID );
	return false;
}



        
/**
 * Pagination for a parent page and their child pages.
 * Use as gp130428_link_pages() in your template file. 
 * Optionally limit to a particular parent page by passing its ID (i.e. 123) to the function: gp130428_link_pages( 123 )
 *
 */
function news_paginate_parent_children( $parent = null ) {
	global $post;
    
	$child  = $post->ID;
	$parent = ( null !== $parent ) ? $parent : $post->post_parent;
	
      
        
	$children = get_posts( array(
				
                                'order'          => 'ASC',
                                'orderby'        => 'post_date',
				'post_parent'	  => $parent,
                                'post_type'=>'news'
				) );
	
	$pages = array( $parent );
        
        
	foreach( $children as $page )
		$pages[] += $page->ID;
	
	if( ! in_array( $child, $pages ) && ! is_page( $parent ) )
		return;
	
	$current = array_search( $child, $pages );
	
        if($current>0){
            $prev = $pages[$current-1];
        } else {
            $prev = array();
        }
        
        if(isset($pages[$current+1])){
	$next = $pages[$current+1];
        } else {
            $next='';
        }

        
        
       
	?>




	<div id="nnav-single" class="clearfix">
	<?php 
		if ( empty( $prev ) && ! is_single( $parent ) ) : 
	?>
            <div class="col-sm-6 pull-left text-left previous"><a href="<?php echo get_permalink( $parent ); ?>" title="<?php echo esc_attr( get_the_title( $parent ) ) ?>"><?php echo get_the_title( $parent ) ?></a></div>
	<?php 
		elseif ( ! empty( $prev ) ) : 
	?>
	<div class="col-sm-6 pull-left text-left previous"><a href="<?php echo get_permalink( $prev ); ?>" title="<?php echo esc_attr( get_the_title( $prev ) ) ?>"><?php echo get_the_title( $prev ) ?></a></div>
	<?php 
		endif;
		if( ! empty( $next ) ) : 
	?>
	<div class="col-sm-6 pull-right text-right next" ><a href="<?php echo get_permalink( $next ); ?>" title="<?php echo esc_attr( get_the_title( $next ) ) ?>"><?php echo get_the_title( $next ) ?></a></div>
	<?php 
		endif; 
	?>
	</div>
 <?php }
 
 
 
/**
* Dynamically add a sub-menu item to an existing menu item in wp_nav_menu
* only if a user is logged in.  isa=iws
*/
  
 function iws_dynamic_submenu_logout_link( $items, $args ) {
  
    $theme_location = 'profile_menu';// Theme Location slug
    $existing_menu_item_db_id = '';
    $new_menu_item_db_id = ''; 
    $label = 'Log out';
    $url = wp_logout_url();
     
    if ( $theme_location !== $args->theme_location ) {
        return $items;
    }
    $new_links = array();
  
    if ( is_user_logged_in() ) {
          
        // only if user is logged-in, do sub-menu link
        $item = array(
            'title'            => $label,
            'menu_item_parent' => $existing_menu_item_db_id,
            'ID'               => 'log-out',
            'db_id'            => $new_menu_item_db_id,
            'url'              => $url,
            'classes'          => array( 'menu-item' )
        );
  
        $new_links[] = (object) $item;  // Add the new menu item to our array
        unset( $item ); // in case we add more items below
 
        $index = count( $items );  // integer, the order number.
  
    // Insert the new links at the appropriate place.
        array_splice( $items, $index, 0, $new_links );
  
    }
  
    return $items;
}
add_filter( 'wp_nav_menu_objects', 'iws_dynamic_submenu_logout_link', 10, 2 );
