<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>


	<div class="blogDetailPage clearfix">

            <div class="col-md-8 col-sm-12">
                

		<?php if ( have_posts() ) : ?>

			
				<?php
					the_archive_title( '<h2 class="page-title">', '</h2>' );
					the_archive_description( '<div class="taxonomy-description"><p>', '</p></div>' );
				?>
			

			<?php
			// Start the Loop.
			while ( have_posts() ) : the_post();
?>
                
                
                        
                        
<div class="col-sm-12 clearfix mainpostrow">
    <?php the_title( sprintf( '<h2><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>


    <?php
    $thumb_src= '';
            $attach_id = get_post_thumbnail_id(get_the_ID());
            if($attach_id>0){
                $image_url = wp_get_attachment_image_src( $attach_id, 'medium' );
                if(isset($image_url[0]) && $image_url[0]!=''){
                    $thumb_src=$image_url[0];
                }
            }
            ?>
    
    <?php if($thumb_src!='') { ?>
<div class="col-sm-3" style="padding-left: 0px;"> <img src="<?php echo $thumb_src; ?>" class="img-responsive" /></div>
<div class="col-sm-7"><?php the_excerpt(); ?></div>
    <div class="col-sm-12 posttypetax withimage"><?php twentyfifteen_entry_meta(); ?></div>

<?php } else { ?>
<div class="col-sm-12" style="padding-left: 0px;"><?php the_excerpt(); ?>
    <div class="col-sm-12 posttypetax"><?php twentyfifteen_entry_meta(); ?></div>
</div>
<?php } ?>



</div>

                
                
                <?php

			// End the loop.
			endwhile;

			// Previous/next page navigation.
			the_posts_pagination( array(
				'prev_text'          => __( 'Previous', 'twentyfifteen' ),
				'next_text'          => __( 'Next', 'twentyfifteen' ),
				'before_page_number' => '',
			) );

		// If no content, include the "No posts found" template.
		else : ?>
			

                 <div class="page-content text-center">
                             <h2><?php _e( 'Nothing Found', 'twentyfifteen' ); ?></h2>
                        <p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'twentyfifteen' ); ?></p>
			<?php get_search_form(); ?>
                        </div>
                       
                
		<?php endif;
		?>

		</div>
                    
                    
                    <?php get_sidebar('ads'); ?>
                    
             

            </div>


<?php get_footer(); ?>
