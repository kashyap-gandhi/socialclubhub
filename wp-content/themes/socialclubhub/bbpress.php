<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

//https://perishablepress.com/bbpress-theme-template-files/
get_header(); ?>


	<div class="blogDetailPage clearfix">

            <div class="col-md-8 col-sm-12">
                
                          
   <?php 
				// Start the Loop.
				while ( have_posts() ) : the_post();
 the_content();
					// Include the page content template.
			/*get_template_part( 'content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;*/

				endwhile;
			?>
  

                       </div>
                    
                    
                    <?php get_sidebar('ads'); ?>
                    
             

            </div>


         
<?php
get_footer();

