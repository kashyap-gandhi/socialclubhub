<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'socialclubhub_import2');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'i~ sJyv4NMQ}@;FR8pdwlc[yGKa5Kp;oKpaizG..E0{^8a%!!e,i@cdj-e!jv-@H');
define('SECURE_AUTH_KEY',  'RBVdTkj4Nv.uJ*hhJV,dV>6<&UnZT.&mo<B*Ri:|/3LXxnN&Ruio32{@ANyF.e3V');
define('LOGGED_IN_KEY',    'F@nS}UZ(g^)`b[tSl|ur/VzrH~ #4Y6ycz3xl(`AdZ>KAA FHUR0[-9lPtCngyU!');
define('NONCE_KEY',        '=YiB IO@;n^MH< G&)Tu;j`[BP&LSe{A!3@&5o-`E_XsPx4W4|Z=8Rf}p$]8zGn&');
define('AUTH_SALT',        'wBmeiXAo4SN*p}~P%d<bA:$zwU9-n7A0&sh>gaJ!R(s6sgIW@SBYDR@yRuw@/%|&');
define('SECURE_AUTH_SALT', '.VF9UpVK_>ICYr>]ez7!9<59~q-eR:dk0w1#4O2JfWf[2F2alZDhm&Lc3[oCm2&o');
define('LOGGED_IN_SALT',   '_Kp4[X.8[oXh(rrZGQwJ!ce]>B_l<oZ>jx-[H9}&cU`Ftl.D_Qirz8}}7G[q+NK1');
define('NONCE_SALT',       '9)5ea<v#2b$&mkSbX82RS@o13gcxuJ:)c~e9:K%y7YRVwOHFegG;ecu/[BD3r0 =');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sch_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
